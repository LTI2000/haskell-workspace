module Main where

import           Prelude
import Data.Word

main :: IO ()
main = printSpiral 9

printSpiral :: Word8 -> IO ()
printSpiral n  | n > 0 = putStrLn "?"
               | otherwise = return ()
