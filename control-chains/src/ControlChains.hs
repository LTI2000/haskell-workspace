module ControlChains where

import Prelude       ( )

import Data.String   ( String
                     )

import Data.Either   ( Either(..)
                     , either
                     )

import Data.Maybe    ( Maybe(..)
                     )


import Data.Eq       ( Eq
                     )

import Text.Show     ( Show
                     )

import System.IO     ( IO
                     )


type Void = IO ()

data Exception = Exception String
               deriving (Eq,Show)

type Consumer a = a -> Void

type BiConsumer a b = (a, b) -> Void

type TriConsumer a b c = (a, b, c) -> Void


type CompletionContinuation = Consumer (Maybe Exception)


type ResultContinuation a = Consumer (Either Exception a)


type Computation0 a = BiConsumer a CompletionContinuation


type Computation1 a b = BiConsumer a (ResultContinuation b)


type Computation2 a b c = TriConsumer a b (ResultContinuation c)

nothing :: Maybe a
nothing = Nothing
just :: a -> Maybe a
just = Just
fail :: a -> Either a b
fail = Left
unit :: b -> Either a b
unit = Right


data Provider = Provider String
              deriving (Eq,Show)
data Mixer = Mixer String Provider
           deriving (Eq,Show)
data Source = Source String Mixer
            deriving (Eq,Show)
data Sink = Sink String Mixer
          deriving (Eq,Show)
data Connection = Connection String Source Sink
                deriving (Eq,Show)

provider :: Provider
provider = Provider "provider"

mixer :: Provider -> Mixer
mixer = Mixer "mixer"

source :: Mixer -> Source
source = Source "source"

sink :: Mixer -> Sink
sink = Sink "sink"

connection :: Source -> Sink -> Connection
connection = Connection "connection"


newMixer :: Computation1 Provider Mixer
newMixer = \(prv, k) -> k (unit (mixer prv))

newMixer' :: Computation1 Provider Mixer
newMixer' = \(_, k) -> k (fail (Exception "no mixer"))

newSource :: Computation1 Mixer Source
newSource = \(mxr, k) -> k (unit (source mxr))

newSource' :: Computation1 Mixer Source
newSource' = \(_, k) -> k (fail (Exception "no source"))

newSink :: Computation1 Mixer Sink
newSink = \(mxr, k) -> k (unit (sink mxr))

newSink' :: Computation1 Mixer Sink
newSink' = \(_, k) -> k (fail (Exception "no sink"))

connect :: Computation2 Source Sink Connection
connect = \(src, snk, k) -> k (unit (connection src snk))

connect' :: Computation2 Source Sink Connection
connect' = \(_, _, k) -> k (fail (Exception "no connection"))

connect2 :: Computation1 (Source, Sink) Connection
connect2 = \((src, snk), k) -> connect (src, snk, k)

connectSource :: Computation1 Sink Connection
connectSource = applyPartial2 (connect, source (mixer provider))

connectSource' :: Computation1 Sink Connection
connectSource' = applyPartial2 (connect', source (mixer provider))

release :: Computation0 Connection
release = \(_, k) -> k nothing

release' :: Computation0 Connection
release' = \(_, k) -> k (just (Exception "no release"))


applyPartial2 :: (Computation2 a b c, a) -> Computation1 b c
applyPartial2 (f, a) = \(b, k) -> f (a, b, k)


applyPartial3 :: (Computation2 a b c, a, b) -> Computation1 _x c
applyPartial3 (f, a, b) = \(_x, k) -> f (a, b, k)


applyPartial :: (Computation1 a b, a) -> Computation1 _x b
applyPartial (f, a) = \(_x, k) -> f (a, k)

applyPartial' :: (Computation1 a b, a) -> Consumer (ResultContinuation b)
applyPartial' (f, a) = \k -> f (a, k)

chain0 :: (Computation1 a b, Computation0 b) -> Computation0 a
chain0 (f, g) = \(a, c) -> f (a, \eb -> eitherVoid (\e -> c (just e), \b -> g (b, c), eb))


chain :: (Computation1 a b, Computation1 b c) -> Computation1 a c
chain (f, g) = \(a, k) -> f (a, \eb -> eitherVoid (\e -> k (fail e), \b -> g (b, k), eb))


--rchain :: (Computation1 a b, Computation1 b c) -> Computation1 a c
--rchain = _


chain2 :: (Computation1 a b, Computation1 a c) -> Computation1 a (b, c)
chain2 (f, g) = \(a, k) -> f (a, \eb -> eitherVoid (\e -> k (fail e), \b -> g (a, \ec -> eitherVoid (\e -> k (fail e), \c -> k (unit (b, c)), ec)), eb))


bind :: (Computation1 a b, b -> Computation1 b c) -> Computation1 a c
bind (f, g) = \(a, k) -> f (a, \eb -> eitherVoid (\e -> k (fail e), \b -> g b (b, k), eb))

bind2 :: (Computation1 a b, b -> Either Exception c) -> Computation1 a c
bind2 (f, g) = \(a, k) -> f (a, \eb -> eitherVoid (\e -> k (fail e), \b -> k (g b), eb))

eitherVoid :: (Consumer Exception, Consumer a, Either Exception a) -> Void
eitherVoid (left, right, ea) = eitherConsumer (left, right) ea


eitherConsumer :: (Consumer Exception, Consumer a) -> ResultContinuation a
eitherConsumer (left, right) = either left right

-- type Computation1 a b = (a, Either Exception b -> Void) -> Void


curry :: BiConsumer a b -> (a -> b -> Void)
curry f = \a -> \b -> f (a, b)

curry2 :: Computation1 a b -> a -> ResultContinuation b -> Void
curry2 f = \a -> \k -> f (a, k)

wrap :: (ResultContinuation a) -> (Either Exception a -> Void)
wrap k = k -- \ea -> either (\e -> k (fail e)) (\a -> k (unit a)) ea

smash :: a -> (a -> Either Exception b) -> ResultContinuation b -> Void
smash a f k = either (\e -> k (fail e)) (\b -> k (unit b)) (f a)


curry2NewMixer :: Provider -> ResultContinuation Mixer -> Void
curry2NewMixer = curry2 newMixer

curry2NewMixerProvider :: ResultContinuation Mixer -> Void
curry2NewMixerProvider = curry2NewMixer provider

--smashCurry2NewMixerProvider :: Provider -> (Provider -> Either Exception Mixer) -> ResultContinuation Mixer
--smashCurry2NewMixerProvider a f = smash a f curry2NewMixerProvider
