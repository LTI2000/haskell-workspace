module Main where

import Prelude       ( )

import System.IO     ( IO
                     , print
                     )

import ControlChains

-- type Computation0 a = (a, Maybe Exception -> Void) -> Void

-- type Computation1 a b = (a, Either Exception b -> Void) -> Void

-- type Computation2 a b c = (a, b, Either Exception c -> Void) -> Void

main :: IO ()
main = do
  newMixer  (provider, print)
  newMixer' (provider, print)

  newSource  (mixer provider, print)
  newSource' (mixer provider, print)

  newSink  (mixer provider, print)
  newSink' (mixer provider, print)

  connect  (source (mixer provider), sink (mixer provider), print)
  connect' (source (mixer provider), sink(mixer provider), print)

  connectSource  (sink (mixer provider), print)
  connectSource' (sink (mixer provider), print)

  (chain (newSink,  connectSource))  (mixer provider, print)
  (chain (newSink', connectSource))  (mixer provider, print)
  (chain (newSink,  connectSource')) (mixer provider, print)

  (chain0 (connectSource,  release))  (sink (mixer provider), print)
  (chain0 (connectSource', release))  (sink (mixer provider), print)
  (chain0 (connectSource,  release')) (sink (mixer provider), print)

  (bind (newMixer, \_mxr -> newSource)) (provider, print)
  (bind (newMixer', \_mxr -> newSource)) (provider, print)
  (bind (newMixer, \_mxr -> newSource')) (provider, print)

  (bind (newMixer, \mxr ->
           bind ((applyPartial (newSource, mxr)), \src ->
                   bind ((applyPartial (newSink, mxr)), \snk ->
                           applyPartial3 (connect, src, snk))))) (provider, print)
