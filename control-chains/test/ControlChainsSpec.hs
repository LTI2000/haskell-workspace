module ControlChainsSpec (spec) where

import Prelude hiding (fail, read)

import Test.Hspec
--import Test.Hspec.QuickCheck
--import Test.QuickCheck

import ControlChains


testProvider :: Provider
testProvider = provider
testMixer :: Mixer
testMixer = mixer testProvider
testSource :: Source
testSource = source testMixer
testSink :: Sink
testSink = sink testMixer
testConnection :: Connection
testConnection = connection testSource testSink

spec :: Spec
spec = do

  it "newMixer returns a mixer" $ do
    newMixer (testProvider, \r -> r `shouldBe` unit testMixer)

  it "newMixer' raises an exception" $ do
    newMixer' (testProvider, \r -> r `shouldBe` fail (Exception "no mixer"))


  it "newSource returns a source" $ do
    newSource (testMixer, \r -> r `shouldBe` unit testSource)

  it "newSource' raises an exception" $ do
    newSource' (testMixer, \r -> r `shouldBe` fail (Exception "no source"))


  it "newSink returns a sink" $ do
    newSink (testMixer, \r -> r `shouldBe` unit testSink)

  it "newSink' raises an exception" $ do
    newSink' (testMixer, \r -> r `shouldBe` fail (Exception "no sink"))


  it "connect returns a connection" $ do
    connect (testSource, testSink, \r -> r `shouldBe` unit testConnection)

  it "connect' raises an exception" $ do
    connect' (testSource, testSink, \r -> r `shouldBe` fail (Exception "no connection"))


  it "connectSource returns a connection" $ do
    connectSource (testSink, \r -> r `shouldBe` unit testConnection)

  it "connectSource' raises an exception" $ do
    connectSource' (testSink, \r -> r `shouldBe` fail (Exception "no connection"))


  it "release returns nothing" $ do
    release (testConnection, \r -> r `shouldBe` nothing)

  it "release' raises an exception" $ do
    release' (testConnection, \r -> r `shouldBe` just (Exception "no release"))


  it "chain0 succeeds if both computations succeed" $ do
    (chain0 (connectSource, release)) (testSink, \r -> r `shouldBe` nothing)

  it "chain0 fails if the first computations fails" $ do
    (chain0 (connectSource', release)) (testSink, \r -> r `shouldBe` just (Exception "no connection"))

  it "chain0 fails if the second computations fails" $ do
    (chain0 (connectSource, release')) (testSink, \r -> r `shouldBe` just (Exception "no release"))


  it "chain succeeds if both computations succeed" $ do
    (chain (newSink, connectSource)) (testMixer, \r -> r `shouldBe` unit testConnection)

  it "chain fails if the first computation fails" $ do
    (chain (newSink', connectSource)) (testMixer, \r -> r `shouldBe` fail (Exception "no sink"))

  it "chain fails if the second computation fails" $ do
    (chain (newSink, connectSource')) (testMixer, \r -> r `shouldBe` fail (Exception "no connection"))


  it "chain2 succeeds if both computations succeed" $ do
    (chain2 (newSource, newSink)) (testMixer, \r -> r `shouldBe` unit (testSource, testSink))

  it "chain2 fails if the first computation fails" $ do
    (chain2 (newSource', newSink)) (testMixer, \r -> r `shouldBe` fail (Exception "no source"))

  it "chain2 fails if the second computation fails" $ do
    (chain2 (newSource, newSink')) (testMixer, \r -> r `shouldBe` fail (Exception "no sink"))


--  it "chain/chain2 fails on first failure" $ do
--    (chain chain2 (newSource, newSink), connect2) (testMixer, \r -> r `shouldBe` unit testConnection)


  it "bind succeeds if both computations succeed" $ do
    (bind (newMixer, \_mxr -> newSource)) (testProvider, \r -> r `shouldBe` unit testSource)

  it "bind fails if the first computation fails" $ do
    (bind (newMixer', \_mxr -> newSource)) (testProvider, \r -> r `shouldBe` fail (Exception "no mixer"))

  it "bind fails if the second computation fails" $ do
    (bind (newMixer, \_mxr -> newSource')) (testProvider, \r -> r `shouldBe` fail (Exception "no source"))


  it "nested binds fail on first failure" $ do
    (bind (newMixer, \mxr ->
             bind ((applyPartial (newSource, mxr)), \src ->
                     bind ((applyPartial (newSink, mxr)), \snk ->
                             applyPartial3 (connect, src, snk))))) (testProvider, \r -> r `shouldBe` unit testConnection)

    (bind (newMixer', \mxr ->
             bind ((applyPartial (newSource, mxr)), \src ->
                     bind ((applyPartial (newSink, mxr)), \snk ->
                             applyPartial3 (connect, src, snk))))) (testProvider, \r -> r `shouldBe` fail (Exception "no mixer"))

    (bind (newMixer, \mxr ->
             bind ((applyPartial (newSource', mxr)), \src ->
                     bind ((applyPartial (newSink, mxr)), \snk ->
                             applyPartial3 (connect, src, snk))))) (testProvider, \r -> r `shouldBe` fail (Exception "no source"))

    (bind (newMixer, \mxr ->
             bind ((applyPartial (newSource, mxr)), \src ->
                     bind ((applyPartial (newSink', mxr)), \snk ->
                             applyPartial3 (connect, src, snk))))) (testProvider, \r -> r `shouldBe` fail (Exception "no sink"))

    (bind (newMixer, \mxr ->
             bind ((applyPartial (newSource, mxr)), \src ->
                     bind ((applyPartial (newSink, mxr)), \snk ->
                             applyPartial3 (connect', src, snk))))) (testProvider, \r -> r `shouldBe` fail (Exception "no connection"))
