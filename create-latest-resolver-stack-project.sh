#!/bin/bash

set -eu

PROJECT=${1?" project name missing!"}

pushd create-stack-template
./create-stack-template.sh
popd #create-stack-template

stack new ${PROJECT} create-stack-template/stack-template
pushd ${PROJECT}
chmod 755 *.sh
./test.sh
./build_and_exec.sh
./profile.sh
popd #${PROJECT}
