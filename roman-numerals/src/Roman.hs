module Roman where

import           Prelude

import           Data.Word

data Letter = I | V | X | L | C | D | M
    deriving (Eq, Ord, Show)

type Roman = [Letter]

toRoman :: Word16 -> Roman
toRoman n0 = go n0 []
  where
    go n acc
        | n >= 1000 = go (n - 1000) (M : acc)
        | n >= 500 = go (n - 500) (D : acc)
        | n >= 100 = go (n - 100) (C : acc)
        | n >= 50 = go (n - 50) (L : acc)
        | n >= 10 = go (n - 10) (X : acc)
        | n >= 5 = go (n - 5) (V : acc)
        | n >= 1 = go (n - 1) (I : acc)
        | otherwise = reverse acc

fromRoman :: Roman -> Word16
fromRoman _ = 0
