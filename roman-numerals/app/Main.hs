module Main where

import Prelude

import Roman

main :: IO ()
main = do
  putStrLn (show (toRoman 1666))
