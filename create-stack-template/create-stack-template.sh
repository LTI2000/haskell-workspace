#!/bin/bash

set -eu

TEMPLATE=template
HSFILES=stack-template.hsfiles

pushd ${TEMPLATE}
FILES=$(find . -type f | sed -e 's@^./@@' | sort)
echo $FILES
popd #${TEMPLATE}
> ${HSFILES}
for i in $FILES ; do
    echo "{-# START_FILE $i #-}" >> ${HSFILES}
    cat ${TEMPLATE}/$i >> ${HSFILES}
    echo "" >> ${HSFILES}
done
