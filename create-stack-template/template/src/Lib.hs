module Lib ( someFunc ) where

import           Control.Applicative ( (*>) )

import           System.IO           ( IO, putStrLn )

import           Internal            ( internalFunc )

someFunc :: IO ()
someFunc = internalFunc *> putStrLn "someFunc"
