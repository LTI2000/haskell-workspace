{-# LANGUAGE FlexibleContexts #-}

module Sdl.Internal.Bracket ( bracket ) where

import           Common.Imports

import           Control.Monad.Trans.Class    ( lift )

import           Control.Monad.Trans.Resource ( MonadBaseControl, MonadThrow
                                              , allocate, runResourceT )


bracket :: (MonadBaseControl IO m, MonadThrow m, MonadIO m)
        => IO a         -- | alloc function
        -> (a -> IO ()) -- | free function
        -> (a -> m b)   -- | inside function
        -> m b          -- | result
bracket alloc free inside =
    runResourceT $ do
        (_releaseKey, resource) <- allocate alloc free
        lift $ inside resource
