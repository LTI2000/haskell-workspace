{-# LANGUAGE PatternSynonyms #-}

module Sdl.Internal.Types where

import           Common.Imports

import           SDL.Vect                     ( Point(..), V2(..), V3(..)
                                              , V4(..) )
import           SDL.Video.Renderer           ( Rectangle(..) )

import           Foreign.C.Types              ( CDouble, CInt ) -- FIXME create module Common.Foreign.C

import           Data.Vector.Storable.Mutable ( IOVector )


import           Free.Types                   ( Bounds(..), Dimension, Location )

type SdlInt = CInt

type SdlFloat = CDouble

type SdlPoint = Point V2 SdlInt

type SdlDimension = V2 SdlInt

pattern SdlDimension :: SdlInt -> SdlInt -> SdlDimension

pattern SdlDimension w h = V2 w h

sdlDimension :: SdlInt -> SdlInt -> SdlDimension
sdlDimension = V2

--dimensionToSdlDimension :: Dimension -> SdlDimension
--dimensionToSdlDimension =
--    (fromIntegral <$>)
--sdlDimensionToDimension :: SdlDimension -> Dimension
--sdlDimensionToDimension =
--    (fromIntegral <$>)
type SdlRectangle = Rectangle SdlInt

sdlDimensionToSdlRectangle :: SdlDimension -> SdlRectangle
sdlDimensionToSdlRectangle =
    Rectangle 0

sdlRectangleTranslate :: SdlPoint -> SdlRectangle -> SdlRectangle
sdlRectangleTranslate v (Rectangle loc size) =
    Rectangle (v + loc) size

--boundsToSdlRectangle :: Bounds -> SdlRectangle
--boundsToSdlRectangle (Bounds loc dim) =
--    Rectangle (fromIntegral <$> loc) (fromIntegral <$> dim)
type SdlColorComponent = Word8

type SdlRgb = V3 SdlColorComponent

type SdlAlpha = SdlColorComponent

type SdlRgba = V4 SdlColorComponent--colorToSdlRgba :: Color -> SdlRgba
                                   --colorToSdlRgba = (fromIntegral <$>)

type SdlPixelData = IOVector Word8

type SdlPixelFormatMasks = V4 Word32

sdlPixelFormatMasks :: Word32
                    -> Word32
                    -> Word32
                    -> Word32
                    -> SdlPixelFormatMasks
sdlPixelFormatMasks = V4

toSdlPoint :: Location -> SdlPoint
toSdlPoint loc = fmap fromIntegral loc

toSdlDimension :: Dimension -> SdlDimension
toSdlDimension size = fmap fromIntegral size

toSdlRectangle :: Bounds -> SdlRectangle
toSdlRectangle (Bounds loc size) =
    Rectangle (fmap fromIntegral loc) (fmap fromIntegral size)
