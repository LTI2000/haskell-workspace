module Sdl.Internal.Input
    ( makeInputVar
    , updateInput
    ) where

import           Common.Imports

import           Data.StateVar            ( ($=), StateVar, get, makeStateVar )

import           SDL.Event                ( Event(..), EventPayload(..)
                                          , InputMotion(..)
                                          , KeyboardEventData(..)
                                          , MouseButtonEventData(..)
                                          , MouseMotionEventData(..)
                                          , WindowLostMouseFocusEventData(..)
                                          , pollEvents )
import           SDL.Input.Mouse          ( MouseButton(..) )
import           SDL.Input.Keyboard       ( Keycode(..), Keysym(..) )
import           SDL.Input.Keyboard.Codes
import           SDL.Time                 ( ticks )

import           Free.Input               ( ButtonInput(..), CursorKey(..)
                                          , FunctionKey(..), Input(..)
                                          , KeyboardInput(..), KeypadKey(..)
                                          , SpecialKey(..) )

import           Debug.Trace -- FIXME create module Common.Debug

makeInputVar :: IO (StateVar Input)
makeInputVar = do
    timestamp <- ticks
    makeIORefStateVar Input { timestamp
                            , mloc = Nothing
                            , buttonsPressed = []
                            , keysPressed = []
                            }

updateInput :: (MonadIO m) => StateVar Input -> m Input
updateInput inputVar = do
    events <- pollEvents
    input <- get inputVar
    timestamp <- ticks
    let input' = eventsToInput input { timestamp
                                     , buttonsPressed = []
                                     , keysPressed = []
                                     }
                               events
    inputVar $= input'
    return input'

makeIORefStateVar :: (MonadIO m) => a -> m (StateVar a) -- FIXME duplicated in Context.hs
makeIORefStateVar x = do
    ref <- liftIO $ newIORef x
    return $ makeStateVar (readIORef ref) (writeIORef ref)

eventsToInput :: Input -> [Event] -> Input
eventsToInput = foldl eventToInput

eventToInput :: Input -> Event -> Input
eventToInput input Event{eventPayload} =
    case eventPayload of
        KeyboardEvent KeyboardEventData{keyboardEventKeyMotion = Pressed,keyboardEventRepeat = False,keyboardEventKeysym = Keysym{keysymKeycode,keysymModifier}} ->
            trace (printf "keysymKeycode=%s,keysymModifier=%s"
                          (show keysymKeycode) -- superset of ASCII characters
                          (show keysymModifier))
                $ updateKeysPressed input keysymKeycode
        MouseButtonEvent MouseButtonEventData{mouseButtonEventMotion = Pressed,mouseButtonEventButton} ->
            updateButtonsPressed input mouseButtonEventButton
        MouseMotionEvent MouseMotionEventData{mouseMotionEventPos} ->
            input { mloc = Just $ fromIntegral <$> mouseMotionEventPos }
        WindowLostMouseFocusEvent WindowLostMouseFocusEventData{} ->
            input { mloc = Nothing }
        _ -> input

updateButtonsPressed :: Input -> MouseButton -> Input
updateButtonsPressed input@Input{buttonsPressed} button =
    case handleButtonPressed button of
        Just buttonInput -> input { buttonsPressed = buttonInput :
                                      buttonsPressed
                                  }
        Nothing -> input

handleButtonPressed :: MouseButton -> Maybe ButtonInput
handleButtonPressed ButtonLeft =
    Just LeftButton
handleButtonPressed ButtonRight =
    Just RightButton
handleButtonPressed _ = Nothing

updateKeysPressed :: Input -> Keycode -> Input
updateKeysPressed input@Input{keysPressed} keycode =
    case handleKeyPressed keycode of
        Just keyboardInput -> input { keysPressed = keyboardInput : keysPressed
                                    }
        Nothing -> input

handleKeyPressed :: Keycode -> Maybe KeyboardInput
handleKeyPressed KeycodeBackspace =
    Just $ SpecialKeyInput Backspace
handleKeyPressed KeycodeTab =
    Just $ SpecialKeyInput Tab
handleKeyPressed KeycodeReturn =
    Just $ SpecialKeyInput Return
handleKeyPressed KeycodeEscape =
    Just $ SpecialKeyInput Escape
handleKeyPressed (Keycode code) | code >= 32 &&
                                      code <= 122 = Just $
                                      CharacterInput (chr $
                                                          fromIntegral code)
handleKeyPressed KeycodeDelete =
    Just $ SpecialKeyInput Delete
handleKeyPressed KeycodeF1 =
    Just $ FunctionKeyInput F1
handleKeyPressed KeycodeF2 =
    Just $ FunctionKeyInput F2
handleKeyPressed KeycodeF3 =
    Just $ FunctionKeyInput F3
handleKeyPressed KeycodeF4 =
    Just $ FunctionKeyInput F4
handleKeyPressed KeycodeF5 =
    Just $ FunctionKeyInput F5
handleKeyPressed KeycodeF6 =
    Just $ FunctionKeyInput F6
handleKeyPressed KeycodeF7 =
    Just $ FunctionKeyInput F7
handleKeyPressed KeycodeF8 =
    Just $ FunctionKeyInput F8
handleKeyPressed KeycodeF9 =
    Just $ FunctionKeyInput F9
handleKeyPressed KeycodeF10 =
    Just $ FunctionKeyInput F10
handleKeyPressed KeycodeF11 =
    Just $ FunctionKeyInput F11
handleKeyPressed KeycodeF12 =
    Just $ FunctionKeyInput F12
handleKeyPressed KeycodeHome =
    Just $ CursorKeyInput CursorHome
handleKeyPressed KeycodePageUp =
    Just $ CursorKeyInput PageUp
handleKeyPressed KeycodeEnd =
    Just $ CursorKeyInput CursorEnd
handleKeyPressed KeycodePageDown =
    Just $ CursorKeyInput PageDown
handleKeyPressed KeycodeRight =
    Just $ CursorKeyInput CursorRight
handleKeyPressed KeycodeLeft =
    Just $ CursorKeyInput CursorLeft
handleKeyPressed KeycodeUp =
    Just $ CursorKeyInput CursorUp
handleKeyPressed KeycodeDown =
    Just $ CursorKeyInput CursorDown
handleKeyPressed KeycodeKPDivide =
    Just $ KeypadInput KeypadDivide
handleKeyPressed KeycodeKPMultiply =
    Just $ KeypadInput KeypadMultiply
handleKeyPressed KeycodeKPMinus =
    Just $ KeypadInput KeypadMinus
handleKeyPressed KeycodeKPPlus =
    Just $ KeypadInput KeypadPlus
handleKeyPressed KeycodeKPEnter =
    Just $ KeypadInput KeypadEnter
handleKeyPressed KeycodeKP1 =
    Just $ KeypadInput Keypad1
handleKeyPressed KeycodeKP2 =
    Just $ KeypadInput Keypad2
handleKeyPressed KeycodeKP3 =
    Just $ KeypadInput Keypad3
handleKeyPressed KeycodeKP4 =
    Just $ KeypadInput Keypad4
handleKeyPressed KeycodeKP5 =
    Just $ KeypadInput Keypad5
handleKeyPressed KeycodeKP6 =
    Just $ KeypadInput Keypad6
handleKeyPressed KeycodeKP7 =
    Just $ KeypadInput Keypad7
handleKeyPressed KeycodeKP8 =
    Just $ KeypadInput Keypad8
handleKeyPressed KeycodeKP9 =
    Just $ KeypadInput Keypad9
handleKeyPressed KeycodeKP0 =
    Just $ KeypadInput Keypad0
handleKeyPressed KeycodeKPPeriod =
    Just $ KeypadInput KeypadPeriod
handleKeyPressed KeycodeKPEquals =
    Just $ KeypadInput KeypadEquals
handleKeyPressed _ = Nothing
