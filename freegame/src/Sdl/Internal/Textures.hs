{-# LANGUAGE PatternSynonyms #-}

module Sdl.Internal.Textures
    ( byteStringToTexture
    , textureSize
    ) where

import           Common.Imports

import           SDL.Video.Renderer           ( PixelFormat, Renderer, Surface
                                              , Texture, TextureInfo(..)
                                              , createRGBSurfaceFrom
                                              , createTextureFromSurface
                                              , freeSurface, masksToPixelFormat
                                              , queryTexture )


import           Sdl.Internal.Types           ( SdlDimension, SdlInt
                                              , SdlRectangle
                                              , pattern SdlDimension
                                              , sdlDimension
                                              , sdlDimensionToSdlRectangle
                                              , sdlPixelFormatMasks
                                              , toSdlDimension )

import           Codec.Picture                ( DynamicImage, Image(..)
                                              , PixelRGBA8, convertRGBA8
                                              , decodeImage )
import           Data.ByteString              ( ByteString )
import           Data.Vector.Storable         ( thaw )
import           Data.Vector.Storable.Mutable ( IOVector )

import           Free.Types                   ( dimension )

byteStringToDynamicImage :: (MonadIO m) => Kleisli m ByteString DynamicImage
byteStringToDynamicImage =
    Kleisli $ either error return . decodeImage

dynamicImageToImageRGBA8 :: (MonadIO m)
                         => Kleisli m DynamicImage (Image PixelRGBA8)
dynamicImageToImageRGBA8 =
    arr convertRGBA8

imageRGBA8ToPixelDataRGBA8 :: (MonadIO m)
                           => Kleisli m (Image PixelRGBA8) ( SdlDimension
                                                           , IOVector Word8
                                                           )
imageRGBA8ToPixelDataRGBA8 =
    Kleisli $
        \Image{imageWidth,imageHeight,imageData} ->
            liftIO $
                (,) <$> pure (toSdlDimension (dimension imageWidth imageHeight))
                    <*> thaw imageData

pixelDataRGBA8ToSurface :: (MonadIO m)
                        => Kleisli m (SdlDimension, IOVector Word8) Surface
pixelDataRGBA8ToSurface =
    Kleisli $
        \(size, pixelData) -> do
            let (SdlDimension w _) = size
            pixelFormat <- pngPixelFormat
            createRGBSurfaceFrom pixelData
                                 size
                                 (w * bytesPerRGBA8Pixel)
                                 pixelFormat
  where
    bytesPerRGBA8Pixel :: SdlInt -- | R G B A
    bytesPerRGBA8Pixel = 4

    pngPixelFormat :: (MonadIO m) => m PixelFormat
    pngPixelFormat = masksToPixelFormat bitsPerPixel pngMasks
      where
        bitsPerPixel = 8 * bytesPerRGBA8Pixel
        pngMasks = sdlPixelFormatMasks 0x000000FF
                                       0x0000FF00
                                       0x00FF0000
                                       0xFF000000

surfaceToTexture :: (MonadIO m) => Renderer -> Kleisli m Surface Texture
surfaceToTexture renderer =
    Kleisli $ createTextureFromSurface renderer

deleteSurface :: (MonadIO m) => Kleisli m Surface ()
deleteSurface = Kleisli freeSurface

byteStringToTexture :: (MonadIO m) => Renderer -> Kleisli m ByteString Texture
byteStringToTexture renderer =
    byteStringToDynamicImage >>>
        dynamicImageToImageRGBA8 >>>
            imageRGBA8ToPixelDataRGBA8 >>>
                pixelDataRGBA8ToSurface >>>
                    surfaceToTexture renderer &&& deleteSurface >>^
                        fst

textureSize :: (MonadIO m) => Kleisli m Texture SdlRectangle
textureSize = Kleisli $
    \texture -> do
        TextureInfo{textureWidth,textureHeight} <- queryTexture texture
        return $
            sdlDimensionToSdlRectangle (sdlDimension textureWidth textureHeight)
