{-# LANGUAGE FlexibleContexts #-}

module Sdl.Internal.Context
    ( SdlContextConfig(..)
    , SdlContext(SdlContext, window, renderer, logicalSize)
    , withSdlContext
    , getTexture
    ) where

import           Common.Imports

import           Control.Monad.Trans.Resource ( MonadBaseControl, MonadThrow )

import           Data.Text                    ( Text )

import           Data.Map.Strict              ( Map )
import qualified Data.Map.Strict              as Map ( empty, insert, lookup )

import           SDL                          ( ($=), get )
import           SDL.Init                     ( initializeAll, quit )
import           SDL.Input.Mouse              ( cursorVisible )
import           SDL.Video                    ( Window, WindowConfig(..)
                                              , WindowMode(..)
                                              , WindowPosition(..)
                                              , createRenderer, createWindow
                                              , destroyRenderer, destroyWindow )
import           SDL.Video.Renderer           ( BlendMode(..), Renderer
                                              , RendererConfig(..)
                                              , RendererType(..), Texture
                                              , rendererDrawBlendMode
                                              , rendererLogicalSize )


import           Free.AssetData               ( AssetData(..) )

import           Sdl.Internal.Types           ( SdlDimension, SdlRectangle )
import           Sdl.Internal.Bracket         ( bracket )
import           Sdl.Internal.Textures        ( byteStringToTexture
                                              , textureSize )

import           Data.StateVar                ( StateVar, makeStateVar )

data SdlContextConfig = SdlContextConfig { size       :: SdlDimension
                                         , fullscreen :: Bool
                                         , caption    :: Text
                                         }

data SdlContext = SdlContext { window      :: Window
                             , renderer    :: Renderer
                             , logicalSize :: SdlDimension
                             , textures    :: StateVar (Map String ( Texture
                                                                   , SdlRectangle
                                                                   ))
                             }

withSdlContext :: (MonadBaseControl IO m, MonadThrow m, MonadIO m)
               => SdlContextConfig
               -> (SdlContext -> m b)
               -> m b
withSdlContext SdlContextConfig{size,fullscreen,caption} =
    let windowConfig = WindowConfig { windowBorder = True
                                    , windowHighDPI = False
                                    , windowInputGrabbed = False
                                    , windowMode = if fullscreen
                                                   then FullscreenDesktop
                                                   else Windowed
                                    , windowOpenGL = Nothing
                                    , windowPosition = Wherever
                                    , windowResizable = True -- False
                                    , windowInitialSize = size
                                    }
        rendererConfig = RendererConfig { rendererType = AcceleratedVSyncRenderer
                                        , rendererTargetTexture = True
                                        }
        alloc = do
            sdlContext <- newSdlContext windowConfig rendererConfig size caption
            cursorVisible $= False
            return sdlContext

        free sdlContext = do
            cursorVisible $= True
            destroySdlContext sdlContext
    in
        bracket alloc free

newSdlContext :: (MonadIO m)
              => WindowConfig
              -> RendererConfig
              -> SdlDimension
              -> Text
              -> m SdlContext
newSdlContext windowConfig rendererConfig logicalSize caption = do
    initializeAll
    window <- createWindow caption windowConfig
    renderer <- createRenderer window (-1) rendererConfig
    rendererLogicalSize renderer $= Just logicalSize
    rendererDrawBlendMode renderer $= BlendAlphaBlend
    textures <- makeIORefStateVar Map.empty
    return SdlContext { window, renderer, logicalSize, textures }

destroySdlContext :: (MonadIO m) => SdlContext -> m ()
destroySdlContext SdlContext{window,renderer} = do
    destroyRenderer renderer
    destroyWindow window
    quit{-
getLogicalSize :: Renderer -> IO SdlDimension
getLogicalSize renderer = do
    size <- get (rendererLogicalSize renderer)
    return (maybe 0 id size)
-}

makeIORefStateVar :: (MonadIO m) => a -> m (StateVar a) -- FIXME duplicated in Input.hs
makeIORefStateVar x = do
    ref <- liftIO $ newIORef x
    return $ makeStateVar (readIORef ref) (writeIORef ref)

getTexture :: (MonadIO m)
           => SdlContext
           -> AssetData
           -> m (Texture, SdlRectangle)
getTexture SdlContext{renderer,textures} (AssetData nam dat) = do
    t <- get textures
    case Map.lookup nam t of
        Just val -> do
            return val
        Nothing -> do
            val <- runKleisli (byteStringToTexture renderer >>>
                                   id &&&
                                       textureSize)
                              dat
            textures $= Map.insert nam val t
            return val
