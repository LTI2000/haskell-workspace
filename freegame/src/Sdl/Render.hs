module Sdl.Render ( runProgram ) where

import           Common.Imports

import           Control.Monad.Trans.State ( StateT(..), evalStateT, gets, put )

import           Data.StateVar             ( ($=), StateVar )

import           Data.Text                 ( pack )


import           SDL.Video.Renderer        ( clear, copy, drawPoint, drawRect
                                           , fillRect, present
                                           , rendererDrawColor
                                           , rendererViewport )

import           Free.Color                ( Color )
import           Free.ContextConfig        ( ContextConfig(..) )
import           Free.Input                ( Input(..) )
import           Free.Render               ( Render, RenderF(..), loopProgram )

import           Sdl.Internal.Types        ( SdlRgba, sdlRectangleTranslate
                                           , toSdlPoint, toSdlRectangle )
import           Sdl.Internal.Context      ( SdlContext(..)
                                           , SdlContextConfig(..), getTexture
                                           , withSdlContext )
import           Sdl.Internal.Input        ( makeInputVar, updateInput )

runProgram :: ContextConfig
           -> state
           -> Render state (StateT state IO) ()
           -> IO ()
runProgram config initialState program = do
    inputVar <- makeInputVar
    evalStateT (withSdlContext (toSdlContextConfig config)
                               (\context -> loopProgram program
                                                        (render inputVar context)))
               initialState

render :: StateVar Input
       -> SdlContext
       -> RenderF state (StateT state IO ())
       -> StateT state IO ()
render inputVar context@SdlContext{renderer} =
    go
  where
    go (GetInput k) = do
        updateInput inputVar >>= k
    go (SetColor color next) = do
        rendererDrawColor renderer $= toSdlColor color
        next
    go (ClearBackground next) = do
        clear renderer
        next
    go (DrawPoint loc next) = do
        drawPoint renderer (toSdlPoint loc)
        next
    go (DrawRectangle bnds next) = do
        drawRect renderer $ Just (toSdlRectangle bnds)
        next
    go (FillRectangle bnds next) = do
        fillRect renderer $ Just (toSdlRectangle bnds)
        next
    go (Fill next) = do
        fillRect renderer $ Nothing
        next
    go (DrawImage dat loc next) = do
        (texture, bnds) <- getTexture context dat
        let dst = sdlRectangleTranslate (toSdlPoint loc) bnds
        copy renderer texture Nothing (Just dst)
        next
    go (SetViewport bnds next) = do
        rendererViewport renderer $= Just (toSdlRectangle bnds)
        next
    go (ResetViewport next) = do
        rendererViewport renderer $= Nothing
        next
    go (Flush next) = do
        present renderer
        next
    go (UpdateState f k) = do
        updateState >>= k
      where
        updateState = do
            i <- gets f
            put i
            return i

toSdlContextConfig :: ContextConfig -> SdlContextConfig
toSdlContextConfig (ContextConfig s f t) =
    SdlContextConfig (fmap fromIntegral s) f (pack t)

toSdlColor :: Color -> SdlRgba
toSdlColor color = fmap fromIntegral color
