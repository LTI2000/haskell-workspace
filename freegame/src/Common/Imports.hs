module Common.Imports
    ( module X
    , whenJust
    ) where

import           Prelude                as X ( (*), (+), (-), (/), Bounded(..)
                                             , Enum(..), div, error
                                             , fromIntegral, mod, undefined )

import           Control.Applicative    as X ( Applicative(..), liftA2 )
import           Control.Arrow          as X ( (<<^), (>>^), (^<<), (^>>)
                                             , Arrow(..), Kleisli(..) )
import           Control.Category       as X ( (<<<), (>>>), Category(..) )
import           Control.Monad          as X ( (<=<), (=<<), (>=>), Monad(..)
                                             , join, unless, when )
import           Control.Monad.IO.Class as X ( MonadIO(..) )

import           Data.Bool              as X ( (&&), (||), Bool(..), not
                                             , otherwise )
import           Data.Char              as X ( Char, chr )
import           Data.Either            as X ( Either(..), either )
import           Data.Eq                as X ( Eq(..) )
import           Data.Foldable          as X ( Foldable(..), traverse_ )
import           Data.Function          as X ( ($), const )
import           Data.Functor           as X ( (<$>), Functor(..) )
import           Data.IORef             as X ( newIORef, readIORef, writeIORef )
import           Data.Int               as X ( Int )
import           Data.Ix                as X ( Ix(..) )
import           Data.List              as X ( (!!), (++), filter, last, map
                                             , zip )
import           Data.Maybe             as X ( Maybe(..), maybe )
import           Data.Ord               as X ( Ord(..) )
import           Data.Word              as X ( Word32, Word64, Word8 )
import           Data.String            as X ( String )
import           Data.Traversable       as X ( Traversable(..) )
import           Data.Tuple             as X ( curry, fst, snd, swap, uncurry )

import           System.IO              as X ( IO, getLine, print, putStr
                                             , putStrLn )

import           Text.Printf            as X ( printf )
import           Text.Show              as X ( Show(..) )

whenJust :: Applicative f => Maybe a -> (a -> f ()) -> f ()
whenJust (Just x) s = s x
whenJust _ _ = pure ()
