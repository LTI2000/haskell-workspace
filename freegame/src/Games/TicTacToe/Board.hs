{-# LANGUAGE GADTs         #-}
{-# LANGUAGE TupleSections #-}

module Games.TicTacToe.Board
    ( Board
    , CellIndex
    , emptyBoard
    , boardCells
    , setCell
    ) where

import           Common.Imports

import           Data.Array.IArray ( (//), Array, array, assocs )

type RowIndex = Int

type ColIndex = Int

type CellIndex = (RowIndex, ColIndex)

type BoardBounds = (CellIndex, CellIndex)

data Board cell where
        Board :: (Eq cell) => Array CellIndex (Maybe cell) -> Board cell

emptyBoard :: (Eq cell) => Int -> Board cell
emptyBoard boardSize = Board $
    array boardBounds ((,Nothing) <$> range boardBounds)
  where
    boardBounds :: BoardBounds
    boardBounds = (loIndex, hiIndex)
      where
        loIndex :: CellIndex
        loIndex = (1, 1)

        hiIndex :: CellIndex
        hiIndex = (boardSize, boardSize)

boardCells :: Board cell -> [(CellIndex, Maybe cell)]
boardCells (Board a) = assocs a

setCell :: Board cell -> CellIndex -> cell -> Board cell
setCell (Board a) i c = Board $
    a // [ (i, Just c) ]
