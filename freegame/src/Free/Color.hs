module Free.Color
    ( Color
    , rgb
    , rgba
    ) where

import           Common.Imports

import           Linear.V4      ( V4(..) )

type Color = V4 Word8

rgb :: Word8 -> Word8 -> Word8 -> Color
rgb r g b = rgba r g b 0xFF

rgba :: Word8 -> Word8 -> Word8 -> Word8 -> Color
rgba r g b a = V4 r g b a
