{-# LANGUAGE TemplateHaskell #-}

module Free.GenerateAssets ( generateAssets ) where

import           Common.Imports

import           Data.FileEmbed
import           Data.List.Split

import           Language.Haskell.TH.Syntax ( Con(..), Dec(..), Info(..), Name
                                            , mkName, reify, reportError )
import           Language.Haskell.TH.Lib    ( DecsQ, ExpQ, appE, conT, litE
                                            , normalB, recConE, sigD, stringL
                                            , valD, varE, varP )

import           Free.AssetData

generateAssets :: String -> Name -> DecsQ
generateAssets varString typeName = do
    info <- reify typeName
    case info of
        (TyConI (DataD _ _ _ _ [ RecC name varBangTypes ] _)) ->
            let varName = mkName varString
                fieldNames = [ var
                             | (var, _, _) <- varBangTypes ]
                fieldExps = [ (,) <$> pure fieldName <*> embedData fieldName
                            | fieldName <- fieldNames ]
            in
                sequence [ sigD varName (conT typeName)
                         , valD (varP varName)
                                (normalB (recConE name fieldExps))
                                []
                         ]
        _ -> do
            reportError (show info)
            fail "assets type error"
  where
    embedData :: Name -> ExpQ
    embedData name = varE 'assetData `appE`
        embedName name `appE`
        embedDataFile name
      where
        embedName :: Show a => a -> ExpQ
        embedName = litE . stringL . strip

        embedDataFile :: Show a => a -> ExpQ
        embedDataFile = embedFile . makeFileName
          where
            makeFileName :: Show a => a -> String
            makeFileName fieldName =
                "assets/" ++
                    strip fieldName ++
                        ".png"
        strip :: Show a => a -> String
        strip = last . (splitOn ".") . show
