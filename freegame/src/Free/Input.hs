module Free.Input
    ( Input(..)
    , ButtonInput(..)
    , KeyboardInput(..)
    , CursorKey(..)
    , FunctionKey(..)
    , KeypadKey(..)
    , SpecialKey(..)
    ) where

import           Common.Imports

import           Free.Types     ( Location )

data Input = Input { timestamp      :: Word32
                   , mloc           :: Maybe Location
                   , buttonsPressed :: [ButtonInput]
                   , keysPressed    :: [KeyboardInput]
                   }

data ButtonInput = LeftButton | RightButton

data KeyboardInput = CursorKeyInput CursorKey
                   | FunctionKeyInput FunctionKey
                   | KeypadInput KeypadKey
                   | SpecialKeyInput SpecialKey
                   | CharacterInput Char
                   | ModifierInput
    deriving (Eq, Show)

data CursorKey = CursorLeft
               | CursorRight
               | CursorUp
               | CursorDown
               | CursorHome
               | CursorEnd
               | PageUp
               | PageDown
    deriving (Eq, Show)

data FunctionKey = F1 | F2 | F3 | F4 | F5 | F6 | F7 | F8 | F9 | F10 | F11 | F12
    deriving (Eq, Show)

data KeypadKey = KeypadDivide
               | KeypadMultiply
               | KeypadMinus
               | KeypadPlus
               | KeypadEnter
               | Keypad1
               | Keypad2
               | Keypad3
               | Keypad4
               | Keypad5
               | Keypad6
               | Keypad7
               | Keypad8
               | Keypad9
               | Keypad0
               | KeypadPeriod
               | KeypadEquals
    deriving (Eq, Show)

data SpecialKey = Backspace | Tab | Return | Escape | Delete
    deriving (Eq, Show)
