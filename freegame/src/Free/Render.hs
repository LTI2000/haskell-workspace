{-# LANGUAGE DeriveFunctor #-}

module Free.Render
    ( RenderF(..)
    , Render
    , getInput
    , setColor
    , clearBackground
    , drawPoint
    , drawRectangle
    , fillRectangle
    , fill
    , drawImage
    , setViewport
    , resetViewport
    , flush
    , inViewport
    , updateState
    , loopProgram
    ) where

import           Common.Imports

import           Control.Monad.Trans.Free ( FreeT, iterT, liftF )

import           Free.Types               ( Bounds, Location )
import           Free.Color               ( Color )
import           Free.Input               ( Input(..) )
import           Free.AssetData           ( AssetData )

loopProgram :: (Monad m) => Render s m () -> (RenderF s (m ()) -> m ()) -> m ()
loopProgram program interpreter =
    iterT interpreter program

data RenderF state next =
      GetInput (Input -> next)
    | SetColor Color next
    | ClearBackground next
    | DrawPoint Location next
    | DrawRectangle Bounds next
    | FillRectangle Bounds next
    | Fill next
    | DrawImage AssetData Location next
    | SetViewport Bounds next
    | ResetViewport next
    | Flush next
    | UpdateState (state -> state) (state -> next)
    deriving Functor

type Render state = FreeT (RenderF state)

getInput :: (Monad m) => Render s m Input
getInput = liftF $ GetInput id

setColor :: (Monad m) => Color -> Render s m ()
setColor color = liftF $ SetColor color ()

clearBackground :: (Monad m) => Render s m ()
clearBackground = liftF $ ClearBackground ()

drawPoint :: (Monad m) => Location -> Render s m ()
drawPoint loc = liftF $ DrawPoint loc ()

drawRectangle :: (Monad m) => Bounds -> Render s m ()
drawRectangle bnds = liftF $ DrawRectangle bnds ()

fillRectangle :: (Monad m) => Bounds -> Render s m ()
fillRectangle bnds = liftF $ FillRectangle bnds ()

fill :: (Monad m) => Render s m ()
fill = liftF $ Fill ()

drawImage :: (Monad m) => AssetData -> Location -> Render s m ()
drawImage img loc = liftF $ DrawImage img loc ()

setViewport :: (Monad m) => Bounds -> Render s m ()
setViewport bnds = liftF $ SetViewport bnds ()

resetViewport :: (Monad m) => Render s m ()
resetViewport = liftF $ ResetViewport ()

flush :: (Monad m) => Render s m ()
flush = liftF $ Flush ()

inViewport :: (Monad m) => Bounds -> Render s m () -> Render s m ()
inViewport bnds body = do
    setViewport bnds
    body
    resetViewport

updateState :: (Monad m) => (s -> s) -> Render s m s
updateState f = liftF $ UpdateState f id
