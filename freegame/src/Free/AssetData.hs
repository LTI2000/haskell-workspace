module Free.AssetData
    ( AssetData(..)
    , assetData
    ) where

import           Common.Imports

import           Data.ByteString ( ByteString )

data AssetData = AssetData String ByteString

assetData :: String -> ByteString -> AssetData
assetData = AssetData
