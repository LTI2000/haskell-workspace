{-# LANGUAGE PatternSynonyms #-}

module Free.Types
    ( Dimension
    , dimension
    , pattern Dimension
    , Location
    , location
    , pattern Location
    , Bounds(..)
    , bounds
    , locationToBounds
    , dimensionToBounds
    , boundsContainsLocation
    ) where

import           Common.Imports

import           Linear.Affine  ( Point(..) )
import           Linear.V2      ( V2(..) )

type Dimension = V2 Int

dimension :: Int -> Int -> Dimension
dimension = V2

pattern Dimension :: Int -> Int -> Dimension

pattern Dimension w h = V2 w h

type Location = Point V2 Int

location :: Int -> Int -> Location
location x y = P (V2 x y)

pattern Location :: Int -> Int -> Point V2 Int

pattern Location x y = P (V2 x y)

data Bounds = Bounds Location Dimension

bounds :: Int -> Int -> Int -> Int -> Bounds
bounds x y w h = Bounds (location x y) (dimension w h)

locationToBounds :: Location -> Int -> Bounds
locationToBounds (P (V2 x y)) radius =
    let r = radius - 1
    in
        bounds (x - r) (y - r) (2 * r + 1) (2 * r + 1)

dimensionToBounds :: Dimension -> Bounds
dimensionToBounds (V2 w h) =
    bounds 0 0 w h

boundsContainsLocation :: Bounds -> Location -> Bool
boundsContainsLocation (Bounds (P (V2 x y)) (V2 w h)) (P (V2 px py)) =
    px >= x && py >= y && px <= (x + w - 1) && py <= (y + h - 1)
