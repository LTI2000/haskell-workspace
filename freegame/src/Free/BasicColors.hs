module Free.BasicColors
    ( black
    , white
    , red
    , green
    , blue
    ) where

import           Free.Color ( Color, rgb )

black :: Color
black = rgb 0 0 0

white :: Color
white = rgb 255 255 255

red :: Color
red = rgb 255 0 0

green :: Color
green = rgb 0 255 0

blue :: Color
blue = rgb 0 0 255
