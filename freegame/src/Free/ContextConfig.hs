module Free.ContextConfig ( ContextConfig(..) ) where

import           Common.Imports

import           Free.Types

data ContextConfig = ContextConfig { size       :: Dimension
                                   , fullscreen :: Bool
                                   , title      :: String
                                   }
