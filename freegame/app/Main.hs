{-# LANGUAGE TemplateHaskell #-}

module Main where

import           Common.Imports

import           Free.Types
--import           Free.BasicColors
import           Free.FlatUiColors
import           Free.ContextConfig
import           Free.Input
import           Free.Render

import           Sdl.Render          ( runProgram )

import           Free.GenerateAssets

--------------------------------------------------------------------------------
import           Assets

$(generateAssets "assets" ''Assets)

--------------------------------------------------------------------------------
data State = State { sx :: Int
                   , sy :: Int
                   , vx :: Int
                   , vy :: Int
                   }

config :: ContextConfig
config = ContextConfig { size = dimension 800 600
                       , fullscreen = False
                       , title = "freegame"
                       }

initialState :: State
initialState = State 0 0 1 1

update :: State -> State
update State{sx,sy,vx,vy} =
    let (sx', vx') = bounceBetween (0, 512) (sx, vx)
        (sy', vy') = bounceBetween (0, 384) (sy, vy)
    in
        State { sx = sx', sy = sy', vx = vx', vy = vy' }
  where
    bounceBetween (minS, maxS) (s, v)
        | v > 0 && s + v > maxS = (s - 1, (-1))
        | v < 0 && s + v < minS = (s + 1, 1)
        | otherwise = (s + v, v)

body :: (Monad m) => Input -> Render State m ()
body input = do
    clearBG
    drawScene input

clearBG :: (Monad m) => Render State m ()
clearBG = do
    setColor asbestos
    clearBackground

drawScene :: (Monad m) => Input -> Render State m ()
drawScene input = do
    drawBG
    drawFG
    drawMouse input
  where
    drawBG = do
        setViewport $ bounds 0 0 600 600
        setColor peter_river
        fill
        resetViewport

        setViewport $ bounds 0 0 200 200
        setColor belize_hole
        fill
        drawImage (checkers_piece_black assets) (location 0 0)
        resetViewport

        setViewport $ bounds 200 0 200 200
        drawImage (checkers_piece_white assets) (location 0 0)
        resetViewport

        setViewport $ bounds 400 0 200 200
        setColor belize_hole
        fill
        drawImage (checkers_piece_black assets) (location 0 0)
        resetViewport

        setViewport $ bounds 0 200 200 200
        drawImage (checkers_piece_white assets) (location 0 0)
        resetViewport

        setViewport $ bounds 200 200 200 200
        setColor belize_hole
        fill
        drawImage (checkers_piece_black assets) (location 0 0)
        resetViewport

        setViewport $ bounds 400 200 200 200
        drawImage (checkers_piece_white assets) (location 0 0)
        resetViewport

        setViewport $ bounds 0 400 200 200
        setColor belize_hole
        fill
        drawImage (checkers_piece_black assets) (location 0 0)
        resetViewport

        setViewport $ bounds 200 400 200 200
        drawImage (checkers_piece_white assets) (location 0 0)
        resetViewport

        setViewport $ bounds 400 400 200 200
        setColor belize_hole
        fill
        drawImage (checkers_piece_black assets) (location 0 0)
        resetViewport

    drawFG = do
        State{sx,sy} <- updateState update
        setColor carrot
        fillRectangle $ bounds (sx - 3) (sy - 3) 7 7
    drawMouse Input{mloc} = do
        case mloc of
            (Just loc) -> do
                setColor clouds
                fillRectangle $ locationToBounds loc 4
            _ -> do
                return ()

--------------------------------------------------------------------------------
loop :: (Monad m) => (Input -> Render s m ()) -> Render s m ()
loop loopBody = go
  where
    go = do
        input <- getInput
        unless (isQuit input) $ do
            loopBody input
            flush
            go

program :: (Monad m) => Render State m ()
program = loop body

--------------------------------------------------------------------------------
isQuit :: Input -> Bool
isQuit Input{keysPressed} =
    SpecialKeyInput Escape `elem` keysPressed

--------------------------------------------------------------------------------
main :: IO ()
main = runProgram config initialState program
