module Free.RenderSpec ( spec ) where

import           Common.Imports

import           Test.Hspec
import           Test.Hspec.QuickCheck
import           Test.QuickCheck

import           Free.Render

spec :: Spec
spec = do
    describe "description" $ do
        prop "property" $ do
            \(b1, b2) -> case (b1, b2) of
                (False, False) -> b1 && b2 `shouldBe` False
                (False, True) -> b1 && b2 `shouldBe` False
                (True, False) -> b1 && b2 `shouldBe` False
                (True, True) -> b1 && b2 `shouldBe` True
