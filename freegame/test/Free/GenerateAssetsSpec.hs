{-# LANGUAGE TemplateHaskell #-}

module Free.GenerateAssetsSpec ( spec ) where

import           Common.Imports

import           Test.Hspec

import           Free.GenerateAssets

--------------------------------------------------------------------------------
import           Free.AssetData

data Assets = Assets { white_1x1            :: AssetData
                     , checkers_piece_black :: AssetData
                     , checkers_piece_white :: AssetData
                     }

$(generateAssets "assets" ''Assets)

--------------------------------------------------------------------------------
spec :: Spec
spec = do
    context "asset images" $ do
        specify "white_1x1 asset name is correct" $ do
            let AssetData name _ = white_1x1 assets
            name `shouldBe` "white_1x1"
        specify "checkers_piece_black asset name is correct" $ do
            let AssetData name _ = checkers_piece_black assets
            name `shouldBe` "checkers_piece_black"
        specify "checkers_piece_white asset name is correct" $ do
            let AssetData name _ = checkers_piece_white assets
            name `shouldBe` "checkers_piece_white"
