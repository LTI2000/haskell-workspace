{-# LANGUAGE TemplateHaskell #-}

module Main ( main ) where

import           Common.Imports

import           Free.Types
import           Free.FlatUiColors
import           Free.ContextConfig
import           Free.Input
import           Free.Render

import           Sdl.Render

import           Free.GenerateAssets

import           Games.TicTacToe.Board

--------------------------------------------------------------------------------
import           Assets

$(generateAssets "assets" ''Assets)

--------------------------------------------------------------------------------
boardSize :: Int
boardSize = 3

pieceSize :: Int
pieceSize = 200

gapSize :: Int
gapSize = 20

displaySize :: Int
displaySize = boardSize * (gapSize + pieceSize) + gapSize

--------------------------------------------------------------------------------
data Player = Black | White
    deriving (Bounded, Enum, Eq, Ord, Show)

data Mode = Playing Player
          | GameOver (Maybe Player)

data State = State { board :: Board Player
                   , mode  :: Mode
                   }

config :: ContextConfig
config = ContextConfig { size = dimension displaySize displaySize
                       , fullscreen = False
                       , title = "tic-tac-toe"
                       }

initialState :: State
initialState = State { board = emptyBoard boardSize, mode = Playing Black }

update :: Input -> State -> State
update Input{mloc,buttonsPressed = [ LeftButton ]} state@State{board,mode = Playing Black} =
    case focussedCells (boardCells board) mloc of
        [ (i, Nothing) ] -> let board' = setCell board i Black
                            in
                                state { board = board', mode = Playing White }
        _ -> state
update Input{timestamp} state@State{board,mode = Playing White} =
    let cells = [ cell
                | cell@(_, Nothing) <- boardCells board ]
        n = length cells
    in
        if n > 0
        then let m = timestamp `mod` (fromIntegral n)
                 (i, _) = cells !! (fromIntegral m)
                 board' = setCell board i White
             in
                 state { board = board', mode = Playing Black }
        else state { mode = GameOver Nothing }
update _ state = state

body :: (Monad m) => Input -> Render State m ()
body input = do
    clearBG
    drawScene input

clearBG :: (Monad m) => Render State m ()
clearBG = do
    setColor asbestos
    clearBackground

drawScene :: (Monad m) => Input -> Render State m ()
drawScene input = do
    state <- updateState (update input)
    drawBG
    drawFG input state
    drawMouse input
  where
    drawBG = do
        setColor belize_hole
        fill

    drawFG Input{mloc} State{board,mode} =
        traverse_ (drawCell $
                       case mode of
                           Playing Black -> mloc
                           _ -> Nothing)
                  (boardCells board)

    drawMouse Input{mloc} = whenJust mloc $
        \loc -> do
            setColor clouds
            fillRectangle $ locationToBounds loc 4

drawCell :: (Monad m)
         => Maybe Location
         -> (CellIndex, Maybe Player)
         -> Render State m ()
drawCell mloc cell@(i, content) =
    inViewport (cellBounds i) $ do
        setColor color
        fill
        whenJust content $
            \player -> do
                let img = case player of
                        Black -> checkers_piece_black
                        White -> checkers_piece_white
                drawImage (img assets) (location 0 0)
  where
    color = case (cellFocussed, content) of
        (False, _) -> peter_river
        (_, Just _) -> alizarin
        (_, Nothing) -> emerald
      where
        cellFocussed = case focussedCells [ cell ] mloc of
            [ _ ] -> True
            _ -> False

--------------------------------------------------------------------------------
cellBounds :: CellIndex -> Bounds
cellBounds (row, col) = bounds (x * (pieceSize + gapSize) + gapSize)
                               (y * (pieceSize + gapSize) + gapSize)
                               pieceSize
                               pieceSize
  where
    x = col - 1
    y = row - 1

focussedCells :: [(CellIndex, Maybe cell)]
              -> Maybe Location
              -> [(CellIndex, Maybe cell)]
focussedCells _ Nothing =
    []
focussedCells cells (Just loc) =
    [ cell
    | cell@(i, _) <- cells
    , boundsContainsLocation (cellBounds i) loc ]

--------------------------------------------------------------------------------
loop :: (Monad m) => (Input -> Render s m ()) -> Render s m ()
loop loopBody = go
  where
    go = do
        input <- getInput
        unless (isQuit input) $ do
            loopBody input
            flush
            go

program :: (Monad m) => Render State m ()
program = loop body

--------------------------------------------------------------------------------
isQuit :: Input -> Bool
isQuit Input{keysPressed} =
    SpecialKeyInput Escape `elem` keysPressed

--------------------------------------------------------------------------------
main :: IO ()
main = do
    runProgram config initialState program
