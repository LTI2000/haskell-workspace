module Assets ( Assets(..) ) where

import           Free.AssetData

data Assets = Assets { white_1x1            :: AssetData
                     , checkers_piece_black :: AssetData
                     , checkers_piece_white :: AssetData
                     }
