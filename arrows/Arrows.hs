import           Prelude     ( undefined ) -- do not import whole prelude,
                                           -- we will define our own stuff
import           Text.Show   ( Show(..) )  -- we need to show things
import           Data.List   ( (++) )
import           Data.String ( String )    -- we use strings
import           Data.Tuple  ( fst, snd )
import           System.IO   ( IO, print ) -- we do terminal IO

infixr 9 .

infixr 1 >>>, <<<

class Category cat where
    id :: cat a a

    (.) :: cat b c -> cat a b -> cat a c

(<<<) :: (Category cat) => cat b c -> cat a b -> cat a c
(<<<) = (.)

(>>>) :: (Category cat) => cat a b -> cat b c -> cat a c
f >>> g = g . f

instance Category (->) where
    id x = x
    f . g = \x -> f (g x)

class Functor f where
    fmap :: (a -> b) -> f a -> f b

class (Functor f) => Applicative f where
    pure :: a -> f a

class (Applicative m) => Monad m where
    bind :: m a -> (a -> m b) -> m b

infixr 3 ***

infixr 3 &&&

class (Category a) => Arrow a where
    arr :: (b -> c) -> a b c

    first :: a b c -> a (b, d) (c, d)

    second :: a b c -> a (d, b) (d, c)
    second f = swap >>> first f >>> swap

    (***) :: a b c -> a b' c' -> a (b, b') (c, c')
    f *** g = first f >>> second g

    (&&&) :: a b c -> a b c' -> a b (c, c')
    f &&& g = dup >>> f *** g

swap :: (Arrow a) => a (b, c) (c, b)
swap = arr swap'
  where
    swap' :: (x, y) -> (y, x)
    swap' ~(x, y) = (y, x)

dup :: (Arrow a) => a b (b, b)
dup = arr dup'
  where
    dup' :: x -> (x, x)
    dup' x = (x, x)

instance Arrow (->) where
    arr f = f
    first f = f *** id
    second f = id *** f

--f *** g ~(x, y) = (f x, g y)
newtype Kleisli m a b = Kleisli { runKleisli :: a -> m b }

instance (Monad m) => Category (Kleisli m) where
    id = Kleisli pure
    (Kleisli f) . (Kleisli g) =
        Kleisli (\b -> bind (g b) f)

instance (Monad m) => Arrow (Kleisli m) where
    arr f = Kleisli (pure . f)
    first (Kleisli f) = Kleisli (\ ~(b, d) -> bind (f b) (\c -> pure (c, d)))
    second (Kleisli f) = Kleisli (\ ~(d, b) -> bind (f b) (\c -> pure (d, c)))

data Either e a = Left e
                | Right a
    deriving Show

either :: (e -> b) -> (a -> b) -> Either e a -> b
either left _ (Left e) =
    left e
either _ right (Right a) =
    right a

instance Functor (Either e) where
    fmap f = either Left (Right . f)

instance Applicative (Either e) where
    pure = Right

instance Monad (Either e) where
    bind ea k = either Left k ea

error :: e -> Either e a
error = Left

type Void = IO ()

data Exception = Exception String
    deriving Show

type ResultContinuation a = Either Exception a -> Void

newtype Cont a = Cont { runCont :: ResultContinuation a -> Void }

instance Functor Cont where
    fmap f ka = bind ka (pure . f)

instance Applicative Cont where
    pure a = Cont (\ra -> ra (pure a))

instance Monad Cont where
    bind ka f = Cont (\rb -> runCont ka
                                     (either (rb . error)
                                             (\a -> runCont (f a) rb)))

newtype ContT r m a = ContT { runContT :: (a -> m r) -> m r }

instance Functor (ContT r m) where
    fmap f ka = ContT (\ra -> runContT ka (ra . f))

instance Applicative (ContT r m) where
    pure a = ContT (\ra -> ra a)

instance Monad (ContT r m) where
    bind ka f = ContT (\ra -> runContT ka (\x -> runContT (f x) ra))

--type Cont0 a = ContT () IO (Either Exception a)
--type KleisliArrow0 a b = Kleisli Cont0 a b
type Computation a b = a -> ResultContinuation b -> Void

type KleisliArrow a b = Kleisli Cont a b

toArrow :: Computation a b -> KleisliArrow a b
toArrow f = Kleisli (\a -> Cont (\rb -> f a rb))

runArrow :: KleisliArrow a b -> Computation a b
runArrow f a rb = runCont (runKleisli f a) rb

data Provider = Provider String
    deriving Show

data Mixer = Mixer Provider
    deriving Show

newMixer :: KleisliArrow Provider Mixer
newMixer = toArrow _newMixer
  where
    _newMixer :: Computation Provider Mixer
    _newMixer p rm = rm (pure (Mixer p))

newMixer' :: KleisliArrow Provider Mixer
newMixer' = toArrow _newMixer'
  where
    _newMixer' :: Computation Provider Mixer
    _newMixer' _ rm = rm (error (Exception "no mixer"))

data Endpoint = Endpoint Mixer
    deriving Show

newEndpoint :: KleisliArrow Mixer Endpoint
newEndpoint = toArrow _newEndpoint
  where
    _newEndpoint :: Computation Mixer Endpoint
    _newEndpoint m re = re (pure (Endpoint m))

newEndpoint' :: KleisliArrow Mixer Endpoint
newEndpoint' = toArrow _newEndpoint'
  where
    _newEndpoint' :: Computation Mixer Endpoint
    _newEndpoint' _ re = re (error (Exception "no endpoint"))

data Sink = Sink Endpoint
    deriving Show

newSink :: KleisliArrow Endpoint Sink
newSink = toArrow _newSink
  where
    _newSink :: Computation Endpoint Sink
    _newSink e rs = rs (pure (Sink e))

newSink' :: KleisliArrow Endpoint Sink
newSink' = toArrow _newSink'
  where
    _newSink' :: Computation Endpoint Sink
    _newSink' _ rs = rs (error (Exception "no sink"))

data Connection = Connection Endpoint Sink
    deriving Show

newConnection :: KleisliArrow (Endpoint, Sink) Connection
newConnection = toArrow _newConnection
  where
    _newConnection :: Computation (Endpoint, Sink) Connection
    _newConnection (e, s) rc =
        rc (pure (Connection e s))

newConnection' :: KleisliArrow (Endpoint, Sink) Connection
newConnection' = toArrow _newConnection'
  where
    _newConnection' :: Computation (Endpoint, Sink) Connection
    _newConnection' _ rc = rc (error (Exception "no connection"))

testArrow :: (Show b) => KleisliArrow a b -> a -> Void
testArrow f a = runArrow f a print

--------------------------------------------------------------------------------
--
asl :: KleisliArrow (a, (b, c)) ((a, b), c)
asl = arr (\(a, (b, c)) -> ((a, b), c))

--------------------------------------------------------------------------------
--
empty :: KleisliArrow a (a, ())
empty = arr (\a -> (a, ()))

push :: KleisliArrow a c -> KleisliArrow (a, b) (c, (a, b))
push f = dup >>> first (arr fst >>> f)

pop :: KleisliArrow (a, b) b
pop = arr snd

op :: KleisliArrow (a, b) d -> KleisliArrow (a, (b, c)) (d, c)
op f = asl >>> first f

--------------------------------------------------------------------------------
--
endpoints :: KleisliArrow (Mixer, Mixer) (Endpoint, Endpoint)
endpoints = newEndpoint *** newEndpoint

connections :: KleisliArrow (Endpoint, Endpoint) (Connection, Connection)
connections = connection &&& (swap >>> connection)
  where
    connection :: KleisliArrow (Endpoint, Endpoint) Connection
    connection = (second newSink >>> newConnection)

remember :: KleisliArrow ((Endpoint, Endpoint), (Mixer, Mixer)) ()
remember = toArrow remember'
  where
    remember' ((e1, e2), (m1, m2)) ru = do
        print (show m1 ++ " => " ++ show m2)
        print (show m1 ++ " -> " ++ show e1)
        print (show m2 ++ " => " ++ show m1)
        print (show m2 ++ " -> " ++ show e2)
        ru (pure ())

connectMixers2 :: KleisliArrow (Mixer, Mixer) ()
connectMixers2 = (empty) >>>
    (push endpoints >>> push connections) >>> (pop >>> op remember >>> pop)

--------------------------------------------------------------------------------
--
connectMixers :: KleisliArrow (Mixer, Mixer) (Connection, Connection)
connectMixers = newEndpoint *** newEndpoint >>>
    (second newSink >>> newConnection) &&&
        (swap >>> second newSink >>> newConnection)

--------------------------------------------------------------------------------
--
main :: Void
main = do
    testArrow newMixer (Provider "1")
    testArrow newMixer' (Provider "1")

    testArrow (newMixer >>> newEndpoint) (Provider "1")
    testArrow (newMixer >>> newEndpoint') (Provider "1")
    testArrow (newMixer' >>> newEndpoint) (Provider "1")
    testArrow (newMixer' >>> newEndpoint') (Provider "1")

    testArrow connectMixers (Mixer (Provider "1"), (Mixer (Provider "2")))

    testArrow connectMixers2 (Mixer (Provider "1"), Mixer (Provider "2"))
