module BigInteger
    ( fromInt
    , neg
    , add
    , sub
    , mul
    ) where

import           Prelude       ( (*), (+), (-), (^), Integer, div, error, rem )
--import           Prelude       ( undefined )
import           Data.Bool     ( Bool(..), otherwise )
import           Data.Eq       ( Eq(..) )
import           Data.Function ( (.) )
import           Data.Int      ( Int )
import           Data.Ord      ( Ord(..), Ordering(..) )
import           Data.List     ( (++), drop, dropWhile, dropWhileEnd, length
                               , map, reverse, take )
import           Data.String   ( String )
import           Data.Word     ( Word32 )

import           Text.Read     ( read )
import           Text.Show     ( Show(..) )

type L = Word32

type D = [L]

data BigInteger = Zero
                | Pos D
                | Neg D
    deriving Eq

instance Show BigInteger where
    show = toString

instance Ord BigInteger where
    compare = cmp

power :: Int
power = 4

base :: L
base = 10 ^ power

fromInt :: Integer -> BigInteger
fromInt = fromString . show

fromString :: String -> BigInteger
fromString ('-' : s) = neg (fromString s)
fromString "0" = Zero
fromString str = fromD (map read (reverse (groupDigits power str)))

groupDigits :: Int -> String -> [String]
groupDigits groupLen str =
    let len = length str
        startLen = len `rem` groupLen
    in
        if startLen > 0
        then (take startLen str) : buildGroups groupLen (drop startLen str)
        else buildGroups groupLen str

buildGroups :: Int -> String -> [String]
buildGroups _ "" = []
buildGroups groupLen rest =
    (take groupLen rest) : buildGroups groupLen (drop groupLen rest)

fromD :: D -> BigInteger
fromD [] = Zero
fromD dx = Pos dx

toString :: BigInteger -> String
toString Zero = "0"
toString (Neg dx) = "-" ++ (toString (Pos dx))
toString (Pos dx) = dropWhile (== '0') (build dx)

build :: Show a => [a] -> String
build [] = error "build: no digits"
build [ l ] = pad (show l)
build (l : hs) = build hs ++ pad (show l)

pad :: String -> String
pad str = if length str < power then pad ("0" ++ str) else str

cmp :: BigInteger -> BigInteger -> Ordering
cmp Zero Zero = EQ
cmp (Pos dx) (Pos dy) = cmp0 dx dy
cmp (Pos _) _ = GT
cmp _ (Pos _) = LT
cmp x@(Neg _) y@(Neg _) =
    cmp (neg y) (neg x)
cmp (Neg _) _ = LT
cmp _ (Neg _) = GT

cmp0 :: D -> D -> Ordering
cmp0 [] [] = EQ
cmp0 (_ : _) [] = GT
cmp0 [] (_ : _) = LT
cmp0 (x0 : xs) (y0 : ys) =
    case cmp0 xs ys of
        EQ -> compare x0 y0
        ordering -> ordering

neg :: BigInteger -> BigInteger
neg Zero = Zero
neg (Neg d) = Pos d
neg (Pos d) = Neg d

add :: BigInteger -> BigInteger -> BigInteger
add x Zero = x
add Zero y = y
add (Pos dx) (Pos dy) = Pos (addD dx dy)
add x@(Pos _) y@(Neg _) =
    sub x (neg y)
add x@(Neg _) y@(Pos _) =
    sub y (neg x)
add x@(Neg _) y@(Neg _) =
    neg (add (neg x) (neg y))

addD :: D -> D -> D
addD = add0 False

add0 :: Bool -> D -> D -> D
add0 False [] [] = []
add0 False dx [] = dx
add0 False [] dy = dy
add0 True [] [] = [ 1 ]
add0 True dx [] = inc0 dx
add0 True [] dy = inc0 dy
add0 carry (xl : xh) (yl : yh) =
    let (carry', l) = addWithCarry carry xl yl
    in
        l : add0 carry' xh yh

addWithCarry :: Bool -> L -> L -> (Bool, L)
addWithCarry carry x y =
    let z = x + y + if carry then 1 else 0
    in
        if (z < base) then (False, z) else (True, z - base)

inc0 :: D -> D
inc0 x = addD x [ 1 ]

sub :: BigInteger -> BigInteger -> BigInteger
sub x Zero = x
sub Zero y = neg y
sub x@(Pos dx) y@(Pos dy) =
    case cmp x y of
        EQ -> Zero
        GT -> Pos (subD dx dy)
        LT -> Neg (subD dy dx)
sub x@(Pos _) y@(Neg _) =
    add x (neg y)
sub x@(Neg _) y@(Pos _) =
    neg (add (neg x) y)
sub x@(Neg _) y@(Neg _) =
    sub (neg y) (neg x)

subD :: D -> D -> D
subD dx dy = klean (sub0 False dx dy)

sub0 :: Bool -> D -> D -> D
sub0 False dx [] = dx
sub0 True dx [] = dec0 dx
sub0 _ [] _ = error "sub0: underflow"
sub0 borrow (xl : xh) (yl : yh) =
    let (borrow', l) = subWithBorrow borrow xl yl
    in
        l : sub0 borrow' xh yh

subWithBorrow :: Bool -> L -> L -> (Bool, L)
subWithBorrow borrow x y =
    let y' = if borrow then y + 1 else y
    in
        if x >= y' then (False, x - y') else (True, base + x - y')

dec0 :: D -> D
dec0 x = subD x [ 1 ]

mul :: BigInteger -> BigInteger -> BigInteger
mul _ Zero = Zero
mul Zero _ = Zero
mul (Pos dx) (Pos dy) = fromD (karatsuba dx dy)
mul x@(Pos _) y@(Neg _) =
    neg (mul x (neg y))
mul x@(Neg _) y@(Pos _) =
    neg (mul (neg x) y)
mul x@(Neg _) y@(Neg _) =
    mul (neg x) (neg y)

karatsuba :: D -> D -> D
karatsuba _ [] = []
karatsuba dx [ 1 ] = dx
karatsuba [] _ = []
karatsuba [ 1 ] dy = dy
karatsuba [ x ] [ y ] = reduce (x * y)

karatsuba x y = let m = max (length x) (length y)
                    m2 = m `div` 2
                    (xl, xh) = splitAt m2 x
                    (yl, yh) = splitAt m2 y
                    z0 = karatsuba xl yl
                    z1 = karatsuba (addD xl xh) (addD yl yh)
                    z2 = karatsuba xh yh
                in
                    addD (addD (shift (2 * m2) z2)
                               (shift m2 (subD z1 (addD z2 z0))))
                         z0

reduce :: L -> D
reduce l
    | l < base = [ l ]
    | otherwise = (l `rem` base) : (reduce (l `div` base))

splitAt :: Int -> D -> (D, D)
splitAt n d = (klean (take n d), klean (drop n d))

shift :: Int -> D -> D
shift 0 d = d
shift _ [] = []
shift n d = shift (n - 1) (0 : d)

klean :: D -> D
klean = dropWhileEnd (== 0)
