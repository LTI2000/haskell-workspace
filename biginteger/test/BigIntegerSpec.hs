module BigIntegerSpec where

import           Prelude

import           Test.Hspec
import           Test.Hspec.QuickCheck
import           Test.QuickCheck

import           BigInteger

largeInteger :: Integer
largeInteger = 10 ^ 100

spec :: Spec
spec = do
    describe "BigNum" $ do
        context "show . fromInt = show" $ do
            prop "x" $ do
                \(LargeInteger x) -> show (fromInt x) `shouldBe` show x

        context "compare" $ do
            prop "x" $ do
                \(LargeInteger x) (LargeInteger y) ->
                    compare (fromInt x) (fromInt y) `shouldBe` compare x y

        context "neg" $ do
            prop "neg x = -x" $ do
                \(LargeInteger x) -> neg (fromInt x) `shouldBe` fromInt (-x)

        context "add" $ do
            prop "x + 0 = x" $
                \(LargeInteger x) -> add (fromInt x) (fromInt 0) `shouldBe`
                    fromInt x
            prop "0 + x = x" $
                \(LargeInteger x) -> add (fromInt 0) (fromInt x) `shouldBe`
                    fromInt x
            prop "x + x = 2 * x" $
                \(LargeInteger x) -> add (fromInt x) (fromInt x) `shouldBe`
                    mul (fromInt 2) (fromInt x)
            prop "x + y" $
                \(LargeInteger x) (LargeInteger y) ->
                    add (fromInt x) (fromInt y) `shouldBe`
                        fromInt (x + y)

        context "sub" $ do
            prop "x - 0 = x" $
                \(LargeInteger x) -> sub (fromInt x) (fromInt 0) `shouldBe`
                    fromInt x
            prop "0 - x = -x" $
                \(LargeInteger x) -> sub (fromInt 0) (fromInt x) `shouldBe`
                    fromInt (-x)
            prop "x - x = 0" $
                \(LargeInteger x) -> sub (fromInt x) (fromInt x) `shouldBe`
                    fromInt 0
            prop "(x + 1) - x = 1" $
                \(LargeInteger x) -> sub (add (fromInt x) (fromInt 1))
                                         (fromInt x) `shouldBe`
                    fromInt 1
            prop "x - y" $
                \(LargeInteger x) (LargeInteger y) ->
                    sub (fromInt x) (fromInt y) `shouldBe`
                        fromInt (x - y)

        context "mul" $ do
            prop "x * 0 = 0" $
                \(LargeInteger x) -> mul (fromInt x) (fromInt 0) `shouldBe`
                    fromInt 0
            prop "x * 1 = x" $
                \(LargeInteger x) -> mul (fromInt x) (fromInt 1) `shouldBe`
                    fromInt x
            prop "0 * x  = 0" $
                \(LargeInteger x) -> mul (fromInt 0) (fromInt x) `shouldBe`
                    fromInt 0
            prop "1 * x = x " $
                \(LargeInteger x) -> mul (fromInt 1) (fromInt x) `shouldBe`
                    fromInt x
            prop "x * y" $
                \(LargeInteger x) (LargeInteger y) ->
                    mul (fromInt x) (fromInt y) `shouldBe`
                        fromInt (x * y)
            it (show (largeInteger + 1) ++
                    " * " ++
                        show (largeInteger - 1) ++
                            " = " ++
                                show ((largeInteger + 1) * (largeInteger - 1))) $
                mul (fromInt (largeInteger + 1)) (fromInt (largeInteger - 1)) `shouldBe`
                    fromInt ((largeInteger + 1) * (largeInteger - 1))

newtype LargeInteger = LargeInteger Integer
    deriving Show

{-
instance Arbitrary LargeInteger where
    arbitrary = LargeInteger <$> choose (-100000, 100000)
-}
instance Arbitrary LargeInteger where
    arbitrary = LargeInteger <$> frequency [ ( n + 1
                                             , let i = 10000 ^ n
                                               in
                                                   choose (-i, i)
                                             )
                                           | n <- [0 .. 10] ]
