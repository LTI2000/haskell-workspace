data Term = Atom String
          | Tuple [Term]
          | List [Term]
          | Binary String
          | Long Integer
    deriving (Eq, Show)

newtype Matcher a = Matcher { match :: Term -> Maybe a }

fmap_ :: ((a -> b), Matcher a) -> Matcher b
fmap_ (f, ma) = Matcher (\o -> f <$> match ma o)

unit :: a -> Matcher a
unit a = Matcher (\_ -> return a)

bind :: Matcher a -> (a -> Matcher b) -> Matcher b
bind ma f = Matcher (\o -> match ma o >>= \a -> match (f a) o)

apply_ :: (Matcher (a -> b), Matcher a) -> Matcher b
apply_ (mf, ma) = Matcher (\o -> match mf o <*> match ma o)

seq_ :: (Matcher a, Matcher b) -> Matcher b
seq_ (ma, mb) = Matcher (\o -> match ma o *> match mb o)

failure :: Matcher a
failure = Matcher (\_ -> Nothing)

option :: Matcher a -> Matcher a -> Matcher a
option ma na = Matcher (\o -> case match ma o of
                            Nothing -> match na o
                            res -> res)

--  instance Functor Matcher where
--      fmap f (Matcher m) = Matcher (\o -> f <$> m o)
--
--  instance Applicative Matcher where
--      pure = unit
--      (Matcher m1) <*> (Matcher m2) =
--          Matcher (\o -> (m1 o) >>= \f -> (m2 o) >>= \x -> return (f x))
--
--  instance Monad Matcher where
--      return = pure
--      (>>=) = bind
item :: Matcher Term
item = Matcher return

satisfy :: (Term -> Bool) -> Matcher Term
satisfy p = item `bind` \t -> if p t then unit t else failure

atom :: Matcher String
atom = Matcher (\o -> case o of
                    Atom value -> Just value
                    _ -> Nothing)

binary :: Matcher String
binary = Matcher (\o -> case o of
                      Binary value -> Just value
                      _ -> Nothing)

long :: Matcher Integer
long = Matcher (\o -> case o of
                    Long value -> Just value
                    _ -> Nothing)

tupleN :: Int -> Matcher ()
tupleN n = Matcher (\o -> case o of
                        Tuple es -> if length es == n then Just () else Nothing
                        _ -> Nothing)

element :: (Int, Matcher a) -> Matcher a
element (n, a) = Matcher (\o -> case o of
                              Tuple es -> match a (es !! n)
                              _ -> Nothing)

list :: Matcher a -> Matcher [a]
list ma = Matcher (\o -> case o of
                       List elements -> traverse (match ma) elements
                       _ -> Nothing)

tuple2 :: Matcher a -> Matcher b -> Matcher (a, b)
tuple2 a b = apply_ ( fmap_ ((,), seq_ (tupleN 2, element (0, a)))
                    , element (1, b)
                    )

tuple3 :: Matcher a -> Matcher b -> Matcher c -> Matcher (a, b, c)
tuple3 a b c = apply_ ( apply_ ( fmap_ ((,,), seq_ (tupleN 3, element (0, a)))
                               , element (1, b)
                               )
                      , element (2, c)
                      )

record2 :: String -> Matcher b -> Matcher c -> Matcher (b, c)
record2 tag b c = apply_ ( fmap_ ( (,)
                                 , seq_ ( tupleN 3
                                        , seq_ ( element ( 0
                                                         , (satisfy (== Atom tag))
                                                         )
                                               , element (1, b)
                                               )
                                        )
                                 )
                         , element (2, c)
                         )

main :: IO ()
main = do
    let term = Tuple [ Atom "OFFERED"
                     , List [ Tuple [ Binary "v1", Binary "k1" ]
                            , Tuple [ Binary "v2", Binary "k2" ]
                            ]
                     , Long 123456789
                     ]
        matcher = record2 "OFFERED" (list (tuple2 binary binary)) long
        term2 = Tuple [ Atom "a", Atom "b", Atom "c" ]
        matcher1 = tuple3 atom atom atom
        matcher2 = record2 "a" atom atom


        matcher3 = fmap_ ( (\(Atom value) -> value)
                         ,
                         -- atom `pseq`
                         (satisfy (== Atom "OFFERED") `option`
                              satisfy (== Atom "ALERTING") `option`
                              satisfy (== Atom "CONNECTED"))
                         )


    print $ match matcher term
    print $ match matcher1 term2
    print $ match matcher2 term2
    print $ match matcher3 (Atom "OFFERED")
    print $ match matcher3 (Atom "ALERTING")
    print $ match matcher3 (Atom "CONNECTED")
    print $ match matcher3 (Atom "DISCONNECTED")
