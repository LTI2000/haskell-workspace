module Parser(Parser
             ,parse
             ,(+++)
             ,space
             ,char
             ,number
             ,string
             ,symbol
             )
       where

import Control.Applicative
import Control.Monad
import Data.Char

-- -----------------------------------------------------------------------------
-- A monad of parsers [ http://www.cs.nott.ac.uk/~gmh/pearl.pdf ]
-- -----------------------------------------------------------------------------

newtype Parser a = Parser { parse :: (String -> [(a,String)]) }

instance Functor Parser where
  fmap = liftM

instance Applicative Parser where
  pure  = return
  (<*>) = ap

instance Monad Parser where
  return a = Parser (\cs -> [(a, cs)])
  p >>= f  = Parser (\cs -> join [parse (f a) cs' | (a, cs') <- parse p cs]) -- FIXME looks like applicative, hence move definitions to Applicative instance

instance Alternative Parser where
  empty   = Parser (const [])
  p <|> q = Parser (\cs -> parse p cs ++ parse q cs)

instance MonadPlus Parser where
  mzero = empty
  mplus = (<|>)

(+++) :: Parser a -> Parser a -> Parser a
p +++ q = Parser (\cs -> case parse (p <|> q) cs of
                          []    -> []
                          (x:_) -> [x])

item :: Parser Char
item = Parser (\cs -> case cs of
                       "" -> []
                       (c : cs) -> [(c, cs)])


sat :: (Char -> Bool) -> Parser Char
sat p = do {c <- item; if p c then return c else empty}

char :: Char -> Parser Char
char c = sat (c ==)

string :: String -> Parser String
string ""       = return ""
string (c : cs) = do { char c; string cs; return (c:cs) }

number :: Parser String
number = some (sat isDigit)

space :: Parser String
space = many (sat isSpace)

symbol :: Parser String
symbol = some (sat isSymbolChar) where
  isSymbolChar '+' = True
  isSymbolChar '-' = True
  isSymbolChar '*' = True
  isSymbolChar '/' = True
  isSymbolChar '<' = True
  isSymbolChar '=' = True
  isSymbolChar '>' = True
  isSymbolChar '?' = True
  isSymbolChar '!' = True
  isSymbolChar '$' = True
  isSymbolChar '@' = True
  isSymbolChar ':' = True
  isSymbolChar '_' = True
  isSymbolChar x = isAlpha x
