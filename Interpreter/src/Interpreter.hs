{-# LANGUAGE TupleSections #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Interpreter ( Value(..)
                   , interpM
                   , execM -- FIXME should not export, currently used for testing
                   , read
                   , write
                   ) where

import           Prelude hiding (read, not)
import qualified Text.Read (read)

import           Control.Applicative
import           Control.Monad.Reader
import           Control.Monad.Cont
import           Control.Monad.State

import           Parser
import           Environment
import           Store

-- *****************************************************************************
-- lazy scheme interpreter in haskell
-- *****************************************************************************

-- -----------------------------------------------------------------------------
-- value
-- -----------------------------------------------------------------------------

type Function = [Value] -> Value
type ScmFunction = [Value] -> Scm Value
data Value = Nil
           | Cons Value Value
           | Boolean Bool
           | Number Integer
           | Symbol Name
           | Procedure Name (Maybe Location) ScmFunction
           | Cell Location
           | Void


instance Eq Value where
  Nil                    == Nil                     = True
  Cons a b               == Cons a' b'              = a == a' && b == b'
  Boolean a              == Boolean a'              = a == a'
  Number a               == Number a'               = a == a'
  Symbol a               == Symbol a'               = a == a'
  Procedure _ (Just a) _ == Procedure _ (Just a') _ = a == a'
  Procedure _ (Just _) _ == Procedure _ Nothing   _ = False
  Procedure _ Nothing  _ == Procedure _ (Just _)  _ = False
  Procedure a _        _ == Procedure a' _        _ = a == a'
  Cell a                 == Cell a'                 = a == a'
  Void                   == Void                    = True
  _                      == _                       = False


read :: String -> Value

read s = case parse sexp s of
              ((value, _) : _) -> value
              _                -> error $ "ERROR:" ++ s
         where
           sexp :: Parser Value
           sexp = do
                    space
                    do
                      do { string "#t"; return (Boolean True) }
                      +++
                      do { string "#f"; return (Boolean False) }
                      +++
                      do { digits <- number; return (Number (Text.Read.read digits)) }
                      +++
                      do { name <- symbol; return (Symbol name) }
                      +++
                      do { char '\''; datum <- sexp; return (Cons (Symbol "quote") (Cons datum Nil)) }
                      +++
                      do { char '('; sexpList }

           sexpList :: Parser Value
           sexpList = do
                        space
                        do
                          do { char ')'; return Nil }
                          +++
                          do
                            car <- sexp;
                            do
                              do { space; char '.'; cdr <- sexp; space; char ')'; return (Cons car cdr) }
                              +++
                              do { cdr <- sexpList; return (Cons car cdr) }


write :: Value -> String

write Nil = "()"

write (Cons car cdr) = "(" ++ write car ++ writeTail cdr
  where
    writeTail Nil = ")"
    writeTail (Cons car cdr) = " " ++ write car ++ writeTail cdr
    writeTail tail = " . " ++ write tail ++ ")"

write (Boolean True) = "#t"

write (Boolean False) = "#f"

write (Number number) = show number

write (Symbol name) = name

write (Procedure name _ _) = name

write (Cell location) = "#<cell:" ++ show location ++ ">"

write Void = "#<void>"

instance Show Value where
  show = write

-- -----------------------------------------------------------------------------
-- primitive procedures, non-monadic
-- -----------------------------------------------------------------------------

nullP :: Function
nullP [Nil] = Boolean True
nullP [_]   = Boolean False

cons :: Function
cons [head, tail] = Cons head tail

car :: Function
car [Cons head _] = head

cdr :: Function
cdr [Cons _ tail] = tail

list :: Function
list (x:xs) = Cons x (list xs)
list [] = Nil

plus :: Function
plus [Number a, Number b] = Number (a + b)
--plus x = error $ show $ map write x

minus :: Function
minus [Number a, Number b] = Number (a - b)

times :: Function
times [Number a, Number b] = Number (a * b)

divided :: Function
divided [Number a, Number b] = Number (a `div` b)

lessThan :: Function
lessThan [Number a, Number b] = Boolean (a < b)

greaterThan :: Function
greaterThan [Number a, Number b] = Boolean (a > b)

procedureP :: Function
procedureP [(Procedure _ _ _)] = Boolean True
procedureP [_]                 = Boolean False

eqP :: Function
eqP [a, b] = Boolean (a == b)

not :: Function
not [Boolean False] = Boolean True
not [_]             = Boolean False

primitives :: [(Name, Function)]
primitives = [ ("null?"       , nullP)
             , ("cons"        , cons)
             , ("car"         , car)
             , ("cdr"         , cdr)
             , ("list"        , list)
             , ("+"           , plus)
             , ("-"           , minus)
             , ( "*"          , times)
             , ( "/"          , divided)
             , ( "<"          , lessThan)
             , ( ">"          , greaterThan)
             , ( "procedure?" , procedureP)
             , ( "eq?"        , eqP)
             , ( "not"        , not)
             ]

-- -----------------------------------------------------------------------------
-- runtime procedures, non-monadic
-- -----------------------------------------------------------------------------

newClosure :: Location -> [Name] -> (Expression Value) -> (Environment Value) -> Value
newClosure tag formals body env =
  Procedure "#<procedure>" (Just tag) f
  where
    f args = local (\_ -> extendEnv (zip formals args) env) (evalM body)

newContinuation ::  Location -> (Value -> Scm Value) -> Value
newContinuation tag k =
  Procedure "#<continuation>" (Just tag) f
  where
    f [arg] = k arg

-- -----------------------------------------------------------------------------
-- essential procedures, monadic
-- -----------------------------------------------------------------------------

procApply :: ScmFunction
procApply [f, args] =
  applyM f $ unpack args
  where
    unpack :: Value -> [Value]
    unpack (Cons car cdr) = car : unpack cdr
    unpack Nil = []

procCallCC :: ScmFunction
procCallCC [f] = do
  store <- get
  let (tag, store') = newLocation Void store
  put store'
  callCC $ \k -> applyM f [newContinuation tag k]

procCell :: ScmFunction
procCell [] =
  do
    store <- get
    let (location, store') = newLocation Void store
    put store'
    return $ Cell location

procCellRef :: ScmFunction
procCellRef [Cell location] =
  do
    store <- get
    return $ lookupStore location store

procCellSet :: ScmFunction
procCellSet [Cell location, arg] =
  do
    store <- get
    let store' = updateStore location arg store
    put store'
    return Void

procedures :: [(Name, ScmFunction)]
procedures = [ ("apply",     procApply)
             , ("call/cc",   procCallCC)
             , ("cell",      procCell)
             , ("cell-ref",  procCellRef)
             , ("cell-set!", procCellSet)
             ]

-- -----------------------------------------------------------------------------
-- initial environment constructed from procedures and primitives
-- -----------------------------------------------------------------------------

initialEnv = extendEnv (map (\(name, function) -> (name, procedure name (id function)))         procedures
                        ++
                        map (\(name, function) -> (name, procedure name ((return .) function))) primitives)
             emptyEnv
  where
    procedure :: String -> ScmFunction -> Value
    procedure name f = Procedure ("#<procedure:" ++ name ++ ">") Nothing f

-- -----------------------------------------------------------------------------
-- analysis
-- -----------------------------------------------------------------------------

data Expression v = Constant v
                  | VariableReference Name
                  | Abstraction [Name] (Expression v)
                  | Application (Expression v) [(Expression v)]
                  | PrimitiveApplication ([v] -> v) [(Expression v)]
                  | Conditional (Expression v) (Expression v) (Expression v)
                  | Sequence (Expression v) (Expression v)
                  | Fail
                  | Amb (Expression v) (Expression v)

keywords :: [String]
keywords = [ "quote"
           , "lambda"
           , "let"
           , "if"
           , "begin"
           , "amb"
           ]

data SyntacticKind = Keyword
                   | Variable
                   | Primitive
                   deriving(Eq)

is :: SyntacticKind -> Name -> Environment SyntacticKind -> Bool
is kind name env = lookupEnv name env == kind


parseEnv = extendEnv (map (, Keyword)           keywords
                      ++
                      map ((, Variable) . fst)  procedures
                      ++
                      map ((, Primitive) . fst) primitives)
           emptyEnv


type Exp = Expression Value


analyze :: Value -> Environment SyntacticKind -> Exp

analyze x@(Boolean _) _ =
  Constant x

analyze x@(Number _) _ =
  Constant x

analyze (Symbol name) env
  | is Variable name env =
      VariableReference name
  | is Primitive name env =
      VariableReference name

analyze (Cons car cdr) env =
  analyzePair car cdr env

analyze value _ = error $ "analyze error: " ++ show value


analyzePair :: Value -> Value -> Environment SyntacticKind -> Exp

analyzePair (Symbol "quote") (Cons datum Nil) env
  | is Keyword "quote" env =
      Constant datum

analyzePair (Symbol "lambda") (Cons formals body) env
  | is Keyword "lambda" env =
      let names = (analyzeNames formals)
          env' = extendEnv (map (, Variable) names) env
      in  Abstraction names (analyzeBody body env')

analyzePair (Symbol "let") (Cons bindings body) env
  | is Keyword "let" env =
      let (names, initializers) = analyzeBindings bindings
          env' = extendEnv (map (, Variable) names) env
      in  Application (Abstraction names (analyzeBody body env')) (analyzeList initializers env)

analyzePair (Symbol "if") (Cons test (Cons consequent (Cons alternate Nil))) env
  | is Keyword "if" env =
      Conditional (analyze test env) (analyze consequent env) (analyze alternate env)

analyzePair (Symbol "if") (Cons test (Cons consequent Nil)) env
  | is Keyword "if" env =
      Conditional (analyze test env) (analyze consequent env) (Constant Void)

analyzePair (Symbol "begin") body env
  | is Keyword "begin" env =
      analyzeBody body env

analyzePair (Symbol "amb") exps env
  | is Keyword "amb" env =
      analyzeAmb exps env

analyzePair (Symbol name) exps env
  | is Primitive name env =
      PrimitiveApplication (lookup name primitives) (analyzeList exps env)
      where
        lookup :: Name -> [(Name, Function)] -> Function
        lookup name ((n, f):nfs) =
          if(name == n) then
            f
          else
            lookup name nfs
        lookup _ [] = error $ "Not in scope: " ++ name

analyzePair car cdr env =
  Application (analyze car env) (analyzeList cdr env)


analyzeNames :: Value -> [Name]
analyzeNames Nil = []
analyzeNames (Cons (Symbol name) rest) = name : (analyzeNames rest)

analyzeBindings :: Value -> ([Name], Value)
analyzeBindings Nil = ([], Nil)
analyzeBindings (Cons (Cons (Symbol name) (Cons initializer Nil)) rest) = let (names, initializers) = (analyzeBindings rest)
                                                                          in  (name : names, Cons initializer initializers)

analyzeBody :: Value -> Environment SyntacticKind -> Exp
analyzeBody (Cons body Nil) env = analyze body env
analyzeBody (Cons first rest) env = Sequence (analyze first env) (analyzeBody rest env)

analyzeAmb :: Value -> Environment SyntacticKind -> Exp
analyzeAmb Nil _ = Fail
--analyzeAmb (Cons exp Nil) env = analyze exp env -- optimization
analyzeAmb (Cons first rest) env = Amb (analyze first env) (analyzeAmb rest env)

analyzeList :: Value -> Environment SyntacticKind -> [Exp]
analyzeList Nil _ = []
analyzeList (Cons car cdr) env = (analyze car env) : (analyzeList cdr env)

-- -----------------------------------------------------------------------------
-- monadic evaluation
-- -----------------------------------------------------------------------------

instance (Alternative m) => Alternative (ContT r m) where
  empty = ContT $ \_ -> empty
  m <|> n = ContT $ \c -> runContT m c <|> runContT n c

instance (Alternative m) => MonadPlus (ContT r m) where
  mzero = empty
  mplus = (<|>)


-- -----------------------------------------------------------------------------
type Env = Environment Value
type Sto = Store Value

--  //  %%  type M a =  EnvT Env                                  (environment)
--  //  %%                   (ContT Answer                        (continuation)
--  //  %%                          (StateT Store                 (store)
--  //  %%                                  (StateT IO            (input/output)
--  //  %%                                          (ErrT         (error reporting)
--  //  %%                                           List)))) a   (nondeterminism)

newtype Scm a = Scm { runScm :: ReaderT Env (ContT Value (StateT Sto [])) a }
              deriving
                ( Functor
                , Applicative
                , Monad
                , MonadReader Env
                , MonadCont
                , MonadState Sto
                , Alternative
                , MonadPlus
                )

runM :: Scm Value -> [Value]

--runM scm = evalStateT (runContT (runReaderT (runScm scm) initialEnv) return) newStore
runM scm = let scm'    = runScm scm                 :: (ReaderT Env (ContT Value (StateT Sto ([])))) Value in
           let reader' = runReaderT scm' initialEnv ::              (ContT Value (StateT Sto ([])))  Value in
           let cont'   = runContT reader' return    ::                           (StateT Sto ([]))   Value in
           let state'  = evalStateT cont' newStore  ::                                       ([])    Value in
           state'

interpM :: String -> [Value]

interpM source = execM (read source)


execM :: Value -> [Value]

execM value = runM $ evalM $ analyze value parseEnv

-- -----------------------------------------------------------------------------
evalM :: Exp -> Scm Value

evalM (Constant value) = do
  return value

evalM (VariableReference name) = do
  env <- ask
  return $ lookupEnv name env

evalM (Abstraction formals body) = do
  store <- get
  let (tag, store') = newLocation Void store
  put store'
  env <- ask
  return $ newClosure tag formals body env

evalM (Application operator operands) = do
  f <- evalM operator
  args <- (mapM evalM operands)
  applyM f args

evalM (PrimitiveApplication f operands) = do
  args <- (mapM evalM operands)
  return $ f args

evalM (Conditional test consequent alternate) = do
  result <- evalM test
  if isTruish result
    then evalM consequent
    else evalM alternate
  where
    isTruish :: Value -> Bool
    isTruish (Boolean False) = False
    isTruish _               = True

evalM (Sequence first second) = do
  evalM first
  evalM second

evalM Fail = do
  empty

evalM (Amb first second) = do
  evalM first <|> evalM second

-- -----------------------------------------------------------------------------
applyM :: Value -> [Value] -> Scm Value

applyM (Procedure _ _ f) args = f args
