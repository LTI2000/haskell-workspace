module Store
       where

import Data.Map (Map)
import qualified Data.Map as Map

-- -----------------------------------------------------------------------------
-- store
-- -----------------------------------------------------------------------------

type Location = Integer
type Store v = Map Location v

newLocation :: v -> Store v -> (Location, Store v)
newLocation v store = let location = toInteger $ Map.size store
                    in (location, Map.insertWith' const location v store)

newStore :: Store v
newStore = Map.empty

updateStore :: Location -> v -> Store v -> Store v
updateStore key value store = Map.insertWith' const key value store

lookupStore :: Location -> Store v -> v
lookupStore key store = store Map.! key
