module Environment
       where

-- -----------------------------------------------------------------------------
-- environment
-- -----------------------------------------------------------------------------

type Name = String

type Environment v = Name -> v

emptyEnv :: Environment v

emptyEnv name =
  error $ name ++ " is unbound"


extendEnv :: [(Name, v)] -> Environment v -> Environment v

extendEnv ((name, value) : bindings) parentEnv = extend name value (extendEnv bindings parentEnv)
  where
    extend :: Name -> v -> Environment v -> Environment v
    extend newName newValue parentEnv name =
      if name == newName then
        newValue
      else
        lookupEnv name parentEnv

extendEnv [] parentEnv = parentEnv


lookupEnv :: Name -> Environment v -> v

lookupEnv = flip ($)
