module Main where

import System.IO
import Interpreter

-- -----------------------------------------------------------------------------
-- main program
-- -----------------------------------------------------------------------------

main :: IO ()
main = do
  hSetBuffering stdout NoBuffering
  putStrLn "********************************************************************************"
  putStrLn "** Nonstrict Nondeterministic Scheme like Interpereter by L.T.I. 2015         **"
  putStrLn "********************************************************************************"
  repl

repl :: IO ()
repl = do
  putStr "> "
  src <- getLine
  putStrLn (show (interpM src))
  repl
