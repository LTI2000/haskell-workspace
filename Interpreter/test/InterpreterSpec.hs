module InterpreterSpec (spec) where

import Prelude hiding (read)

import Test.Hspec
import Test.Hspec.QuickCheck
import Test.QuickCheck

import Interpreter



spec :: Spec
spec = do
  describe "nothing" $ do
    it "to it" $ do
      True

-- -----------------------------------------------------------------------------
-- main program
-- -----------------------------------------------------------------------------

main :: IO ()
main = do
  main2
  result1 <- quickCheckResult prop_WriteRead
  result2 <- quickCheckResult prop_EqP
  result3 <- quickCheckResult prop_EqP2
  return ()

main2 :: IO ()
main2 = do
  mapM_ check x
    where
      check (source, expected) = do
        let actual = map write (interpM source)
        if expected == actual then
          putStrLn $ show actual
        else
          error $ "expected: " ++ (show expected) ++ " actual: " ++ (show actual)

      x = [ (
               "#t",

               ["#t"]
            )
          , (
               "'a",

               ["a"]
            )
          , (
               "(quote ())",

               ["()"]
            )
          , (
               "(cons #t #f)",

               ["(#t . #f)"]
            )
          , (
               "(list)",

               ["()"]
            )
          , (
               "(list 1)",

               ["(1)"]
            )
          , (
               "(list 1 2 3)",

               ["(1 2 3)"]
            )
          , (
               "(lambda () '())",

               ["#<procedure>"]
            )
          , (
               "((lambda () #f) #t)",

               ["#f"]
            )
          , (
               "((lambda (x) (null? x)) (quote ()))",

               ["#t"]
            )
          , (
               "((lambda (x y) (cons x y)) 'car 'cdr)",

               ["(car . cdr)"]
            )
          , (
               "((lambda (x y) (cons x y) (cons y x)) 'car 'cdr)",

               ["(cdr . car)"]
            )
          , (
               "(if #t 1 2)",

               ["1"]
            )
          , (
               "(if #f 1 2)",

               ["2"]
            )
          , (
               "(if '() 1 2)",

               ["1"]
            )
          , (
               "(if #t 1)",

               ["1"]
            )
          , (
               "(if #f 1)",

               ["#<void>"]
            )
          , (
               "(if '() 1)",

               ["1"]
            )
          , (
               "(begin 1)",

               ["1"]
            )
          , (
               "(begin 2 3 4)",

               ["4"]
            )
          , (
               "((lambda (quote) (quote 1)) (lambda (ignored) (quote y)))",

               ["y"]
            )
          , (
               " (((lambda (f)                                  \
               \     ((lambda (x) (f (lambda (y) ((x x) y))))   \
               \      (lambda (x) (f (lambda (y) ((x x) y)))))) \
               \   (lambda (g) (lambda (n)                      \
               \                 (if (< n 1)                    \
               \                     1                          \
               \                     (* n (g (- n 1)))))))      \
               \  10)                                           ",

               ["3628800"]
            )
          , (
               "(let () (list))",

               ["()"]
            )
          , (
               "(let ((a 1)) (list a))",

               ["(1)"]
            )
          , (
               "(let ((a 1) (b 2)) (list a b))",

               ["(1 2)"]
            )
          , (
               "(let ((a 1) (b 2) (c 3)) (list a b c))",

               ["(1 2 3)"]
            )
          , (
               "(let ((a 1) (b 2)) (+ a b))",

               ["3"]
            )
          , (
               "(amb)",

               []
            )
          , (
               "(amb 1)",

               ["1"]
            )
          , (
               "(amb 1 2)",

               ["1", "2"]
            )
          , (
               "(+ 1 (amb 2 3))",

               ["3", "4"]
            )
          , (
               "(* (amb 1 2) (amb 3 4))",

               ["3", "4", "6", "8"]
            )
          , (
               "apply",

               ["#<procedure:apply>"]
            )
          , (
               "(apply list (list))",

               ["()"]
            )
          , (
               "(apply + '(1 2))",

               ["3"]
            )
          , (
               "(apply (amb + *) (cons (amb 1 2) (cons (amb 3 4) '())))",

               ["4", "5", "5", "6", "3", "4", "6", "8"]
            )
          , (
               "call/cc",

               ["#<procedure:call/cc>"]
            )
          , (
               "(call/cc procedure?)",

               ["#t"]
            )
          , (
               "(procedure? call/cc)",

               ["#t"]
            )
          , (
               "(procedure? procedure?)",

               ["#t"]
            )
          , (
               "(procedure? apply)",

               ["#t"]
            )
          , (
               "(procedure? (lambda (k) k))",

               ["#t"]
            )
          , (
               "(procedure? (call/cc (lambda (k) k)))",

               ["#t"]
            )
          , (
               "(procedure? #t)",

               ["#f"]
            )
          , (
               "(call/cc null?)",

               ["#f"]
            )
          , (
               "(call/cc (lambda (k) k))",

               ["#<continuation>"]
            )
          , (
               "(call/cc (lambda (k) (+ 1 (k 2))))",

               ["2"]
            )
          , (
               "(call/cc (lambda (k) (+ 1 (amb (k 2)))))",

               ["2"]
            )
          , (
               "(call/cc (lambda (k) (+ 1 (amb (k 2) (k 3)))))",

               ["2", "3"]
            )
          , (
               "(cell)",

               ["#<cell:0>"]
            )
          , (
               "(cell-ref (cell))",

               ["#<void>"]
            )
          , (
               "(cell-set! (cell) #t)",

               ["#<void>"]
            )
          , (
               "((lambda (x) (cell-set! x 41) (+ 1 (cell-ref x))) (cell))",

               ["42"]
            )
          , (
               "((lambda (x) (amb (cell-set! x 41) (cell-set! x 42)) (+ 1 (cell-ref x))) (cell))",

               ["42", "43"]
            )
          , (
               " (list                                                          \
               \       (eq? #f #f)                                              \
               \  (not (eq? #f #t))                                             \
               \  (not (eq? #t #f))                                             \
               \       (eq? #t #t)                                              \
               \       (eq? + +)                                                \
               \  (not (eq? + -))                                               \
               \  (not (eq? * (lambda () #t)))                                  \
               \  (not (eq? (lambda () #t) *))                                  \
               \  (not (eq? (lambda () #t) (lambda () #t)))                     \
               \       (let ((f (lambda () #t))) (eq? f f))                     \
               \  (not (eq? (cell) (cell)))                                     \
               \       (let ((c (cell))) (eq? c c))                             \
               \       (eq? 'a 'a)                                              \
               \  (not (eq? 'a 'b))                                             \
               \       (eq? 1 1)                                                \
               \  (not (eq? 1 2))                                               \
               \  (not (eq? (call/cc (lambda (k) k)) (call/cc (lambda (k) k)))) \
               \       (let ((k (call/cc (lambda (k) k)))) (eq? k k))           \
               \ )                                                              ",

               ["(#t #t #t #t #t #t #t #t #t #t #t #t #t #t #t #t #t #t)"]
            )
          ]


-- -----------------------------------------------------------------------------
-- quickcheck tests
-- -----------------------------------------------------------------------------

prop_WriteRead :: Value -> Bool
prop_WriteRead v = (read . write) v == v

instance Arbitrary Value where
  arbitrary = sized value
    where
      value 0 = oneof [ return $ Nil
                      , return $ Boolean True
                      , return $ Boolean False
                      , Number <$> choose (0, 100)
                      , Symbol <$> listOf1 (elements ['a'..'z'])
                      -- , return $ Void
                      ]
      value n = Cons <$> subvalue <*> subvalue
        where
          subvalue = value (n `div` 2)


prop_EqP :: Value -> Value -> Bool
prop_EqP v w =
  case execM (Cons (Symbol "eq?") (Cons (Cons (Symbol "quote") (Cons v Nil)) (Cons (Cons (Symbol "quote") (Cons w Nil)) Nil))) of
    [(Boolean True)]  -> v == w
    [(Boolean False)] -> v /= w

prop_EqP2 :: Value -> Value -> Bool
prop_EqP2 v w =
  let (v', w') = (map write $ execM (Cons (Symbol "quote") (Cons v Nil)), map write $ execM (Cons (Symbol "quote") (Cons w Nil)))
  in  (v == w) == (v' == w')
