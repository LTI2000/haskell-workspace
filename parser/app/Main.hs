module Main where

import Prelude
import Control.Monad

import           NanoParsec

main :: IO ()
main = do
  print (parse (some item) "")
  print (parse (some item) "a")
  print (parse (some item) "ab")
  print (parse (some item) "abc")

  print (parse (many item) "")
  print (parse (many item) "a")
  print (parse (many item) "ab")
  print (parse (many item) "abc")

  forever $ do
    putStr "> "
    a <- getLine
    print $ eval $ run a
