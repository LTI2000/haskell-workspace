module GonzalezSpec ( spec ) where

import           Data.Bool
import           Data.Function

import           Test.Hspec
import           Test.Hspec.QuickCheck
import           Test.QuickCheck

import           Gonzalez

spec :: Spec
spec = do
    describe "description" $ do
        prop "property" $ do
            \(b1, b2) -> case (b1, b2) of
                (False, False) -> b1 && b2 `shouldBe` False
                (False, True) -> b1 && b2 `shouldBe` False
                (True, False) -> b1 && b2 `shouldBe` False
                (True, True) -> b1 && b2 `shouldBe` True
