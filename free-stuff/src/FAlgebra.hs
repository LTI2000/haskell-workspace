{-# LANGUAGE DeriveFunctor #-}

module FAlgebra where

import           Prelude hiding ( const )

--------------------------------------------------------------------------------
newtype Fix f = In (f (Fix f))

unFix :: Fix f -> f (Fix f)
unFix (In x) = x

cata :: Functor f => (f a -> a) -> Fix f -> a
cata alg = alg . fmap (cata alg) . unFix

fixIn0 :: f (Fix f) -> Fix f
fixIn0 c = In c

fixIn1 :: (a -> f (Fix f)) -> (a -> Fix f)
fixIn1 c a = In (c a)

fixIn2 :: (a -> (b -> f (Fix f))) -> (a -> (b -> Fix f))
fixIn2 c a b = In (c a b)

--------------------------------------------------------------------------------
data ListF a b = Nil
               | Cons a b

instance Functor (ListF a) where
    fmap _ Nil = Nil
    fmap f (Cons e x) = Cons e (f x)

type List a = Fix (ListF a)

algSum :: ListF Int Int -> Int
algSum Nil = 0
algSum (Cons e acc) = e + acc

nil :: List a
nil = fixIn0 Nil

cons :: a -> List a -> List a
cons = fixIn2 Cons

lst :: List Int
lst = cons 2 $ cons 3 $ cons 4 nil

example1 :: Int
example1 = (cata algSum) lst

--------------------------------------------------------------------------------
data ExprF a = Const Int
             | Add a a
             | Mul a a
    deriving (Show, Functor)

type Expr = Fix ExprF

algExpr :: ExprF Int -> Int
algExpr (Const i) = i
algExpr (Add a b) = a + b
algExpr (Mul a b) = a * b

const :: Int -> Expr
const = fixIn1 Const

add :: Expr -> Expr -> Expr
add = fixIn2 Add

mul :: Expr -> Expr -> Expr
mul = fixIn2 Mul

expr :: Expr
expr = add (mul (const 1) (const 2)) (mul (const 3) (const 4))

example2 :: Int
example2 = (cata algExpr) expr

--------------------------------------------------------------------------------
example0 :: Int
example0 = foldr (\e acc -> e + acc) 0 [ 2, 3, 4 ]
