{-# LANGUAGE DeriveFunctor #-}

module FreeTExample ( demo ) where

import           Prelude

import           Control.Monad.Trans.Free

data TeletypeF next = PutString String next
                    | GetString (String -> next)
    deriving Functor

type Teletype = FreeT TeletypeF

putString :: (Monad m) => String -> Teletype m ()
putString str = liftF $ PutString str ()

getString :: (Monad m) => Teletype m String
getString = liftF $ GetString id

prompt :: (Monad m) => Teletype m ()
prompt = do
    putString "Supply the next step: (greet|forward|quit)"
    cmd <- getString
    case cmd of
        "forward" -> do
            str <- getString
            putString str
            prompt
        "greet" -> do
            putString "Hello, world!"
            prompt
        _ -> return ()

demo :: IO ()
demo = iterT interpreterIO prompt

interpreterIO :: TeletypeF (IO ()) -> IO ()
interpreterIO x = case x of
    (PutString str t') -> do
        putStrLn str
        t'
    (GetString k) -> do
        str <- getLine
        k str
