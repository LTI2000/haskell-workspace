{-# LANGUAGE DeriveFunctor #-}

module Handwritten where

import           Data.Function
import           Control.Applicative
import           Control.Monad

import           Data.List
import           Data.String
import           Text.Show

data Free f a = Free (f (Free f a))
              | Pure a

instance Functor f => Functor (Free f) where
    fmap f (Pure a) = Pure $ f a
    fmap f (Free fa) = Free $ (fmap . fmap) f fa

instance Functor f => Applicative (Free f) where
    pure = Pure
    Pure a <*> Pure b = Pure $ a b
    Pure a <*> b@(Free _) = fmap a b
    Free ma <*> b = Free $ fmap (<*> b) ma

instance Functor f => Monad (Free f) where
    return = pure
    Pure a >>= k = k a
    Free ma >>= k = Free $ fmap (>>= k) ma

liftF :: Functor f => f a -> Free f a
liftF command = Free $ fmap pure command

--------------------------------------------------------------------------------
data CmdF a next = Output a next
                 | Done
    deriving Functor

type Cmd a = Free (CmdF a)

output :: a -> Cmd a ()
output msg = liftF (Output msg ())

done :: Cmd a r
done = liftF Done

program :: Cmd String r
program = do
    output "Hello"
    done

showProgram :: (Show a, Show r) => Cmd a r -> String
showProgram (Free (Output a x)) =
    "output " ++ show a ++ "\n" ++ showProgram x
showProgram (Free Done) =
    "done\n"
showProgram (Pure r) = "return " ++ show r ++ "\n"

--------------------------------------------------------------------------------
