{-# LANGUAGE DeriveFunctor #-}

module ToyDsl where

import           Data.Function
import           Data.Functor
import           Control.Applicative      ( Applicative(..) )
import           Control.Monad            ( Monad(..) )
import           Control.Monad.Trans.Free

import           Data.Int                 ( Int )
import           Prelude                  ( (*), (+) )
import           Text.Show

data Expr a = Const Int
             | Add a a
             | Mul a a
    deriving (Show, Functor)

const :: Int -> Free Expr a
const x = liftF (Const x)

add :: Free Expr a -> Free Expr a -> Free Expr a
add x y = do
    a <- x
    b <- y
    liftF (Add a b)

mul :: Free Expr a -> Free Expr a -> Free Expr a
mul x y = do
    a <- x
    b <- y
    liftF (Mul a b)

--eval :: Free Expr a -> Int
