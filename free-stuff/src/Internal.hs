module Internal ( internalFunc ) where

import           System.IO ( IO, putStrLn )

internalFunc :: IO ()
internalFunc = putStrLn "internalFunc"
