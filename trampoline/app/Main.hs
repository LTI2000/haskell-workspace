{-# LANGUAGE NoImplicitPrelude #-}

module Main where

import Prelude (IO, print, (++), Int, (+))

import Trampoline
import Category
import Arrow
import ResultContinuation
import KleisliArrow


zero :: Int
zero = 0

succ :: Computation Int Int
succ(i, ki) = callResult(ki)(i + 1)

succA :: KleisliArrow Int Int
succA = toArrow(succ)

bounce :: Void -> IO ()
bounce(Done a) =
  do
    print "Done"
    a
bounce(Continue f) =
  do
    print "Continue"
    bounce(f(()))

main :: IO ()
main = do
  bounce(runArrow(arr(id),                             zero, ki))
  bounce(runArrow(succA,                               zero, ki))
  bounce(runArrow(compose(succA,        arr(id)),      zero, ki))
  bounce(runArrow(compose(succA,        succA),        zero, ki))
  bounce(runArrow(compose(errorA("e1"), succA),        zero, ki))
  bounce(runArrow(compose(succA,        errorA("e2")), zero, ki))
  bounce(runArrow(compose(errorA("e1"), errorA("e2")), zero, ki))
    where
      ki :: ResultContinuation Int
      ki = handle(\e -> done(do
                                print("error: " ++ e)
                                print("")
                            ),
                  \i -> done(do
                                print(i)
                                print("")
                            ))
