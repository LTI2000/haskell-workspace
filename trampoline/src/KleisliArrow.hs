{-# LANGUAGE NoImplicitPrelude #-}

module KleisliArrow where

import Kleisli
import ResultContinuation
import Cont


type KleisliArrow a b = Kleisli Cont a b

errorA :: Exception -> KleisliArrow a b
errorA(e) = Kleisli(\_ -> throwError(e))

toArrow :: Computation a b -> KleisliArrow a b
toArrow(f) = Kleisli(\a -> Cont(\rb -> f(a, rb)))

runArrow :: (KleisliArrow a b, a, ResultContinuation b) -> Void
runArrow(Kleisli f, a, rb) = callCont(f(a))(rb)
