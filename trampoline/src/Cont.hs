{-# LANGUAGE NoImplicitPrelude #-}

module Cont where

import Trampoline
import Monad
import ResultContinuation


newtype Cont a = Cont { callCont :: ResultContinuation a -> Void }

instance Functor Cont

instance Applicative Cont where
  pure(a) = Cont(\ra -> continue(\_ -> callResult(ra)(a)))

instance Monad Cont where
  bind(ka, f) = Cont(\rb -> continue(\_ -> callCont(ka)(handle(\e -> callError(rb)(e),
                                                               \a -> callCont(f(a))(rb)))))

throwError :: Exception -> Cont a
throwError(e)= Cont(\ra -> continue(\_ -> callError(ra)(e)))

catchError :: (Cont a, Exception -> Cont a) -> Cont a
catchError(ka, h) = Cont(\ra -> continue(\_ -> callCont(ka)(handle(\e -> callCont(h(e))(ra),
                                                                   \a -> callResult(ra)(a)))))
