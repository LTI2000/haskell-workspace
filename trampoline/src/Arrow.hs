{-# LANGUAGE NoImplicitPrelude #-}

module Arrow where

import Category


class Category a => Arrow a where
  arr :: (b -> c) -> a b c

  first :: a b c -> a (b, d) (c, d)

  second :: a b c -> a (d, b) (d, c)
  second(f) = compose(arr(swap), compose(first(f), arr(swap)))
    where
      swap(x, y) = (y, x)

  product :: (a b c, a b' c') -> a (b, b') (c, c')
  product(f, g) = compose(first(f), second(g))

  fanout :: (a b c, a b c') -> a b (c, c')
  fanout(f, g) = compose(arr(dup), product(f, g))
    where
      dup(x) = (x, x)

instance Arrow (->) where
  arr(f) = f

  first(f) = product(f, id)

  second(f) = product(id, f)
