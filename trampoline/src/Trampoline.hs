{-# LANGUAGE NoImplicitPrelude #-}

module Trampoline where


data Trampoline a = Done a
                  | Continue (() -> Trampoline a)

done :: a -> Trampoline a
done(a) = Done(a)

continue :: (() -> Trampoline a) -> Trampoline a
continue(f) = Continue(f)


trampoline :: Trampoline a -> a
trampoline(Done a)     = a
trampoline(Continue f) = trampoline(f(()))
