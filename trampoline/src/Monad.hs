{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE DefaultSignatures #-}

module Monad where

import Category


class Functor f where
  fmap :: (a -> b, f a) -> f b
  default fmap :: Monad f => (a -> b, f a) -> f b
  fmap(f, ma) = bind(ma, \a -> pure(f(a)))

class Functor f => Applicative f where
  pure  :: a -> f a

  apply :: (f (a -> b), f a) -> f b
  default apply :: Monad f => (f (a -> b), f a) -> f b
  apply(mf, ma) = bind(mf, \f -> fmap(f, ma))

class Applicative m => Monad m where
  bind :: (m a, a -> m b) -> m b
  bind(ma, f) = join(fmap(f, ma))

  join :: m (m a) -> m a
  join(mma) = bind(mma, id)


liftM :: Functor m => (a -> b) -> (m a -> m b)
liftM(f) = \ma -> fmap(f, ma)

liftM2 :: Monad m => ((a, b) -> c) -> ((m a, m b) -> m c)
liftM2(f) = \(ma, mb) -> bind(ma, \a -> bind(mb, \b -> pure(f(a, b))))

kleisliCompose :: Monad m => (a -> m b, b -> m c) -> a -> m c
kleisliCompose(f, g) = \a -> bind(f(a), g)
