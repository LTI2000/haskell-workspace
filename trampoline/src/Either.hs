{-# LANGUAGE NoImplicitPrelude #-}

module Either where

import Monad


data Either e a = Left e
                | Right a

either :: (e -> b, a -> b, Either e a) -> b
either(left, _,     (Left e))  = left(e)
either(_   , right, (Right a)) = right(a)

instance Functor (Either e) where
  fmap(f, ea) = either(Left, \a -> Right(f(a)), ea)

instance Applicative (Either e) where
  pure(a) = Right(a)

instance Monad (Either e) where
  bind(ma, f) = either(Left, f, ma)

error :: e -> Either e a
error(e) = Left(e)
