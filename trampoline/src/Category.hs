{-# LANGUAGE NoImplicitPrelude #-}

module Category where


class Category cat where
  id :: cat a a

  compose :: (cat a b, cat b c) -> cat a c

instance Category (->) where
  id = \a -> a

  compose(f, g) = \a -> g(f(a))
