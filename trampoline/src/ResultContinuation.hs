{-# LANGUAGE NoImplicitPrelude #-}

module ResultContinuation where

import Prelude (IO, String)

import Trampoline
import Monad
import Either


type Void = Trampoline (IO ())

type Exception = String

newtype ResultContinuation a = ResultContinuation { callContinuation :: Either Exception a -> Void }

type Computation a b = (a, ResultContinuation b) -> Void

callError :: ResultContinuation a -> Exception -> Void
callError(ra)(e) = callContinuation(ra)(error(e))

callResult :: ResultContinuation a -> a -> Void
callResult(ra)(a) = callContinuation(ra)(pure(a))

handle :: (Exception -> Void, a -> Void) -> ResultContinuation a
handle(left, right) = ResultContinuation(\ea -> continue(\_ -> eitherVoid(ea, left, right)))

eitherVoid :: (Either Exception a, Exception -> Void, a -> Void) -> Void
eitherVoid(ea, left, right) = either(left, right, ea)
