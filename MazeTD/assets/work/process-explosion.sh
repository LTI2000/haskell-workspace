#!/bin/sh

CURRENT_DIR=$(pwd -P)
WORK_DIR=explosion-work

rm -rf ${WORK_DIR}
mkdir -p ${WORK_DIR}
pushd ${WORK_DIR}
cp ${CURRENT_DIR}/explosion.hasgraphics.png .
#identify explosion.hasgraphics.png
convert explosion.hasgraphics.png -crop 100x100 +repage tile_%03d.png
ls tile*.png | sort | head -n 72 > tiles.txt
montage -background transparent -mode concatenate -tile 8x @tiles.txt -depth 8 explosion.png
#identify explosion.png
mv explosion.png ${CURRENT_DIR}/..
popd #${WORK_DIR}
rm -rf ${WORK_DIR}
