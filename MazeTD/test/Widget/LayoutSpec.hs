module Widget.LayoutSpec ( spec ) where

import           Test.Hspec

import           Common.SdlTypes

import           Widget.Layout

spec :: Spec
spec = do
    describe "borderLayout" $ do
        it "works for 0 gaps" $ do
            let rect = Rectangle (P (V2 0 0)) (V2 32 32)
            doLayout (borderLayout (const 0) (const 0) (const 0) (const 0)) rect `shouldBe`
                [ rect ]
        it "works for different gaps" $ do
            let rect = Rectangle (P (V2 0 0)) (V2 32 32)
                rect1 = Rectangle (P (V2 1 4)) (V2 29 20)
            doLayout (borderLayout (const 1) (const 2) (const 4) (const 8)) rect `shouldBe`
                [ rect1 ]
    describe "gridLayout" $ do
        it "works for a single row and column" $ do
            let rect = Rectangle (P (V2 0 0)) (V2 32 32)
            doLayout (gridLayout 1 1 (const 0) (const 0)) rect `shouldBe`
                [ rect ]
        it "works for a single row and column (with gaps)" $ do
            let rect = Rectangle (P (V2 0 0)) (V2 32 32)
            doLayout (gridLayout 1 1 (const 16) (const 16)) rect `shouldBe`
                [ rect ]
        it "works for a single row and two columns" $ do
            let rect = Rectangle (P (V2 0 0)) (V2 32 32)
                rect1 = Rectangle (P (V2 0 0)) (V2 16 32)
                rect2 = Rectangle (P (V2 16 0)) (V2 16 32)
            doLayout (gridLayout 2 1 (const 0) (const 0)) rect `shouldBe`
                [ rect1, rect2 ]
        it "works for two rows and a single column" $ do
            let rect = Rectangle (P (V2 0 0)) (V2 32 32)
                rect1 = Rectangle (P (V2 0 0)) (V2 32 16)
                rect2 = Rectangle (P (V2 0 16)) (V2 32 16)
            doLayout (gridLayout 1 2 (const 0) (const 0)) rect `shouldBe`
                [ rect1, rect2 ]
        it "works for two rows and two columns" $ do
            let rect = Rectangle (P (V2 0 0)) (V2 32 32)
                rect1 = Rectangle (P (V2 0 0)) (V2 16 16)
                rect2 = Rectangle (P (V2 16 0)) (V2 16 16)
                rect3 = Rectangle (P (V2 0 16)) (V2 16 16)
                rect4 = Rectangle (P (V2 16 16)) (V2 16 16)
            doLayout (gridLayout 2 2 (const 0) (const 0)) rect `shouldBe`
                [ rect1, rect2, rect3, rect4 ]
        it "works for two rows and three columns" $ do
            let rect = Rectangle (P (V2 0 0)) (V2 48 48)
                rect1 = Rectangle (P (V2 0 0)) (V2 16 24)
                rect2 = Rectangle (P (V2 16 0)) (V2 16 24)
                rect3 = Rectangle (P (V2 32 0)) (V2 16 24)
                rect4 = Rectangle (P (V2 0 24)) (V2 16 24)
                rect5 = Rectangle (P (V2 16 24)) (V2 16 24)
                rect6 = Rectangle (P (V2 32 24)) (V2 16 24)
            doLayout (gridLayout 3 2 (const 0) (const 0)) rect `shouldBe`
                [ rect1, rect2, rect3, rect4, rect5, rect6 ]
        it "works for two rows and three columns (with gaps)" $ do
            let rect = Rectangle (P (V2 0 0))
                                 (V2 (16 + 4 + 16 + 4 + 16) (24 + 4 + 24))
                rect1 = Rectangle (P (V2 0 0)) (V2 16 24)
                rect2 = Rectangle (P (V2 20 0)) (V2 16 24)
                rect3 = Rectangle (P (V2 40 0)) (V2 16 24)
                rect4 = Rectangle (P (V2 0 28)) (V2 16 24)
                rect5 = Rectangle (P (V2 20 28)) (V2 16 24)
                rect6 = Rectangle (P (V2 40 28)) (V2 16 24)
            doLayout (gridLayout 3 2 (const 4) (const 4)) rect `shouldBe`
                [ rect1, rect2, rect3, rect4, rect5, rect6 ]
