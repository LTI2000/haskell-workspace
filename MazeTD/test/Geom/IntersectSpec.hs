module Geom.IntersectSpec ( spec ) where

import qualified Debug.Trace

import           Test.Hspec
--  import           Test.Hspec.QuickCheck
import           Test.QuickCheck
import           Common.SdlTypes

import           Geom.Intersect

spec :: Spec
spec = do
    describe "predictIntersection" $ do
        it "aims correctly for target going up" $ do
            predictIntersection (V2 1 1) (V2 0 (-1)) (V2 0 0) 1 `shouldBe` Just (V2 1 0)
        it "aims correctly for target going left" $ do
            predictIntersection (V2 1 1) (V2 (-1) 0) (V2 0 0) 1 `shouldBe` Just (V2 0 1)
        it "does not aim for target going left too fast" $ do
            predictIntersection (V2 1 1) (V2 (-2) 0) (V2 0 0) 1 `shouldBe` Nothing
        it "aims correctly for target going diagonally" $ do
            predictIntersection (V2 1 1) (V2 (-1) (-1)) (V2 0 0) 1 `shouldBe` Just (V2 (sqrt 2 - 1) (sqrt 2 - 1))
        it "aims correctly for target when gunner not at origin" $ do
            predictIntersection (V2 1 1) (V2 (-1) 0) (V2 1 0) (sqrt 2) `shouldSatisfy`
                (\(Just v) -> v `eqV2Double` (V2 0 1))

    describe "intersectCircleCircle" $ do
        it "works for two non-intersecting circles (too far apart)" $
            property $ do
                x <- arbitrarySizedFractional
                y <- arbitrarySizedFractional
                theta <- resize 180 arbitrarySizedIntegral :: Gen Int
                let theta' = (fromIntegral theta) / 180.0 * pi
                let c1 = V2 x y
                let r1 = 1.0
                let c2 = c1 ^+^ V2 3.0 0.0
                let r2 = 1.5

                let c1' = rotateV2 theta' c1
                let c2' = rotateV2 theta' c2
                return $ intersectCircleCircle c1' r1 c2' r2 `shouldBe` []
        it "works for two intersecting circles (enclosed)" $
            property $ do
                x <- arbitrarySizedFractional
                y <- arbitrarySizedFractional
                theta <- resize 180 arbitrarySizedIntegral :: Gen Int
                let theta' = (fromIntegral theta) / 180.0 * pi
                let c1 = V2 x y
                let r1 = 3.0
                let c2 = c1 ^+^ V2 0.5 0.0
                let r2 = 1.0

                let c1' = rotateV2 theta' c1
                let c2' = rotateV2 theta' c2
                return $ intersectCircleCircle c1' r1 c2' r2 `shouldBe` []
        it "works for two touching circles" $ do
            let c1 = V2 0.0 0.0
            let r1 = 1.0
            let c2 = c1 ^+^ V2 2.0 0.0
            let r2 = 1.0
            Debug.Trace.trace (" c1: " ++ show c1 ++ " c2: " ++ show c2)
                              (intersectCircleCircle c1 r1 c2 r2 `shouldBe` [ c1 ^+^ V2 1.0 0.0 ])

rotateV2 :: (Floating a) => a -> V2 a -> V2 a
rotateV2 theta (V2 x y) =
    V2 (x * cos theta - y * sin theta) (x * sin theta + y * cos theta)

rotateV2' :: (Floating a) => a -> V2 a -> V2 a
rotateV2' theta v = let m = V2 (V2 (cos theta) (-(sin theta))) (V2 (sin theta) (cos theta))
                    in
                        m !* v

eqV2Double :: (Ord a, Fractional a) => V2 a -> V2 a -> Bool
eqV2Double (V2 x0 y0) (V2 x1 y1) =
    eqDouble x0 x1 && eqDouble y0 y1

eqDouble :: (Ord a, Fractional a) => a -> a -> Bool
eqDouble x y = abs (x - y) < 0.000001
