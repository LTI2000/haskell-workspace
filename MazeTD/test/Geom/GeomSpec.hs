module Geom.GeomSpec ( spec ) where

import           Test.Hspec
import           Test.Hspec.QuickCheck
-- import           Test.QuickCheck
import           Common.SdlTypes

import           Geom.Geom

spec :: Spec
spec = do
    describe "bearing" $ do
        prop "returns 0 for two equal points" $ do
            \x y -> let p = P (V2 (toNativeInt x) (toNativeInt y))
                    in
                        bearing p p `shouldBe` 180

        it "returns 0 for target at (0,-1)" $ do
            bearing (P (V2 0 0)) (P (V2 0 (-1))) `shouldBe` 0

        it "returns 45 for target at (1,-1)" $ do
            bearing (P (V2 0 0)) (P (V2 1 (-1))) `shouldBe` 45

        it "returns 90 for target at (1,0)" $ do
            bearing (P (V2 0 0)) (P (V2 1 0)) `shouldBe` 90

        it "returns 135 for target at (1,1)" $ do
            bearing (P (V2 0 0)) (P (V2 1 1)) `shouldBe` 135

        it "returns 180 for target at (0,1)" $ do
            bearing (P (V2 0 0)) (P (V2 0 1)) `shouldBe` 180

        it "returns 225 for target at (-1,1)" $ do
            bearing (P (V2 0 0)) (P (V2 (-1) 1)) `shouldBe` 225

        it "returns 270 for target at (-1,0)" $ do
            bearing (P (V2 0 0)) (P (V2 (-1) 0)) `shouldBe` 270

        it "returns 315 for target at (-1,-1)" $ do
            bearing (P (V2 0 0)) (P (V2 (-1) (-1))) `shouldBe` 315

{-
+---------+---------+---------+
| (-1,-1) | ( 0,-1) | ( 1,-1) |
|   315   |     0   |    45   |
+---------+---------+---------+
| (-1, 0) |         | ( 1, 0) |
|   270   |         |    90   |
+---------+---------+---------+
| (-1, 1) | ( 0, 1) | ( 1, 1) |
|   225   |   180   |   135   |
+---------+---------+---------+

-}
toNativeInt :: Int -> NativeInt
toNativeInt = fromIntegral
