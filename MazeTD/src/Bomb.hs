module Bomb ( newBomb ) where

import           Data.Vector         ( Vector, singleton )

import           Common.SdlTypes

import           Gfx.Colors
import           Gfx.Draw
import           Gfx.Input
import           Gfx.Renderable

import           Engine.Path
import           Engine.CollisionMap
import           Engine.World
import           Engine.Object

import           Assets
import           Level
import           SplashDamage

data Bomb = Bomb { objId :: NativeId
                 , level :: Level
                 , path  :: BombPath
                 }

instance Renderable Bomb where
    render = renderBomb

instance ObjectClass Bomb where
    updateCollisionMap = const
    updateObject = updateBomb

instance ZOrdered Bomb where
    zOrder = const High

newBomb :: NativeId
        -> Level
        -> NativePoint
        -> NativePoint
        -> NativeFloat
        -> Object
newBomb objId level from to speed =
    Object Bomb { objId, level, path = newBombPath from to speed }

renderBomb :: RenderableArgs -> Bomb -> IO ()
renderBomb RenderableArgs{input = input@Input{inputGridSize},renderer} Bomb{level,path} = do
    let Assets{assetsSprites} = getUserData input
    drawSprite assetsSprites
               (spriteIndex level)
               spritesBombY
               (bombPathPos path |+|
                    ((+ bombPathAltitude path) <$> inputGridSize))
               0.0
               white
               renderer

updateBomb :: (WorldClass w)
           => Input
           -> w
           -> CollisionMap
           -> Bomb
           -> (w, Vector Object)
updateBomb _ world _ bomb@Bomb{level,path} =
    case updateBombPath path of
        Right path' -> let bomb' = bomb { path = path' }
                       in
                           (world, singleton (Object bomb'))
        Left targetPos -> die targetPos
  where
    die pos = let (objId, world') = nextObjId world
              in
                  ( world'
                  , singleton (newSplashDamage objId
                                               pos
                                               (bombExplosionRadius level)
                                               (bombDamage level))
                  )
