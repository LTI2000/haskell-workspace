module Engine.GameLoop ( startGame ) where

import           Control.Exception ( bracket )
import           Data.IORef        ( IORef, atomicModifyIORef', newIORef )
import           Data.Text         ( Text, pack )
import           Control.Monad.RWS ( evalRWS )

import           Common.SdlTypes

import           Gfx.GfxContext
import           Gfx.Renderable
import           Gfx.Draw
import           Gfx.Input
import           Gfx.Background

import           Engine.Scene
import           Engine.ExitScene

startGame :: String
          -> WindowConfig
          -> RendererConfig
          -> NativeDimension
          -> NativeDimension
          -> (NativeContext -> IO Dynamic)
          -> SceneMonad SceneState
          -> IO ()
startGame caption windowConfig rendererConfig displaySize gridSize assets sceneM = do
    genRef <- newStdGen >>= newIORef
    withRenderer windowConfig rendererConfig displaySize (pack caption) (runGame genRef)
  where
    runGame :: IORef StdGen -> NativeContext -> IO ()
    runGame genRef gfxCtx@(_, renderer) = do
        Just size <- get $ rendererLogicalSize renderer
        background <- newBackground gfxCtx
        userData <- assets gfxCtx
        ticks0 <- fromIntegral <$> ticks
        let input = initialInput (Rectangle (P (V2 0 0)) size) gridSize userData
            args = RenderableArgs { input, background, getRandom, renderer }
        let (scene, ()) = evalRWS sceneM args []
        gameLoop [ scene, newExitScene ] args ticks0 0
        clearBackground background
      where
        getRandom ival = atomicModifyIORef' genRef (swap . randomR ival)

gameLoop :: SceneStack -> RenderableArgs -> Word64 -> NativeFrameNo -> IO ()
gameLoop [] _ _ _ = return ()
gameLoop sceneStack args@RenderableArgs{input,renderer} ticks0 frameNo = do
    millis <- fromIntegral <$> ticks
    let fps = (1000 * frameNo) `div` (millis - ticks0)
    input' <- updateEvents input frameNo fps
    unless (inputWindowClosed input') $ do
        let args' = args { input = input' }
            sceneStack' = runSceneMonad updateStack args' sceneStack
            frameNo' = frameNo + 1
        clearAll renderer (V4 0 0 0 255)
        renderStack sceneStack' args'
        present renderer
        gameLoop sceneStack' args' ticks0 frameNo'

withRenderer :: WindowConfig -> RendererConfig -> NativeDimension -> Text -> (NativeContext -> IO ()) -> IO ()
withRenderer w r s c = bracket acquire release
  where
    acquire = do
        gfxCtx <- newGfxContext w r s c
        cursorVisible $= False
        return gfxCtx
    release gfxCtx = do
        cursorVisible $= True
        destroyGfxContext gfxCtx
