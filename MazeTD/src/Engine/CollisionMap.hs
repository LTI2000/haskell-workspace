{-# LANGUAGE LambdaCase #-}
module Engine.CollisionMap
    ( CollisionMap
    , Collision(..)
    , newCollisionMap
    , addSegment
    , addCircle
    , addRadarCircle
    , getCollisions
    , getPosition
    , Owner(..)
    ) where

import           Data.List       ( head )
import           Data.Array      ( Array, accum, array, bounds )
import qualified Data.Array      as A ( (!) )
import           Data.Map.Strict ( Map, alter, empty, insert, lookup )
import qualified Data.Map.Strict as M ( (!) )

import           Common.SdlTypes

import           Geom.Geom
import           Geom.Grid
import           Geom.Shape
import           Geom.Intersect

data Owner = Player | Enemy
    deriving (Eq)

data CEntry = CEntry CRole NativePoint Shape

data CRole = Regular { velocity :: V2 NativeFloat
                     , dmg      :: NativeInt
                     }
           | Radar { projectile_velocity :: Maybe NativeFloat }

data CollisionMap = CollisionMap { gridSize :: NativeDimension
                                 , arr      :: Array NativePoint [( NativeId
                                                                  , Owner
                                                                  )]
                                 , hash     :: Map NativeId CEntry
                                 , hash2    :: Map NativeId [( NativeId
                                                             , NativePoint
                                                             )]
                                 }

data Collision = Collision { cObjId :: NativeId
                           , cPos   :: NativePoint
                           , cDmg   :: NativeInt
                           }

newCollisionMap :: NativeRectangle -> NativeDimension -> CollisionMap
newCollisionMap viewBounds gridSize =
    CollisionMap { gridSize, arr, hash, hash2 }
  where
    (tl, _, br, _) = corners viewBounds
    ixs = (viewToModel gridSize tl, viewToModel gridSize br)
    arr = array ixs
                [ (ix, [])
                | ix <- range ixs ]
    hash = empty
    hash2 = empty

addSegment :: CollisionMap
           -> NativeId
           -> Owner
           -> NativePoint
           -> NativePoint
           -> V2 NativeFloat
           -> NativeInt
           -> CollisionMap
addSegment cmap objId owner start end velocity dmg =
    addShape cmap objId owner (CEntry Regular { velocity, dmg } start shape)
  where
    shape = Segment start end

addCircle :: CollisionMap
          -> NativeId
          -> Owner
          -> NativePoint
          -> NativeInt
          -> V2 NativeFloat
          -> NativeInt
          -> CollisionMap
addCircle cmap objId owner center radius velocity dmg =
    addShape cmap objId owner (CEntry Regular { velocity, dmg } center shape)
  where
    shape = Circle center radius

addRadarCircle :: CollisionMap
               -> NativeId
               -> Owner
               -> NativePoint
               -> NativeInt
               -> Maybe NativeFloat
               -> CollisionMap
addRadarCircle cmap objId owner center radius projectile_velocity =
    addShape cmap
             objId
             owner
             (CEntry Radar { projectile_velocity } center shape)
  where
    shape = Circle center radius

addShape :: CollisionMap -> NativeId -> Owner -> CEntry -> CollisionMap
addShape cmap@CollisionMap{gridSize,arr,hash,hash2} objId owner centry =
    cmap { arr = arr', hash = hash', hash2 = hash2' }
  where
    ixs :: (NativePoint, NativePoint)
    ixs = clampIxBounds (viewToModel gridSize vix0, viewToModel gridSize vix1)
                        (bounds arr)
      where
        (vix0, vix1) = shapeIxBounds centry
          where
            shapeIxBounds :: CEntry -> (NativePoint, NativePoint)
            shapeIxBounds (CEntry _ _ shape) =
                let (tl, _, br, _) = corners (boundingBox shape)
                in
                    (tl, br)

    objIds' :: [NativeId]
    objIds' = nub [ objId'
                  | ix <- range ixs
                  , (objId', owner') <- arr A.! ix
                  , owner /= owner' ]

    centries0' :: [(NativeId, CEntry)]
    centries0' = [ (objId', hash M.! objId')
                 | objId' <- objIds' ]

    points0' :: [(NativeId, [NativePoint])]
    points0' = [ (objId', intersectCEntries centry centry')
               | (objId', centry') <- centries0' ]

    points1' :: [(NativeId, NativePoint)]
    points1' = [ (objId', head points')
               | (objId', points') <- points0'
               , not (null points') ]

    hash2' = alterAll objId points1' hash2
    arr' = accum (flip (:))
                 arr
                 [ (ix, (objId, owner))
                 | ix <- range ixs ]
    hash' = insert objId centry hash

intersectCEntries :: CEntry -> CEntry -> [NativePoint]
intersectCEntries (CEntry Regular{} _ shape) (CEntry Regular{} _ shape') =
    intersectShapes shape shape'
intersectCEntries (CEntry Regular{velocity} _ shape) (CEntry Radar{projectile_velocity} _ shape') =
    case projectile_velocity of
        Nothing -> intersectShapes shape shape'
        Just p -> case intersectShapes shape shape' of
            [] -> []
            _ -> let P s = fromIntegral <$> shapeCenter shape
                     v = velocity
                     P g = fromIntegral <$> shapeCenter shape'
                 in
                     case predictIntersection s v g p of
                         Nothing -> []
                         Just pnt -> [ P (round <$> pnt) ]
intersectCEntries centry@(CEntry Radar{} _ _) centry'@(CEntry Regular{} _ _) =
    intersectCEntries centry' centry
intersectCEntries _ _ = []

alterAll :: NativeId
         -> [(NativeId, NativePoint)]
         -> Map NativeId [(NativeId, NativePoint)]
         -> Map NativeId [(NativeId, NativePoint)]
alterAll _ [] hash = hash
alterAll objId ((objId', point') : objIds) hash =
    alter2 objId objId' point' (alterAll objId objIds hash)

alter2 :: NativeId
       -> NativeId
       -> NativePoint
       -> Map NativeId [(NativeId, NativePoint)]
       -> Map NativeId [(NativeId, NativePoint)]
alter2 objId objId' point' hash =
    alter' objId' objId point' (alter' objId objId' point' hash)

alter' :: NativeId
       -> NativeId
       -> NativePoint
       -> Map NativeId [(NativeId, NativePoint)]
       -> Map NativeId [(NativeId, NativePoint)]
alter' objId objId' point' =
    alter (\case
               Nothing -> Just [ (objId', point') ]
               (Just objIds') -> Just ((objId', point') : objIds'))
          objId

getCollisions :: CollisionMap -> NativeId -> [Collision]
getCollisions CollisionMap{hash,hash2} objId =
    case lookup objId hash2 of
        Nothing -> []
        Just points -> (\(objId', point') -> Collision objId'
                                                       point'
                                                       (getDmg (hash M.! objId'))) <$> points
  where
    getDmg (CEntry Regular{dmg} _ _) =
        dmg
    getDmg _ = 0

getPosition :: CollisionMap -> NativeId -> Maybe NativePoint
getPosition CollisionMap{hash} objId =
    getPos <$> lookup objId hash
  where
    getPos (CEntry _ pos _) =
        pos

clampIxBounds :: (NativePoint, NativePoint) -- ^ index bounds to clamp
              -> (NativePoint, NativePoint) -- ^ allowed index bounds
              -> (NativePoint, NativePoint) -- ^ clamped index bounds
clampIxBounds (P (V2 x0 y0), P (V2 x1 y1)) (P (V2 mx0 my0), P (V2 mx1 my1)) =
    (P (V2 (max x0 mx0) (max y0 my0)), P (V2 (min x1 mx1) (min y1 my1)))
