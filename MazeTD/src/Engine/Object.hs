{-# LANGUAGE GADTs      #-}
{-# LANGUAGE Rank2Types #-}

module Engine.Object
    ( ObjectClass(..)
    , Object(..)
    , ZOrdered(..)
    , ZOrder(..)
    , updateObjects
    ) where

import           Data.Vector         ( Vector, concat, foldl, head, tail, unstablePartition )

import           Common.SdlTypes     hiding ( filter, foldl )

import           Gfx.Input
import           Gfx.Renderable

import           Engine.World
import           Engine.CollisionMap

class (Renderable o, ZOrdered o) => ObjectClass o where
    updateCollisionMap :: CollisionMap -> o -> CollisionMap
    updateObject :: (WorldClass w) => Input -> w -> CollisionMap -> o -> (w, Vector Object)

data Object where
        Object :: (ObjectClass o) => o -> Object

(.$) :: Object -> (forall o. (ObjectClass o) => o -> r) -> r
(Object x) .$ f = f x

class ZOrdered o where
    zOrder :: o -> ZOrder

data ZOrder = Lowest | Low | Medium | High | Highest
    deriving (Eq, Enum, Bounded)

americanFlagSort :: [a -> Bool] -- ^ list of predicates
                 -> Vector a      -- ^ input vector
                 -> Vector a      -- ^ output vector
americanFlagSort = (concat .) . multiPartition . (unstablePartition <$>)
  where
    multiPartition :: [a -> (a, a)] -> a -> [a]
    multiPartition [] x = [ x ]
    multiPartition (partition : partitions) x =
        let (x1, x2) = partition x
        in
            x1 : multiPartition partitions x2

instance Renderable Object where
    render args = (.$ render args)

instance ObjectClass Object where
    updateCollisionMap cmap =
        (.$ updateCollisionMap cmap)
    updateObject input world cmap =
        (.$ updateObject input world cmap)

instance ZOrdered Object where
    zOrder = (.$ zOrder)

updateWorld' :: (w -> CollisionMap -> Object -> (w, Vector Object))
             -> w
             -> CollisionMap
             -> Vector Object
             -> (w, [Vector Object])
updateWorld' f world cmap objs
    | null objs = (world, [])
    | otherwise = let (world', objs') = f world cmap (head objs)
                      (world'', objs'') = updateWorld' f world' cmap (tail objs)
                  in
                      (world'', objs' : objs'')

updateWorld :: Input -> (w -> CollisionMap -> Object -> (w, Vector Object)) -> w -> Vector Object -> (w, Vector Object)
updateWorld Input{inputBounds,inputGridSize} f world objs =
    let cmap = foldl updateCollisionMap (newCollisionMap inputBounds inputGridSize) objs
        (world', lobs) = updateWorld' f world cmap objs
        objs' = concat lobs
    in
        (world', zSort objs')
  where
    zSort :: Vector Object -> Vector Object
    zSort = americanFlagSort (mkPredicate <$> [minBound .. pred maxBound])
      where
        mkPredicate :: ZOrder -> Object -> Bool
        mkPredicate = (. zOrder) . (==)

updateObjects :: (WorldClass w) => Input -> w -> Vector Object -> (w, Vector Object)
updateObjects = updateWorld <*> updateObject
