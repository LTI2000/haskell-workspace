{-# LANGUAGE DuplicateRecordFields #-}

module Engine.Path
    ( Path(pathPos, pathHeading)
    , newPath
    , updatePath
    , nextPathPos
    , pathVelocity
    , MultiPath
    , newMultiPath
    , updateMultiPath
    , multiPathPos
    , multiPathHeading
    , multiPathVelocity
    , BombPath(bombPathPos, bombPathAltitude)
    , newBombPath
    , updateBombPath
    , HomingPath
    , newHomingPath
    , updateHomingPath
    , homingPathPos
    , homingPathVelocity
    , homingPathHeading
    ) where

import           Common.SdlTypes

import           Geom.Geom

data Path = Path { dist        :: NativeFloat
                 , pathPos     :: NativePoint
                 , pos0        :: V2 NativeFloat
                 , speed       :: NativeFloat
                 , pathHeading :: NativeFloat
                 , d           :: V2 NativeFloat
                 , n           :: NativeFloat
                 }

newPath :: NativePoint -> NativePoint -> NativeFloat -> Path
newPath from@(P pos0) to@(P pos1) speed =
    let pathHeading = bearing from to
        d = fromIntegral <$> pos1 - pos0
        n = norm d
    in
        Path { dist = 0
             , pathPos = from
             , pos0 = fromIntegral <$> pos0
             , speed
             , pathHeading
             , d
             , n
             }

updatePath :: NativeRectangle -> Path -> Maybe Path
updatePath = updatePath' True

updatePath' :: Bool -> NativeRectangle -> Path -> Maybe Path
updatePath' extend bounds path@Path{dist,pos0,speed,d,n} =
    if n > 0.0
    then let dist' = dist + speed
         in
             if extend || dist' < n
             then let pos' = round <$> P (pos0 ^+^ d ^* dist' ^/ n)
                  in
                      if bounds `contains` pos'
                      then Just path { dist = dist', pathPos = pos' }
                      else Nothing
             else Nothing
    else Nothing

nextPathPos :: Path -> NativePoint
nextPathPos Path{dist,pos0,speed,d,n} =
    round <$> P (pos0 ^+^ d ^* (dist + speed) ^/ n)

pathVelocity :: Path -> V2 NativeFloat
pathVelocity Path{speed,d,n} =
    d ^* speed ^/ n

newtype MultiPath = MultiPath { paths :: (Path, [Path]) }

newMultiPath :: [NativePoint] -> NativeFloat -> MultiPath
newMultiPath [] _ = error "empy multi path"
newMultiPath [ _ ] _ = error "single point multi path"
newMultiPath [ point0, point1 ] speed =
    MultiPath { paths = (newPath point0 point1 speed, []) }
newMultiPath (point0 : point1 : points) speed =
    let MultiPath{paths = (path, paths)} =
            newMultiPath (point1 : points) speed
    in
        MultiPath { paths = (newPath point0 point1 speed, path : paths) }

updateMultiPath :: NativeRectangle -> MultiPath -> Maybe MultiPath
updateMultiPath bounds multiPath@MultiPath{paths = (path, paths)} =
    case updatePath' False bounds path of
        Just path' -> Just multiPath { paths = (path', paths) }
        Nothing -> case paths of
            p : ps -> updateMultiPath bounds multiPath { paths = (p, ps) }
            [] -> Nothing

multiPathPos :: MultiPath -> NativePoint
multiPathPos MultiPath{paths = (path, _)} =
    pathPos path

multiPathHeading :: MultiPath -> NativeFloat
multiPathHeading MultiPath{paths = (path, _)} =
    pathHeading path

multiPathVelocity :: MultiPath -> V2 NativeFloat
multiPathVelocity MultiPath{paths = (path, _)} =
    pathVelocity path

data BombPath = BombPath { dist             :: NativeFloat
                         , bombPathPos      :: NativePoint
                         , bombPathAltitude :: NativeInt
                         , targetPos        :: NativePoint
                         , pos0             :: V2 NativeFloat
                         , speed            :: NativeFloat
                         , d                :: V2 NativeFloat
                         , n                :: NativeFloat
                         }

newBombPath :: NativePoint -> NativePoint -> NativeFloat -> BombPath
newBombPath from@(P pos0) to@(P pos1) speed =
    let d = fromIntegral <$> pos1 - pos0
        n = norm d
    in
        BombPath { dist = 0.0
                 , bombPathPos = from
                 , bombPathAltitude = 0
                 , targetPos = to
                 , pos0 = fromIntegral <$> pos0
                 , speed
                 , d
                 , n
                 }

updateBombPath :: BombPath -> Either NativePoint BombPath
updateBombPath path@BombPath{dist,targetPos,pos0,speed,d,n} =
    if n > 0
    then let dist' = dist + speed
         in
             if dist' < n
             then let pos' = round <$> P (pos0 ^+^ d ^* dist' ^/ n)
                      altitude = round (dist' * (2 - 2 * dist' / n))
                  in
                      Right path { dist = dist'
                                 , bombPathPos = pos'
                                 , bombPathAltitude = altitude
                                 }
             else Left targetPos
    else Left targetPos

data HomingPath = HomingPath { pos      :: Point V2 NativeFloat
                             , velocity :: V2 NativeFloat
                             , speed    :: NativeFloat
                             }

newHomingPath :: Point V2 NativeFloat
              -> V2 NativeFloat
              -> NativeFloat
              -> HomingPath
newHomingPath pos velocity speed =
    HomingPath { pos, velocity, speed }

updateHomingPath :: NativeRectangle
                  -> Maybe (Point V2 NativeFloat)
                  -> HomingPath
                  -> Maybe HomingPath
updateHomingPath bounds mtpos homingPath@HomingPath{pos,velocity,speed} =
    let velocity' = case mtpos of
            Just tpos -> let d = tpos .-. pos
                             a = normalize d ^* (speed / 10.0)
                         in
                             velocity ^+^ a
            Nothing -> velocity
        pos' = pos .+^ velocity'
    in
        if bounds `contains` (round <$> pos')
        then Just homingPath { pos = pos', velocity = velocity' ^* 0.995 }
        else Nothing

homingPathPos :: HomingPath -> Point V2 NativeFloat
homingPathPos = pos

homingPathVelocity :: HomingPath -> V2 NativeFloat
homingPathVelocity = velocity

homingPathHeading :: HomingPath -> NativeFloat
homingPathHeading = bearing' . velocity
