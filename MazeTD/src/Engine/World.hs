module Engine.World ( WorldClass(..) ) where

import           Common.SdlTypes

class WorldClass w where
    nextObjId :: w -> (NativeId, w)
    modifyUserData :: (Dynamic -> Dynamic) -> w -> (Dynamic, w)
    maybeModifyUserData :: (Dynamic -> Maybe Dynamic) -> w -> Maybe (Dynamic, w)
