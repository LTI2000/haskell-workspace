{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs             #-}
{-# LANGUAGE Rank2Types        #-}

module Engine.Scene
    ( SceneMonad
    , runSceneMonad
    , SceneState
    , SceneStack
    , askInput
    , askUserData
    , SceneCommand(..)
    , pushScene
    , replaceScene
    , popScene
    , SceneClass(..)
    , Scene(..)
    , updateStack
    , renderStack
    ) where

import           Control.Monad.RWS ( RWS, asks, get, local, put, runRWS )

import           Common.SdlTypes   hiding ( get )

import           Gfx.Input
import           Gfx.Renderable
import           Gfx.Painter

import           Widget.Widget

type SceneReader = RenderableArgs

type SceneWriter = ()

type SceneState = (Widget, Scene)

type SceneStack = [SceneState]

type SceneMonad = RWS SceneReader SceneWriter SceneStack

runSceneMonad :: SceneMonad a -> SceneReader -> SceneStack -> SceneStack
runSceneMonad m args sceneStack =
    let (_, sceneStack', _) = runRWS m args sceneStack
    in
        sceneStack'

askInput :: SceneMonad Input
askInput = asks input

askUserData :: (Typeable a) => SceneMonad a
askUserData = askInput >>= (getUserData >>> return)

newtype SceneCommand = SceneCommand ((SceneState, SceneStack) -> SceneStack)

instance Semigroup SceneCommand where
    (SceneCommand f) <> (SceneCommand g) =
        SceneCommand h
        where h (top, stack) = case f (top, stack) of
                  (top' : stack') -> g (top', stack')
                  _ -> error "empty scene stack"

instance Monoid SceneCommand where
    mempty = SceneCommand f
      where
        f (top, stack) = top : stack

pushScene :: SceneState -> SceneCommand
pushScene new = SceneCommand f
  where
    f (top, stack) = new : top : stack

replaceScene :: (SceneClass a) => a -> SceneCommand
replaceScene new = SceneCommand f
  where
    f ((widget, _), stack) =
        (widget, Scene new) : stack

popScene :: SceneCommand
popScene = SceneCommand f
  where
    f (_, stack) = stack

class (Renderable a) => SceneClass a where
    updateScene :: a -> SceneMonad SceneCommand
    mousePainter :: a -> Painter

data Scene where
        Scene :: (SceneClass a) => a -> Scene

(.$) :: Scene -> (forall a. SceneClass a => a -> b) -> b
(Scene x) .$ f = f x

instance Renderable Scene where
    render args = (.$ render args)

instance SceneClass Scene where
    updateScene = (.$ updateScene)
    mousePainter = (.$ mousePainter)

updateStack :: SceneMonad ()
updateStack = do
    input <- askInput
    sceneState <- get
    case sceneState of
        [] -> do
            return ()
        ((widget, scene) : stack) -> do
            let (widget', ids) = updateWidget input widget
            local (updateInput (updateInput' ids input)) $ do
                SceneCommand cmd <- updateScene scene
                let sceneState' = cmd ((widget', scene), stack)
                put sceneState'
  where
    updateInput' [] input = input
    updateInput' ids input =
        input { inputMousePos = Nothing, inputLeftButtonReleased = False, inputActionIds = ids }

renderStack :: SceneStack -> RenderableArgs -> IO ()
renderStack [] _ = return ()
renderStack ((widget, scene) : _) args = do
    render args scene
    render args widget
    paint (mousePainter scene) args (inputBounds (input args))
