module Engine.Heading
    ( Heading
    , newHeading
    , updateHeading
    , translatePoint
    , getHeadingAngle
    ) where

import           Common.SdlTypes

import           Geom.Geom

data Heading = Heading { maxForce  :: NativeFloat
                       , tolerance :: NativeFloat
                       , speed     :: NativeFloat
                       , heading   :: NativeFloat
                       }

newHeading :: NativeFloat -> NativeFloat -> NativeFloat -> Heading
newHeading maxForce tolerance heading =
    Heading { maxForce = maxForce * 0.25, tolerance = tolerance * 1.0, speed = 0, heading }

updateHeading :: Heading -> Maybe NativeFloat -> (Bool, Heading)
updateHeading h@Heading{speed,heading} Nothing =
    let amount = 0
        speed' = speed + amount
        heading' = normalizeAngle (heading + speed')
        aimed = False
    in
        (aimed, h { speed = speed' * friction, heading = heading' })
updateHeading h@Heading{maxForce,tolerance,speed,heading} (Just target) =
    let (dir, dev) = deviation (heading + speed) target
        amount = min dev maxForce * (-dir)
        speed' = speed + amount / 2.0
        heading' = normalizeAngle (heading + speed')
        aimed = snd (deviation heading' target) <= 5.0 * tolerance
    in
        (aimed, h { speed = speed' * friction, heading = heading' })
  where
    deviation :: NativeFloat -> NativeFloat -> (NativeFloat, NativeFloat)
    deviation alpha beta = let gamma = normalizeAngle (alpha - beta)
                           in
                               if gamma <= 180.0 then (1.0, gamma) else (-1.0, 360.0 - gamma)

translatePoint :: NativePoint -> Heading -> NativeFloat -> NativePoint
translatePoint (P (V2 x y)) Heading{heading} dist =
    let alpha = heading * pi / 180.0
    in
        P (V2 (round (fromIntegral x + dist * sin alpha)) (round (fromIntegral y - dist * cos alpha)))

getHeadingAngle :: Heading -> NativeFloat
getHeadingAngle = heading

friction :: NativeFloat
friction = 0.92
