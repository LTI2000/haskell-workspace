module Engine.Cooldown
    ( Cooldown
    , newCooldown
    , newMultiCooldown
    , updateCooldown
    , resetCooldown
    ) where

import           Common.SdlTypes

data Cooldown = Cooldown { rate     :: NativeInt
                         , cooldown :: NativeInt
                         }
              | MultiCooldown { mainCooldown    :: Cooldown
                              , capacity        :: NativeInt
                              , currentCapacity :: NativeInt
                              , reloadCooldown  :: Cooldown
                              }

newCooldown :: NativeInt -> Cooldown
newCooldown rate = Cooldown { rate, cooldown = rate }

newMultiCooldown :: NativeInt -> NativeInt -> NativeInt -> Cooldown
newMultiCooldown rate capacity reloadRate =
    MultiCooldown { mainCooldown = newCooldown rate
                  , capacity
                  , currentCapacity = capacity
                  , reloadCooldown = newCooldown reloadRate
                  }

updateCooldown :: Cooldown -> (Bool, Cooldown)
updateCooldown c@Cooldown{cooldown}
    | cooldown > 0 = (False, c { cooldown = cooldown - 1 })
    | otherwise = (True, c)
updateCooldown c@MultiCooldown{mainCooldown,capacity,currentCapacity,reloadCooldown} =
    let (mainReady, mainCooldown') =
            updateCooldown mainCooldown
        (currentCapacity', reloadCooldown') =
            updateReloadCountdown
    in
        ( mainReady && currentCapacity' > 0
        , c { mainCooldown = mainCooldown'
            , currentCapacity = currentCapacity'
            , reloadCooldown = reloadCooldown'
            }
        )
  where
    updateReloadCountdown = let (reloadReady, reloadCooldown') =
                                    updateCooldown reloadCooldown
                            in
                                if reloadReady && currentCapacity < capacity
                                then let reloadCooldown'' = resetCooldown reloadCooldown'
                                     in
                                         (currentCapacity + 1, reloadCooldown'')
                                else (currentCapacity, reloadCooldown')

resetCooldown :: Cooldown -> Cooldown
resetCooldown c@Cooldown{rate,cooldown}
    | cooldown > 0 = c
    | otherwise = c { cooldown = rate }
resetCooldown c@MultiCooldown{mainCooldown = mc@Cooldown{cooldown},currentCapacity,reloadCooldown}
    | cooldown >
          0 ||
          currentCapacity <
              1 = c
    | otherwise = c { mainCooldown = resetCooldown mc
                    , currentCapacity = currentCapacity - 1
                    , reloadCooldown = resetCooldown reloadCooldown
                    }
