module Engine.ExitScene ( newExitScene ) where

import           Common.SdlTypes

import           Gfx.Draw
import           Gfx.Renderable
import           Gfx.Painter

import           Engine.Scene

import           Widget.Filler

newtype ExitScene = ExitScene { countDown :: NativeColorComponent }

instance Renderable ExitScene where
    render = renderExitScene

instance SceneClass ExitScene where
    updateScene = updateExitScene
    mousePainter _ = emptyPainter

newExitScene :: SceneState
newExitScene = (widget, Scene ExitScene { countDown = 255 })
  where
    widget = newFiller

renderExitScene :: RenderableArgs -> ExitScene -> IO ()
renderExitScene RenderableArgs{renderer} ExitScene{countDown = c} = do
    clearAll renderer (V4 c c c 255)

updateExitScene :: ExitScene -> SceneMonad SceneCommand
updateExitScene ExitScene{countDown = 0} =
    return popScene
updateExitScene exitScene@ExitScene{countDown = c} =
    return (replaceScene exitScene { countDown = c - 5 })
