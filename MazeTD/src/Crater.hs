module Crater ( newCrater ) where

import           Data.Vector         ( Vector, empty )

import           Common.SdlTypes

import           Gfx.Colors
import           Gfx.Draw
import           Gfx.Input
import           Gfx.Renderable

import           Engine.CollisionMap
import           Engine.Object

import           Assets

data Crater = Crater { mazeId :: NativeId
                     , pos    :: NativePoint
                     }

instance Renderable Crater where
    render = renderCrater

instance ObjectClass Crater where
    updateCollisionMap = const
    updateObject = updateCrater

instance ZOrdered Crater where
    zOrder = const Highest

newCrater :: NativeId -> NativePoint -> Object
newCrater mazeId pos = Object Crater { mazeId, pos }

renderCrater :: RenderableArgs -> Crater -> IO ()
renderCrater args@RenderableArgs{input = input@Input{inputGridSize}} Crater{mazeId,pos} = do
    let Assets{assetsTerrain} = getUserData input
    theta <- getRandomAngle args
    renderToBackground' mazeId
                        False
                        args
                        (drawSprite assetsTerrain 0 terrainCraterY (pos |+| inputGridSize) theta (rgbaMod white 51))

updateCrater :: Input -> w -> CollisionMap -> Crater -> (w, Vector Object)
updateCrater _input world _cmap _crater =
    (world, empty)
