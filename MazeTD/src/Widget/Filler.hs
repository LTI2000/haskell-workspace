module Widget.Filler ( newFiller ) where

import           Common.SdlTypes

import           Gfx.Renderable

import           Widget.Widget

data Filler = Filler

instance Renderable Filler where
    render _ _ = return ()

instance WidgetClass Filler where
    updateWidget _ filler = (Widget filler, [])

newFiller :: Widget
newFiller = Widget Filler
