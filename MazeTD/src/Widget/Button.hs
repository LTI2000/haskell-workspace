module Widget.Button
    ( newButton
    , newTextButton
    ) where

import           Common.SdlTypes

import           Geom.Geom

import           Gfx.Input
import           Gfx.Atlas
import           Gfx.Renderable
import           Gfx.Painter

import           Widget.UiColors
import           Widget.Widget

data Button = Button { actionId :: NativeId
                     , shortcut :: Char
                     , bounds   :: NativeRectangle
                     , painter  :: Bool -> Painter
                     , focus    :: Bool
                     }

instance Renderable Button where
    render = renderButton

instance WidgetClass Button where
    updateWidget = updateButton

newButton :: NativeId -> Char -> (Bool -> Painter) -> Widget
newButton actionId shortcut painter =
    Widget Button { actionId, shortcut, bounds = Rectangle (P (V2 0 0)) (V2 0 0), painter, focus = False }

newTextButton :: NativeId -> Char -> Atlas -> String -> NativeDimension -> Widget
newTextButton actionId shortcut atlas text size =
    newButton actionId
              shortcut
              (\focus -> if focus
                         then withColor uiBackgroundFocus filledRectPainter <>
                             withColor uiForegroundFocus (textPainter atlas text size) <>
                             withColor uiBorder rectPainter
                         else withColor uiBackground filledRectPainter <>
                             withColor uiForeground (textPainter atlas text size) <>
                             withColor uiBorder rectPainter)

renderButton :: RenderableArgs -> Button -> IO ()
renderButton args Button{bounds,painter,focus} = do
    paint (painter focus) args bounds

updateButton :: Input -> Button -> (Widget, [NativeId])
updateButton Input{inputBounds,inputMousePos,inputLeftButtonReleased,inputKeysPressed} button@Button{actionId,shortcut} =
    let shortcutPressed = shortcut `elem` inputKeysPressed
        mouseFocus = maybe False (inputBounds `contains`) inputMousePos
        clicked = inputLeftButtonReleased && mouseFocus
        actionIds = [actionId | clicked || shortcutPressed]
    in
        (Widget button { bounds = inputBounds, focus = shortcutPressed || mouseFocus }, actionIds)
