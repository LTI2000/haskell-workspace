module Widget.Layout
    ( (=!)
    , (=%)
    , Layout(..)
    , borderLayout
    , gridLayout
    , stackLayout
    , transformLayout
    ) where

import           Common.SdlTypes

(=!) :: NativeInt -> NativeInt -> NativeInt
_ =! value = value

(=%) :: NativeInt -> Word8 -> NativeInt
context =% percentage = context * fromIntegral percentage `div` 100

newtype Layout = Layout { doLayout :: NativeRectangle -> [NativeRectangle] }

borderLayout :: (NativeInt -> NativeInt)
             -> (NativeInt -> NativeInt)
             -> (NativeInt -> NativeInt)
             -> (NativeInt -> NativeInt)
             -> Layout
borderLayout leftGap rightGap topGap bottomGap =
    Layout { doLayout }
  where
    doLayout (Rectangle (P (V2 x0 y0)) (V2 w h)) =
        [ Rectangle (P (V2 (x0 + leftGap') (y0 + topGap')))
                    (V2 (w - (leftGap' + rightGap'))
                        (h - (topGap' + bottomGap')))
        ]
      where
        leftGap' = leftGap w
        rightGap' = rightGap w
        topGap' = topGap h
        bottomGap' = bottomGap h

gridLayout :: NativeInt
           -> NativeInt
           -> (NativeInt -> NativeInt)
           -> (NativeInt -> NativeInt)
           -> Layout
gridLayout cols rows hgap vgap =
    Layout { doLayout }
  where
    doLayout (Rectangle (P (V2 x0 y0)) (V2 w h)) =
        [ let x1 = x * w' `div` cols
              y1 = y * h' `div` rows
              x2 = (x + 1) * w' `div` cols
              y2 = (y + 1) * h' `div` rows
          in
              Rectangle (P (V2 (x0 + x1 + x * hgap') (y0 + y1 + y * vgap')))
                        (V2 (x2 - x1) (y2 - y1))
        | y <- [0 .. rows - 1]
        , x <- [0 .. cols - 1] ]
      where
        hgap' = hgap w
        vgap' = vgap h
        w' = w - (cols - 1) * hgap'
        h' = h - (rows - 1) * vgap'

stackLayout :: Layout
stackLayout = Layout { doLayout = repeat }

transformLayout :: (NativeRectangle -> NativeRectangle) -> Layout
transformLayout f = Layout { doLayout = pure . f }
