{-# LANGUAGE GADTs      #-}
{-# LANGUAGE Rank2Types #-}

module Widget.Widget
    ( WidgetClass(..)
    , Widget(..)
    ) where

import           Common.SdlTypes

import           Gfx.Input
import           Gfx.Renderable

class (Renderable a) => WidgetClass a where
    updateWidget :: Input -> a -> (Widget, [NativeId])

data Widget where
        Widget :: (WidgetClass a) => a -> Widget

(.$) :: Widget -> (forall a. WidgetClass a => a -> b) -> b
(Widget x) .$ f = f x

instance Renderable Widget where
    render args = (.$ render args)

instance WidgetClass Widget where
    updateWidget input = (.$ updateWidget input)
