module Widget.UiColors
    ( uiBackground
    , uiBackgroundFocus
    , uiBorder
    , uiForeground
    , uiForegroundFocus
    , module X
    ) where

import           Common.SdlTypes

import           Gfx.Colors      as X

uiBackground :: NativeRgba
uiBackground = rgbaMod green 51

uiBackgroundFocus :: NativeRgba
uiBackgroundFocus = rgbaMod green 102

uiBorder :: NativeRgba
uiBorder = rgbMod green 51

uiForeground :: NativeRgba
uiForeground = rgbMod green 102

uiForegroundFocus :: NativeRgba
uiForegroundFocus = rgbMod green 153
