module Widget.AutoHide ( newAutoHide ) where

import           Common.SdlTypes

import           Geom.Geom

import           Gfx.Input
import           Gfx.Renderable

import           Widget.Widget

data AutoHide = AutoHide { maxDisplacement :: NativeInt
                         , alignment       :: Alignment
                         , displacement    :: NativeInt
                         , child           :: Widget
                         }

instance Renderable AutoHide where
    render = renderAutoHide

instance WidgetClass AutoHide where
    updateWidget = updateAutoHide

newAutoHide :: NativeInt -> Alignment -> Widget -> Widget
newAutoHide maxDisplacement alignment child =
    Widget AutoHide { maxDisplacement, alignment, displacement = 0, child }

renderAutoHide :: RenderableArgs -> AutoHide -> IO ()
renderAutoHide args AutoHide{child} = do
    render args child

updateAutoHide :: Input -> AutoHide -> (Widget, [NativeId])
updateAutoHide input@Input{inputBounds = Rectangle (P (V2 x y)) (V2 w h),inputMousePos} autoHide@AutoHide{maxDisplacement,alignment = AlignBottom,displacement,child} =
    let bounds' = Rectangle (P (V2 x (y + displacement))) (V2 w h)
        focus = maybe False (bounds' `contains`) inputMousePos
        targetDisplacement = if focus then 0 else max 1 (h - maxDisplacement)
        delta = targetDisplacement - displacement
        (sign, amount) = (signum delta, abs delta)
        amount' = sign * (min 8 amount)
        displacement' = displacement + amount'
        bounds'' = Rectangle (P (V2 x (y + displacement'))) (V2 w h)
        (child', ids) = updateWidget input { inputBounds = bounds'' } child
    in
        (Widget autoHide { displacement = displacement', child = child' }, ids)
updateAutoHide input@Input{inputBounds = Rectangle (P (V2 x y)) (V2 w h),inputMousePos} autoHide@AutoHide{maxDisplacement,alignment = AlignRight,displacement,child} =
    let bounds' = Rectangle (P (V2 (x + displacement) y)) (V2 w h)
        focus = maybe False (bounds' `contains`) inputMousePos
        targetDisplacement = if focus then 0 else max 1 (w - maxDisplacement)
        delta = targetDisplacement - displacement
        (sign, amount) = (signum delta, abs delta)
        amount' = sign * (min 8 amount)
        displacement' = displacement + amount'
        bounds'' = Rectangle (P (V2 (x + displacement') y)) (V2 w h)
        (child', ids) = updateWidget input { inputBounds = bounds'' } child
    in
        (Widget autoHide { displacement = displacement', child = child' }, ids)
