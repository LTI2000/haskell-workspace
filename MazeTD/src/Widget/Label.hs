module Widget.Label
    ( newLabel
    , newTextLabel
    ) where

import           Common.SdlTypes

import           Gfx.Input
import           Gfx.Atlas
import           Gfx.Renderable
import           Gfx.Painter

import           Widget.UiColors
import           Widget.Widget

data Label = Label { bounds  :: NativeRectangle
                   , painter :: Painter
                   }

instance Renderable Label where
    render = renderLabel

instance WidgetClass Label where
    updateWidget = updateLabel

newLabel :: Painter -> Widget
newLabel painter = Widget Label { bounds = Rectangle (P (V2 0 0)) (V2 0 0), painter }

newTextLabel :: Atlas -> String -> NativeDimension -> Widget
newTextLabel atlas text size =
    newLabel (withColor uiBackground filledRectPainter <>
                  withColor uiForeground (textPainter atlas text size) <>
                  withColor uiBorder rectPainter)

renderLabel :: RenderableArgs -> Label -> IO ()
renderLabel args Label{bounds,painter} = do
    paint painter args bounds

updateLabel :: Input -> Label -> (Widget, [NativeId])
updateLabel Input{inputBounds} label =
    (Widget label { bounds = inputBounds }, [])
