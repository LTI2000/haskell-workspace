module Widget.Container ( newContainer ) where

import           Common.SdlTypes

import           Geom.Geom

import           Gfx.Input
import           Gfx.Renderable

import           Widget.Layout
import           Widget.Widget

data Container = Container { layout  :: Layout
                           , widgets :: [Widget]
                           }

instance Renderable Container where
    render = renderContainer

instance WidgetClass Container where
    updateWidget = updateContainer

newContainer :: Layout -> [Widget] -> Widget
newContainer layout widgets =
    Widget Container { layout, widgets }

renderContainer :: RenderableArgs -> Container -> IO ()
renderContainer args Container{widgets} = do
    render args widgets

updateContainer :: Input -> Container -> (Widget, [NativeId])
updateContainer input@Input{inputBounds,inputMousePos} container@Container{layout,widgets} =
    let focus = maybe False (inputBounds `contains`) inputMousePos
        (widgets', idss) = unzip [ updateWidget input { inputBounds = widgetBounds
                                                      , inputMousePos = if focus then inputMousePos else Nothing
                                                      }
                                                widget
                                 | (widget, widgetBounds) <- zip widgets (doLayout layout inputBounds) ]
    in
        (Widget container { widgets = widgets' }, join idss)
