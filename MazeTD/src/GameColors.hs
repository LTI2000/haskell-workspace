module GameColors
    ( startSceneColor
    , mazeColor
    , hudMoneyColor
    , hudHealthColor
    , mouseColor
    , pauseSceneColor
    , module X
    ) where

import           Common.SdlTypes

import           Widget.UiColors as X

startSceneColor :: NativeRgba
startSceneColor = rgbMod orange 102

mazeColor :: NativeRgba
mazeColor = rgbMod carrot 153

hudMoneyColor :: NativeRgba
hudMoneyColor = clouds

hudHealthColor :: NativeRgba
hudHealthColor = silver

mouseColor :: NativeRgba
mouseColor = rgbMod white 204

pauseSceneColor :: NativeRgba
pauseSceneColor = rgbMod wisteria 102
