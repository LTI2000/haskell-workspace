module MazeWalker
    ( newMazeWalker
    , walkMaze
    ) where

import           Common.SdlTypes

import           Maze

data MazeWalker = MazeWalker { maze          :: Maze
                             , mpos          :: NativePoint
                             , direction     :: Direction
                             , preferredTurn :: Turn
                             }

newMazeWalker :: Maze -> MazeWalker
newMazeWalker maze = MazeWalker { maze, mpos, direction, preferredTurn }
  where
    mpos = P (V2 1 1)
    direction = North
    preferredTurn = TurnLeft

walkMaze :: MazeWalker -> [NativePoint]
walkMaze mazeWalker = reverse (addPoint (mpos mazeWalker) (loop mazeWalker (addPoint (mpos mazeWalker) [])))
  where
    loop mw path = let mw' = moveMazeWalker mw
                   in
                       if mpos mazeWalker == mpos mw' && direction mazeWalker == direction mw'
                       then path
                       else loop mw' (addPoint (mpos mw') path)
    addPoint p0 [] = [ p0 ]
    addPoint p0 ps@(p1 : _) -- FIXME add case for three points in a line: #-#-# is optimized to #---#
        | p0 == p1 = ps
        | otherwise = p0 : ps

moveMazeWalker :: MazeWalker -> MazeWalker
moveMazeWalker mazeWalker@MazeWalker{maze,mpos,direction,preferredTurn} =
    let moves = [ getNextPos maze mpos direction preferredTurn
                , getNextPos maze mpos direction NoTurn
                , getForcedNextPos mpos direction (opposite preferredTurn)
                ]
        (mpos', direction') : _ =
            catMaybes moves
    in
        mazeWalker { mpos = mpos', direction = direction' }
  where
    getNextPos :: Maze -> NativePoint -> Direction -> Turn -> Maybe (NativePoint, Direction)
    getNextPos m p d t = if getMazeSpotAt m nextPos then Nothing else Just (nextPos, turn d t)
      where
        nextPos = move p (turn d t)

    getForcedNextPos :: NativePoint -> Direction -> Turn -> Maybe (NativePoint, Direction)
    getForcedNextPos p d t =
        Just (p, turn d t)

data Direction = North | East | South | West
    deriving (Eq)

data Turn = TurnLeft | TurnRight | NoTurn

turn :: Direction -> Turn -> Direction
turn North TurnRight = East
turn East TurnRight = South
turn South TurnRight = West
turn West TurnRight = North
turn North TurnLeft = West
turn West TurnLeft = South
turn South TurnLeft = East
turn East TurnLeft = North
turn direction NoTurn = direction

opposite :: Turn -> Turn
opposite TurnLeft = TurnRight
opposite TurnRight = TurnLeft
opposite NoTurn = NoTurn

move :: NativePoint -> Direction -> NativePoint
move (P (V2 x y)) North =
    P (V2 x (y - 1))
move (P (V2 x y)) East =
    P (V2 (x + 1) y)
move (P (V2 x y)) South =
    P (V2 x (y + 1))
move (P (V2 x y)) West =
    P (V2 (x - 1) y)
