module Creep ( newCreep ) where

import           Data.Vector         ( Vector, fromList, singleton )

import           Common.SdlTypes

import           Geom.Grid

import           Gfx.Colors
import           Gfx.Draw
import           Gfx.Input
import           Gfx.Renderable

import           Engine.Path
import           Engine.CollisionMap
import           Engine.World
import           Engine.Object

import           Assets
import           Level
import           WorldData
import           Maze
import           MazeWalker
import           Explosion
import           Crater

data Creep = Creep { maze      :: Maze
                   , objId     :: NativeId
                   , level     :: Level
                   , hitpoints :: NativeInt
                   , heading   :: NativeFloat
                   , path      :: MultiPath
                   }

instance Renderable Creep where
    render = renderCreep

instance ObjectClass Creep where
    updateCollisionMap cmap Creep{objId,hitpoints,path} =
        addCircle cmap
                  objId
                  Enemy
                  (multiPathPos path)
                  13
                  (multiPathVelocity path)
                  hitpoints
    updateObject = updateCreep

instance ZOrdered Creep where
    zOrder = const High

newCreep :: Maze -> NativeId -> Level -> NativeDimension -> Object
newCreep maze objId level gridSize =
    Object Creep { maze, objId, level, hitpoints, heading, path }
  where
    hitpoints = creepHitpoints level
    heading = 0
    speed = creepSpeed level
    points = walkMaze (newMazeWalker maze)
    path = newMultiPath (modelToView gridSize <$> points) speed

renderCreep :: RenderableArgs -> Creep -> IO ()
renderCreep RenderableArgs{input = input@Input{inputGridSize},renderer} Creep{level,heading,path} = do
    let Assets{assetsSprites} = getUserData input
    drawSprite assetsSprites
               (spriteIndex level)
               spritesCreepY
               (multiPathPos path |+| inputGridSize)
               heading
               white
               renderer

updateCreep :: WorldClass w
            => Input
            -> w
            -> CollisionMap
            -> Creep
            -> (w, Vector Object)
updateCreep Input{inputFrameNo,inputBounds} world cmap creep@Creep{maze,objId,level,hitpoints,heading,path} =
    let collisions = getCollisions cmap objId
        hitpoints' = damage hitpoints collisions
    in
        if hitpoints' > 0
        then case updateMultiPath inputBounds path of
            Just path' -> let creep' = creep { hitpoints = hitpoints'
                                             , heading = heading + 1
                                             , path = path'
                                             }
                          in
                              (world, singleton (Object creep'))
            Nothing -> die 0
        else die (creepBounty level)
  where
    damage hp [] = hp
    damage hp (Collision{cDmg} : cs) =
        damage (hp - cDmg) cs

    die money = let theta = fromIntegral (inputFrameNo `mod` 360)
                    explosion = newExplosion (multiPathPos path) 32 theta 1 True
                    crater = newCrater (mazeId maze) (multiPathPos path)
                in
                    ( modifyMoney (+ money) world
                    , fromList [ explosion, crater ]
                    )
