module Explosion ( newExplosion ) where

import           Data.Vector         ( Vector, empty, singleton )

import           Common.SdlTypes

import           Gfx.Colors
import           Gfx.Draw
import           Gfx.Input
import           Gfx.Renderable
import           Gfx.Atlas

import           Engine.CollisionMap
import           Engine.Object

import           Assets

data Explosion = Explosion { pos   :: NativePoint
                           , size  :: NativeInt
                           , theta :: NativeFloat
                           , speed :: NativeInt
                           , phase :: NativeInt
                           }

instance Renderable Explosion where
    render = renderExplosion

instance ObjectClass Explosion where
    updateCollisionMap = const
    updateObject = updateExplosion

instance ZOrdered Explosion where
    zOrder = const Highest

newExplosion :: NativePoint -- ^ pos
             -> NativeInt   -- ^ size
             -> NativeFloat -- ^ theta
             -> NativeInt   -- ^ speed
             -> Bool        -- ^ refactoring-dummy
             -> Object
newExplosion pos size theta speed _ =
    Object Explosion { pos, size, theta, speed, phase = 0 }

renderExplosion :: RenderableArgs -> Explosion -> IO ()
renderExplosion RenderableArgs{input,renderer} Explosion{pos,size,theta,phase} = do
    let Assets{assetsExplosion = atlas} =
            getUserData input
        (V2 atlasWidth _) = atlasSize atlas
        atlasX = phase `mod` atlasWidth
        atlasY = phase `div` atlasWidth
    drawSprite atlas
               atlasX
               atlasY
               (pos |+| V2 size size)
               theta
               (aMod white 204)
               renderer

updateExplosion :: Input -> w -> CollisionMap -> Explosion -> (w, Vector Object)
updateExplosion input world _cmap explosion@Explosion{speed,phase} =
    let Assets{assetsExplosion = atlas} =
            getUserData input
        (V2 atlasWidth atlasHeight) =
            atlasSize atlas
        phase' = phase + speed
    in
        if phase' < atlasWidth * atlasHeight
        then let explosion' = Object explosion { phase = phase' }
             in
                 (world, singleton explosion')
        else (world, empty)
