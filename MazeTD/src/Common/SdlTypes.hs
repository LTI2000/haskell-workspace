{-# OPTIONS_GHC -fno-warn-orphans #-}

module Common.SdlTypes
    ( NativeContext
    , NativeInt
    , NativeFloat
    , NativePoint
    , NativeVector
    , NativeDimension
    , NativeRectangle
    , NativeColorComponent
    , NativeRgb
    , NativeAlpha
    , NativeRgba
    , NativeBinary
    , NativeFrameNo
    , NativeId
    , NativeMoney
    , module X
    , (<<>>)
    ) where

import           System.Random       as X ( StdGen, mkStdGen, newStdGen
                                          , randomR )
import           Text.Show           as X ( Show, show )
import           System.IO           as X ( IO, print, putStr, putStrLn )
import           Prelude             as X ( Bounded(..), Enum(..), Floating(..)
                                          , Fractional(..), Integral(..)
                                          , Num(..), RealFloat(..)
                                          , RealFrac(..), error, fromIntegral )

import           Data.Function       as X ( ($), const, flip, on )
import           Data.Monoid         as X ( Monoid(..) )
import           Data.Semigroup      as X ( Semigroup(..) )   
import           Data.Foldable       as X ( Foldable(..), forM_, mapM_ )
import           Data.Functor        as X ( ($>), (<$>), Functor(..), void )
import           Control.Category    as X ( (<<<), (>>>), Category(..) )
import           Control.Applicative as X ( Applicative(..), liftA, liftA2 )
import           Control.Monad       as X ( (<=<), (=<<), (>=>), Monad(..)
                                          , filterM, join, mapM, sequence
                                          , unless, when )

import           Data.Eq             as X ( Eq(..) )
import           Data.Ord            as X ( Ord(..), comparing )
import           Data.Bool           as X ( (&&), (||), Bool(..), not
                                          , otherwise )
import           Data.Int            as X ( Int )
import           Data.Char           as X ( Char, chr, ord )
import           Data.String         as X ( String )
import           Data.Word           as X ( Word32, Word64, Word8 )
import           Data.Maybe          as X ( Maybe(..), catMaybes, maybe )
import           Data.Either         as X ( Either(..), either )
import           Data.List           as X ( (!!), (++), (\\), any, delete
                                          , filter, intercalate, iterate, nub
                                          , repeat, replicate, reverse, sort
                                          , sortBy, take, unzip, zip )
import           Data.Tuple          as X ( fst, snd, swap )
import           Data.ByteString     as X ( ByteString )
import           Data.Typeable       as X ( Typeable )
import           Data.Dynamic        as X ( Dynamic, fromDyn, toDyn )

import           SDL                 as X hiding ( Vector )
import           Foreign.C.Types     as X

import           Data.Ix             as X ( Ix(..) )
import           GHC.Arr             as X ( unsafeIndex )

type NativeContext = (Window, Renderer)

type NativeInt = CInt

type NativeFloat = CDouble

type NativePoint = Point V2 NativeInt

type NativeVector = V2 NativeInt

type NativeDimension = V2 NativeInt

type NativeRectangle = Rectangle NativeInt

type NativeColorComponent = Word8

type NativeRgb = V3 NativeColorComponent

type NativeAlpha = NativeColorComponent

type NativeRgba = V4 NativeColorComponent

type NativeBinary = ByteString

type NativeFrameNo = Word64

type NativeId = Word64

type NativeMoney = Word64

instance Ix CInt where
    range (a, b) = [a .. b]
    unsafeIndex (a, _) c = fromIntegral (c - a)
    inRange (a, b) c = a <= c && c <= b

(<<>>) :: (Applicative f, Monoid a) => f a -> f a -> f a
(<<>>) = liftA2 (<>)
