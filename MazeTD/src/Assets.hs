{-# LANGUAGE TemplateHaskell #-}

module Assets
    ( Assets(..)
    , createAssets
    , spritesCreepY
    , spritesCannonY
    , spritesProjectileY
    , spritesGroundPlateY
    , spritesMortarY
    , spritesBombY
    , spritesLauncherY
    , spritesMissileY
    , terrainDirtY
    , terrainCraterY
    , buildingsHeadQuarterY
    , standardMousePainter
    , fpsPainter
    ) where

import           Common.SdlTypes

import           Resource.Resource
import           Resource.ImageLoader

import           Gfx.GfxContext
import           Gfx.Atlas
import           Gfx.Input
import           Gfx.Renderable
import           Gfx.Painter
import           Gfx.Draw

import           GameColors

data Assets = Assets { assetsFont        :: Atlas
                     , assetsCursors     :: Atlas
                     , assetsSprites     :: Atlas
                     , assetsExplosion   :: Atlas
                     , assetsRadar       :: Atlas
                     , assetsTerrain     :: Atlas
                     , assetsBuildings   :: Atlas
                     , assetsBackground0 :: Atlas
                     }

createAssets :: NativeContext -> IO Assets
createAssets gfxCtx = do
    Assets <$> toAtlas ($(embedImage "font"), V2 8 8)
           <*> toAtlas ($(embedImage "cursors"), V2 32 32)
           <*> toAtlas ($(embedImage "sprites"), V2 32 32)
           <*> toAtlas ($(embedImage "explosion"), V2 100 100)
           <*> toAtlas ($(embedImage "radar"), V2 512 512)
           <*> toAtlas ($(embedImage "terrain"), V2 32 32)
           <*> toAtlas ($(embedImage "buildings"), V2 64 64)
           <*> toAtlas ($(embedImage "background0"), V2 750 500)
  where
    toAtlas :: (NativeBinary, NativeDimension) -> IO Atlas
    toAtlas (imageData, tileSize) = do
        texture <- newPngSurface imageData >>= copySurface gfxCtx >>=
                       newStaticTexture gfxCtx
        textureInfo@TextureInfo{textureWidth,textureHeight} <- queryTexture texture
        putStrLn $ "Static Texture Info: " ++ show textureInfo
        return Atlas { texture
                     , textureSize = V2 textureWidth textureHeight
                     , tileSize
                     }

spritesCreepY :: NativeInt
spritesCreepY = 0

spritesCannonY :: NativeInt
spritesCannonY = 1

spritesProjectileY :: NativeInt
spritesProjectileY = 2

spritesGroundPlateY :: NativeInt
spritesGroundPlateY = 3

spritesMortarY :: NativeInt
spritesMortarY = 4

spritesBombY :: NativeInt
spritesBombY = 5

spritesLauncherY :: NativeInt
spritesLauncherY = 6

spritesMissileY :: NativeInt
spritesMissileY = 7

terrainDirtY :: NativeInt
terrainDirtY = 0

terrainCraterY :: NativeInt
terrainCraterY = 1

buildingsHeadQuarterY :: NativeInt
buildingsHeadQuarterY = 0

standardMousePainter :: Painter
standardMousePainter = Painter { paint }
  where
    paint RenderableArgs{input = input@Input{inputGridSize,inputMousePos = Just mousePos},renderer} _ = do
        let Assets{assetsCursors} = getUserData input
        drawSprite assetsCursors
                   0
                   0
                   (mousePos |+| inputGridSize)
                   0.0
                   mouseColor
                   renderer
    paint RenderableArgs{input = Input{inputMousePos = Nothing}} _ = do
        return ()

fpsPainter :: Painter
fpsPainter = Painter { paint }
  where
    paint RenderableArgs{input = input@Input{inputFps},renderer} _ = do
        let Assets{assetsFont} = getUserData input
        let fpsString = "FPS: " ++ show inputFps
        drawText assetsFont
                 fpsString
                 (Rectangle (P (V2 0 0)) (V2 (80 + 2) (8 + 2)))
                 (V2 1 1)
                 asbestos
                 renderer
