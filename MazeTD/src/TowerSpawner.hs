module TowerSpawner ( newTowerSpawner ) where

import           Data.Vector         ( Vector, fromList, singleton )
import           Data.Array          ( (!), (//), Array, array )

import           Common.SdlTypes

import           Geom.Geom
import           Geom.Grid

import           Gfx.Input
import           Gfx.Colors
import           Gfx.Draw
import           Gfx.Renderable

import           Engine.CollisionMap
import           Engine.World
import           Engine.Object

import           Assets
import           Maze
import           Level
import           WorldData
import           TowerWarpIn
import           Cannon
import           Mortar
import           Launcher

data BuildMode = BuildTower { buildModeId :: NativeId
                            , level       :: Level
                            , towerCost   :: Level -> NativeMoney
                            , towerRange  :: Level -> NativeInt
                            , newTower    :: NativeId
                                          -> NativeId
                                          -> Level
                                          -> NativePoint
                                          -> Object
                            }

instance Eq BuildMode where
    BuildTower{buildModeId = id0} == BuildTower{buildModeId = id1} =
        id0 == id1

data TowerSpawner = TowerSpawner { maze      :: Maze
                                 , buildable :: Array NativePoint Bool
                                 , buildMode :: Maybe BuildMode
                                 }

instance Renderable TowerSpawner where
    render = renderTowerSpawner

instance ObjectClass TowerSpawner where
    updateCollisionMap = const
    updateObject = updateTowerSpawner

instance ZOrdered TowerSpawner where
    zOrder = const High

newTowerSpawner :: Maze -> Object
newTowerSpawner maze = Object TowerSpawner { maze
                                           , buildable
                                           , buildMode = Nothing
                                           }
  where
    (tl, _, br, _) = corners (mazeBounds maze)
    ixs = (tl, br)
    buildable = array ixs
                      [ (ix, True)
                      | ix <- range ixs ]

updateTowerSpawner :: (WorldClass w)
                   => Input
                   -> w
                   -> CollisionMap
                   -> TowerSpawner
                   -> (w, Vector Object)
updateTowerSpawner Input{inputLeftButtonReleased = False,inputActionIds} world _ towerSpawner@TowerSpawner{buildMode} =
    let buildMode' = updateBuildMode buildMode inputActionIds
    in
        (world, singleton (Object towerSpawner { buildMode = buildMode' }))
  where
    actionIdToBuildMode actionId
        | actionId == 10 = x actionId
                             (Level 0)
                             cannonCost
                             cannonRange
                             spritesCannonY
                             newCannon
        | actionId == 20 = x actionId
                             (Level 1)
                             cannonCost
                             cannonRange
                             spritesCannonY
                             newCannon
        | actionId == 30 = x actionId
                             (Level 2)
                             cannonCost
                             cannonRange
                             spritesCannonY
                             newCannon
        | actionId == 40 = x actionId
                             (Level 0)
                             mortarCost
                             mortarRange
                             spritesMortarY
                             newMortar
        | actionId == 50 = x actionId
                             (Level 1)
                             mortarCost
                             mortarRange
                             spritesMortarY
                             newMortar
        | actionId == 60 = x actionId
                             (Level 2)
                             mortarCost
                             mortarRange
                             spritesMortarY
                             newMortar
        | actionId == 70 = x actionId
                             (Level 2)
                             launcherCost
                             launcherRange
                             spritesLauncherY
                             newLauncher
        | otherwise = Nothing

    x buildModeId level towerCost towerRange spritesTowerY newTower =
        Just BuildTower { buildModeId
                        , level
                        , towerCost
                        , towerRange
                        , newTower = newTowerWarpIn spritesTowerY newTower
                        }


    updateBuildMode :: Maybe BuildMode -> [NativeId] -> Maybe BuildMode
    updateBuildMode mode [] =
        mode
    updateBuildMode mode (actionId : actionIds) =
        updateBuildMode (maybe mode
                               (\mode' -> if mode == Just mode'
                                          then Nothing
                                          else Just mode')
                               (actionIdToBuildMode actionId))
                        actionIds

updateTowerSpawner Input{inputGridSize,inputMousePos = Just mousePos,inputLeftButtonReleased = True} world _ towerSpawner@TowerSpawner{maze = Maze{mazeId},buildable,buildMode = Just BuildTower{level,towerCost,newTower}} =
    if isMazeSpotBuildable towerSpawner inputGridSize mousePos
    then case requestMoney (towerCost level) world of
        Just world' -> let (objId, world'') = nextObjId world'
                           tower = newTower objId
                                            mazeId
                                            level
                                            (alignViewPoint inputGridSize
                                                            mousePos)
                           buildable' = buildable //
                               [ (viewToModel inputGridSize mousePos, False) ]
                           buildMode' = Nothing
                       in
                           ( world''
                           , fromList [ Object towerSpawner { buildable = buildable'
                                                             , buildMode = buildMode'
                                                             }
                                      , tower
                                      ]
                           )
        Nothing -> (world, singleton (Object towerSpawner))
    else (world, singleton (Object towerSpawner))

updateTowerSpawner _ world _ towerSpawner =
    (world, singleton (Object towerSpawner))

renderTowerSpawner :: RenderableArgs -> TowerSpawner -> IO ()
renderTowerSpawner = renderBuildSpot

renderBuildSpot :: RenderableArgs -> TowerSpawner -> IO ()
renderBuildSpot RenderableArgs{input = Input{inputMousePos = Nothing}} _ = do
    return ()
renderBuildSpot _ TowerSpawner{buildMode = Nothing} = do
    return ()
renderBuildSpot RenderableArgs{input = input@Input{inputFrameNo,inputGridSize,inputMousePos = Just mousePos},renderer} towerSpawner@TowerSpawner{buildMode = Just BuildTower{level,towerRange}} = do
    let Assets{assetsRadar} = getUserData input
        spotPos = alignViewPoint inputGridSize mousePos
        isBuildable = isMazeSpotBuildable towerSpawner inputGridSize mousePos
        color = if isBuildable then green else red
        radius = towerRange level
    drawSprite assetsRadar
               0
               0
               (spotPos |+| (2 ^* radius))
               (fromIntegral (inputFrameNo `mod` 3600) / 10.0)
               (aMod color 34)
               renderer

isMazeSpotBuildable :: TowerSpawner -> NativeDimension -> NativePoint -> Bool
isMazeSpotBuildable TowerSpawner{maze,buildable} gridSize vpos =
    (mazeBounds maze `contains` mpos) && (getMazeSpotAt maze mpos && buildable ! mpos)
  where
    mpos = viewToModel gridSize vpos
