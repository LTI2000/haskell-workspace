module Cannon ( newCannon ) where

import           Data.Vector         ( Vector, fromList, singleton )

import           Common.SdlTypes

import           Geom.Geom

import           Gfx.Colors
import           Gfx.Draw
import           Gfx.Input
import           Gfx.Renderable

import           Engine.CollisionMap
import           Engine.Heading
import           Engine.Cooldown
import           Engine.World
import           Engine.Object

import           Assets
import           Level
import           Projectile

data Cannon = Cannon { objId    :: NativeId
                     , level    :: Level
                     , pos      :: NativePoint
                     , heading  :: Heading
                     , cooldown :: Cooldown
                     }

instance Renderable Cannon where
    render = renderCannon

instance ObjectClass Cannon where
    updateCollisionMap cmap Cannon{objId,level,pos} =
        addRadarCircle cmap
                       objId
                       owner
                       pos
                       radius
                       (Just (projectileSpeed level)) -- Nothing
      where
        owner = Player
        radius = cannonRange level
    updateObject = updateCannon

instance ZOrdered Cannon where
    zOrder = const Medium

newCannon :: NativeId -> Level -> NativePoint -> Object
newCannon objId level pos =
    Object Cannon { objId
                  , level
                  , pos
                  , heading = newHeading (cannonHeadingSpeed level)
                                         (cannonHeadingTolerance level)
                                         0
                  , cooldown = newCooldown rate
                  }
  where
    rate = cannonCooldownRate level

renderCannon :: RenderableArgs -> Cannon -> IO ()
renderCannon RenderableArgs{input = input@Input{inputGridSize},renderer} Cannon{level,pos,heading} = do
    let Assets{assetsSprites} = getUserData input
    drawSprite assetsSprites
               (spriteIndex level)
               spritesCannonY
               (pos |+| inputGridSize)
               (getHeadingAngle heading)
               white
               renderer

updateCannon :: (WorldClass w)
             => Input
             -> w
             -> CollisionMap
             -> Cannon
             -> (w, Vector Object)
updateCannon _input world cmap cannon@Cannon{objId,level,pos,heading,cooldown} =
    case sortBy (flip compare `on` cObjId) (getCollisions cmap objId)    -- FIXME extract duplicate code below
     of
        [] -> let (_, heading') = updateHeading heading Nothing
                  (_, cooldown') = updateCooldown cooldown
                  cannon' = cannon { heading = heading', cooldown = cooldown' }
              in
                  (world, singleton (Object cannon'))
        Collision{cPos} : _ -> let (aimed, heading') = updateHeading heading
                                                                     (Just (bearing pos
                                                                                    cPos))
                                   (ready, cooldown') = updateCooldown cooldown
                                   cannon' = cannon { heading = heading'
                                                    , cooldown = cooldown'
                                                    }
                               in
                                   if aimed && ready
                                   then let (objId', world') = nextObjId world
                                            projectile = newProjectile objId'
                                                                       level
                                                                       pos
                                                                       (translatePoint pos
                                                                                       heading'
                                                                                       100.0)
                                                                       (projectileSpeed level)
                                            cooldown'' = resetCooldown cooldown'
                                            cannon'' = cannon' { cooldown = cooldown''
                                                               }
                                        in
                                            ( world'
                                            , fromList [ projectile
                                                       , Object cannon''
                                                       ]
                                            )
                                   else (world, singleton (Object cannon'))
