module Launcher ( newLauncher ) where

import           Data.Vector         ( Vector, fromList, singleton )

import           Common.SdlTypes

import           Geom.Geom

import           Gfx.Colors
import           Gfx.Draw
import           Gfx.Input
import           Gfx.Renderable

import           Engine.CollisionMap
import           Engine.Heading
import           Engine.Cooldown
import           Engine.World
import           Engine.Object

import           Assets
import           Level
import           Missile

data Launcher = Launcher { objId    :: NativeId
                         , level    :: Level
                         , pos      :: NativePoint
                         , heading  :: Heading
                         , cooldown :: Cooldown
                         }

instance Renderable Launcher where
    render = renderLauncher

instance ObjectClass Launcher where
    updateCollisionMap cmap Launcher{objId,level,pos} =
        addRadarCircle cmap objId owner pos radius (Just (missileSpeed level)) -- Nothing
      where
        owner = Player
        radius = launcherRange level
    updateObject = updateLauncher

instance ZOrdered Launcher where
    zOrder = const Medium

newLauncher :: NativeId -> Level -> NativePoint -> Object
newLauncher objId level pos =
    Object Launcher { objId
                    , level
                    , pos
                    , heading = newHeading (launcherHeadingSpeed level)
                                           (launcherHeadingTolerance level)
                                           0
                    , cooldown = newMultiCooldown (rate `div` 10) 6 rate
                    }
  where
    rate = launcherCooldownRate level

renderLauncher :: RenderableArgs -> Launcher -> IO ()
renderLauncher RenderableArgs{input = input@Input{inputGridSize},renderer} Launcher{level,pos,heading} = do
    let Assets{assetsSprites} = getUserData input
    drawSprite assetsSprites
               (spriteIndex level)
               spritesLauncherY
               (pos |+| inputGridSize)
               (getHeadingAngle heading)
               white
               renderer

updateLauncher :: (WorldClass w)
               => Input
               -> w
               -> CollisionMap
               -> Launcher
               -> (w, Vector Object)
updateLauncher _input world cmap launcher@Launcher{objId,level,pos,heading,cooldown} =
    case sortBy (flip (comparing (dist . cPos))) (getCollisions cmap objId)    -- FIXME extract duplicate code below
     of
        [] -> let (_, heading') = updateHeading heading Nothing
                  (_, cooldown') = updateCooldown cooldown
                  launcher' = launcher { heading = heading'
                                       , cooldown = cooldown'
                                       }
              in
                  (world, singleton (Object launcher'))
        Collision{cObjId,cPos} : _ ->
            let (aimed, heading') = updateHeading heading
                                                  (Just (bearing pos cPos))
                (ready, cooldown') = updateCooldown cooldown
                launcher' = launcher { heading = heading'
                                     , cooldown = cooldown'
                                     }
            in
                if aimed && ready
                then let (objId', world') = nextObjId world
                         missile = newMissile objId'
                                              level
                                              pos
                                              cPos
                                              cObjId
                                              (missileSpeed level)
                         cooldown'' = resetCooldown cooldown'
                         launcher'' = launcher' { cooldown = cooldown'' }
                     in
                         (world', fromList [ missile, Object launcher'' ])
                else (world, singleton (Object launcher'))
  where
    dist :: NativePoint -> NativeInt
    dist pos' = quadrance (pos ^-^ pos')
