module Level
    ( Level(..) -- FIXME make constructor private
    , spriteIndex
    , headQuartersHitpoints
    , creepSpeed
    , creepHitpoints
    , creepBounty
    , cannonCost
    , cannonCooldownRate
    , cannonHeadingSpeed
    , cannonHeadingTolerance
    , cannonRange
    , projectileSpeed
    , projectileDamage
    , mortarCost
    , mortarCooldownRate
    , mortarHeadingSpeed
    , mortarHeadingTolerance
    , mortarRange
    , bombSpeed
    , bombDamage
    , bombExplosionRadius
    , launcherCost
    , launcherCooldownRate
    , launcherHeadingSpeed
    , launcherHeadingTolerance
    , launcherRange
    , missileSpeed
    , missileDamage
    ) where

import           Common.SdlTypes

newtype Level = Level NativeInt
    deriving (Eq)

spriteIndex :: Level -> NativeInt
spriteIndex (Level n) = n

headQuartersHitpoints :: Level -> NativeInt
headQuartersHitpoints (Level n) =
    1000 + 1000 * n

creepSpeed :: Level -> NativeFloat
creepSpeed (Level n) = fromIntegral (4 - n)

creepHitpoints :: Level -> NativeInt
creepHitpoints (Level n) =
    100 * (n + 1)

creepBounty :: Level -> NativeMoney
creepBounty (Level n) = fromIntegral (n + 1)

cannonCost :: Level -> NativeMoney
cannonCost (Level n) = fromIntegral (10 * (n + 1))

cannonCooldownRate :: Level -> NativeInt
cannonCooldownRate (Level n) =
    10 + 10 * n

cannonHeadingSpeed :: Level -> NativeFloat
cannonHeadingSpeed (Level n) =
    fromIntegral (2 * (4 - n))

cannonHeadingTolerance :: Level -> NativeFloat
cannonHeadingTolerance (Level n) =
    fromIntegral (1 + n)

cannonRange :: Level -> NativeInt
cannonRange (Level n) = 48 + 48 * (n + 1)

projectileSpeed :: Level -> NativeFloat
projectileSpeed (Level n) =
    5.0 - fromIntegral n / 10.0

projectileDamage :: Level -> NativeInt
projectileDamage (Level n) =
    50 * (n + 1)

mortarCost :: Level -> NativeMoney
mortarCost (Level n) = fromIntegral (50 * (n + 1))

mortarCooldownRate :: Level -> NativeInt
mortarCooldownRate (Level n) =
    50 + 50 * n

mortarHeadingSpeed :: Level -> NativeFloat
mortarHeadingSpeed (Level n) =
    fromIntegral (2 * (4 - n))

mortarHeadingTolerance :: Level -> NativeFloat
mortarHeadingTolerance (Level n) =
    fromIntegral (2 * (1 + n))

mortarRange :: Level -> NativeInt
mortarRange (Level n) = 64 + 64 * (n + 1)

bombSpeed :: Level -> NativeFloat
bombSpeed (Level n) = 4.0 - fromIntegral n / 5.0

bombDamage :: Level -> NativeInt
bombDamage (Level n) = 250 * (n + 1)

bombExplosionRadius :: Level -> NativeInt
bombExplosionRadius (Level n) =
    32 + 8 * n

launcherCost :: Level -> NativeMoney
launcherCost (Level n) =
    fromIntegral (10 * (n + 1))

launcherCooldownRate :: Level -> NativeInt
launcherCooldownRate (Level n) =
    10 + 10 * n

launcherHeadingSpeed :: Level -> NativeFloat
launcherHeadingSpeed (Level n) =
    fromIntegral (2 * (4 - n))

launcherHeadingTolerance :: Level -> NativeFloat
launcherHeadingTolerance (Level n) =
    fromIntegral (1 + n)

launcherRange :: Level -> NativeInt
launcherRange (Level n) =
    128 + 64 * (n + 1)

missileSpeed :: Level -> NativeFloat
missileSpeed (Level n) =
    10.0 - fromIntegral n / 5.0

missileDamage :: Level -> NativeInt
missileDamage (Level n) =
    25 * (n + 1)
