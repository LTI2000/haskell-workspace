module Projectile ( newProjectile ) where

import           Data.Vector         ( Vector, empty, fromList, singleton )

import           Common.SdlTypes

import           Gfx.Colors
import           Gfx.Draw
import           Gfx.Input
import           Gfx.Renderable

import           Engine.Path
import           Engine.CollisionMap
import           Engine.Object

import           Assets
import           Level
import           Explosion

data Projectile = Projectile { objId :: NativeId
                             , level :: Level
                             , path  :: Path
                             }

instance Renderable Projectile where
    render = renderProjectile

instance ObjectClass Projectile where
    updateCollisionMap cmap Projectile{objId,level,path} =
        addSegment cmap
                   objId
                   Player
                   (pathPos path)
                   (nextPathPos path)
                   (pathVelocity path)
                   (projectileDamage level)
    updateObject = updateProjectile

instance ZOrdered Projectile where
    zOrder = const Low

newProjectile :: NativeId
              -> Level
              -> NativePoint
              -> NativePoint
              -> NativeFloat
              -> Object
newProjectile objId level from to speed =
    Object Projectile { objId, level, path = newPath from to speed }

renderProjectile :: RenderableArgs -> Projectile -> IO ()
renderProjectile RenderableArgs{input = input@Input{inputGridSize},renderer} Projectile{level,path} = do
    let Assets{assetsSprites} = getUserData input
    drawSprite assetsSprites
               (spriteIndex level)
               spritesProjectileY
               (pathPos path |+| inputGridSize)
               (pathHeading path)
               white
               renderer

updateProjectile :: Input
                 -> w
                 -> CollisionMap
                 -> Projectile
                 -> (w, Vector Object)
updateProjectile Input{inputFrameNo,inputBounds} world cmap projectile@Projectile{objId,path} =
    let collisions = getCollisions cmap objId
    in
        if null collisions
        then case updatePath inputBounds path of
            Just path' -> let projectile' = projectile { path = path' }
                          in
                              (world, singleton (Object projectile'))
            Nothing -> (world, empty)
        else die collisions
  where
    die collisions = let theta = fromIntegral (inputFrameNo `mod` 360)
                         explosions = [ newExplosion cPos (32 - 16) theta 2 True
                                      | Collision{cPos} <- collisions ]
                     in
                         (world, fromList explosions)
