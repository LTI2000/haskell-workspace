module Geom.Geom
    ( Alignment(..)
    , bearing
    , bearing'
    , normalizeAngle
    , contains
    , rectangle
    , shrink
    , corners
    , centerRectangle
    , centerPoint
    , alignRectangle
    , cutRectangle
    , squareRectangle
    ) where

import           Common.SdlTypes

data Alignment = AlignLeft | AlignRight | AlignTop | AlignBottom
    deriving (Eq, Show, Enum, Bounded)

bearing :: NativePoint -> NativePoint -> NativeFloat
bearing p0 p1 = bearing' (fromIntegral <$> p1 .-. p0)

bearing' :: RealFloat a => V2 a -> a
bearing' (V2 x y) = let theta = atan2 dx dy * 180.0 / pi
                    in
                        if theta >= 0 then theta else 360.0 + theta
  where
    dx = x
    dy = -y

normalizeAngle :: (Ord a, Fractional a) => a -> a
normalizeAngle theta
    | theta < 0 = normalizeAngle (theta + 360.0)
    | theta >= 360.0 = normalizeAngle (theta - 360.0)
    | otherwise = theta

contains :: NativeRectangle -> NativePoint -> Bool
(Rectangle (P (V2 x0 y0)) (V2 w h)) `contains` (P (V2 x y)) =
    x >= x0 && y >= y0 && x < (x0 + w) && y < (y0 + h)

rectangle :: (NativePoint, NativePoint) -> NativeRectangle
rectangle (P tl, P br) =
    Rectangle (P tl) (br - tl + 1)

shrink :: NativeInt -> NativeRectangle -> NativeRectangle
shrink d (Rectangle (P (V2 x y)) (V2 w h)) =
    Rectangle (P (V2 (x + d) (y + d))) (V2 (x + w - 1 - d) (y + h - 1 - d))

corners :: NativeRectangle
        -> (NativePoint, NativePoint, NativePoint, NativePoint)
corners (Rectangle (P (V2 x y)) (V2 w h)) =
    ( P (V2 x y)
    , P (V2 (x + w - 1) y)
    , P (V2 (x + w - 1) (y + h - 1))
    , P (V2 x (y + h - 1))
    )

centerRectangle :: NativeDimension -> NativePoint -> NativeRectangle
centerRectangle size (P pos) =
    Rectangle (P (pos - ((`div` 2) <$> size))) size

centerPoint :: NativeRectangle -> NativePoint
centerPoint (Rectangle (P (V2 x y)) (V2 w h)) =
    P (V2 ((w - 1) `div` 2 + x) ((h - 1) `div` 2 + y))

alignRectangle :: NativeDimension -> NativeRectangle -> NativeRectangle
alignRectangle size@(V2 w0 h0) (Rectangle (P (V2 x1 y1)) (V2 w1 h1)) =
    Rectangle (P (V2 (x1 + (w1 - w0) `div` 2) (y1 + (h1 - h0) `div` 2))) size

transposeRectangle :: NativeRectangle -> NativeRectangle
transposeRectangle (Rectangle (P (V2 x y)) (V2 w h)) =
    Rectangle (P (V2 y x)) (V2 h w)

cutRectangle :: Word8 -> Alignment -> NativeRectangle -> NativeRectangle
cutRectangle percent AlignLeft (Rectangle (P (V2 x y)) (V2 w h)) =
    Rectangle (P (V2 x y)) (V2 w' h)
  where
    w' = percent `percentOf` w
cutRectangle percent AlignRight (Rectangle (P (V2 x y)) (V2 w h)) =
    Rectangle (P (V2 (x + (w - w')) y)) (V2 w' h)
  where
    w' = percent `percentOf` w
cutRectangle percent AlignTop rect =
    transposeRectangle (cutRectangle percent AlignLeft (transposeRectangle rect))
cutRectangle percent AlignBottom rect =
    transposeRectangle (cutRectangle percent
                                     AlignRight
                                     (transposeRectangle rect))

percentOf :: (Integral a, Integral b) => a -> b -> b
percentOf p v = fromIntegral p * v `div` 100

squareRectangle :: NativeRectangle -> NativeRectangle
squareRectangle (Rectangle (P (V2 x y)) (V2 w h)) =
    let minLength = min w h
        deltaW = w - minLength
        deltaH = h - minLength
    in
        Rectangle (P (V2 (x + deltaW `div` 2) (y + deltaH `div` 2)))
                  (V2 (w - deltaW) (h - deltaH))
