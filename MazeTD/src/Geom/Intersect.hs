module Geom.Intersect
    ( rectanglesIntersect
    , intervalsIntersect
    , intersectLineCircle
    , intersectCircleCircle
    , intersectCircleCircle'
    , intersectCircleCircle''
    , predictIntersection
    ) where

import           Common.SdlTypes

import           Math.Solve

rectanglesIntersect :: NativeRectangle -- ^ first rectangle
                    -> NativeRectangle -- ^ second rectangle
                    -> Bool            -- ^ true if rectangles intersect
rectanglesIntersect r1 r2 =
    not (r1 `isAbove` r2 ||
             r2 `isAbove` r1 ||
                 r1 `isLeftOf` r2 || r2 `isLeftOf` r1)
  where
    Rectangle (P (V2 _ y0)) (V2 _ h0) `isAbove` Rectangle (P (V2 _ y1)) _ =
        y0 + h0 < y1
    Rectangle (P (V2 x0 _)) (V2 w0 _) `isLeftOf` Rectangle (P (V2 x1 _)) _ =
        x0 + w0 < x1

intervalsIntersect :: (NativeFloat, NativeFloat) -- ^ first interval
                   -> (NativeFloat, NativeFloat) -- ^ second interval
                   -> Bool                       -- ^ true if intervals intersect
intervalsIntersect a b =
    not (a `isAbove` b || b `isAbove` a)
  where
    (_, a1) `isAbove` (b0, _) =
        a1 < b0

intersectLineCircle :: V2 NativeFloat -- ^ point vector of line 's'
                    -> V2 NativeFloat -- ^ direction vector of line 'u'
                    -> V2 NativeFloat -- ^ center of circle
                    -> NativeFloat    -- ^ radius of circle
                    -> [NativeFloat]  -- ^ 0, 1 or 2 coefficients 'x', intersection points are given by 's + x * u'
intersectLineCircle s u m r =
    let s' = s ^-^ m
        a = u `dot` u
        b = 2 * (s' `dot` u)
        c = s' `dot` s' - r * r
    in
        solveQuadratic a b c

intersectCircleCircle :: V2 NativeFloat   -- ^ center of first circle
                      -> NativeFloat      -- ^ radius of first circle
                      -> V2 NativeFloat   -- ^ center of second circle
                      -> NativeFloat      -- ^ radius of second circle
                      -> [V2 NativeFloat] -- ^ 0, 1 or 2 intersection points
intersectCircleCircle (V2 x1 y1) r1 (V2 x2 y2) r2 =
    let dx = x2 - x1
        dy = y2 - y1
        d = sqrt (dx * dx + dy * dy)
    in
        if d > 0
        then let l = (r1 * r1 - r2 * r2 + d * d) / (2 * d)
                 radicand = r1 * r1 - l * l
             in
                 if radicand >= 0.0
                 then if radicand > 0.0
                      then let h = sqrt radicand
                           in
                               let p1 = V2 (l / d * dx + h / d * dy + x1)
                                           (l / d * dy - h / d * dx + y1)
                                   p2 = V2 (l / d * dx - h / d * dy + x1)
                                           (l / d * dy + h / d * dx + y1)
                               in
                                   [ p1, p2 ]
                      else [ V2 (l / d * dx + x1) (l / d * dy + y1) ]
                 else []
        else []

intersectCircleCircle' :: V2 NativeFloat   -- ^ center of first circle
                       -> NativeFloat      -- ^ radius of first circle
                       -> V2 NativeFloat   -- ^ center of second circle
                       -> NativeFloat      -- ^ radius of second circle
                       -> [V2 NativeFloat] -- ^ empty or 1 intersection point, bisecting the connecting segment between the centers
intersectCircleCircle' c1 r1 c2 r2 =
    let d_2 = quadrance (c1 - c2)
        r = r1 + r2
        r_2 = r * r
    in
        [ (c1 ^+^ c2) ^/ 2
        | d_2 < r_2 ]

intersectCircleCircle'' :: V2 NativeFloat   -- ^ center of first circle
                        -> NativeFloat      -- ^ radius of first circle
                        -> V2 NativeFloat   -- ^ center of second circle
                        -> NativeFloat      -- ^ radius of second circle
                        -> [V2 NativeFloat] -- ^ empty or 1 intersection point, which is the center of the first circle
intersectCircleCircle'' c1 r1 c2 r2 =
    let d_2 = quadrance (c1 - c2)
        r = r1 + r2
        r_2 = r * r
    in
        [ c1
        | d_2 < r_2 ]

{- |

Aim at target S, moving with constant velocity V,
using a gunner G firing projectile with velocity p.

In order to do so, we assume G to be at origin, and translate S accordingly.
Then we set the distances to target and projectile equal at time t.

@
Solve

                   || S + V t ||  =  p t

for t by the following approach:

                               2      2  2
                  || S + V t ||   =  p  t

<=>
             2              2  2      2  2
            S  + 2 S V t + V  t   =  p  t

<=>
       2    2   2              2
     (V  - p ) t  + 2 S V t + S   =  0,


then solve the quadratic equation

                     2
                  a t  + b t + c  =  0

with
                                      2    2
                               a  =  V  - p


                               b  =  2 S V

                                      2
                               c  =  S .

The smallest non-negative solution for t (if any), is used to construct the actual aiming location:

                               A  =  S + V t'
@
-}
predictIntersection :: V2 NativeFloat         -- ^ target location (S)
                    -> V2 NativeFloat         -- ^ target velocity vector (V)
                    -> V2 NativeFloat         -- ^ gunner vector (G)
                    -> NativeFloat            -- ^ projectile velocity (p)
                    -> Maybe (V2 NativeFloat) -- ^ aiming location
predictIntersection s v g p =
    let s' = s ^-^ g
        a = v `dot` v - p * p
        b = 2 * (s' `dot` v)
        c = s' `dot` s'
        ts = [ t
             | t <- solveQuadratic a b c
             , t >= 0 ]
    in
        case sort ts of
            [] -> Nothing
            (t : _) -> Just (s ^+^ v ^* t)
