module Geom.Grid
    ( viewToModel
    , modelToView
    , alignViewPoint
    , alignViewRectangle
    ) where

import           Common.SdlTypes

viewToModel :: NativeDimension -> NativePoint -> NativePoint
viewToModel (V2 gridWidth gridHeight) (P (V2 vx vy)) =
    P (V2 (vx `div` gridWidth) (vy `div` gridHeight))

modelToView :: NativeDimension -> NativePoint -> NativePoint
modelToView (V2 gridWidth gridHeight) (P (V2 mx my)) =
    P (V2 (mx * gridWidth + gridWidth `div` 2) (my * gridHeight + gridHeight `div` 2))

alignViewPoint :: NativeDimension -> NativePoint -> NativePoint
alignViewPoint gridSize =
    modelToView gridSize . viewToModel gridSize

alignViewRectangle :: NativeDimension -> NativePoint -> NativeRectangle
alignViewRectangle gridSize vpos =
    Rectangle (viewToModel gridSize vpos * P gridSize) gridSize
