module Geom.Shape
    ( rectanglesIntersect
    , Shape(..)
    , shapeCenter
    , boundingBox
    , intersectShapes
    ) where

import           Common.SdlTypes

import           Geom.Intersect

data Shape = Segment { start :: NativePoint
                     , end   :: NativePoint
                     }
           | Circle { center :: NativePoint
                    , radius :: NativeInt
                    }

shapeCenter :: Shape -> NativePoint
shapeCenter Segment{start,end} =
    div 2 <$> start ^+^ end
shapeCenter Circle{center} =
    center

boundingBox :: Shape -> NativeRectangle
boundingBox Segment{start = P (V2 x0 y0),end = P (V2 x1 y1)} =
    Rectangle (P (V2 x0' y0')) (V2 w h)
  where
    (x0', x1') = sortPair (x0, x1)
    (y0', y1') = sortPair (y0, y1)
    w = x1' - x0'
    h = y1' - y0'
    sortPair p@(x, y) = if x < y then p else (y, x)
boundingBox Circle{center = P (V2 x0 y0),radius} =
    Rectangle (P (V2 x0' y0')) (V2 d d)
  where
    x0' = x0 - radius
    y0' = y0 - radius
    d = 2 * radius

intersectShapes :: Shape -> Shape -> [NativePoint]
intersectShapes shape1 shape2 =
    if rectanglesIntersect (boundingBox shape1) (boundingBox shape2) then intersect shape1 shape2 else []
  where
    intersect Segment{start,end} Circle{center,radius} =
        let P p0 = fromIntegral <$> start
            P p1 = fromIntegral <$> end
            P c = fromIntegral <$> center
            u = p1 ^-^ p0
            r = fromIntegral radius
            xs = intersectLineCircle p0 u c r
        in
            [ P (round <$> (p0 + s *^ u))
            | s <- case sort xs of
                       [] -> []
                       [ x ] -> [ x
                                | x >= 0.0
                                , x <= 1.0 ]
                       (x1 : x2 : _) -> [ x1
                                        | intervalsIntersect (x1, x2) (0.0, 1.0) ] ]

    intersect Circle{center = center0,radius = radius0} Circle{center = center1,radius = radius1} =
        let P p0 = fromIntegral <$> center0
            r0 = fromIntegral radius0
            P p1 = fromIntegral <$> center1
            r1 = fromIntegral radius1
            s = intersectCircleCircle'' p0 r0 p1 r1
        in
            [ P (round <$> x)
            | x <- s ]

    intersect circle@Circle{} segment@Segment{} =
        intersect segment circle

    intersect _ _ = error "NYI intersect segment/segment"
