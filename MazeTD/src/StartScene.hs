module StartScene ( newStartScene ) where

import           Common.SdlTypes

import           Gfx.Draw
import           Gfx.Input
import           Gfx.Renderable

import           Engine.Scene

import           Widget.Layout
import           Widget.Container
import           Widget.Label
import           Widget.Button

import           Assets
import           GameColors
import           Maze
import           MazeScene

data StartScene = StartScene { gen        :: StdGen
                             , mazeWidth  :: NativeInt
                             , mazeHeight :: NativeInt
                             }

instance Renderable StartScene where
    render = renderStartScene

instance SceneClass StartScene where
    updateScene = updateStartScene
    mousePainter _ = standardMousePainter

newStartScene :: StdGen -> NativeInt -> NativeInt -> SceneMonad SceneState
newStartScene gen mazeWidth mazeHeight =
    (,) <$> widget <*> pure (Scene StartScene { gen, mazeWidth, mazeHeight })
  where
    widget = do
        Assets{assetsFont} <- askUserData
        return $
            newContainer (borderLayout (=% 10) (=% 10) (=% 10) (=% 10))
                         [ newContainer (gridLayout 1 2 (=% 10) (=% 10))
                                        [ newTextLabel assetsFont
                                                       "MazeTD"
                                                       (V2 8 4)
                                        , newContainer (gridLayout 2
                                                                   2
                                                                   (=% 10)
                                                                   (=% 10))
                                                       [ newTextButton 1
                                                                       's'
                                                                       assetsFont
                                                                       "Start"
                                                                       (V2 4 3)
                                                       , newTextLabel assetsFont
                                                                      ""
                                                                      (V2 1 1)
                                                       , newTextButton 2
                                                                       'x'
                                                                       assetsFont
                                                                       "Exit"
                                                                       (V2 4 3)
                                                       , newTextLabel assetsFont
                                                                      ""
                                                                      (V2 1 1)
                                                       ]
                                        ]
                         ]

renderStartScene :: RenderableArgs -> StartScene -> IO ()
renderStartScene args@RenderableArgs{input = input@Input{inputBounds}} _ =
    renderToBackground' 0 True args f
  where
    f renderer = do
        let Assets{assetsBackground0} =
                getUserData input
        drawSprite assetsBackground0 0 0 inputBounds 0.0 white renderer
        return ()

updateStartScene :: StartScene -> SceneMonad SceneCommand
updateStartScene startScene@StartScene{gen,mazeWidth,mazeHeight} = do
    Input{inputGridSize,inputActionIds} <- askInput
    case inputActionIds of
        (1 : _) -> do
            let (seed, gen') = randomR (0, 65535) gen
                maze = genMaze seed mazeWidth mazeHeight
            pure (replaceScene startScene { gen = gen' }) <<>>
                (pushScene <$> newMazeScene maze inputGridSize)
        (2 : _) -> do
            return popScene
        _ -> do
            return (replaceScene startScene)
