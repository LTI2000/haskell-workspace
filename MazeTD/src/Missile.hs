module Missile
    ( newMissile
    ) where

import           Data.Vector                    ( Vector
                                                , empty
                                                , fromList
                                                , singleton
                                                )

import           Common.SdlTypes

import           Gfx.Colors
import           Gfx.Draw
import           Gfx.Input
import           Gfx.Renderable

import           Engine.CollisionMap
import           Engine.Object
import           Engine.Path

import           Assets
import           Explosion
import           Level

data Missile = Missile
    { objId    :: NativeId
    , level    :: Level
    , targetId :: NativeId
    , path     :: HomingPath
    }

instance Renderable Missile where
    render = renderMissile

instance ObjectClass Missile where
    updateCollisionMap cmap Missile { objId, level, path } = addCircle
        cmap
        objId
        Player
        (round <$> homingPathPos path)
        8
        (homingPathVelocity path)
        (missileDamage level)
    updateObject = updateMissile

instance ZOrdered Missile where
    zOrder = const Low

newMissile
    :: NativeId
    -> Level
    -> NativePoint
    -> NativePoint
    -> NativeId
    -> NativeFloat
    -> Object
newMissile objId level from to targetId speed = Object Missile { objId
                                                               , level
                                                               , targetId
                                                               , path
                                                               }
  where
    pos      = fromIntegral <$> from
    velocity = let d = fromIntegral <$> to .-. from in normalize d
    path     = newHomingPath pos velocity speed

renderMissile :: RenderableArgs -> Missile -> IO ()
renderMissile RenderableArgs { input = input@Input { inputGridSize }, renderer } Missile { level, path }
    = do
        let Assets { assetsSprites } = getUserData input
            pos                      = homingPathPos path
            theta                    = homingPathHeading path
        drawSprite assetsSprites
                   (spriteIndex level)
                   spritesMissileY
                   ((round <$> pos) |+| inputGridSize)
                   theta
                   white
                   renderer

updateMissile :: Input -> w -> CollisionMap -> Missile -> (w, Vector Object)
updateMissile Input { inputFrameNo, inputBounds } world cmap missile@Missile { objId }
    = let collisions = getCollisions cmap objId
      in
          if null collisions
              then case updatePos inputBounds cmap missile of
                  Just path' ->
                      let missile' = missile { path = path' }
                      in
                          if quadrance (homingPathVelocity path') < 1.0
                              then
                                  let
                                      theta =
                                          fromIntegral (inputFrameNo `mod` 360)
                                      explosion = newExplosion
                                          (round <$> homingPathPos path')
                                          (32 - 16)
                                          theta
                                          2
                                          True
                                  in
                                      (world, singleton explosion)
                              else (world, singleton (Object missile'))
                  Nothing -> (world, empty)
              else die collisions
  where
    die collisions =
        let theta = fromIntegral (inputFrameNo `mod` 360)
            explosions =
                [ newExplosion cPos (32 - 16) theta 2 True
                | Collision { cPos } <- collisions
                ]
        in  (world, fromList explosions)

updatePos :: NativeRectangle -> CollisionMap -> Missile -> Maybe HomingPath
updatePos bounds cmap Missile { targetId, path } = updateHomingPath
    bounds
    ((fmap . fmap) fromIntegral (getPosition cmap targetId))
    path
