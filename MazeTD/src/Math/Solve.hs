module Math.Solve
    ( solveLinear
    , solveQuadratic
    ) where

import           Common.SdlTypes

{- |
@
                                                         -b
Solve the linear equation a x + b = 0 by calculating x = --
                                                          a
@
-}
solveLinear :: (Fractional x, Eq x)
            => x   -- ^ a
            -> x   -- ^ b
            -> [x] -- ^ solution for x, if a =/= 0
solveLinear a b = [ (-b) / a
                  | a /= 0 ]

{- |
@
                                                                               __________
                                                                              / 2
                                2                                     -b +- \/ b  - 4 a c
Solve the quadratic equation a x  + b x + c = 0 by calculating x    = -------------------
                                                                1,2          2 a
@
-}
solveQuadratic :: (Ord x, Floating x)
               => x   -- ^ a
               -> x   -- ^ b
               -> x   -- ^ c
               -> [x] -- ^ 0 to 2 solution(s) for x
solveQuadratic 0 b c = solveLinear b c
solveQuadratic a b c = solve (-b) (b * b - 4 * a * c) (2 * a)
  where
    solve p discr q
        | discr < 0 = []
        | discr > 0 = [ p' / q
                      | p' <- p +- sqrt discr ]
        | otherwise = [ p / q ]
      where
        x +- y = [ x + y, x - y ]
