module Gfx.Input
    ( Input(..)
    , initialInput
    , updateEvents
    , getUserData
    ) where

import           Common.SdlTypes

data Input = Input { inputFrameNo             :: NativeFrameNo
                   , inputFps                 :: NativeFrameNo
                   , inputBounds              :: NativeRectangle
                   , inputGridSize            :: NativeDimension
                   , inputMousePos            :: Maybe NativePoint
                   , inputLeftButtonPressed   :: Bool
                   , inputLeftButtonDown      :: Bool
                   , inputLeftButtonReleased  :: Bool
                   , inputRightButtonPressed  :: Bool
                   , inputRightButtonDown     :: Bool
                   , inputRightButtonReleased :: Bool
                   , inputWindowClosed        :: Bool
                   , inputKeysPressed         :: [Char]
                   , inputKeysDown            :: [Char]
                   , inputKeysReleased        :: [Char]
                   , inputActionIds           :: [NativeId]
                   , inputUserData            :: Dynamic -- FIXME make private
                   }

initialInput :: NativeRectangle -> NativeDimension -> Dynamic -> Input
initialInput inputBounds inputGridSize inputUserData =
    Input { inputFrameNo = 0
          , inputFps = 0
          , inputBounds
          , inputGridSize
          , inputMousePos = Nothing
          , inputLeftButtonPressed = False
          , inputLeftButtonDown = False
          , inputLeftButtonReleased = False
          , inputRightButtonPressed = False
          , inputRightButtonDown = False
          , inputRightButtonReleased = False
          , inputWindowClosed = False
          , inputKeysPressed = []
          , inputKeysDown = []
          , inputKeysReleased = []
          , inputActionIds = []
          , inputUserData
          }

updateEvents :: Input -> NativeFrameNo -> NativeFrameNo -> IO Input
updateEvents input inputFrameNo inputFps = do
    handleEvents input { inputFrameNo
                           , inputFps
                           , inputLeftButtonPressed = False
                           , inputLeftButtonReleased = False
                           , inputRightButtonPressed = False
                           , inputRightButtonReleased = False
                           , inputKeysPressed = []
                           , inputKeysReleased = []
                           , inputActionIds = []
                           } <$> pollEvents

handleEvents :: Input -> [Event] -> Input
handleEvents = foldl handleEvent

handleEvent :: Input -> Event -> Input
handleEvent input (Event _ (WindowLostMouseFocusEvent (WindowLostMouseFocusEventData _))) =
    input { inputMousePos = Nothing }

handleEvent input (Event _ (MouseMotionEvent (MouseMotionEventData _ _ _ p _))) =
    input { inputMousePos = Just $ fromIntegral <$> p }

handleEvent input (Event _ (MouseButtonEvent (MouseButtonEventData _ Pressed _ ButtonLeft _ p))) =
    input { inputMousePos = Just $ fromIntegral <$> p, inputLeftButtonPressed = True, inputLeftButtonDown = True }
handleEvent input (Event _ (MouseButtonEvent (MouseButtonEventData _ Released _ ButtonLeft _ p))) =
    input { inputMousePos = Just $ fromIntegral <$> p, inputLeftButtonDown = False, inputLeftButtonReleased = True }

handleEvent input (Event _ (MouseButtonEvent (MouseButtonEventData _ Pressed _ ButtonRight _ p))) =
    input { inputMousePos = Just $ fromIntegral <$> p, inputRightButtonPressed = True, inputRightButtonDown = True }
handleEvent input (Event _ (MouseButtonEvent (MouseButtonEventData _ Released _ ButtonRight _ p))) =
    input { inputMousePos = Just $ fromIntegral <$> p, inputRightButtonDown = False, inputRightButtonReleased = True }

handleEvent input@Input{inputKeysPressed,inputKeysDown} (Event _ (KeyboardEvent (KeyboardEventData _ Pressed False (Keysym _ (Keycode c) _)))) =
    let cMin = (fromIntegral . ord) (minBound :: Char)
        cMax = (fromIntegral . ord) (maxBound :: Char)
    in
        if cMin <= c && c <= cMax
        then let k = (chr . fromIntegral) c
             in
                 input { inputKeysPressed = k : inputKeysPressed, inputKeysDown = k : inputKeysDown }
        else input

handleEvent input@Input{inputKeysDown,inputKeysReleased} (Event _ (KeyboardEvent (KeyboardEventData _ Released _ (Keysym _ (Keycode c) _)))) =
    let k = chr $ fromIntegral c
    in
        input { inputKeysDown = inputKeysDown \\ [ k ], inputKeysReleased = k : inputKeysReleased }

handleEvent input (Event _ (WindowClosedEvent (WindowClosedEventData _))) =
    input { inputWindowClosed = True }

handleEvent input _ = input

getUserData :: (Typeable a) => Input -> a
getUserData Input{inputUserData} =
    castUserData inputUserData

-- private
castUserData :: (Typeable a) => Dynamic -> a
castUserData userData = fromDyn userData (error "castUserData failed!")
