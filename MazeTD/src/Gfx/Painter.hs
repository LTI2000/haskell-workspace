module Gfx.Painter
    ( Painter(..)
    , emptyPainter
    , withColor
    , rectPainter
    , filledRectPainter
    , textPainter
    , spritePainter
    ) where

import           Common.SdlTypes

import           Gfx.Atlas
import           Gfx.Draw
import           Gfx.Renderable

newtype Painter = Painter { paint :: RenderableArgs -> NativeRectangle -> IO () }

emptyPainter :: Painter
emptyPainter = Painter { paint }
  where
    paint _ _ = return ()

instance Semigroup Painter where
  p1 <> p2 = Painter { paint }
      where
        paint args bounds = do
            doPaint p1 args bounds
            doPaint p2 args bounds

instance Monoid Painter where
    mempty = emptyPainter

doPaint :: Painter -> RenderableArgs -> NativeRectangle -> IO ()
doPaint = paint

withColor :: NativeRgba -> Painter -> Painter
withColor color painter =
    Painter { paint }
  where
    paint args@RenderableArgs{renderer} bounds = do
        rendererDrawColor renderer $=
            color
        doPaint painter args bounds

rectPainter :: Painter
rectPainter = Painter { paint }
  where
    paint RenderableArgs{renderer} bounds = do
        drawRect renderer (Just bounds)

filledRectPainter :: Painter
filledRectPainter = Painter { paint }
  where
    paint RenderableArgs{renderer} bounds = do
        fillRect renderer (Just bounds)

textPainter :: Atlas -> String -> NativeDimension -> Painter
textPainter atlas text size =
    Painter { paint }
  where
    paint RenderableArgs{renderer} bounds = do
        color <- get (rendererDrawColor renderer)
        drawText atlas text bounds size color renderer

spritePainter :: Atlas -> NativeInt -> NativeInt -> NativeFloat -> NativeRgba -> Painter
spritePainter atlas atlasX atlasY theta rgba =
    Painter { paint }
  where
    paint RenderableArgs{renderer} bounds = do
        drawSprite atlas atlasX atlasY bounds theta rgba renderer
