module Gfx.Draw
    ( clearAll
    , drawText
    , (|+|)
    , drawSprite
    , drawPath
    ) where

import           Data.Vector     ( fromList, imapM_ )

import           Common.SdlTypes

import           Geom.Geom

import           Gfx.Atlas
import           Gfx.Colors

clearAll :: Renderer -> NativeRgba -> IO ()
clearAll renderer color = do
    rendererDrawColor renderer $= color
    clear renderer

drawText :: Atlas -> String -> NativeRectangle -> NativeDimension -> NativeRgba -> Renderer -> IO ()
drawText Atlas{texture,tileSize = tileSize@(V2 tileW _)} text bounds s@(V2 sx _) rgba renderer = do
    mapM_ (\offset -> drawText' (pos + offset) black) offsets
    drawText' pos rgba
  where
    textSize = s * tileSize * V2 (fromIntegral (length text)) 1
    (Rectangle pos _) = alignRectangle textSize bounds
    offsets = P <$> delete (V2 0 0) (V2 <$> [(-1) .. 1] <*> [(-1) .. 1])

    drawText' :: NativePoint -> NativeRgba -> IO ()
    drawText' (P (V2 x y)) color = do
        textureRgbaMod texture color
        imapM_ (\i c -> do
                    let c' = fromIntegral (ord c)
                        x' = c' `mod` 16
                        y' = c' `div` 16
                        i' = fromIntegral i
                    copy renderer
                         texture
                         (Just (Rectangle (P (V2 x' y' * tileSize)) tileSize))
                         (Just (Rectangle (P (V2 (x + sx * tileW * i') y)) (s * tileSize))))
               (fromList text)

(|+|) :: NativePoint -> NativeDimension -> NativeRectangle
(|+|) = flip centerRectangle

drawSprite :: Atlas           -- ^ atlas
           -> NativeInt       -- ^ atlasX
           -> NativeInt       -- ^ atlasY
           -> NativeRectangle -- ^ dst
           -> NativeFloat     -- ^ theta
           -> NativeRgba      -- ^ trgba
           -> Renderer        -- ^ renderer
           -> IO ()
drawSprite Atlas{texture,tileSize} atlasX atlasY dst theta rgba renderer = do
    textureRgbaMod texture rgba
    copyEx renderer texture (Just src) (Just dst) theta Nothing (V2 False False)
  where
    src = Rectangle (P (V2 atlasX atlasY * tileSize)) tileSize

drawPath :: NativeFloat -> (Renderer -> NativePoint -> IO ()) -> [NativePoint] -> Renderer -> IO ()
drawPath _ _ [] _ = do
    return ()
drawPath _ f [ pnt ] renderer = do
    f renderer pnt
drawPath spacing f (pnt0 : pnts@(pnt1 : _)) renderer = do
    mapM_ (f renderer) (pathPoints spacing)
    drawPath spacing f pnts renderer
  where
    pathPoints :: NativeFloat -> [NativePoint]
    pathPoints v = let P p0 = fromIntegral <$> pnt0
                       P p1 = fromIntegral <$> pnt1
                       r = p1 - p0
                       n = norm r
                   in
                       if n > 0.0 then go p0 r 0.0 n else [ pnt0 ]
      where
        go :: V2 NativeFloat -> V2 NativeFloat -> NativeFloat -> NativeFloat -> [NativePoint]
        go p r s n
            | s < n = P (floor <$> (p + r ^* s ^/ n)) : go p r (s + v) n
            | otherwise = []

-- private
textureRgbaMod :: Texture -> NativeRgba -> IO ()
textureRgbaMod texture (V4 r g b a) = do
    textureColorMod texture $= V3 r g b
    textureAlphaMod texture $= a
