{-# LANGUAGE FlexibleInstances #-}

module Gfx.Renderable
    ( RenderableArgs(..)
    , updateInput
    , Renderable(..)
    , renderToBackground
    , renderToBackground'
    , getRandomAngle
    ) where

import           Common.SdlTypes

import           Gfx.Background
import           Gfx.Input

data RenderableArgs = RenderableArgs { input      :: Input
                                     , background :: Background
                                     , getRandom  :: (Int, Int) -> IO Int
                                     , renderer   :: Renderer
                                     }

updateInput :: Input -> RenderableArgs -> RenderableArgs
updateInput input renderableArgs =
    renderableArgs { input = input }

class Renderable r where
    render :: RenderableArgs -> r -> IO ()

instance (Foldable m, Renderable r) => Renderable (m r) where
    render args = mapM_ (render args)

renderToBackground :: (Renderable r) => NativeId -> Bool -> RenderableArgs -> r -> IO ()
renderToBackground bgId once args renderable =
    renderToBackground' bgId once args (\_ -> render args renderable)

renderToBackground' :: NativeId -> Bool -> RenderableArgs -> (Renderer -> IO ()) -> IO ()
renderToBackground' bgId once args@RenderableArgs{renderer} f = do
    (texture, new) <- getBackgroundTexture bgId args
    when (not once || new) $ do
        rendererRenderTarget renderer $= Just texture
        f renderer
        rendererRenderTarget renderer $= Nothing
    when once $ do
        copy renderer texture Nothing Nothing

getBackgroundTexture :: NativeId -> RenderableArgs -> IO (Texture, Bool)
getBackgroundTexture bgId RenderableArgs{input = Input{inputBounds = Rectangle _ size},background} = do
    mval <- getBackground background bgId
    case mval of
        Nothing -> do
            texture <- newBackgroundTexture background size
            setBackground background bgId texture
            return (texture, True)
        Just texture -> do
            return (texture, False)

getRandomAngle :: RenderableArgs -> IO NativeFloat
getRandomAngle args = fromIntegral <$> getRandom args (0, 359)
