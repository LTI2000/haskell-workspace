module Gfx.Background
    ( Background
    , newBackground
    , newBackgroundTexture
    , getBackground
    , setBackground
    , clearBackground
    ) where

import           Data.IORef      ( IORef, newIORef, readIORef, writeIORef )

import           Common.SdlTypes

import           Gfx.GfxContext

data Background = Background { gfxCtx :: NativeContext
                             , texRef :: IORef (Maybe (NativeId, Texture))
                             }

newBackground :: NativeContext -> IO Background
newBackground gfxCtx = do
    texRef <- newIORef Nothing
    return Background { gfxCtx, texRef }

newBackgroundTexture :: Background -> NativeDimension -> IO Texture
newBackgroundTexture Background{gfxCtx} =
    newTargetTexture gfxCtx

getBackground :: Background -> NativeId -> IO (Maybe Texture)
getBackground Background{texRef} bgId = do
    mval <- readIORef texRef
    case mval of
        Just (bgId', texture) -> do
            if bgId == bgId' then return (Just texture) else return Nothing
        Nothing -> do
            return Nothing

setBackground :: Background -> NativeId -> Texture -> IO ()
setBackground background bgId texture =
    writeBackground background (Just (bgId, texture))

clearBackground :: Background -> IO ()
clearBackground background =
    writeBackground background Nothing

writeBackground :: Background -> Maybe (NativeId, Texture) -> IO ()
writeBackground Background{texRef} mval = do
    oldMval <- readIORef texRef
    case oldMval of
        Just (_, texture) -> do
            destroyTexture texture
        Nothing -> do
            return ()
    writeIORef texRef mval
