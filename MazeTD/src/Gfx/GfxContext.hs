module Gfx.GfxContext
    ( newGfxContext
    , destroyGfxContext
    , copySurface
    , newStaticTexture
    , newTargetTexture
    ) where

import           Data.Text       ( Text )

import           Common.SdlTypes

newGfxContext :: WindowConfig -> RendererConfig -> NativeDimension -> Text -> IO NativeContext
newGfxContext w r s c = do
    initializeAll
    window <- createWindow c w
    renderer <- createRenderer window (-1) r
    rendererLogicalSize renderer $= Just s
    rendererDrawBlendMode renderer $= BlendAlphaBlend
    return (window, renderer)

destroyGfxContext :: NativeContext -> IO ()
destroyGfxContext (window, renderer) = do
    destroyRenderer renderer
    destroyWindow window
    quit

newSurface :: NativeContext -> NativeDimension -> IO Surface
newSurface (window, _) size = do
    pixelFormat <- getWindowPixelFormat window
    putStrLn $ "newSurface: windowPixelFormat = " ++ show pixelFormat
    pixelFormat' <- alternatePixelFormat
    putStrLn $ "newSurface: alternatePixelFormat = " ++ show pixelFormat'
    createRGBSurface size pixelFormat'
  where
    alternateBytesPerPixel :: NativeInt
    alternateBytesPerPixel =
        4

    alternatePixelFormat :: IO PixelFormat
    alternatePixelFormat = do
        masksToPixelFormat (8 * alternateBytesPerPixel) alternateMasks
      where
        alternateMasks = V4 0x00FF0000 0x0000FF00 0x000000FF 0xFF000000

copySurface :: NativeContext -> Surface -> IO Surface
copySurface gfxCtx pngSurface = do
    if False
        then do
            textureSurface <- newSurface gfxCtx =<< surfaceDimensions pngSurface
            _ <- surfaceBlit pngSurface Nothing textureSurface Nothing
            freeSurface pngSurface
            return textureSurface
        else do
            return pngSurface

newStaticTexture :: NativeContext -> Surface -> IO Texture
newStaticTexture (_, renderer) surface = do
    texture <- createTextureFromSurface renderer surface
    freeSurface surface
    return texture

newTargetTexture :: NativeContext -> NativeDimension -> IO Texture
newTargetTexture (window, renderer) size = do
    pixelFormat <- getWindowPixelFormat window
    texture <- createTexture renderer pixelFormat TextureAccessTarget size
    textureInfo <- queryTexture texture
    putStrLn $ "Target Texture Info: " ++ show textureInfo
    return texture
