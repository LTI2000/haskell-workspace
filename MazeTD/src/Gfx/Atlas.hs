module Gfx.Atlas
    ( Atlas(..)
    , atlasSize
    ) where

import           Common.SdlTypes

data Atlas = Atlas { texture     :: Texture
                   , textureSize :: NativeDimension
                   , tileSize    :: NativeDimension
                   }

atlasSize :: Atlas -> NativeDimension
atlasSize Atlas{textureSize = V2 textureWidth textureHeight,tileSize = V2 tileWidth tileHeight} =
    V2 (textureWidth `div` tileWidth) (textureHeight `div` tileHeight)
