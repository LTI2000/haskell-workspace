module Gfx.Colors
    ( -- ** Color constants
      -- * Basic colors
      black
    , white
    , red
    , green
    , blue
      -- * Flat UI colors
    , turquoise
    , greenSea
    , emerald
    , nephritis
    , peterRiver
    , belizeHole
    , amethyst
    , wisteria
    , wetAsphalt
    , midnightBlue
    , sunFlower
    , orange
    , carrot
    , pumpkin
    , alizarin
    , pomegranate
    , clouds
    , silver
    , concrete
    , asbestos
      -- ** color modification
    , rgbMod
    , rgbaMod
    , aMod
    ) where

import           Common.SdlTypes

black, white, red, green, blue :: NativeRgba
black = V4 0x00 0x00 0x00 255

white = V4 0xFF 0xFF 0xFF 255

red = V4 0xFF 0x00 0x00 255

green = V4 0x00 0xFF 0x00 255

blue = V4 0x00 0x00 0xFF 255

turquoise, greenSea, emerald, nephritis, peterRiver, belizeHole, amethyst, wisteria, wetAsphalt, midnightBlue, sunFlower, orange, carrot, pumpkin, alizarin, pomegranate, clouds, silver, concrete, asbestos :: NativeRgba
turquoise = V4 0x1A 0xBC 0x9C 255

greenSea = V4 0x16 0xA0 0x85 255

emerald = V4 0x2E 0xCC 0x71 255

nephritis = V4 0x27 0xAE 0x60 255

peterRiver = V4 0x34 0x98 0xDB 255

belizeHole = V4 0x29 0x80 0xB9 255

amethyst = V4 0x9B 0x59 0xB6 255

wisteria = V4 0x8E 0x44 0xAD 255

wetAsphalt = V4 0x34 0x49 0x5E 255

midnightBlue = V4 0x2C 0x3E 0x50 255

sunFlower = V4 0xF1 0xC4 0x0F 255

orange = V4 0xF3 0x9C 0x12 255

carrot = V4 0xE6 0x7E 0x22 255

pumpkin = V4 0xD3 0x54 0x00 255

alizarin = V4 0xE7 0x4C 0x3C 255

pomegranate = V4 0xC0 0x39 0x2B 255

clouds = V4 0xEC 0xF0 0xF1 255

silver = V4 0xBD 0xC3 0xC7 255

concrete = V4 0x95 0xA5 0xA6 255

asbestos = V4 0x7F 0x8C 0x8D 255

rgbMod :: NativeRgba -> NativeRgb -> NativeRgba
rgbMod (V4 cr cg cb ca) (V3 mr mg mb) =
    V4 (modulate cr mr) (modulate cg mg) (modulate cb mb) ca

rgbaMod :: NativeRgba -> NativeRgba -> NativeRgba
rgbaMod (V4 cr cg cb ca) (V4 mr mg mb ma) =
    V4 (modulate cr mr) (modulate cg mg) (modulate cb mb) (modulate ca ma)

aMod :: NativeRgba -> NativeColorComponent -> NativeRgba
aMod (V4 cr cg cb ca) ma =
    V4 cr cg cb (modulate ca ma)

modulate :: NativeColorComponent -> NativeColorComponent -> NativeColorComponent
modulate c m = let c' = fromIntegral c :: Word32
                   m' = fromIntegral m :: Word32
               in
                   fromIntegral (c' * m' `div` 255)
