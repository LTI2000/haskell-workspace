module MazeScene ( newMazeScene ) where

import           Data.Vector      ( fromList )

import           Common.SdlTypes

import           Geom.Geom
import           Geom.Grid

import           Gfx.Atlas
import           Gfx.Draw
import           Gfx.Input
import           Gfx.Renderable
import           Gfx.Painter

import           Engine.Scene
import           Engine.Object

import           Widget.Layout
import           Widget.Container
import           Widget.Label
import           Widget.Button
import           Widget.AutoHide

import           Assets
import           GameColors
import           Maze
import           MazeWalker
import           Level
import           WorldData
import           PauseScene
import           HeadQuarters
import           CreepSpawner
import           TowerSpawner

data MazeScene = MazeScene { world  :: World
                           , maze   :: Maze
                           , paused :: Bool
                           }

instance Renderable MazeScene where
    render = renderMazeScene

instance SceneClass MazeScene where
    updateScene = updateMazeScene
    mousePainter _ = standardMousePainter <> fpsPainter

newtype RenderableMaze = RenderableMaze { getMaze :: Maze }

instance Renderable RenderableMaze where
    render = renderMaze

newMazeScene :: Maze -> NativeDimension -> SceneMonad SceneState
newMazeScene maze gridSize =
    (,) <$> widget <*> pure (Scene scene)
  where
    (_upperLeft, upperRight, lowerRight, lowerLeft) =
        corners (shrink 1 (mazeBounds maze))
    objects = fromList [ newHeadQuarters 0 (Level 0) gridSize upperRight
                       , newHeadQuarters 1 (Level 1) gridSize lowerRight
                       , newHeadQuarters 2 (Level 2) gridSize lowerLeft
                       , newCreepSpawner maze
                       , newTowerSpawner maze
                       ]
    spacing = 8
    widget = do
        Assets{assetsFont,assetsSprites} <- askUserData
        let color = amethyst
            areaMod = 102
            borderMod = 204
            cannonPainter level focus =
                let areaHighlight = if focus then 51 else 0
                    borderHighlight = if focus then 51 else 0
                in
                    withColor (rgbaMod color (areaMod + areaHighlight))
                              filledRectPainter <>
                        withColor (rgbMod color (borderMod + borderHighlight))
                                  rectPainter <>
                        spritePainter assetsSprites
                                      (spriteIndex level)
                                      spritesCannonY
                                      0.0
                                      (if focus
                                       then white
                                       else rgbaMod white 153)
            mortarPainter level focus =
                let areaHighlight = if focus then 51 else 0
                    borderHighlight = if focus then 51 else 0
                in
                    withColor (rgbaMod color (areaMod + areaHighlight))
                              filledRectPainter <>
                        withColor (rgbMod color (borderMod + borderHighlight))
                                  rectPainter <>
                        spritePainter assetsSprites
                                      (spriteIndex level)
                                      spritesMortarY
                                      0.0
                                      (if focus
                                       then white
                                       else rgbaMod white 153)
            launcherPainter level focus =
                let areaHighlight = if focus then 51 else 0
                    borderHighlight = if focus then 51 else 0
                in
                    withColor (rgbaMod color (areaMod + areaHighlight))
                              filledRectPainter <>
                        withColor (rgbMod color (borderMod + borderHighlight))
                                  rectPainter <>
                        spritePainter assetsSprites
                                      (spriteIndex level)
                                      spritesLauncherY
                                      0.0
                                      (if focus
                                       then white
                                       else rgbaMod white 153)
        return $
            newContainer stackLayout
                         [ newContainer (transformLayout (cutRectangle 5
                                                                       AlignRight >>>
                                                              cutRectangle 20
                                                                           AlignTop))
                                        [ newAutoHide spacing
                                                      AlignRight
                                                      (newContainer stackLayout
                                                                    [ newLabel (withColor (rgbaMod color
                                                                                                   areaMod)
                                                                                          filledRectPainter <>
                                                                                    withColor (rgbMod color
                                                                                                      borderMod)
                                                                                              rectPainter)
                                                                    , newContainer (borderLayout (=! spacing)
                                                                                                 (=! spacing)
                                                                                                 (=! spacing)
                                                                                                 (=! spacing))
                                                                                   [ newContainer (gridLayout 1
                                                                                                              3
                                                                                                              (=! spacing)
                                                                                                              (=! spacing))
                                                                                                  [ (newContainer (transformLayout squareRectangle) .
                                                                                                         pure) w
                                                                                                  | w <- [ newTextButton 1
                                                                                                                         'p'
                                                                                                                         assetsFont
                                                                                                                         "||"
                                                                                                                         (V2 1
                                                                                                                             1)
                                                                                                         , newTextButton 2
                                                                                                                         'r'
                                                                                                                         assetsFont
                                                                                                                         "|>"
                                                                                                                         (V2 1
                                                                                                                             1)
                                                                                                         , newTextButton 3
                                                                                                                         'f'
                                                                                                                         assetsFont
                                                                                                                         ">>"
                                                                                                                         (V2 1
                                                                                                                             1)
                                                                                                         ] ]
                                                                                   ]
                                                                    ])
                                        ]
                         , newContainer (transformLayout (cutRectangle 10
                                                                       AlignBottom >>>
                                                              cutRectangle 60
                                                                           AlignLeft))
                                        [ newAutoHide spacing
                                                      AlignBottom
                                                      (newContainer stackLayout
                                                                    [ newLabel (withColor (rgbaMod color
                                                                                                   areaMod)
                                                                                          filledRectPainter <>
                                                                                    withColor (rgbMod color
                                                                                                      borderMod)
                                                                                              rectPainter)
                                                                    , newContainer (borderLayout (=! spacing)
                                                                                                 (=! spacing)
                                                                                                 (=! spacing)
                                                                                                 (=! spacing))
                                                                                   [ newContainer (gridLayout 7
                                                                                                              1
                                                                                                              (=! spacing)
                                                                                                              (=! spacing))
                                                                                                  [ (newContainer (transformLayout squareRectangle) .
                                                                                                         pure) w
                                                                                                  | w <- [ newButton 10
                                                                                                                     '1'
                                                                                                                     (cannonPainter (Level 0))
                                                                                                         , newButton 20
                                                                                                                     '2'
                                                                                                                     (cannonPainter (Level 1))
                                                                                                         , newButton 30
                                                                                                                     '3'
                                                                                                                     (cannonPainter (Level 2))
                                                                                                         , newButton 40
                                                                                                                     '4'
                                                                                                                     (mortarPainter (Level 0))
                                                                                                         , newButton 50
                                                                                                                     '5'
                                                                                                                     (mortarPainter (Level 1))
                                                                                                         , newButton 60
                                                                                                                     '6'
                                                                                                                     (mortarPainter (Level 2))
                                                                                                         , newButton 70
                                                                                                                     '7'
                                                                                                                     (launcherPainter (Level 2))
                                                                                                         ] ]
                                                                                   ]
                                                                    ])
                                        ]
                         ]
    world = newWorld objects 2000
    scene = MazeScene { world, maze, paused = False }

renderMazeScene :: RenderableArgs -> MazeScene -> IO ()
renderMazeScene args MazeScene{world = world@World{objects},maze = maze@Maze{mazeId}} = do
    renderToBackground mazeId True args RenderableMaze { getMaze = maze }
    render args objects
    renderHud args world

renderMaze :: RenderableArgs -> RenderableMaze -> IO ()
renderMaze args@RenderableArgs{input = input@Input{inputGridSize},renderer} RenderableMaze{getMaze = maze} = do
    let Assets{assetsTerrain} = getUserData input
    drawMazeBackground args assetsTerrain
    let pnts = modelToView inputGridSize <$> walkMaze (newMazeWalker maze)
    drawMaze args assetsTerrain pnts renderer

drawMazeBackground :: RenderableArgs -> Atlas -> IO ()
drawMazeBackground args@RenderableArgs{input = Input{inputBounds = Rectangle (P (V2 x0 y0)) (V2 w h),inputGridSize},renderer} atlas = do
    clearAll renderer mazeColor
    forM_ [ (128, pomegranate, 20), (64, amethyst, 10), (32, greenSea, 5) ]
          (\(enlarge, color, alpha) ->
               forM_ ([0 .. 1000] :: [Int])
                     (\_ -> do
                          displacement <- (fmap . fmap) fromIntegral
                                                        (V2 <$> getRandom args
                                                                          ( fromIntegral x0
                                                                          , fromIntegral (x0 +
                                                                                              w -
                                                                                              1)
                                                                          )
                                                            <*> getRandom args
                                                                          ( fromIntegral y0
                                                                          , fromIntegral (y0 +
                                                                                              h -
                                                                                              1)
                                                                          ))
                          theta <- getRandomAngle args
                          atlasX <- fromIntegral <$> getRandom args (0, 1)
                          drawSprite atlas
                                     atlasX
                                     terrainDirtY
                                     (P displacement |+|
                                          (inputGridSize + V2 enlarge enlarge))
                                     theta
                                     (aMod (rgbMod color 200) alpha)
                                     renderer))

drawMaze :: RenderableArgs -> Atlas -> [NativePoint] -> Renderer -> IO ()
drawMaze args@RenderableArgs{input = Input{inputGridSize}} atlas pnts renderer = do
    drawPath 16 (f (-24) (aMod black 16)) pnts renderer
    drawPath 8 (f 0 (aMod black 18)) pnts renderer
    drawPath 4 (f 20 (aMod black 20)) pnts renderer
  where
    f reduce rgba rend pnt = do
        displacement <- (fmap . fmap) fromIntegral
                                      (V2 <$> getRandom args (-2, 2)
                                          <*> getRandom args (-2, 2))
        theta <- getRandomAngle args
        atlasX <- fromIntegral <$> getRandom args (0, 1)
        drawSprite atlas
                   atlasX
                   terrainDirtY
                   ((pnt + P displacement) |+|
                        (inputGridSize - V2 reduce reduce))
                   theta
                   rgba
                   rend

renderHud :: RenderableArgs -> World -> IO ()
renderHud RenderableArgs{input = input@Input{inputBounds = Rectangle (P (V2 x0 y0)) (V2 w _)},renderer} world = do
    let Assets{assetsFont} = getUserData input
    let money = getMoney world
        moneyString = "  MONEY: " ++ show money
        health = getHealth world
        healthString = "  HEALTH: " ++ show health
    drawText assetsFont
             moneyString
             (Rectangle (P (V2 x0 y0)) (V2 (w `div` 2) 24))
             (V2 3 2)
             hudMoneyColor
             renderer
    drawText assetsFont
             healthString
             (Rectangle (P (V2 (x0 + (w `div` 2)) y0)) (V2 (w `div` 2) 24))
             (V2 3 2)
             hudHealthColor
             renderer

updateMazeScene :: MazeScene -> SceneMonad SceneCommand
updateMazeScene mazeScene@MazeScene{world,paused} = do
    input <- askInput
    case input of
        Input{inputRightButtonReleased = True} ->
            pushScene <$> newPauseScene
        Input{inputActionIds} -> do
            let paused' = if 1 `elem` inputActionIds then not paused else paused
                world' = updateWorld input paused'
            return $
                replaceScene mazeScene { world = world', paused = paused' }
  where
    updateWorld :: Input -> Bool -> World
    updateWorld input False =
        let world' = modifyHealth (const 0) world
            (world'', objects') = updateObjects input world' (objects world')
        in
            world'' { objects = objects' }
    updateWorld _ True = world
