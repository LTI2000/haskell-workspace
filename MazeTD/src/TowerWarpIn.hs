module TowerWarpIn ( newTowerWarpIn ) where

import           Data.Vector         ( Vector, singleton )

import           Common.SdlTypes

import           Gfx.Colors
import           Gfx.Draw
import           Gfx.Input
import           Gfx.Renderable

import           Engine.CollisionMap
import           Engine.Object

import           Assets
import           Level

data TowerWarpIn = TowerWarpIn { objId         :: NativeId
                               , mazeId        :: NativeId
                               , level         :: Level
                               , pos           :: NativePoint
                               , dist          :: NativeFloat
                               , maxDist       :: NativeFloat
                               , spritesTowerY :: NativeInt
                               , newTower      :: NativeId
                                               -> Level
                                               -> NativePoint
                                               -> Object
                               }

instance Renderable TowerWarpIn where
    render = renderTowerWarpIn

instance ObjectClass TowerWarpIn where
    updateCollisionMap = const
    updateObject = updateTowerWarpIn

instance ZOrdered TowerWarpIn where
    zOrder = const Highest

newTowerWarpIn :: NativeInt                                    -- ^ sprite Y position
               -> (NativeId -> Level -> NativePoint -> Object) -- ^ tower constructor function
               -> NativeId                                     -- ^ object ID
               -> NativeId                                     -- ^ maze ID
               -> Level                                        -- ^ tower level
               -> NativePoint                                  -- ^ tower location
               -> Object
newTowerWarpIn spritesTowerY newTower objId mazeId level pos =
    Object TowerWarpIn { objId
                       , mazeId
                       , level
                       , pos
                       , dist
                       , maxDist
                       , spritesTowerY
                       , newTower
                       }
  where
    dist = 8
    maxDist = 9

renderTowerWarpIn :: RenderableArgs -> TowerWarpIn -> IO ()
renderTowerWarpIn args@RenderableArgs{input = input@Input{inputGridSize},renderer} TowerWarpIn{mazeId,level,pos,dist,maxDist,spritesTowerY} = do
    let Assets{assetsSprites} = getUserData input
    renderToBackground' mazeId
                        False
                        args
                        (drawSprite assetsSprites
                                    2
                                    spritesGroundPlateY
                                    (pos |+| inputGridSize)
                                    0.0
                                    (V4 255 alpha alpha 255))
    drawSprite assetsSprites
               (spriteIndex level)
               spritesTowerY
               (pos |+| size')
               0.0
               rgba
               renderer
  where
    alpha = floor ((maxDist - dist) / maxDist * 255)
    rgba = aMod white alpha
    scale = round (exp dist)
    size' = inputGridSize + V2 scale scale

updateTowerWarpIn :: Input
                  -> w
                  -> CollisionMap
                  -> TowerWarpIn
                  -> (w, Vector Object)
updateTowerWarpIn _input world _cmap towerWarpIn@TowerWarpIn{objId,level,pos,dist,newTower} =
    let dist' = dist - 0.25
    in
        if dist' > 0
        then let towerWarpIn' = towerWarpIn { dist = dist' }
             in
                 (world, singleton (Object towerWarpIn'))
        else (world, singleton (newTower objId level pos))
