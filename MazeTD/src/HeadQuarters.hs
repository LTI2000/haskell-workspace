module HeadQuarters ( newHeadQuarters ) where

import           Data.Vector         ( Vector, singleton )

import           Common.SdlTypes

import           Geom.Grid

import           Gfx.Colors
import           Gfx.Draw
import           Gfx.Input
import           Gfx.Renderable

import           Engine.CollisionMap
import           Engine.World
import           Engine.Object

import           Assets
import           Level
import           WorldData
import           Explosion

data HeadQuarters = HeadQuarters { objId     :: NativeId
                                 , level     :: Level
                                 , hitpoints :: NativeInt
                                 , pos       :: NativePoint
                                 }

instance Renderable HeadQuarters where
    render = renderHeadQuarters

instance ObjectClass HeadQuarters where
    updateCollisionMap cmap HeadQuarters{objId,hitpoints,pos} =
        addCircle cmap objId Player pos 15 (V2 0.0 0.0) hitpoints
    updateObject = updateHeadQuarters

instance ZOrdered HeadQuarters where
    zOrder = const Highest

newHeadQuarters :: NativeId -> Level -> NativeDimension -> NativePoint -> Object
newHeadQuarters objId level gridSize pos =
    Object HeadQuarters { objId
                        , level
                        , hitpoints
                        , pos = modelToView gridSize pos
                        }
  where
    hitpoints = headQuartersHitpoints level

renderHeadQuarters :: RenderableArgs -> HeadQuarters -> IO ()
renderHeadQuarters RenderableArgs{input = input@Input{inputGridSize},renderer} HeadQuarters{level,pos} = do
    let Assets{assetsBuildings} =
            getUserData input
    drawSprite assetsBuildings
               (spriteIndex level)
               buildingsHeadQuarterY
               (pos |+| (2 *^ inputGridSize))
               0.0
               white
               renderer

updateHeadQuarters :: WorldClass w
                   => Input
                   -> w
                   -> CollisionMap
                   -> HeadQuarters
                   -> (w, Vector Object)
updateHeadQuarters Input{inputFrameNo} world cmap headQuarters@HeadQuarters{objId,hitpoints,pos} =
    let collisions = getCollisions cmap objId
        hitpoints' = damage hitpoints collisions
    in
        if hitpoints' > 0
        then let headQuarters' = Object headQuarters { hitpoints = hitpoints' }
                 world' = modifyHealth (+ hitpoints') world
             in
                 (world', singleton headQuarters')
        else die
  where
    damage hp [] = hp
    damage hp (Collision{cDmg} : cs) =
        damage (hp - cDmg) cs

    die = let theta = fromIntegral (inputFrameNo `mod` 360)
              explosion = newExplosion pos (32 - (-32)) theta 1 True
          in
              (world, singleton explosion)
