module WorldData
    ( World(..)
    , newWorld
    , modifyMoney
    , getMoney
    , requestMoney
    , modifyHealth
    , getHealth
    ) where

import           Data.Vector     ( Vector )

import           Common.SdlTypes

import           Engine.World
import           Engine.Object

newWorld :: Vector Object -> NativeMoney -> World
newWorld objects money =
    world
  where
    worldData = toDyn WorldData { money, health = 0 }
    world = World { objIdSerial = 10, objects, worldData }

-- FIXME make generic and move to Engine.World
data World = World { objIdSerial :: NativeId
                   , objects     :: Vector Object
                   , worldData   :: Dynamic
                   }

instance WorldClass World where
    nextObjId world@World{objIdSerial} =
        let objId' = objIdSerial + 1
        in
            (objId', world { objIdSerial = objId' })
    modifyUserData f world@World{worldData} =
        let worldData' = f worldData
        in
            (worldData', world { worldData = worldData' })
    maybeModifyUserData f world@World{worldData} =
        case f worldData of
            Just worldData' -> Just (worldData', world { worldData = worldData' })
            Nothing -> Nothing

data WorldData = WorldData { money  :: NativeMoney
                           , health :: NativeInt
                           }

modifyMoney :: (WorldClass w) => (NativeMoney -> NativeMoney) -> w -> w
modifyMoney f world = snd (modifyUserData (mapWorldData modify) world)
  where
    modify :: WorldData -> WorldData
    modify worldData@WorldData{money} =
        worldData { money = f money }

getMoney :: (WorldClass w) => w -> NativeMoney
getMoney = money . getWorldData

requestMoney :: (WorldClass w) => NativeMoney -> w -> Maybe w
requestMoney amount world =
    case maybeModifyUserData (maybeMapWorldData modify) world of
        Just (_, world') -> Just world'
        Nothing -> Nothing
  where
    modify worldData@WorldData{money}
        | money >= amount = Just worldData { money = money - amount }
        | otherwise = Nothing

modifyHealth :: (WorldClass w) => (NativeInt -> NativeInt) -> w -> w
modifyHealth f world = snd (modifyUserData (mapWorldData modify) world)
  where
    modify :: WorldData -> WorldData
    modify worldData@WorldData{health} =
        worldData { health = f health }

getHealth :: (WorldClass w) => w -> NativeInt
getHealth = health . getWorldData

-- private
getWorldData :: (WorldClass w) => w -> WorldData
getWorldData world = castWorldData (fst (modifyUserData id world))

mapWorldData :: (WorldData -> WorldData) -> Dynamic -> Dynamic
mapWorldData f dyn = toDyn (f (castWorldData dyn))

maybeMapWorldData :: (WorldData -> Maybe WorldData) -> Dynamic -> Maybe Dynamic
maybeMapWorldData f dyn =
    toDyn <$> f (castWorldData dyn)

castWorldData :: Dynamic -> WorldData
castWorldData dyn = fromDyn dyn (error "castWorldData failed!")
