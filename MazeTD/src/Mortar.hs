module Mortar ( newMortar ) where

import           Data.Vector         ( Vector, fromList, singleton )

import           Common.SdlTypes

import           Geom.Geom

import           Gfx.Colors
import           Gfx.Draw
import           Gfx.Input
import           Gfx.Renderable

import           Engine.CollisionMap
import           Engine.Heading
import           Engine.Cooldown
import           Engine.World
import           Engine.Object

import           Assets
import           Level
import           Bomb

data Mortar = Mortar { objId    :: NativeId
                     , level    :: Level
                     , pos      :: NativePoint
                     , heading  :: Heading
                     , cooldown :: Cooldown
                     }

instance Renderable Mortar where
    render = renderMortar

instance ObjectClass Mortar where
    updateCollisionMap cmap Mortar{objId,level,pos} =
        addRadarCircle cmap objId owner pos radius (Just (bombSpeed level)) -- Nothing
      where
        owner = Player
        radius = mortarRange level
    updateObject = updateMortar

instance ZOrdered Mortar where
    zOrder = const Medium

newMortar :: NativeId -> Level -> NativePoint -> Object
newMortar objId level pos =
    Object Mortar { objId
                  , level
                  , pos
                  , heading = newHeading (mortarHeadingSpeed level)
                                         (mortarHeadingTolerance level)
                                         0
                  , cooldown = newCooldown rate
                  }
  where
    rate = mortarCooldownRate level

renderMortar :: RenderableArgs -> Mortar -> IO ()
renderMortar RenderableArgs{input = input@Input{inputGridSize},renderer} Mortar{level,pos,heading} = do
    let Assets{assetsSprites} = getUserData input
    drawSprite assetsSprites
               (spriteIndex level)
               spritesMortarY
               (pos |+| inputGridSize)
               (getHeadingAngle heading)
               white
               renderer

updateMortar :: (WorldClass w)
             => Input
             -> w
             -> CollisionMap
             -> Mortar
             -> (w, Vector Object)
updateMortar _input world cmap mortar@Mortar{objId,level,pos,heading,cooldown} =
    case sortBy (flip compare `on` cDmg) (getCollisions cmap objId)    -- FIXME extract duplicate code below
     of
        [] -> let (_, heading') = updateHeading heading Nothing
                  (_, cooldown') = updateCooldown cooldown
                  mortar' = mortar { heading = heading', cooldown = cooldown' }
              in
                  (world, singleton (Object mortar'))
        Collision{cPos} : _ -> let (aimed, heading') = updateHeading heading
                                                                     (Just (bearing pos
                                                                                    cPos))
                                   (ready, cooldown') = updateCooldown cooldown
                                   mortar' = mortar { heading = heading'
                                                    , cooldown = cooldown'
                                                    }
                               in
                                   if aimed && ready
                                   then let (objId', world') = nextObjId world
                                            bomb = newBomb objId'
                                                           level
                                                           pos
                                                           (translatePoint pos
                                                                           heading'
                                                                           100.0)
                                                           (bombSpeed level)
                                            cooldown'' = resetCooldown cooldown'
                                            mortar'' = mortar' { cooldown = cooldown''
                                                               }
                                        in
                                            ( world'
                                            , fromList [ bomb, Object mortar'' ]
                                            )
                                   else (world, singleton (Object mortar'))
