module SplashDamage ( newSplashDamage ) where

import           Data.Vector         ( Vector, singleton )

import           Common.SdlTypes

import           Gfx.Input
import           Gfx.Renderable

import           Engine.CollisionMap
import           Engine.Object

import           Explosion

data SplashDamage = SplashDamage { objId  :: NativeId
                                 , pos    :: NativePoint
                                 , radius :: NativeInt
                                 , dmg    :: NativeInt
                                 }

instance Renderable SplashDamage where
    render _ _ = return ()

instance ObjectClass SplashDamage where
    updateCollisionMap cmap SplashDamage{objId,pos,radius,dmg} =
        addCircle cmap objId Player pos radius (V2 0.0 0.0) dmg
    updateObject = updateSplashDamage

instance ZOrdered SplashDamage where
    zOrder = const Lowest

newSplashDamage :: NativeId -> NativePoint -> NativeInt -> NativeInt -> Object
newSplashDamage objId pos radius dmg =
    Object SplashDamage { objId, pos, radius, dmg }

updateSplashDamage :: Input
                   -> w
                   -> CollisionMap
                   -> SplashDamage
                   -> (w, Vector Object)
updateSplashDamage Input{inputFrameNo} world _cmap SplashDamage{pos,radius} =
    let theta = fromIntegral (inputFrameNo `mod` 360)
        explosion = newExplosion pos (2 * radius) theta 1 True
    in
        (world, singleton explosion)
