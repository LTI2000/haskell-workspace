{-# LANGUAGE FlexibleContexts #-}

module Maze
    ( Maze(..)
    , genMaze
    ) where

import           Prelude         ( undefined )
import           Data.Array      ( (!), Array, bounds )
import           Data.Array.ST   ( mapArray, newArray, readArray, runSTArray, writeArray )

import           Common.SdlTypes

import           Geom.Geom

data Maze = Maze { mazeId        :: NativeId
                 , mazeBounds    :: NativeRectangle
                 , getMazeSpotAt :: NativePoint -> Bool
                 , forMaze_      :: (NativePoint -> Bool -> IO ()) -> IO ()
                 }

data Spot = Wall | BreadCrumb | Clear
    deriving (Eq)

allDirs :: [V2 NativeInt]
allDirs = take 4 (iterate perp (unit _x))

choose :: [a] -> StdGen -> (Maybe a, StdGen)
choose [] gen = (Nothing, gen)
choose as gen = let (i, gen') = randomR (0, length as - 1) gen
                in
                    (Just (as !! i), gen')

genMazeArray :: StdGen -> NativeInt -> NativeInt -> Array NativePoint Bool
genMazeArray stdGen w h =
    runSTArray $ do
        array <- newArray mazeBounds Wall
        maze <- genLoop startPos stdGen array
        mapArray (/= Clear) maze
  where
    step :: NativeInt
    step = 2
    startPos = P (V2 1 1)
    mazeBounds = (P (V2 0 0), P (step *^ V2 w h))
    genLoop pos gen maze = do
        unvisitedDirs <- filterM (testMaze maze Wall pos step) (allowedDirs pos)
        let (unvisitedDir, gen') = choose unvisitedDirs gen
        pos' <- case unvisitedDir of
                    Just dir -> do
                        walkMaze maze BreadCrumb pos step dir
                    _ -> do
                        returnDirs <- filterM (testMaze maze BreadCrumb pos 1) allDirs
                        case returnDirs of
                            (dir : _) -> do
                                walkMaze maze Clear pos step dir
                            _ -> do
                                walkMaze maze Clear pos (0 * step) undefined
        if pos' == startPos
            then do
                writeArray maze startPos Clear
                return maze
            else do
                genLoop pos' gen' maze
    allowedDirs pos = filter (inMaze pos) allDirs
    inMaze (P pos) dir = inRange mazeBounds (P (pos + step *^ dir))
    walkMaze maze spot (P pos) dist dir
        | dist > 0 = do
              writeArray maze (P pos) spot
              walkMaze maze spot (P (pos + dir)) (dist - 1) dir
        | otherwise = do
              writeArray maze (P pos) spot
              return (P pos)
    testMaze maze spot (P pos) dist dir = do
        readArray maze (P (pos + dist *^ dir)) >>= return . (== spot)

genMaze :: Int -> NativeInt -> NativeInt -> Maze
genMaze seed w h = let gen = mkStdGen seed
                       array = genMazeArray gen w h
                   in
                       Maze { mazeId = fromIntegral seed
                            , mazeBounds = rectangle (bounds array)
                            , getMazeSpotAt = (array !)
                            , forMaze_ = \f -> do
                                let (P (V2 x0 y0), P (V2 x1 y1)) =
                                        bounds array
                                forM_ [y0 .. y1] $
                                    \y -> do
                                        forM_ [x0 .. x1] $
                                            \x -> do
                                                let pos = P (V2 x y)
                                                let spot = array ! pos
                                                f pos spot
                            }
