module PauseScene ( newPauseScene ) where

import           Common.SdlTypes

import           Gfx.Draw
import           Gfx.Input
import           Gfx.Renderable

import           Engine.Scene

import           Widget.Layout
import           Widget.Container
import           Widget.Label
import           Widget.Button

import           Assets
import           GameColors

data PauseScene = PauseScene

instance Renderable PauseScene where
    render = renderPauseScene

instance SceneClass PauseScene where
    updateScene = updatePauseScene
    mousePainter _ = standardMousePainter

newPauseScene :: SceneMonad SceneState
newPauseScene = (,) <$> widget <*> pure (Scene PauseScene)
  where
    widget = do
        Assets{assetsFont} <- askUserData
        return $
            newContainer (borderLayout (=% 10) (=% 10) (=% 10) (=% 10))
                         [ newContainer (gridLayout 1 2 (=% 10) (=% 10))
                                        [ newTextLabel assetsFont
                                                       "GAME PAUSED"
                                                       (V2 8 4)
                                        , newContainer (gridLayout 2
                                                                   2
                                                                   (=% 10)
                                                                   (=% 10))
                                                       [ newTextButton 1
                                                                       'c'
                                                                       assetsFont
                                                                       "Continue"
                                                                       (V2 4 3)
                                                       , newTextLabel assetsFont
                                                                      ""
                                                                      (V2 1 1)
                                                       , newTextButton 2
                                                                       'x'
                                                                       assetsFont
                                                                       "Exit"
                                                                       (V2 4 3)
                                                       , newTextLabel assetsFont
                                                                      ""
                                                                      (V2 1 1)
                                                       ]
                                        ]
                         ]

renderPauseScene :: RenderableArgs -> PauseScene -> IO ()
renderPauseScene RenderableArgs{renderer} _ = do
    clearAll renderer pauseSceneColor

updatePauseScene :: PauseScene -> SceneMonad SceneCommand
updatePauseScene pauseScene = do
    Input{inputActionIds} <- askInput
    case inputActionIds of
        (1 : _) -> do
            return popScene
        (2 : _) -> do
            return (popScene <> popScene)
        _ -> do
            return (replaceScene pauseScene)
