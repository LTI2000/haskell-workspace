module CreepSpawner ( newCreepSpawner ) where

import           Data.Vector         ( Vector, fromList, singleton )

import           Common.SdlTypes

import           Gfx.Input
import           Gfx.Renderable

import           Engine.CollisionMap
import           Engine.Cooldown
import           Engine.World
import           Engine.Object

import           Maze
import           Level
import           WorldData
import           Creep

data CreepSpawner = CreepSpawner { maze     :: Maze
                                 , cooldown :: Cooldown
                                 }

instance Renderable CreepSpawner where
    render _ _ = return ()

instance ObjectClass CreepSpawner where
    updateCollisionMap = const
    updateObject = updateCreepspawner

instance ZOrdered CreepSpawner where
    zOrder = const High

newCreepSpawner :: Maze -> Object
newCreepSpawner maze = Object CreepSpawner { maze, cooldown = newCooldown rate }
  where
    rate = 75

_updateCreepspawner :: (WorldClass w)
                    => Input
                    -> w
                    -> CollisionMap
                    -> CreepSpawner
                    -> (w, Vector Object)
_updateCreepspawner Input{inputGridSize} world _cmap creepSpawner@CreepSpawner{maze,cooldown} =
    let (ready, cooldown') = updateCooldown cooldown
        creepSpawner' = creepSpawner { cooldown = cooldown' }
    in
        if ready
        then let money = getMoney world + 10
                 mlevel = calcLevel money
             in
                 case mlevel of
                     Nothing -> (world, singleton (Object creepSpawner'))
                     Just level -> let (objId, world') = nextObjId world
                                       creep = newCreep maze
                                                        objId
                                                        level
                                                        inputGridSize
                                       cooldown'' = resetCooldown cooldown'
                                       creepSpawner'' = creepSpawner' { cooldown = cooldown''
                                                                      }
                                   in
                                       ( world'
                                       , fromList [ Object creepSpawner''
                                                  , creep
                                                  ]
                                       )
        else (world, singleton (Object creepSpawner'))
  where
    calcLevel :: NativeMoney -> Maybe Level
    calcLevel money
        | money >= 30 = Just (Level 2)
        | money >= 20 = Just (Level 1)
        | money >= 10 = Just (Level 0)
        | otherwise = Nothing

updateCreepspawner :: (WorldClass w)
                   => Input
                   -> w
                   -> CollisionMap
                   -> CreepSpawner
                   -> (w, Vector Object)
updateCreepspawner Input{inputFrameNo,inputGridSize} world _cmap creepSpawner@CreepSpawner{maze,cooldown} =
    let (ready, cooldown') = updateCooldown cooldown
        creepSpawner' = creepSpawner { cooldown = cooldown' }
    in
        if ready
        then let (objId, world') = nextObjId world
                 level = Level (fromIntegral (inputFrameNo `mod` 3))
                 creep = newCreep maze objId level inputGridSize
                 cooldown'' = resetCooldown cooldown'
                 creepSpawner'' = creepSpawner' { cooldown = cooldown'' }
             in
                 (world', fromList [ Object creepSpawner'', creep ])
        else (world, singleton (Object creepSpawner'))
