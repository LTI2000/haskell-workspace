module Resource.ImageLoader ( newPngSurface ) where

import           Codec.Picture        ( DynamicImage(..), Image(..), convertRGBA8, decodeImage )
import           Data.Vector.Storable ( thaw )

import           Common.SdlTypes

newPngSurface :: NativeBinary -> IO Surface
newPngSurface = either error imageToSurface . decodeImage
  where
    imageToSurface :: DynamicImage -> IO Surface
    imageToSurface image = do
        pixelData <- thaw imageData
        createRGBSurfaceFrom pixelData (V2 w h) (w * pngBytesPerPixel) =<<
            pngPixelFormat
      where
        Image{imageWidth,imageHeight,imageData} =
            convertRGBA8 image
        w = fromIntegral imageWidth
        h = fromIntegral imageHeight

pngBytesPerPixel :: NativeInt
pngBytesPerPixel = 4

pngPixelFormat :: IO PixelFormat
pngPixelFormat = do
    masksToPixelFormat (8 * pngBytesPerPixel) pngMasks
  where
    pngMasks = V4 0x000000FF 0x0000FF00 0x00FF0000 0xFF000000
