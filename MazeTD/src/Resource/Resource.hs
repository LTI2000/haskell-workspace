module Resource.Resource ( embedImage ) where

import           Language.Haskell.TH.Syntax ( Exp, Q )
import           System.FilePath            ( (<.>), (</>), FilePath )
import           Data.FileEmbed             ( embedFile )

embedImage :: FilePath -> Q Exp
embedImage name = embedFile ("assets" </> name <.> "png")
