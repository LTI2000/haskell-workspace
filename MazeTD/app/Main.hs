module Main ( main ) where

import           Options
import           Data.Proxy

import           Common.SdlTypes hiding ((.))

import           Engine.GameLoop
import           Engine.Scene

import           Assets
import           StartScene

caption :: String
caption = "MazeTD"

fullscreen :: Bool
fullscreen = False

windowConfig :: WindowConfig
windowConfig = WindowConfig { windowBorder = True
                            , windowHighDPI = False
                            , windowInputGrabbed = False
                            , windowMode = if fullscreen
                                           then FullscreenDesktop
                                           else Windowed
                            , windowGraphicsContext = NoGraphicsContext
                            , windowPosition = Wherever
                            , windowResizable = False
                            , windowInitialSize = V2 0 0
                            , windowVisible = True
                            }

rendererConfig :: RendererConfig
rendererConfig = RendererConfig { rendererType = AcceleratedVSyncRenderer
                                , rendererTargetTexture = True
                                }

gridSize :: V2 NativeInt
gridSize = V2 32 32

displaySize :: V2 NativeInt -> NativeDimension
displaySize mazeSize = ((2 *^ mazeSize ^+^ 1) ^+^ V2 0 0) * gridSize

startScene :: V2 NativeInt -> StdGen -> SceneMonad SceneState
startScene (V2 mazeWidth mazeHeight) gen =
    newStartScene gen mazeWidth mazeHeight

main' :: MainOptions -> [String] -> IO ()
main' opts _leftOverArgs = do
    let windowMode = if optFullscreen opts then FullscreenDesktop else Windowed
        mazeSize = case optMazeSize opts of
            Tiny -> V2 5 3
            Small -> V2 11 7
            Medium -> V2 17 9
            Large -> V2 19 11
    gen <- newStdGen
    startGame caption
              windowConfig { windowMode
                           , windowInitialSize = displaySize mazeSize
                           }
              rendererConfig
              (displaySize mazeSize)
              gridSize
              (fmap toDyn . createAssets)
              (startScene mazeSize gen)

data MazeSize = Tiny | Small | Medium | Large
    deriving (Bounded, Enum, Show)

data MainOptions = MainOptions { optMazeSize   :: MazeSize
                               , optFullscreen :: Bool
                               }

instance Options MainOptions where
    defineOptions =
      MainOptions <$> defineOption (optionType_enum "")
                         (\o -> o { optionShortFlags = [ 's' ]
                                  , optionLongFlags = [ "mazesize" ]
                                  , optionDefault = Medium
                                  , optionDescription = "The size of the maze. " ++ showEnum (Proxy :: Proxy MazeSize)
                                  })
                  <*> defineOption optionType_bool
                         (\o -> o { optionShortFlags = [ 'f' ]
                                  , optionLongFlags = [ "fullscreen" ]
                                  , optionDescription = "Whether to run in fullscreen mode."
                                  })

showEnum :: (Enum a, Bounded a, Show a) => Proxy a -> String
showEnum p = "Possible values: " ++
    intercalate ", " (show <$> [min' p .. max' p]) ++ "."
  where
    min' :: (Bounded a) => Proxy a -> a
    min' _ = minBound
    max' :: (Bounded a) => Proxy a -> a
    max' _ = maxBound

main :: IO ()
main = runCommand main'
