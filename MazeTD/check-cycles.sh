# check if Gfx references Engine (should no be the case)
grep -r "import.*Engine\." src/Gfx && (echo " Gfx references Engine" ; exit 1)

# check if Gfx references Widget (should no be the case)
grep -r "import.*Widget\." src/Gfx && (echo " Gfx references Widget" ; exit 1)

# check if Widget references Engine (should no be the case)
grep -r "import.*Engine\." src/Widget && (echo " Widget references Engine" ; exit 1)

# check if Widget references Gfx (ok)
grep -r "import.*Gfx\." src/Widget || exit 0

# check if Engine references Gfx (ok)
grep -r "import.*Gfx\." src/Engine || exit 0

# check if Engine references Widget (ok)
grep -r "import.*Widget\." src/Engine || exit 0
