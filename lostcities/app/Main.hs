{-
   Lost Cities card game: Main module
   Pedro Vasconcelos, 2009
   pbv@dcc.fc.up.pt
-}
module Main where
import GUI
import Graphics.UI.WX hiding (when)
import System.Environment
import System.Random
import Control.Monad(when)

-- application starts here
-- use any command line args to set random number seed
main :: IO ()
main = do args <- getArgs
          when (length args>0) $
               do let s = sum (map fromEnum (concat args))
                  putStrLn ("Random number seed: "++show s)
                  setStdGen (mkStdGen s)
          start gui
