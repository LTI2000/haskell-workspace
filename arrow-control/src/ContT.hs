{-# LANGUAGE NoImplicitPrelude #-}

module ContT where

import           Monad
import           ResultContinuation

newtype ContT a = ContT (ResultContinuation a -> Void)

callCont :: (ContT a, ResultContinuation a) -> Void
callCont (ContT ka, ra) =
    ka ra

instance Functor ContT

instance Applicative ContT where
    pure (a) = ContT (\ra -> callResult (ra, a))

instance Monad ContT where
    bind (ka, f) = ContT (\rb -> callCont ( ka
                                          , handle ( \e -> callError (rb, e)
                                                   , \a -> callCont (f (a), rb)
                                                   )
                                          ))

throwError :: Exception -> ContT a
throwError (e) = ContT (\ra -> callError (ra, e))

catchError :: (ContT a, Exception -> ContT a) -> ContT a
catchError (ka, h) = ContT (\ra -> callCont ( ka
                                            , handle ( \e -> callCont ( h (e)
                                                                      , ra
                                                                      )
                                                     , \a -> callResult (ra, a)
                                                     )
                                            ))
