{-# LANGUAGE NoImplicitPrelude #-}

module ResultContinuation where

import           Prelude ( IO, String )

import           Monad
import           Either

type Void = IO ()

type Exception = String

newtype ResultContinuation a = ResultContinuation (Either Exception a -> Void)

type Computation a b = (a, ResultContinuation b) -> Void

callContinuation :: (ResultContinuation a, Either Exception a) -> Void
callContinuation (ResultContinuation ra, ea) =
    ra ea

callError :: (ResultContinuation a, Exception) -> Void
callError (ra, e) = callContinuation (ra, error (e))

callResult :: (ResultContinuation a, a) -> Void
callResult (ra, a) = callContinuation (ra, pure (a))

handle :: (Exception -> Void, a -> Void) -> ResultContinuation a
handle (left, right) = ResultContinuation (\ea -> eitherVoid (ea, left, right))

eitherVoid :: (Either Exception a, Exception -> Void, a -> Void) -> Void
eitherVoid (ea, left, right) =
    either (left, right, ea)

contraMap :: (ResultContinuation a, b -> a) -> ResultContinuation b
contraMap (ra, f) = ResultContinuation (\eb -> eitherVoid ( eb
                                                          , \e -> callError ( ra
                                                                            , e
                                                                            )
                                                          , \b -> callResult ( ra
                                                                             , (f b)
                                                                             )
                                                          ))

contraMapE :: (ResultContinuation a, b -> Either Exception a)
           -> ResultContinuation b
contraMapE (ra, f) = ResultContinuation (\eb -> eitherVoid ( eb
                                                           , \e -> callError ( ra
                                                                             , e
                                                                             )
                                                           , \b -> callContinuation ( ra
                                                                                    , (f b)
                                                                                    )
                                                           ))
