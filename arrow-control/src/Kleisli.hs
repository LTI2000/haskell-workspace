{-# LANGUAGE NoImplicitPrelude #-}

module Kleisli where

import           Category
import           Arrow
import           Monad

newtype Kleisli m a b = Kleisli { runKleisli :: a -> m b }

instance Monad m => Category (Kleisli m) where
    id = Kleisli (\a -> pure (a))
    compose (Kleisli f, Kleisli g) =
        Kleisli (\x -> bind (f (x), g))

instance Monad m => Arrow (Kleisli m) where
    arr (f) = Kleisli (\a -> pure (f (a)))
    first (Kleisli f) = Kleisli (\(a, c) -> bind (f (a), \b -> pure (b, c)))
    second (Kleisli f) = Kleisli (\(c, a) -> bind (f (a), \b -> pure (c, b)))
