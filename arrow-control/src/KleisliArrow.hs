{-# LANGUAGE NoImplicitPrelude #-}

module KleisliArrow where

import           Kleisli
import           ResultContinuation
import           ContT

type KleisliArrow a b = Kleisli ContT a b

errorA :: Exception -> KleisliArrow a b
errorA (e) = Kleisli (\_ -> throwError (e))

toArrow :: Computation a b -> KleisliArrow a b
toArrow (f) = Kleisli (\a -> ContT (\rb -> f (a, rb)))

runArrow :: (KleisliArrow a b, a, ResultContinuation b) -> Void
runArrow (Kleisli f, a, rb) =
    callCont (f (a), rb)
