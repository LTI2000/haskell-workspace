{-# LANGUAGE NoImplicitPrelude #-}

module Main where

import           Prelude            ( (+), (++), IO, Int, Show(show), String
                                    , putStrLn )

import           Category
import           Arrow
import           ResultContinuation
import           KleisliArrow
import           Either

println :: String -> IO ()
println = putStrLn

toString :: (Show a) => a -> String
toString = show

zero :: Int
zero = 0

succ :: Computation Int Int
succ (i, ki) = callResult (ki, i + 1)

succA :: KleisliArrow Int Int
succA = toArrow (succ)

main :: IO ()
main = do
    runArrow (arr (id), zero, ki)
    runArrow (arr (id), zero, contraMap (ki, \i -> i + 42))
    runArrow (arr (id), zero, contraMapE (ki, \i -> Right (i + 42)))
    runArrow (arr (id), zero, contraMapE (ki, \_ -> Left ("nope")))
    runArrow (succA, zero, ki)
    runArrow (compose (succA, arr (id)), zero, ki)
    runArrow (compose (succA, succA), zero, ki)
    runArrow (compose (errorA ("e1"), succA), zero, ki)
    runArrow (compose (succA, errorA ("e2")), zero, ki)
    runArrow (compose (errorA ("e1"), errorA ("e2")), zero, ki)
  where
    ki :: ResultContinuation Int
    ki = handle ( \e -> (do
                             println ("error: " ++ e)
                             println (""))
                , \i -> (do
                             println (toString (i))
                             println (""))
                )
