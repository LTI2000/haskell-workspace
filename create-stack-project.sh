#!/bin/bash

set -eu

PROJECT=${1?" project name missing!"}
RESOLVER='lts-10.3'
AUTHOR_FIRST_NAME='Timo'
AUTHOR_LAST_NAME='Koepke'
AUTHOR_EMAIL='timo.koepke@gmail.com'
GITLAB_USERNAME='LTI2000'
YEAR=$(date +%Y)
CATEGORY='Research'

#pushd create-stack-template
#./create-stack-template.sh
#popd #create-stack-template

# stack new ${PROJECT} create-stack-template/stack-template

cp -r create-stack-template/template/ ${PROJECT}/
pushd ${PROJECT}

mv {{name}}.cabal ${PROJECT}.cabal

find . -type f -print0 | xargs -0 sed -i 's/Copyright {{author-name}}{{\^author-name}}Author name here{{\/author-name}} (c) 2018/Copyright {{copyright}}/g'
find . -type f -print0 | xargs -0 sed -i 's@{{\^.*/.*}}@@g'
find . -type f -print0 | xargs -0 sed -i 's/{{author-name}}/'${AUTHOR_FIRST_NAME}' '${AUTHOR_LAST_NAME}'/g'
find . -type f -print0 | xargs -0 sed -i 's/{{author-email}}/'${AUTHOR_EMAIL}'/g'
find . -type f -print0 | xargs -0 sed -i 's/{{gitlab-username}}/'${GITLAB_USERNAME}'/g'
find . -type f -print0 | xargs -0 sed -i 's/{{copyright}}/'${AUTHOR_FIRST_NAME}' '${AUTHOR_LAST_NAME}' '[${AUTHOR_EMAIL}]' (c) '${YEAR}' /g'
find . -type f -print0 | xargs -0 sed -i 's/{{category}}/'${CATEGORY}'/g'
find . -type f -print0 | xargs -0 sed -i 's/{{name}}/'${PROJECT}'/g'


cat <<EOF > stack.yaml
# ${PROJECT}
resolver: ${RESOLVER}
packages:
- .
flags: {}
extra-package-dbs: []
EOF

chmod 755 *.sh
# ./test.sh
./build_and_exec.sh
# ./profile.sh
popd #${PROJECT}
