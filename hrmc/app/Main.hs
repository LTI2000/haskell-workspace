module Main
  ( main
  ) where

import           Control.Monad                  ( forM_ )

import           Converter
import           Optimizer
import           Parser
import           TAC_Dump

main :: IO ()
main = do
  --  let src = "{ outbox( - ( - inbox( ) ) ) ; }"
  --  let src = "{ auto x = 0; auto y = inbox(); auto z = -0; outbox(x); }"
  --  let src = "while(true) { auto x = inbox(); { auto x = inbox(); {/**/} outbox(x); } outbox(x); }"
  --  let src = "while(true) { auto x = inbox(); if (x < 0) {} else outbox(x); }"
  let src = "while(0 * 1 < 1 * 2 && true != false || ! true) outbox(inbox());"
  putStrLn "Source:"
  putStrLn $ "  " ++ src

  case parseStmt src of
    Left parseError -> do
      print parseError
    Right stmt' -> do
      case runConvert stmt' of
        Left converterError -> do
          putStrLn $ converterError
        Right stmt -> do
          putStrLn $ "Ast:"
          putStrLn $ "  " ++ show stmt

          --let code = dump stmt
          --putStrLn "Code:"
          --forM_ code print

          let optimizedStmt = optimize stmt
          putStrLn "Optimized Ast:"
          putStrLn $ "  " ++ show optimizedStmt

          let optimizedCode = dump optimizedStmt
          putStrLn "Optimized Code:"
          forM_ optimizedCode print
