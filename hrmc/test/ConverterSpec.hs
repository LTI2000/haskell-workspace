module ConverterSpec
  ( spec
  ) where

import           Test.Hspec
import           Test.Hspec.QuickCheck

import           Control.Monad.Except

import           Ast
import           Converter
import           UntypedAst



spec :: Spec
spec = do



  describe "convertStmt" $ do

    it "converts an empty statement" $ do
      (runExcept . convertStmt) (Seq' []) `shouldBe` Right Nop



    it "converts a nested empty statement" $ do
      (runExcept . convertStmt)
          (Seq' [Seq' [], Seq' [Seq' [Seq' [], Seq' [], Seq' []]], Seq' []])
        `shouldBe` Right
                     (Seq
                       Nop
                       (Seq (Seq (Seq Nop (Seq Nop (Seq Nop Nop))) Nop)
                            (Seq Nop Nop)
                       )
                     )



  describe "convertExpr" $ do

    prop "converts CVals'" $ do
      \a -> (runExcept . convertExpr) (CVal' a) `shouldBe` Right (CVal a)



  describe "convertBool" $ do

    prop "converts CBools'" $ do
      \a -> (runExcept . convertBool) (CBool' a) `shouldBe` Right (CBool a)

    it "fails to convert a logical operator with value operands" $ do
      (runExcept . convertBool) (BinOp' And' (CVal' 0) (CVal' 1))
        `shouldBe` Left "not a boolean expression: CVal' 0"
