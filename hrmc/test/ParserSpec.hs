module ParserSpec
  ( spec
  ) where

import           Test.Hspec
import           Test.Hspec.QuickCheck
import           Test.QuickCheck

import           Parser                         ( parseStmt )
import           UntypedAst

spec :: Spec
spec = do

  describe "parseStmt" $ do

    it "parses an inbox expression" $ do
      parseStmt "auto x = inbox();" `shouldBe` Right (Assign' "x$0" Inbox')

    it "parses an outbox statement" $ do
      parseStmt "outbox(2 * inbox());"
        `shouldBe` Right (Outbox' (BinOp' Times' (CVal' 2) Inbox'))

    it "parses an empty statement" $ do
      parseStmt ";" `shouldBe` Right (Seq' [])

    it "parses an empty block" $ do
      parseStmt "{}" `shouldBe` Right (Seq' [])

    prop "parses integer constants" $ do
      \s ->
        let e = parseStmt ("auto x = " ++ intText s ++ ";") in e `shouldBe` e

    prop "parses boolean constants" $ do
      \s ->
        let e = parseStmt ("while(" ++ boolText s ++ ") {}") in e `shouldBe` e

    it "parses an if statement" $ do
      parseStmt "if(true) outbox(1); else { outbox(0); }" `shouldBe` Right
        (If' (CBool' True) (Outbox' (CVal' 1)) (Seq' [Outbox' (CVal' 0)]))

    it "parses a while statement" $ do
      parseStmt "while(false) {}"
        `shouldBe` Right (While' (CBool' False) (Seq' []))

    it "parses a do .. while statement" $ do
      parseStmt "do {} while(true)"
        `shouldBe` Right (DoWhile' (Seq' []) (CBool' True))

    it "reports type errors" $ do
      show (parseStmt "do {} while(1 || false)")
        `shouldBe` "Left \"-\" (line 1, column 24):\nunexpected illegal type: Illegal Bool Val"



newtype IntSource = IntSource { intText :: String }
instance Show IntSource where
  show = intText
instance Semigroup IntSource where
  s1 <> s2 = IntSource (intText s1 <> intText s2)
instance Monoid IntSource where
  mempty = IntSource ""

intSource :: Int -> IntSource
intSource i = IntSource (show i)

parensIntSource :: IntSource -> IntSource
parensIntSource s = IntSource "(" <> s <> IntSource ")"

uPlusIntSource :: IntSource -> IntSource
uPlusIntSource s = IntSource "+(" <> s <> IntSource ")"

uMinusIntSource :: IntSource -> IntSource
uMinusIntSource s = IntSource "-(" <> s <> IntSource ")"

binOpSource :: IntSource -> String -> IntSource -> IntSource
binOpSource s1 op s2 = s1 <> IntSource op <> s2

instance Arbitrary IntSource where
  arbitrary = sized value
   where
    value 0 = intSource <$> choose (0, 9)
    value n = oneof
      [ parensIntSource <$> subvalue2
      , uPlusIntSource <$> subvalue2
      , uMinusIntSource <$> subvalue2
      , binOpSource <$> subvalue <*> elements ["*", "/", "+", "-"] <*> subvalue
      ]
     where
      subvalue  = value (n `div` 2)
      subvalue2 = value (n - 1)


newtype BoolSource = BoolSource { boolText :: String }
instance Show BoolSource where
  show = boolText
instance Semigroup BoolSource where
  s1 <> s2 = BoolSource (boolText s1 <> boolText s2)
instance Monoid BoolSource where
  mempty = BoolSource ""

boolSource :: Bool -> BoolSource
boolSource True  = BoolSource "true"
boolSource False = BoolSource "false"

parensBoolSource :: BoolSource -> BoolSource
parensBoolSource s = BoolSource "(" <> s <> BoolSource ")"

notBoolSource :: BoolSource -> BoolSource
notBoolSource s = BoolSource "!(" <> s <> BoolSource ")"

logOpSource :: BoolSource -> String -> BoolSource -> BoolSource
logOpSource s1 op s2 = s1 <> BoolSource (" " <> op <> " ") <> s2

relOpSource :: IntSource -> String -> IntSource -> BoolSource
relOpSource s1 op s2 =
  BoolSource { boolText = intText (s1 <> IntSource (" " <> op <> " ") <> s2) }

instance Arbitrary BoolSource where
  arbitrary = sized value
   where
    value 0 = boolSource <$> elements [False, True]
    value n = oneof
      [ parensBoolSource <$> subvalue2
      , notBoolSource <$> subvalue2
      , logOpSource <$> subvalue <*> elements ["&&", "||"] <*> subvalue
      , relOpSource
      <$> arbitrary
      <*> elements ["==", "!=", "<", "<=", ">", ">="]
      <*> arbitrary
      ]
     where
      subvalue  = value (n `div` 2)
      subvalue2 = value (n - 1)
