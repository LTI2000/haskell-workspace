{-# LANGUAGE GADTs #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module OptimizerSpec
  ( spec
  ) where

import           Test.Hspec
import           Test.Hspec.QuickCheck
import           Test.QuickCheck

import           Ast
import           Optimizer
import           Val

spec :: Spec
spec = do
  describe "optimize" $ do
    it "does not loop on epressions containing variables" $ do
      let e = BinOp Plus (Var "x") (Var "y")
      optimize e `shouldBe` e

    prop "optimizes self assignments" $ do
      \i1 i2 ->
        let e = Assign i1 (Var i2)
        in  if i1 == i2
              then optimize e `shouldBe` Nop
              else optimize e `shouldBe` e

    prop "optimizes constant integer expressions" $ do
      \e -> optimize e `shouldBe` CVal (eval e)

    prop "optimizes variable expressions" $ do
      \e -> optimize e `shouldBe` CVal (eval e)

    prop "optimizes constant boolean expressions" $ do
      \i -> let e = Var i in optimize e `shouldBe` e

    prop "optimizes constant expression statements" $ do
      \e -> optimize (Outbox e) `shouldBe` Outbox (CVal (eval e))

    it "optimizes inbox expressions" $ do
      optimize Inbox `shouldBe` Inbox

    it "optimizes if(!(x < 0)) statements" $ do
      optimize
          (If (NotOp (RelOp LessThan Inbox (CVal 0)))
              (Outbox (CVal 1))
              (Outbox (CVal 0))
          )
        `shouldBe` If (RelOp LessThan Inbox (CVal 0))
                      (Outbox (CVal 0))
                      (Outbox (CVal 1))

    prop "optimizes if(e && !e) statements" $ do
      \e ->
        optimize
            (If (LogOp And e (NotOp e)) (Outbox (CVal 1)) (Outbox (CVal 0)))
          `shouldBe` Outbox (CVal 0)

    prop "optimizes if(e || !e) statements" $ do
      \e ->
        optimize (If (LogOp Or e (NotOp e)) (Outbox (CVal 1)) (Outbox (CVal 0)))
          `shouldBe` Outbox (CVal 1)

    prop "optimizes while(e && !e) statements" $ do
      \e ->
        optimize (While (LogOp And e (NotOp e)) (Outbox (CVal 1)))
          `shouldBe` Nop

    prop "optimizes while(e || !e) statements" $ do
      \e ->
        optimize (While (LogOp Or e (NotOp e)) (Outbox (CVal 1)))
          `shouldBe` While (CBool True) (Outbox (CVal 1))

    prop "optimizes do ... while(e && !e) statements" $ do
      \e -> optimize (DoWhile (Outbox (CVal 1)) (LogOp And e (NotOp e)))
        `shouldBe` Outbox (CVal 1)

    prop "optimizes do ... while(e || !e) statements" $ do
      \e ->
        optimize (DoWhile (Outbox (CVal 1)) (LogOp Or e (NotOp e)))
          `shouldBe` DoWhile (Outbox (CVal 1)) (CBool True)

    it "optimizes statement sequences" $ do
      optimize
          (Seq (Outbox (BinOp Times (CVal 0) (CVal 1)))
               (Outbox (BinOp Plus (CVal 0) (CVal 1)))
          )
        `shouldBe` Seq (Outbox (CVal 0)) (Outbox (CVal 1))

    it "optimizes an empty statement" $ do
      optimize Nop `shouldBe` Nop

    it "optimizes a nested empty statement" $ do
      optimize
          (Seq Nop
               (Seq (Seq (Seq Nop (Seq Nop (Seq Nop Nop))) Nop) (Seq Nop Nop))
          )
        `shouldBe` Nop

    it "optimizes a nested statement" $ do
      optimize
          (Seq
            (Outbox (CVal 0))
            (Seq (Seq (Seq Nop (Seq Nop (Seq Nop (Outbox (CVal 1))))) Nop)
                 (Seq Nop Nop)
            )
          )
        `shouldBe` Seq (Outbox (CVal 0)) (Outbox (CVal 1))

class Eval e a where
  eval :: e a -> a

instance Eval Expr Val where
  eval (CVal c        ) = c
  eval (UnOp UPlus  e ) = eval e
  eval (UnOp UMinus e ) = (-eval e)
  eval (BinOp op e1 e2) = calculate op (eval e1) (eval e2)
   where
    calculate Times  = (*)
    calculate Divide = div
    calculate Plus   = (+)
    calculate Minus  = (-)

instance Eval Expr Bool where
  eval (CBool b       ) = b
  eval (NotOp e       ) = not (eval e)
  eval (LogOp op e1 e2) = calculate op (eval e1) (eval e2)
   where
    calculate Eq  = (==)
    calculate Xor = (/=)
    calculate And = (&&)
    calculate Or  = (||)
  eval (RelOp op e1 e2) = calculate op (eval e1) (eval e2)
   where
    calculate Equal        = (==)
    calculate NotEqual     = (/=)
    calculate LessThan     = (<)
    calculate LessEqual    = (<=)
    calculate GreaterThan  = (>)
    calculate GreaterEqual = (>=)


subvalue :: (Int -> Gen a) -> Int -> Gen a
subvalue value n = value (n `div` 2)
subvalue2 :: (Int -> Gen a) -> Int -> Gen a
subvalue2 value n = value (n - 1)

instance Arbitrary (Expr Val) where
  arbitrary = sized value
   where
    value :: Int -> Gen (Expr Val)
    value 0 = CVal <$> choose (-999, 999)
    value n = oneof
      [ UnOp <$> arbitrary <*> subvalue2 value n
      , BinOp <$> arbitrary <*> subvalue value n <*> subvalue value n
      ]

instance Arbitrary UnOp where
  arbitrary = elements [UPlus, UMinus]

instance Arbitrary BinOp where
  arbitrary = elements [Times, Plus, Minus]

instance Arbitrary (Expr Bool) where
  arbitrary = sized value
   where
    value :: Int -> Gen (Expr Bool)
    value 0 = CBool <$> elements [False, True]
    value n = oneof
      [ NotOp <$> subvalue2 value n
      , LogOp <$> arbitrary <*> subvalue value n <*> subvalue value n
      , RelOp <$> arbitrary <*> arbitrary <*> arbitrary
      ]

instance Arbitrary LogOp where
  arbitrary = elements [Eq, Xor, And, Or]

instance Arbitrary RelOp where
  arbitrary =
    elements [Equal, NotEqual, LessThan, LessEqual, GreaterThan, GreaterEqual]
