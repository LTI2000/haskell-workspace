module TAC_DumpSpec
  ( spec
  ) where

-- import Test.QuickCheck
import           Test.Hspec
import           Test.Hspec.QuickCheck

import           Data.Sequence

import           Ast
import           TAC
import           TAC_Dump



spec :: Spec
spec = do

  describe "dump" $ do

    it "dumps an inbox expression"
      $          do
                   dump (Assign "X" Inbox)
      `shouldBe` fromList [INBOX "T0", COPY "X" "T0"]

    it "dumps an outbox statement"
      $          do
                   dump (Outbox (CVal 0))
      `shouldBe` fromList [OUTBOX "0"]

    it "dumps an empty statement"
      $          do
                   dump Nop
      `shouldBe` fromList []

    prop "dumps assignments" $ do
      \a -> dump (Assign a (CVal 0)) `shouldBe` fromList [COPY a "0"]

    it "dumps if(x < 0) ... statements"
      $          do
                   dump
                     (If (RelOp LessThan (Var "X") (CVal 0))
                         (Outbox (CVal 0))
                         (Outbox (CVal 1))
                     )
      `shouldBe` fromList
                   [ JUMPN "X" "IF_TRUE0"
                   , OUTBOX "1"
                   , JUMP "IF_END1"
                   , LABEL "IF_TRUE0"
                   , OUTBOX "0"
                   , LABEL "IF_END1"
                   ]

    it "dumps while(true) statements"
      $          do
                   dump (While (CBool True) (Outbox (CVal 0)))
      `shouldBe` fromList [LABEL "WHILE0", OUTBOX "0", JUMP "WHILE0"]

    it "dumps do ... while(true) statements"
      $          do
                   dump (DoWhile (Outbox (CVal 0)) (CBool True))
      `shouldBe` fromList [LABEL "DO_WHILE0", OUTBOX "0", JUMP "DO_WHILE0"]
