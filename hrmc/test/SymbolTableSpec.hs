module SymbolTableSpec
  ( spec
  ) where

-- import Test.QuickCheck
import           Test.Hspec
import           Test.Hspec.QuickCheck

import           SymbolTable

spec :: Spec
spec = do

  describe "emptySymbolTable" $ do

    prop "contains no symbols" $ do
      \i -> lookupSymbol i emptySymbolTable == Nothing

  describe "declareSymbol" $ do

    prop "declares different symbols in an empty symbol table" $ do
      \i ->
        let i0                         = i ++ "0"
            i1                         = i ++ "1"
            name0'                     = i0 ++ "$0"
            name1'                     = i1 ++ "$1"
            Just (name0, symbolTable0) = declareSymbol i0 emptySymbolTable
            Just (name1, symbolTable1) = declareSymbol i1 symbolTable0
        in  name0
              == name0'
              && lookupSymbol i0 symbolTable0
              == Just name0'
              && name1
              == name1'
              && lookupSymbol i1 symbolTable1
              == Just name1'

    prop "does not declare symbols twice in the same symbol table" $ do
      \i ->
        let Just (_, symbolTable) = declareSymbol i emptySymbolTable
        in  case declareSymbol i symbolTable of
              Nothing -> True
              _       -> False

    prop "declares same symbols in an empty and a pushed symbol table" $ do
      \i ->
        let
          name0'                     = i ++ "$0"
          name1'                     = i ++ "$1"
          Just (name0, symbolTable0) = declareSymbol i emptySymbolTable
          Just (name1, symbolTable1) =
            declareSymbol i (pushSymbolTable symbolTable0)
        in
          name0
          == name0'
          && lookupSymbol i symbolTable0
          == Just name0'
          && name1
          == name1'
          && lookupSymbol i symbolTable1
          == Just name1'
          && name0'
          /= name1'

  describe "popSymbolTable" $ do

    prop "makes previously established symbol declarations disappear" $ do
      \i ->
        let
          i0                     = i ++ "0"
          i1                     = i ++ "1"
          name0'                 = i0 ++ "$0"
          name1'                 = i1 ++ "$1"
          Just (_, symbolTable0) = declareSymbol i0 emptySymbolTable
          Just (_, symbolTable1) =
            declareSymbol i1 (pushSymbolTable symbolTable0)
          symbolTable2 = popSymbolTable symbolTable1
        in
          lookupSymbol i0 symbolTable0
          == Just name0'
          && lookupSymbol i1 symbolTable1
          == Just name1'
          && lookupSymbol i1 symbolTable2
          == Nothing
          && lookupSymbol i0 symbolTable2
          == Just name0'
