module IntegrationSpec
  ( spec
  ) where

-- import Test.QuickCheck
-- import Test.Hspec.QuickCheck
import           Test.Hspec

import           Data.Sequence

import           Converter
import           Optimizer
import           Parser
import           TAC
import           TAC_Dump



spec :: Spec
spec = do



  describe "parsing, converting, optimizing and dumping" $ do

    do
      let src = "{ outbox(inbox()); }"
      it ("works on " ++ src)
        $          do
                     compile src
        `shouldBe` Right (fromList [INBOX "T0", OUTBOX "T0"])

    do
      let src = "while(true) { outbox(inbox()); }"
      it ("works on " ++ src)
        $          do
                     compile src
        `shouldBe` Right
                     (fromList
                       [LABEL "WHILE0", INBOX "T1", OUTBOX "T1", JUMP "WHILE0"]
                     )

    do
      let src = "{ outbox(2 * inbox()); }"
      it ("works on " ++ src)
        $          do
                     compile src
        `shouldBe` Right
                     (fromList
                       [INBOX "T0", BINOP "T1" "2" "Times" "T0", OUTBOX "T1"]
                     )

compile :: String -> Either String Code
compile src = case parseStmt src of
  Left  parseError -> Left $ show parseError
  Right stmt'      -> case runConvert stmt' of
    Left converterError -> Left $ show converterError
    Right stmt ->
      let optimizedStmt = optimize stmt in Right $ dump optimizedStmt
