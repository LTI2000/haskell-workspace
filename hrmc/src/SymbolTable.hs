module SymbolTable
  ( emptySymbolTable
  , declareSymbol
  , lookupSymbol
  , pushSymbolTable
  , popSymbolTable
  , SymbolTable
  ) where

import           Data.Map                       ( Map )
import qualified Data.Map                      as Map

import           Val

data SymbolTable = SymbolTable
  { symbolCounter :: Int
  , symbolTable   :: [Map Name Name]
  }

emptySymbolTable :: SymbolTable
emptySymbolTable =
  pushSymbolTable SymbolTable { symbolCounter = 0, symbolTable = [] }

declareSymbol :: Name -> SymbolTable -> Maybe (Name, SymbolTable)
declareSymbol i u = declareName i (symbolTable u)
 where
  declareName name (h : _) = case Map.lookup name h of
    Nothing ->
      let (name', u') = genSymbol name u
      in  Just (name', insertSymbol name name' u')
    _ -> Nothing
  declareName _ [] = Nothing

lookupSymbol :: Name -> SymbolTable -> Maybe Name
lookupSymbol i u = lookupName i (symbolTable u)
 where
  lookupName name (h : t) = case Map.lookup name h of
    Nothing -> lookupName name t
    entry   -> entry
  lookupName _ [] = Nothing

genSymbol :: Name -> SymbolTable -> (Name, SymbolTable)
genSymbol i u =
  let c  = symbolCounter u
      c' = c + 1
      i' = i ++ "$" ++ show c
  in  (i', u { symbolCounter = c' })

insertSymbol :: Name -> Name -> SymbolTable -> SymbolTable
insertSymbol i i' u =
  let (h : t) = symbolTable u
      h'      = Map.insert i i' h
  in  u { symbolTable = h' : t }

pushSymbolTable :: SymbolTable -> SymbolTable
pushSymbolTable u = u { symbolTable = Map.empty : symbolTable u }

popSymbolTable :: SymbolTable -> SymbolTable
popSymbolTable u = let (_ : t) = symbolTable u in u { symbolTable = t }
