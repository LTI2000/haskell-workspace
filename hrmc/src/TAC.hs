module TAC ( TAC (..)
           )
       where

import Text.Printf (printf)

type Name = String
type UnOp = String
type BinOp = String
type Label = String

data TAC = INBOX Name
         | OUTBOX Name
         | COPY Name Name
         | UNOP UnOp Name Name
         | BINOP Name BinOp Name Name
         | LABEL Label
         | JUMP Label
         | JUMPZ Name Label
         | JUMPN Name Label
         deriving Eq

instance Show TAC where
  show (INBOX dst)              = printf "    %s := INBOX" dst
  show (OUTBOX src)             = printf "    OUTBOX := %s" src
  show (COPY dst src)           = printf "    %s := %s" dst src
  show (UNOP dst op src)        = printf "    %s := %s %s" dst op src
  show (BINOP dst src1 op src2) = printf "    %s := %s %s %s" dst src1 op src2
  show (LABEL label)            = printf "%s:" label
  show (JUMP label)             = printf "    JUMP %s" label
  show (JUMPZ src label)        = printf "    IF %s = 0 %s" src label
  show (JUMPN src label)        = printf "    IF %s < 0 %s" src label
