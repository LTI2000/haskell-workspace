module Converter
  ( convertStmt
  , convertExpr
  , convertBool
  , ConvertError
  , runConvert
  ) where

import           Control.Monad.Except

import           Ast
import           UntypedAst
import           Val



type ConvertError = String

convertStmt :: Stmt' -> Except ConvertError (Expr ())
convertStmt (Outbox' e'  ) = Outbox <$> convertExpr e'
convertStmt (Assign' i e') = Assign i <$> convertExpr e'
convertStmt (If' t' c' a') =
  If <$> convertBool t' <*> convertStmt c' <*> convertStmt a'
convertStmt (While'   t' b') = While <$> convertBool t' <*> convertStmt b'
convertStmt (DoWhile' b' t') = DoWhile <$> convertStmt b' <*> convertBool t'
convertStmt (Seq' ss'      ) = foldr Seq Nop <$> mapM convertStmt ss'

convertExpr :: Expr' -> Except ConvertError (Expr Val)
convertExpr Inbox'         = pure Inbox
convertExpr (CVal' v     ) = pure (CVal v)
convertExpr (Var'  i     ) = pure (Var i)
convertExpr (UnOp' op' e') = UnOp <$> convertUnOp op' <*> convertExpr e'
convertExpr (BinOp' op' e1' e2') =
  BinOp <$> convertBinOp op' <*> convertExpr e1' <*> convertExpr e2'
convertExpr e = throwError $ "not an expression: " ++ show e

convertBool :: Expr' -> Except ConvertError (Expr Bool)
convertBool (CBool' b          ) = pure (CBool b)
convertBool (UnOp' Not' e'     ) = NotOp <$> convertBool e'
convertBool (BinOp' op' e1' e2') = case typeOfOp op' of
  Rel -> RelOp <$> convertRelOp op' <*> convertExpr e1' <*> convertExpr e2'
  Log -> LogOp <$> convertLogOp op' <*> convertBool e1' <*> convertBool e2'
  Bin -> throwError $ "not a boolean operator: " ++ show op'
convertBool e = throwError $ "not a boolean expression: " ++ show e



data BinOpType = Rel
               | Log
               | Bin
               deriving (Eq, Show)

typeOfOp :: BinOp' -> BinOpType
typeOfOp Times'        = Bin
typeOfOp Divide'       = Bin
typeOfOp Plus'         = Bin
typeOfOp Minus'        = Bin
typeOfOp Equal'        = Rel
typeOfOp NotEqual'     = Rel
typeOfOp LessThan'     = Rel
typeOfOp LessEqual'    = Rel
typeOfOp GreaterThan'  = Rel
typeOfOp GreaterEqual' = Rel
typeOfOp Eq'           = Log
typeOfOp Xor'          = Log
typeOfOp And'          = Log
typeOfOp Or'           = Log

convertUnOp :: UnOp' -> Except ConvertError UnOp
convertUnOp UPlus'  = return UPlus
convertUnOp UMinus' = return UMinus
convertUnOp op      = throwError $ "not an unary operator: " ++ show op

convertBinOp :: BinOp' -> Except ConvertError BinOp
convertBinOp Times'  = return Times
convertBinOp Divide' = return Divide
convertBinOp Plus'   = return Plus
convertBinOp Minus'  = return Minus
convertBinOp op      = throwError ("not a binary operator: " ++ show op)

convertRelOp :: BinOp' -> Except ConvertError RelOp
convertRelOp Equal'        = return Equal
convertRelOp NotEqual'     = return NotEqual
convertRelOp LessThan'     = return LessThan
convertRelOp LessEqual'    = return LessEqual
convertRelOp GreaterThan'  = return GreaterThan
convertRelOp GreaterEqual' = return GreaterEqual
convertRelOp op = throwError $ "not a relational operator: " ++ show op

convertLogOp :: BinOp' -> Except ConvertError LogOp
convertLogOp Eq'  = return Eq
convertLogOp Xor' = return Xor
convertLogOp And' = return And
convertLogOp Or'  = return Or
convertLogOp op   = throwError $ "not a logical operator: " ++ show op

runConvert :: Stmt' -> Either ConvertError (Expr ())
runConvert = runExcept . convertStmt
