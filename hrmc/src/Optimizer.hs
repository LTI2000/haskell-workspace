{-# LANGUAGE GADTs #-}

module Optimizer (optimize) where

import Val
import Ast

optimize :: Expr a -> Expr a

optimize (Outbox e)       = Outbox (optimize e)
optimize (Assign i e)     = optimizeAssign i (optimize e)
optimize (If e1 e2 e3)    = optimizeIf (optimize e1) (optimize e2) (optimize e3)
optimize (While e1 e2)    = optimizeWhile (optimize e1) (optimize e2)
optimize (DoWhile e1 e2)  = optimizeDoWhile (optimize e1) (optimize e2)
optimize (Seq e1 e2)      = optimizeSeq (optimize e1) (optimize e2)
optimize e@Nop            = e
optimize e@Inbox          = e
optimize e@(CVal _)       = e
optimize e@(Var _)        = e
optimize (UnOp op e)      = optimizeUnOp op (optimize e)
optimize (BinOp op e1 e2) = optimizeBinOp op (optimize e1) (optimize e2)
optimize (RelOp op e1 e2) = optimizeRelOp op (optimize e1) (optimize e2)
optimize e@(CBool _)      = e
optimize (NotOp e)        = optimizeNotOp (optimize e)
optimize (LogOp op e1 e2) = optimizeLogOp op (optimize e1) (optimize e2)



optimizeAssign :: Name -> Expr Val -> Expr ()

optimizeAssign i1 o@(Var i2) =
  if i1 == i2 then
    Nop
  else
    Assign i1 o

optimizeAssign i o = Assign i o


optimizeIf :: Expr Bool -> Expr () -> Expr () -> Expr ()

optimizeIf (CBool False) _ o3 = o3
optimizeIf (CBool True) o2 _  = o2

optimizeIf (NotOp o1) o2 o3 = If o1 o3 o2

optimizeIf o1 o2 o3 = If o1 o2 o3



optimizeWhile :: Expr Bool -> Expr () -> Expr ()

optimizeWhile (CBool False) _ = Nop

optimizeWhile o1 o2 = While o1 o2



optimizeDoWhile :: Expr () -> Expr Bool -> Expr ()

optimizeDoWhile o1 (CBool False) = o1

optimizeDoWhile o1 o2 = DoWhile o1 o2



optimizeSeq :: Expr() -> Expr () -> Expr ()

optimizeSeq Nop o2 = o2
optimizeSeq o1 Nop = o1

optimizeSeq o1 o2 = Seq o1 o2



optimizeUnOp :: UnOp -> Expr Val -> Expr Val

optimizeUnOp UPlus o = o

optimizeUnOp UMinus (CVal a)        = CVal (-a)
optimizeUnOp UMinus (UnOp UMinus e) = optimize e

optimizeUnOp op o = UnOp op o



optimizeBinOp :: BinOp -> Expr Val -> Expr Val -> Expr Val

optimizeBinOp Times (CVal 0) o2       = CVal 0 -- FIXME keep effect
optimizeBinOp Times (CVal 1) o2       = o2
optimizeBinOp Times o1 (CVal 0)       = CVal 0 -- FIXME keep effect
optimizeBinOp Times o1 (CVal 1)       = o1
optimizeBinOp Times (CVal a) (CVal b) = CVal (a * b)

optimizeBinOp Divide (CVal 0) o2 = CVal 0 -- FIXME keep effect
optimizeBinOp Divide o1 (CVal 1) = o1

optimizeBinOp Plus (CVal 0) o2       = o2
optimizeBinOp Plus o1 (CVal 0)       = o1
optimizeBinOp Plus (CVal a) (CVal b) = CVal (a + b)

optimizeBinOp Minus o1 (CVal 0)       = o1
optimizeBinOp Minus (CVal a) (CVal b) = CVal (a - b)

optimizeBinOp op o1 o2 = BinOp op o1 o2



optimizeRelOp :: RelOp -> Expr Val -> Expr Val -> Expr Bool

optimizeRelOp op (CVal a) (CVal b) = CBool (calculate op a b)
  where
    calculate Equal        = (==)
    calculate NotEqual     = (/=)
    calculate LessThan     = (<)
    calculate LessEqual    = (<=)
    calculate GreaterThan  = (>)
    calculate GreaterEqual = (>=)

optimizeRelOp op o1 o2 = RelOp op o1 o2



optimizeNotOp :: Expr Bool -> Expr Bool

optimizeNotOp (CBool b) = CBool (not b)

optimizeNotOp o = NotOp o



optimizeLogOp :: LogOp -> Expr Bool -> Expr Bool -> Expr Bool

optimizeLogOp Eq (CBool a) (CBool b) = CBool (a == b)

optimizeLogOp Xor (CBool a) (CBool b) = CBool (a /= b)

optimizeLogOp And (CBool False) _  = CBool False
optimizeLogOp And (CBool True)  o2 = o2
optimizeLogOp And o1 (CBool False) = CBool False -- FIXME keep effect
optimizeLogOp And o1 (CBool True)  = o1

optimizeLogOp Or (CBool False) o2 = o2
optimizeLogOp Or (CBool True) o2  = CBool True -- FIXME keep effect
optimizeLogOp Or o1 (CBool False) = o1
optimizeLogOp Or o1 (CBool True)   = CBool True -- FIXME keep effect

optimizeLogOp op o1 o2 = LogOp op o1 o2
