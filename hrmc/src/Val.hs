module Val ( Name
           , Val
           ) where

type Name = String
type Val = Int
