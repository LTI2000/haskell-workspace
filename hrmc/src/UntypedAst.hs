module UntypedAst
  ( Stmt'(..)
  , Expr'(..)
  , UnOp'(..)
  , BinOp'(..)
  , ExprType(..)
  , opType
  ) where

import           Val

data Stmt' = Outbox' Expr'
           | Assign' Name Expr'
           | If' Expr' Stmt' Stmt'
           | While' Expr' Stmt'
           | DoWhile' Stmt' Expr'
           | Seq' [Stmt']
           deriving
             (Eq, Show)

data Expr' = Inbox'
           | CVal' Val
           | CBool' Bool
           | Var' Name
           | UnOp' UnOp' Expr'
           | BinOp' BinOp' Expr' Expr'
           deriving
             (Eq, Show)

data UnOp' = UPlus'
           | UMinus'
           | Not'
           deriving
             (Eq, Show)

data BinOp' = Times'
            | Divide'
            | Plus'
            | Minus'
            | LessThan'
            | LessEqual'
            | GreaterThan'
            | GreaterEqual'
            | Equal'    | Eq'
            | NotEqual' | Xor'
            | And'
            | Or'
            deriving
              (Eq, Show)

data ExprType = Val
              | Bool
              | Illegal ExprType ExprType -- expected type, actual type
              deriving (Eq, Show)

class TypedOp a where
  opType :: a -> [([ExprType], a -> a, ExprType)]

instance TypedOp UnOp' where
  opType UPlus'  = [([Val], id, Val)]
  opType UMinus' = [([Val], id, Val)]
  opType Not'    = [([Bool], id, Bool)]

instance TypedOp BinOp' where
  opType Times'        = [([Val, Val], id, Val)]
  opType Divide'       = [([Val, Val], id, Val)]
  opType Plus'         = [([Val, Val], id, Val)]
  opType Minus'        = [([Val, Val], id, Val)]
  opType LessThan'     = [([Val, Val], id, Bool)]
  opType LessEqual'    = [([Val, Val], id, Bool)]
  opType GreaterThan'  = [([Val, Val], id, Bool)]
  opType GreaterEqual' = [([Val, Val], id, Bool)]
  opType Equal' = [([Val, Val], id, Bool), ([Bool, Bool], const Eq', Bool)]
  opType Eq'           = [([Bool, Bool], id, Bool)]
  opType NotEqual' = [([Val, Val], id, Bool), ([Bool, Bool], const Xor', Bool)]
  opType Xor'          = [([Bool, Bool], id, Bool)]
  opType And'          = [([Bool, Bool], id, Bool)]
  opType Or'           = [([Bool, Bool], id, Bool)]


-- +----------------+-----------+---------------+
-- | Category       | Operator  | Associativity |
-- +----------------+-----------+---------------+
-- | Unary          | + - !     | Right to left |
-- +----------------+-----------+---------------+
-- | Multiplicative | * /       | Left to right |
-- | Additive       | + -       | Left to right |
-- | Relational     | < <= > >= | Left to right |
-- | Equality       | == !=     | Left to right |
-- | Logical AND    | &&        | Left to right |
-- | Logical OR     | ||        | Left to right |
-- | Assignment     | =         | Right to left |
-- +----------------+-----------+---------------+


-- +------------+--------------+---------------------------------------------------+---------------+
-- | Precedence | Operator     | Description                                       | Associativity |
-- +------------+--------------+---------------------------------------------------+---------------+
-- |  1         | ++ --        | Suffix/postfix increment and decrement            | Left-to-right |
-- |            | ()           | Function call                                     |               |
-- |            | []           | Array subscripting                                |               |
-- |            | .            | Structure and union member access                 |               |
-- |            | ->           | Structure and union member access through pointer |               |
-- |            | (type){list} | Compound literal(C99)                             |               |
-- +------------+--------------+---------------------------------------------------+---------------+
-- |  2         | ++ --        | Prefix increment and decrement                    | Right-to-left |
-- |            | + -          | Unary plus and minus                              |               |
-- |            | ! ~          | Logical NOT and bitwise NOT                       |               |
-- |            | (type)       | Type cast                                         |               |
-- |            | *            | Indirection (dereference)                         |               |
-- |            | &            | Address-of                                        |               |
-- |            | sizeof       | Size-of                                           |               |
-- |            | _Alignof     | Alignment requirement(C11)                        |               |
-- +------------+--------------+---------------------------------------------------+---------------+
-- |  3         | * / %        | Multiplication, division, and remainder           | Left-to-right |
-- +------------+--------------+---------------------------------------------------+               +
-- |  4         | + -          | Addition and subtraction                          |               |
-- +------------+--------------+---------------------------------------------------+               +
-- |  5         | << >>        | Bitwise left shift and right shift                |               |
-- +------------+--------------+---------------------------------------------------+               +
-- |  6         | < <=         | For relational operators < and ≤ respectively     |               |
-- |            | > >=         | For relational operators > and ≥ respectively     |               |
-- +------------+--------------+---------------------------------------------------+               +
-- |  7         | == !=        | For relational = and ≠ respectively               |               |
-- +------------+--------------+---------------------------------------------------+               +
-- |  8         | &            | Bitwise AND                                       |               |
-- +------------+--------------+---------------------------------------------------+               +
-- |  9         | ^            | Bitwise XOR (exclusive or)                        |               |
-- +------------+--------------+---------------------------------------------------+               +
-- | 10         | |            | Bitwise OR (inclusive or)                         |               |
-- +------------+--------------+---------------------------------------------------+               +
-- | 11         | &&           | Logical AND                                       |               |
-- +------------+--------------+---------------------------------------------------+               +
-- | 12         | ||           | Logical OR                                        |               |
-- +------------+--------------+---------------------------------------------------+---------------+
-- | 13[note 1] | ?:           | Ternary conditional[note 2]                       | Right-to-Left |
-- +------------+--------------+---------------------------------------------------+               +
-- | 14         | =            | Simple assignment                                 |               |
-- |            | += -=        | Assignment by sum and difference                  |               |
-- |            | *= /= %=     | Assignment by product, quotient, and remainder    |               |
-- |            | <<= >>=      | Assignment by bitwise left shift and right shift  |               |
-- |            | &= ^= |=     | Assignment by bitwise AND, XOR, and OR            |               |
-- +------------+--------------+---------------------------------------------------+---------------+
-- | 15         | ,            | Comma                                             | Left-to-right |
-- +------------+--------------+---------------------------------------------------+---------------+
--
-- 1. Fictional precedence level, see Notes below
-- 2. The expression in the middle of the conditional operator (between ? and :) is parsed as if parenthesized: its
--    precedence relative to ?: is ignored.
--
-- When parsing an expression, an operator which is listed on some row will be bound tighter (as if by parentheses) to its
-- arguments than any operator that is listed on a row further below it. For example, the expression *p++ is parsed as
-- *(p++), and not as (*p)++.
--
-- Operators that are in the same cell (there may be several rows of operators listed in a cell) are evaluated with the
-- same precedence, in the given direction. For example, the expression a=b=c is parsed as a=(b=c), and not as
-- (a=b)=c because of right-to-left associativity.

-- +----------------+----------------------------------+---------------+
-- | Category       | Operator                         | Associativity |
-- +----------------+----------------------------------+---------------+
-- | Postfix        | () [] -> . ++ --                 | Left to right |
-- | Unary          | + - ! ~ ++ -- (type)* & sizeof   | Right to left |
-- | Multiplicative | * / %                            | Left to right |
-- | Additive       | + -                              | Left to right |
-- | Shift          | << >>                            | Left to right |
-- | Relational     | < <= > >=                        | Left to right |
-- | Equality       | == !=                            | Left to right |
-- | Bitwise AND    | &                                | Left to right |
-- | Bitwise XOR    | ^                                | Left to right |
-- | Bitwise OR     | |                                | Left to right |
-- | Logical AND    | &&                               | Left to right |
-- | Logical OR     | ||                               | Left to right |
-- | Conditional    | ?:                               | Right to left |
-- | Assignment     | = += -= *= /= %=>>= <<= &= ^= |= | Right to left |
-- | Comma          | ,                                | Left to right |
-- +----------------+----------------------------------+---------------+
