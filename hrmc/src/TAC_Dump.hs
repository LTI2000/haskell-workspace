{-# LANGUAGE GADTs #-}

module TAC_Dump (dump, Code) where

import Data.Sequence
import Control.Monad.RWS

import Ast
import TAC

type Code = Seq TAC
type Addr = String

type R = ()
type W = Code
type S = Int

type Dump a = RWS R W S a

dump :: Expr () -> Code

dump stmt =
  let (_, _, w) = runRWS (dumpStmt stmt) startEnv startState
  in w
  where
    startEnv :: ()
    startEnv = ()

    startState :: Int
    startState = 0



dumpStmt :: Expr () -> Dump ()

dumpStmt (Outbox expr) = do
  e <- dumpExpr expr
  emit (OUTBOX e)

dumpStmt (Assign name expr) = do
  e <- dumpExpr expr
  emit (COPY name e)

dumpStmt (If (RelOp LessThan expr (CVal 0)) trueStmt falseStmt) = do
  e <- dumpExpr expr
  trueLabel <- newLabel "IF_TRUE"
  exitLabel <- newLabel "IF_END"
  emit (JUMPN e trueLabel)
  dumpStmt falseStmt
  emit (JUMP exitLabel)
  emit (LABEL trueLabel)
  dumpStmt trueStmt
  emit (LABEL exitLabel)

--  dumpStmt (If expr stmt1 stmt2) =
--    dumpIf expr
--      where
--        dumpIf :: Expr Bool -> RWS () (Seq String) Int ()
--
--        dumpIf (CBool True) = do
--          emit "[ignoring if true]"
--          dumpStmt stmt1
--
--        dumpIf (CBool False) = do
--          emit "[ignoring if false]"
--          dumpStmt stmt2
--
--        dumpIf (RelOp LessThan (Var x) (CVal 0)) = do
--          n1 <- get <* modify (+1)
--          let trueLabel = "true$" ++ show n1 ++ ":"
--          n2 <- get <* modify (+1)
--          let exitLabel = "exit$" ++ show n2 ++ ":"
--          emit ("IF " ++ x ++ " < 0" ++ " " ++ trueLabel)
--          dumpStmt stmt2
--          emit ("JUMP " ++ exitLabel)
--          emit trueLabel
--          dumpStmt stmt1
--          emit exitLabel
--
--        dumpIf (RelOp Equal (Var x) (CVal 0)) = do
--          emit ("IF " ++ x ++ " == 0" ++ "!!!")

dumpStmt (While (CBool True) stmt) = do
  whileLabel <- newLabel "WHILE"
  emit (LABEL whileLabel)
  dumpStmt stmt
  emit (JUMP whileLabel)

dumpStmt (DoWhile stmt (CBool True)) = do
  doWhileLabel <- newLabel "DO_WHILE"
  emit (LABEL doWhileLabel)
  dumpStmt stmt
  emit (JUMP doWhileLabel)

dumpStmt (Seq stmt1 stmt2) = do
  dumpStmt stmt1
  dumpStmt stmt2

dumpStmt Nop = do
  return ()


dumpExpr :: Expr Int -> Dump Addr

dumpExpr Inbox = do
  t <- newTemporary
  emit (INBOX t)
  return t

dumpExpr (CVal i) = do
  return (show i)

dumpExpr (Var n) = do
  return n

dumpExpr (UnOp op e) = do
  t1 <- dumpExpr e
  t2 <- newTemporary
  emit (UNOP t2 (show op) t1)
  return t2

dumpExpr (BinOp op e1 e2) = do
  t1 <- dumpExpr e1
  t2 <- dumpExpr e2
  t3 <- newTemporary
  emit (BINOP t3 t1 (show op) t2)
  return t3



newTemporary :: Dump String
newTemporary = do
  n <- get <* modify (+1)
  return $ "T" ++ show n

newLabel :: String -> Dump String
newLabel i = do
  n <- get <* modify (+1)
  return $ i ++ show n

emit :: TAC -> Dump ()
emit = tell . singleton
