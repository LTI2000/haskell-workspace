{-# LANGUAGE GADTs #-}
{-# LANGUAGE StandaloneDeriving #-}

module Ast ( Expr (..)
           , UnOp (..)
           , BinOp (..)
           , LogOp (..)
           , RelOp (..)
           )
       where

import Val

data Expr a where
  Outbox  :: Expr Val                        -> Expr ()
  Assign  :: Name -> Expr Val                -> Expr ()
  If      :: Expr Bool -> Expr () -> Expr () -> Expr ()
  While   :: Expr Bool -> Expr ()            -> Expr ()
  DoWhile :: Expr () -> Expr Bool            -> Expr ()
  Seq     :: Expr () -> Expr ()              -> Expr ()
  Nop     ::                                    Expr ()

  Inbox   ::                                  Expr Val
  CVal    :: Val                           -> Expr Val
  Var     :: Name                          -> Expr Val
  UnOp    :: UnOp -> Expr Val              -> Expr Val
  BinOp   :: BinOp -> Expr Val -> Expr Val -> Expr Val

  CBool   :: Bool                            -> Expr Bool
  NotOp   :: Expr Bool                       -> Expr Bool
  LogOp   :: LogOp -> Expr Bool -> Expr Bool -> Expr Bool
  RelOp   :: RelOp -> Expr Val -> Expr Val   -> Expr Bool

deriving instance Eq (Expr a)
deriving instance Show (Expr a)


data UnOp = UPlus
          | UMinus
          deriving
            (Show, Eq)

data BinOp = Times
           | Divide
           | Plus
           | Minus
           deriving
             (Show, Eq)


data LogOp = Eq
           | Xor
           | And
           | Or
           deriving
             (Show, Eq)


data RelOp = Equal
           | NotEqual
           | LessThan
           | LessEqual
           | GreaterThan
           | GreaterEqual
           deriving
             (Show, Eq)
