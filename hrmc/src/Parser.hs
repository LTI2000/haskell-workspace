module Parser ( parseStmt
              , parseExpr
              ) where

import Prelude hiding (error)
import Data.List (nub)
import Text.Parsec
--import Text.Parsec.String
import Text.Parsec.Expr
import Text.Parsec.Token
import Text.Parsec.Language

import Control.Monad.Identity
import Val
import UntypedAst
import SymbolTable

opPlus, opMinus, opNot, opTimes, opDivide, opLessThan, opLessEqual, opGreaterThan, opGreaterEqual, opEqual, opNotEqual, opAnd, opOr, opEquals :: String
opPlus         = "+"
opMinus        = "-"
opNot          = "!"
opTimes        = "*"
opDivide       = "/"
opLessThan     = "<"
opLessEqual    = "<="
opGreaterThan  = ">"
opGreaterEqual = ">="
opEqual        = "=="
opNotEqual     = "!="
opAnd          = "&&"
opOr           = "||"
opEquals       = "="

opNames :: [String]
opNames = [ opPlus
          , opMinus
          , opNot
          , opTimes
          , opDivide
          , opLessThan
          , opLessEqual
          , opGreaterThan
          , opGreaterEqual
          , opEqual
          , opNotEqual
          , opAnd
          , opOr
          , opEquals
          ]

kwInbox, kwOutbox, kwAuto, kwIf, kwElse, kwTrue, kwFalse, kwDo, kwWhile :: String
kwInbox  = "inbox"
kwOutbox = "outbox"
kwAuto   = "auto"
kwIf     = "if"
kwElse   = "else"
kwTrue   = "true"
kwFalse  = "false"
kwDo     = "do"
kwWhile  = "while"

kwNames :: [String]
kwNames = [ kwInbox
          , kwOutbox
          , kwAuto
          , kwIf
          , kwElse
          , kwTrue
          , kwFalse
          , kwDo
          , kwWhile
          ]

type U = SymbolTable
type P = ParsecT String U Identity

languageDef :: LanguageDef U
languageDef = emptyDef { commentStart    = "/*"
                       , commentEnd      = "*/"
                       , commentLine     = "//"
                       , opStart         = oneOf (nub (map head opNames))
                       , opLetter        = oneOf (nub (concat (map tail opNames)))
                       , reservedOpNames = opNames
                       , reservedNames   = kwNames
                       }

m_integer    :: P Integer
-- m_symbol     :: String -> P String
m_whiteSpace :: P ()
m_parens     :: P a -> P a
m_braces     :: P a -> P a
-- m_brackets   :: P a -> P a
m_identifier :: P String
m_reservedOp :: String -> P ()
m_reserved   :: String -> P ()
m_semi       :: P String
TokenParser { integer    = m_integer
            -- , symbol     = m_symbol
            , whiteSpace = m_whiteSpace
            , parens     = m_parens
            , braces     = m_braces
            -- , brackets   = m_brackets
            , identifier = m_identifier
            , reservedOp = m_reservedOp
            , reserved   = m_reserved
            , semi       = m_semi
            } = makeTokenParser languageDef

_declare :: P Name
_declare = do
  i <- m_identifier
  u <- getState
  case declareSymbol i u of
   Just (name,  u') -> do
     putState u'
     return name
   Nothing -> do
     unexpected ("duplicate declaration: " ++ i)

_lookup :: P Name
_lookup = do
  i <- m_identifier
  u <- getState
  case lookupSymbol i u of
   Just name -> do
     return name
   Nothing -> do
     unexpected ("unknown name: " ++ i)

_scope :: P a -> P a
_scope p = do
  u <- getState
  let u' = pushSymbolTable u
  putState u'
  x <- p
  let u'' = popSymbolTable u'
  putState u''
  return x

_val :: P (Expr', ExprType, SourcePos ) -> P Expr'
_val p = do
  (e, t, _) <- p
  case t of
   Val ->
     return e
   _ ->
     unexpected ("illegal type: " ++ show t)

_boolean :: P (Expr', ExprType, SourcePos ) -> P Expr'
_boolean p = do
  (e, t, _) <- p
  case t of
   Bool ->
     return e
   _ ->
     unexpected ("illegal type: " ++ show t)

stmt :: P Stmt'
stmt = outboxStmt
       <|>
       emptyStmt
       <|>
       declStmt
       <|>
       assignStmt
       <|>
       ifStmt
       <|>
       whileStmt
       <|>
       doWhileStmt
       <|>
       blockStmt
  where
    outboxStmt = do
      m_reserved kwOutbox
      e <- _val (m_parens expr)
      _ <- m_semi
      return (Outbox' e)

    emptyStmt = do
      _ <- m_semi
      return (Seq' [])

    declStmt = do
      m_reserved kwAuto
      name <- _declare
      _ <- m_reservedOp opEquals
      e <- _val expr
      _ <- m_semi
      return (Assign' name e)

    assignStmt = do
      name <- _lookup
      _ <- m_reservedOp opEquals
      e <- _val expr
      _ <- m_semi
      return (Assign' name e)

    ifStmt = do
      m_reserved kwIf
      t <- _boolean (m_parens expr)
      c <- _scope stmt
      m_reserved kwElse
      a <- _scope stmt
      return (If' t c a)

    whileStmt = do
      m_reserved kwWhile
      t <- _boolean (m_parens expr)
      b <- _scope stmt
      return (While' t b)

    doWhileStmt = do
      m_reserved kwDo
      b <- _scope stmt
      m_reserved kwWhile
      t <- _boolean (m_parens expr)
      return (DoWhile' b t)

    blockStmt = do
      ss <- m_braces (_scope (many stmt))
      return (Seq' ss)

expr :: P (Expr', ExprType, SourcePos)
expr = buildExpressionParser table primary <?> "expression"
  where
    unOp' :: UnOp' -> ExprType -> (UnOp', ExprType)
    unOp' op@UPlus'  Val  = (op, Val)
    unOp' op@UPlus'  t1   = (op, Illegal Val t1)

    unOp' op@UMinus' Val  = (op, Val)
    unOp' op@UMinus' t1   = (op, Illegal Val t1)

    unOp' op@Not'    Bool = (op, Bool)
    unOp' op@Not'    t1   = (op, Illegal Bool t1)

    unOp :: UnOp' -> SourcePos -> (Expr', ExprType, SourcePos) -> (Expr', ExprType, SourcePos)
    unOp op p0 (e1, t1, _) = let (op', t1') = unOp' op t1
                             in (UnOp' op' e1, t1', p0)

    binOp' :: BinOp' -> ExprType -> ExprType -> (BinOp', ExprType)
    binOp' op@Times'        Val  Val  = (op,   Val)
    binOp' op@Times'        t1   _    = (op,   Illegal Val t1)

    binOp' op@Divide'       Val  Val  = (op,   Val)
    binOp' op@Divide'       t1   _    = (op,   Illegal Val t1)

    binOp' op@Plus'         Val  Val  = (op,   Val)
    binOp' op@Plus'         t1   _    = (op,   Illegal Val t1)

    binOp' op@Minus'        Val  Val  = (op,   Val)
    binOp' op@Minus'        t1   _    = (op,   Illegal Val t1)

    binOp' op@LessThan'     Val  Val  = (op,   Bool)
    binOp' op@LessThan'     t1   _    = (op,   Illegal Bool t1)

    binOp' op@LessEqual'    Val  Val  = (op,   Bool)
    binOp' op@LessEqual'    t1   _    = (op,   Illegal Bool t1)

    binOp' op@GreaterThan'  Val  Val  = (op,   Bool)
    binOp' op@GreaterThan'  t1   _    = (op,   Illegal Bool t1)

    binOp' op@GreaterEqual' Val  Val  = (op,   Bool)
    binOp' op@GreaterEqual' t1   _    = (op,   Illegal Bool t1)

    binOp' op@Equal'        Val  Val  = (op,   Bool)
    binOp'    Equal'        Bool Bool = (Eq',  Bool)
    binOp' op@Equal'        t1   _    = (op,   Illegal Bool t1)

    binOp' op@NotEqual'     Val  Val  = (op,   Bool)
    binOp'    NotEqual'     Bool Bool = (Xor', Bool)
    binOp' op@NotEqual'     t1 _      = (op,   Illegal Bool t1)

    binOp' op@Eq'           Bool Bool = (op,   Bool)
    binOp' op@Eq'           t1   _    = (op,   Illegal Bool t1)

    binOp' op@Xor'          Bool Bool = (op,   Bool)
    binOp' op@Xor'          t1   _    = (op,   Illegal Bool t1)

    binOp' op@And'          Bool Bool = (op,   Bool)
    binOp' op@And'          t1   _    = (op,   Illegal Bool t1)

    binOp' op@Or'           Bool Bool = (op,   Bool)
    binOp' op@Or'           t1   _    = (op,   Illegal Bool t1)

    binOp :: BinOp' -> SourcePos -> (Expr', ExprType, SourcePos) -> (Expr', ExprType, SourcePos) -> (Expr', ExprType, SourcePos)
    binOp op p0 (e1, t1, _) (e2, t2, _) = let (op', t1') = binOp' op t1 t2
                                          in (BinOp' op' e1 e2, t1', p0)


    unary :: String -> UnOp' -> Operator String U Identity (Expr', ExprType, SourcePos)
    unary s op = Prefix (do { p <- getPosition ; m_reservedOp s >> return (unOp op p) })

    binary :: String -> BinOp' -> Assoc -> Operator String U Identity (Expr', ExprType, SourcePos)
    binary s op = Infix  (do { p <- getPosition ; m_reservedOp s >> return (binOp op p) })


    table :: OperatorTable String U Identity (Expr', ExprType, SourcePos)
    table = [ [ unary  opPlus         UPlus'
              , unary  opMinus        UMinus'
              , unary  opNot          Not'
              ]
            , [ binary opTimes        Times'        AssocLeft
              , binary opDivide       Divide'       AssocLeft
              ]
            , [ binary opPlus         Plus'         AssocLeft
              , binary opMinus        Minus'        AssocLeft
              ]
            , [ binary opLessThan     LessThan'     AssocLeft
              , binary opLessEqual    LessEqual'    AssocLeft
              , binary opGreaterThan  GreaterThan'  AssocLeft
              , binary opGreaterEqual GreaterEqual' AssocLeft
              ]
            , [ binary opEqual        Equal'        AssocLeft
              , binary opNotEqual     NotEqual'     AssocLeft
              ]
            , [ binary opAnd          And'          AssocLeft
              ]
            , [ binary opOr           Or'           AssocLeft
              ]
            ]

    primary = m_parens expr
           <|>
           inboxExpr
           <|>
           valExpr
           <|>
           boolExpr
           <|>
           varExpr

    inboxExpr = do
      p <- getPosition
      m_reserved kwInbox
      m_parens m_whiteSpace
      return (Inbox', Val, p)

    valExpr = do
      p <- getPosition
      v <- m_integer
      return (CVal' (fromInteger v), Val, p)

    boolExpr =
      do
        p <- getPosition
        m_reserved kwFalse
        return (CBool' False, Bool, p)
      <|>
      do
        p <- getPosition
        m_reserved kwTrue
        return (CBool' True, Bool, p)

    varExpr = do
      p <- getPosition
      name <- _lookup
      return (Var' name, Val, p)

parse' :: P a -> String -> Either ParseError a
parse' p = runParser (m_whiteSpace >> p <* eof) emptySymbolTable "-"

parseStmt :: String -> Either ParseError Stmt'
parseStmt = parse' stmt

parseExpr :: String -> Either ParseError Expr'
parseExpr input =
  case parse' expr input of
   Left err        -> Left err
   Right (e, _, _) -> Right e
