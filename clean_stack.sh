set -eux

STACK_DIR=${HOME}/.stack
CURRENT_DIR=$(pwd)

rm -rf ${STACK_DIR}
mkdir ${STACK_DIR}
ln -s ${CURRENT_DIR}/_stack_config.yaml ${STACK_DIR}/config.yaml
mkdir ${STACK_DIR}/global-project
ln -s ${CURRENT_DIR}/_stack_global-project_stack.yaml ${STACK_DIR}/global-project/stack.yaml
