module MainSpec ( spec ) where

import           Prelude
import           Text.Printf
import           Test.Hspec
import           Test.Hspec.QuickCheck

import           Value
import           CL
import           CL.Eval
import           Lam.Transform
import           Expression.Parse
import           Expression.Translate

spec :: Spec
spec = do
    describe "compile" $ do
        it "optimizes (=)" $ do
            compileSource "1=1" `shouldBe` CON (Boolean True)
            compileSource "0=1" `shouldBe` CON (Boolean False)
        it "optimizes (~)" $ do
            compileSource "~(1=1)" `shouldBe` CON (Boolean False)
            compileSource "~(0=1)" `shouldBe` CON (Boolean True)
        it "optimizes (&)" $ do
            compileSource "(1=1)&(1=1)" `shouldBe` CON (Boolean True)
            compileSource "x&(0=1)" `shouldBe` CON (Boolean False)
        it "optimizes (|)" $ do
            compileSource "x|(1=1)" `shouldBe` CON (Boolean True)
            compileSource "x|(0=1)" `shouldBe` VAR "x"
        it "optimizes hd, tl" $ do
            compileSource "hd (0^1)" `shouldBe` CON (Integer 0)
            compileSource "tl (0^1)" `shouldBe` CON (Integer 1)
    describe "run" $ do
        prop "integer literals" $ do
            \i -> runSource (show i) `shouldBe` Integer i
        "{ FAC N == N = 0 -> 1, N * FAC (N - 1)\n; RESULT FAC 10\n}\n" -=>
            Integer 3628800
        "{ odd  n==n=0->false,even(n-1)\n; even n==n=0->true ,odd (n-1)\n; result even 43\n}\n" -=>
            Boolean False
        "{ ones == 1 ^ ones\n; result hd ones\n}\n" -=>
            Integer 1

(-=>) :: String -> Value -> Spec
source -=> value = it (printf "program\n%s -=> %s\n" source (show value)) $ do
    runSource source `shouldBe` value

compileSource :: String -> CL
compileSource = lamToCL . expressionToLam . stringToExpression

runSource :: String -> Value
runSource = clToValue . compileSource
