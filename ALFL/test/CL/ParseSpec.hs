module CL.ParseSpec ( spec ) where

import           Prelude

import           Test.Hspec
import           Test.Hspec.QuickCheck

import           CL
import           CL.Parse

spec :: Spec
spec = do
    describe "parses" $ do
        prop "parse . show = id" $ do
            parse (:@:) |=| show

(|=|) :: (Eq a, Show a) => (b -> a) -> (a -> b) -> a -> Expectation
f |=| g = \t -> (f . g) t `shouldBe` t
