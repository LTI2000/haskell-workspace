module CL.OptimizeSpec ( spec ) where

import           Prelude

import           Test.Hspec

import           CL.Parse
import           CL.Optimize

spec :: Spec
spec = do
    describe "optimizes" $ do
        "   S K             " ==| "   K I         "
        "   S (K I)         " ==| "   I           "
        "   S (K (K x))     " ==| "   K (K x)     "
        "   S (K x) I       " ==| "   x           "
        "   S (K x) (K y)   " ==| "   K (x y)     "
        "   S f g x         " ==| "   f x (g x)   "
        "   K x y           " ==| "   x           "
        "   I x             " ==| "   x           "
        "   Y (K x)         " ==| "   x           "

(==|) :: String -> String -> Spec
t ==| u = it (t ++ " => " ++ u) $
    parse (.@.) t `shouldBe` parse (.@.) u
