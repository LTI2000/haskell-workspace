module Expression.ParseSpec ( spec ) where

import           Prelude

import           Test.Hspec
import           Test.Hspec.QuickCheck
import           Test.QuickCheck

import           Value
import           Expression
import           Expression.Parse

spec :: Spec
spec = do
    describe "parses" $ do
        prop "integer constants" $
            \(Positive i) -> stringToExpression (show i) `shouldBe`
                Constant (Integer i)
        prop "boolean constants" $
            \b -> stringToExpression (show b) `shouldBe`
                Constant (Boolean b)

        it "variables" $
            stringToExpression "x" `shouldBe` Variable "x"

        it "empty equation group" $
            stringToExpression "{result 0}" `shouldBe`
                EquationGroup [] (Result (Constant (Integer 0)))

        it "empty equation group with trailing semicolon" $
            stringToExpression "{result 0;}" `shouldBe`
                EquationGroup [] (Result (Constant (Integer 0)))

        it "1-equation group with result at end" $
            stringToExpression "{f == 0; result f}" `shouldBe`
                EquationGroup [ Equation "f" [] (Constant (Integer 0)) ]
                              (Result (Variable "f"))

        it "2-equation group with result at end" $
            stringToExpression "{f == 0; g == f; result g}" `shouldBe`
                EquationGroup [ Equation "f" [] (Constant (Integer 0))
                              , Equation "g" [] (Variable "f")
                              ]
                              (Result (Variable "g"))

        it "1-equation group with result at beginning" $
            stringToExpression "{result f; f == 0}" `shouldBe`
                EquationGroup [ Equation "f" [] (Constant (Integer 0)) ]
                              (Result (Variable "f"))

        it "2-equation group with result at beginning" $
            stringToExpression "{result g; f == 0; g == f}" `shouldBe`
                EquationGroup [ Equation "f" [] (Constant (Integer 0))
                              , Equation "g" [] (Variable "f")
                              ]
                              (Result (Variable "g"))

        it "equation group with result in the middle" $
            stringToExpression "{g == 0; result f; f == g}" `shouldBe`
                EquationGroup [ Equation "g" [] (Constant (Integer 0))
                              , Equation "f" [] (Variable "g")
                              ]
                              (Result (Variable "f"))
        it "equation group with result in the middle and trailing semicolon" $
            stringToExpression "{g == 0; result f; f == g;}" `shouldBe`
                EquationGroup [ Equation "g" [] (Constant (Integer 0))
                              , Equation "f" [] (Variable "g")
                              ]
                              (Result (Variable "f"))
