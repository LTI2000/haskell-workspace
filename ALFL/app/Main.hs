module Main ( main ) where

import           Prelude              ( IO )

import           Control.Monad        ( forM_ )
import           Data.String          ( String )

import           Text.Show            ( show )
import           Text.Printf          ( printf )

import           Expression.Parse
import           Expression.Translate
import           Lam.Transform
import           CL.Reduce
import           CL.Eval

main :: IO ()
main = do
    forM_ [ "{ fac n == n = 0 -> 1, n * fac (n - 1); result fac 10}"
          , "{ odd n == n = 0 -> 0, even (n-1); even n == n = 0 -> 1, odd (n-1); result even 43}"
          , "{ L == [1,2,3,4]; RESULT HD (TL L); }"
          , "{ L x == [1,x,3]; RESULT HD (TL (L 2)); }"
          , "{ L x == x ^ [1,2,3,4]; RESULT L; }"
          , "{ L x == [1,x,3,4]; RESULT L; }"
          , "{ ones == 1 ^ ones; result hd ones; }"
          , "{ numbersfrom n == n ^ numbersfrom (n + 1); result hd (tl (numbersfrom 1)); }"
          ]
          evalString
    evalFilePath "alfl-src/ones.alfl"

evalString :: String -> IO ()
evalString source = do
    let program = stringToExpression source
        lam = expressionToLam program
        cl = lamToCL lam
        result = clToValue cl
    printf "\n"
    printf "source:     %s\n" (show source)
    printf "Expression: %s\n" (show program)
    printf "Lam:        %s\n" (show lam)
    printf "CL:         %s\n" (show cl)
    printf "CL reduced: %s\n" (show (reduce cl))
    printf "Value:      %s\n" (show result)

evalFilePath :: String -> IO ()
evalFilePath filePath = do
    program <- filePathToExpression filePath
    let lam = expressionToLam program
        cl = lamToCL lam
        result = clToValue cl
    printf "\n"
    printf "Expression: %s\n" (show program)
    printf "Lam:        %s\n" (show lam)
    printf "CL:         %s\n" (show cl)
    printf "CL reduced: %s\n" (show (reduce cl))
    printf "Value:      %s\n" (show result)
