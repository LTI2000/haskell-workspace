module Lam.Transform
    ( lamToCL
    ) where

import           Data.Functor ( (<$>) )

import           Symbol
import           CL
import           CL.Optimize
import           CL.Eliminate
import           Lam

lamToCL :: Lam -> CL
lamToCL = transform

transform :: Lam -> CL
transform (Con c) = CON c
transform (Var x) = VAR x
transform (UnOp unOp t) =
    UNOP unOp .@. (transform t)
transform (BinOp binOp t u) =
    BINOP binOp .@. (transform t) .@. (transform u)
transform (App t u) = (transform t) .@. (transform u)
transform (Abs x t) = eliminate x (transform t)
transform (If t u v) = IF .@. (transform t) .@. (transform u) .@. (transform v)
transform (LetRec ds t) =
    transformLetRec ((\(f, e) -> (f, transform e)) <$> ds) (transform t)

transformLetRec :: [(Symbol, CL)] -> CL -> CL
transformLetRec [] r = r
transformLetRec ((f1, e1) : ds) r =
    let e1' = Y .@. (eliminate f1 e1)
        ds' = (\(f, e) -> (f, (eliminate f1 e) .@. e1')) <$> ds
        r' = (eliminate f1 r) .@. e1'
    in
        transformLetRec ds' r'
