module Lam
    ( Lam(..)
    ) where

import           Prelude                   ( (-), div )

import           Control.Applicative       ( Applicative((<*>)) )
import           Data.Functor              ( (<$>) )
import           Data.List                 ( intercalate )
import           Data.String               ( String )
import           Text.Printf               ( printf )
import           Text.Show                 ( Show(show) )

import           Test.QuickCheck.Arbitrary ( Arbitrary(arbitrary) )
import           Test.QuickCheck.Gen       ( oneof, sized )

import           Symbol
import           Value
import           Operators

data Lam = Con Value
         | Var Symbol
         | UnOp UnOp Lam
         | BinOp BinOp Lam Lam
         | App Lam Lam
         | Abs Symbol Lam
         | If Lam Lam Lam
         | LetRec [(Symbol, Lam)] Lam

instance Show Lam where
    show (Con c) = printf "%s" (show c)
    show (Var x) = printf "%s" x
    show (UnOp unOp t) = printf "(%s %s)" (show unOp) (show t)
    show (BinOp binOp t u) =
        printf "(%s %s %s)" (show t) (show binOp) (show u)
    show (App t u@(App _ _)) =
        printf "%s (%s)" (show t) (show u)
    show (App t u) = case u of
        (App _ _) -> printf "%s (%s)" (show t) (show u)
        _ -> printf "%s %s" (show t) (show u)
    show (Abs x t) = printf "λ%s.%s" x (show t)
    show (If t u v) = printf "%s -> %s, %s" (show t) (show u) (show v)
    show (LetRec defs t) = printf "let rec %s in %s" (showDefs defs) (show t)
      where
        showDefs :: [(Symbol, Lam)] -> String
        showDefs ds = intercalate " and "
                                  ((\(n, e) -> printf "%s = %s" n (show e)) <$> ds)

--------------------------------------------------------------------------------
instance Arbitrary Lam where
    arbitrary = sized arbitraryLam
      where
        arbitraryLam 0 = oneof [ Con <$> arbitrary, Var <$> arbitrarySym ]
        arbitraryLam n = oneof [ App <$> arbitraryLam n0 <*> arbitraryLam n1
                               , Abs <$> arbitrarySym <*> arbitraryLam n1
                               ]
          where
            n0 = n `div` 2
            n1 = (n - 1) `div` 2
