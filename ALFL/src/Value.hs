module Value ( Value(..) ) where

import           Prelude                   ( Integer, error )
import           Data.Bool                 ( (&&), Bool(False, True) )
import           Data.Eq                   ( Eq((==)) )
import           Data.Functor              ( (<$>) )
import           Data.String               ( String )

import           Text.Printf               ( printf )
import           Text.Show                 ( Show(show) )

import           Test.QuickCheck.Arbitrary ( Arbitrary(arbitrary, shrink) )

data Value = Boolean Bool
           | Integer Integer
           | Nil
           | Pair Value Value
           | Fun (Value -> Value)

instance Eq Value where
    (Boolean a) == (Boolean b) =
        a == b
    (Integer a) == (Integer b) =
        a == b
    Nil == Nil = True
    (Pair a b) == (Pair c d) =
        a == c && b == d
    (Fun _) == (Fun _) = error "error: cant compare fucntions"
    _ == _ = False

instance Show Value where
    show (Boolean False) = printf "false"
    show (Boolean True) = printf "true"
    show (Integer i) = printf "%s" (show i)
    show Nil = printf "[]"
    show p@(Pair _ _) = printf "[%s]" (showPair p)
      where
        showPair :: Value -> String
        showPair (Pair h Nil) = show h
        showPair (Pair h t) = printf "%s,%s" (show h) (showPair t)
    show (Fun _) = printf "<function>"

--------------------------------------------------------------------------------
instance Arbitrary Value where
    arbitrary = Integer <$> arbitrary
    shrink (Integer x) = Integer <$> shrink x
