module CL ( CL(..) ) where

import           Prelude                   ( (-), div )

import           Control.Applicative       ( Applicative(pure, (<*>)) )
import           Data.Eq                   ( Eq )
import           Data.Functor              ( (<$>) )
import           Text.Printf               ( printf )
import           Text.Show                 ( Show(show) )

import           Test.QuickCheck.Arbitrary ( Arbitrary(arbitrary) )
import           Test.QuickCheck.Gen       ( oneof, sized )

import           Symbol
import           Value
import           Operators

data CL = CON Value
        | VAR Symbol
        | UNOP UnOp
        | BINOP BinOp
        | IF
        | (:@:) CL CL
        | S
        | K
        | I
        | Y
    deriving Eq

instance Show CL where
    show (CON c) = printf "%s" (show c)
    show (VAR x) = printf "%s" x
    show (UNOP unOp) = printf "%s" (show unOp)
    show (BINOP binOp) = printf "%s" (show binOp)
    show IF = printf "IF"
    show (t :@: u) = case u of
        (_ :@: _) -> printf "%s (%s)" (show t) (show u)
        _ -> printf "%s %s" (show t) (show u)
    show S = printf "S"
    show K = printf "K"
    show I = printf "I"
    show Y = printf "Y"

--------------------------------------------------------------------------------
instance Arbitrary CL where
    arbitrary = sized arbitraryCL
      where
        arbitraryCL 0 = oneof [ pure S
                              , pure K
                              , pure I
                              , pure Y
                              , VAR <$> arbitrarySym
                              ]
        arbitraryCL n = (:@:) <$> arbitraryCL n0 <*> arbitraryCL n1
          where
            n0 = n `div` 2
            n1 = (n - 1) `div` 2
