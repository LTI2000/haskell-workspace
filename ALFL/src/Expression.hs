module Expression
    ( Expression(..)
    , Equation(..)
    , Result(..)
    ) where

import           Data.Eq   ( Eq )

import           Text.Show ( Show )

import           Symbol
import           Value
import           Operators

data Expression = EquationGroup [Equation] Result
                | Conditional Expression Expression Expression
                | Variable Symbol
                | UnaryOperator UnOp Expression
                | BinaryOperator BinOp Expression Expression
                | Constant Value
                | Abstraction Symbol Expression
                | Application Expression Expression
    deriving (Eq, Show)

data Equation = Equation Symbol [Symbol] Expression
    deriving (Eq, Show)

data Result = Result Expression
    deriving (Eq, Show)
