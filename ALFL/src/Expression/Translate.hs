module Expression.Translate ( expressionToLam ) where

import           Data.Functor ( (<$>) )
import           Data.List    ( foldr )

import           Symbol
import           Lam
import           Expression

expressionToLam :: Expression -> Lam
expressionToLam = translateExpression

translateExpression :: Expression -> Lam
translateExpression (EquationGroup equs (Result exp)) =
    LetRec (translateEquation <$> equs) (translateExpression exp)
translateExpression (Conditional exp con alt) =
    If (translateExpression exp)
       (translateExpression con)
       (translateExpression alt)
translateExpression (Constant val) =
    Con val
translateExpression (Variable sym) =
    Var sym
translateExpression (UnaryOperator unOp exp) =
    UnOp unOp (translateExpression exp)
translateExpression (BinaryOperator binOp exp1 exp2) =
    BinOp binOp (translateExpression exp1) (translateExpression exp2)
translateExpression (Abstraction sym exp) =
    Abs sym (translateExpression exp)
translateExpression (Application rator rand) =
    App (translateExpression rator) (translateExpression rand)

translateEquation :: Equation -> (Symbol, Lam)
translateEquation (Equation sym args exp) =
    (sym, translateExpression (foldr Abstraction exp args))
