{-# LANGUAGE TupleSections #-}

module Expression.Parse
    ( stringToExpression
    , filePathToExpression
    ) where

import           Text.Parsec
import           Text.ParserCombinators.Parsec.Expr
import           Text.ParserCombinators.Parsec.Language
import qualified Text.ParserCombinators.Parsec.Token    as Token

import           Prelude                                ( Integer, error )
import           Control.Applicative                    ( Applicative((<*>), (<*), (*>)) )
import           Control.Arrow                          ( Arrow((***)) )
import           Control.Monad
import           Data.Bool                              ( Bool(False, True) )
import           Data.Char                              ( Char, toLower )
import           Data.Either                            ( Either(Left, Right)
                                                        , isLeft, lefts, rights )
import           Data.Function                          ( ($), (.) )
import           Data.Functor                           ( (<$>) )
import           Data.List                              ( (++), foldl1, foldr
                                                        , nub, partition, take )
import           Data.Maybe                             ( Maybe(Nothing, Just) )
import           Data.Map                               ( Map )
import qualified Data.Map                               as Map
import           Data.String                            ( String )

import           Text.Show                              ( Show(show) )

import           System.IO                              ( IO, readFile )

import           Symbol
import           Value
import           Operators
import           Expression

--------------------------------------------------------------------------------
data Kind = Name
          | Val Value
          | Un UnOp
          | Bin BinOp

type Env = [Map Symbol Kind]

userState :: Env
userState = [ Map.fromList $
                constants ++ unOps ++ binOps
            ]
  where
    constants = [ ("false", Val (Boolean False)), ("true", Val (Boolean True)) ]
    unOps = [ (name, Un op)
            | (name, op) <- unOpsCurried ]
    binOps = [ (name, Bin op)
             | (name, op) <- binOpsCurried ]

lookupName :: String -> MyParsec Kind
lookupName name = do
    envs <- getState
    case lookup name envs of
        Just kind -> return kind
        Nothing -> return Name -- unexpected (printf "not in scope: %s" name)
  where
    lookup _ [] = Nothing
    lookup k (map : maps) = case Map.lookup k map of
        Nothing -> lookup k maps
        v -> v

localNames :: [String] -> MyParsec a -> MyParsec a
localNames names p = do
    envs <- getState
    let env' = Map.fromList $ (,Name) <$> names
    putState (env' : envs)
    x <- p
    putState envs
    return x

--------------------------------------------------------------------------------
type UserState = Env

type MyLanguageDef = LanguageDef UserState

type MyTokenParser = Token.TokenParser UserState

type MyParsec = Parsec String UserState

type MyOperatorTable a = OperatorTable Char UserState a

--------------------------------------------------------------------------------
languageDef :: MyLanguageDef
languageDef = emptyDef { Token.commentLine = "%"
                       , Token.identStart = letter
                       , Token.identLetter = alphaNum
                       , Token.opStart = oneOf opStarts
                       , Token.opLetter = oneOf opLetters
                       , Token.reservedNames = [ "result" ]
                       , Token.reservedOpNames = opTokens
                       , Token.caseSensitive = False
                       }
  where
    opStarts :: String
    opStarts = nub $ join $ take 1 <$> opTokens

    opLetters :: String
    opLetters = nub $ join opTokens

    opTokens :: [String]
    opTokens = nub (unOpNames ++ binOpNames ++ [ "->" ])

lexer :: MyTokenParser
lexer = Token.makeTokenParser languageDef

identifier :: MyParsec String
identifier = (toLower <$>) <$> Token.identifier lexer

reserved :: String -> MyParsec ()
reserved = Token.reserved lexer

reservedOp :: String -> MyParsec ()
reservedOp = Token.reservedOp lexer

parens :: MyParsec a -> MyParsec a
parens = Token.parens lexer

braces :: MyParsec a -> MyParsec a
braces = Token.braces lexer

brackets :: MyParsec a -> MyParsec a
brackets = Token.brackets lexer

natural :: MyParsec Integer
natural = Token.natural lexer

semi :: MyParsec String
semi = Token.semi lexer

comma :: MyParsec String
comma = Token.comma lexer

whiteSpace :: MyParsec ()
whiteSpace = Token.whiteSpace lexer

--------------------------------------------------------------------------------
{-

Operator Prededence

 // \\ || ::
      .            (function composition)
      :
   <space>         (function application)
     + -           (the unary versions)
    * / \
     + -           (the binary versions)
= < > >= <= <>
      ~
      &
      |
      ^^
      ^
      -> ,         (conditional)
    @ ==           (infix lambda)

-}
operatorTable :: MyOperatorTable Expression
operatorTable = [ [ prefixOp Plus, prefixOp Negate ]
                , [ infixOp Mult AssocLeft
                  , infixOp Div AssocLeft
                  , infixOp Rem AssocLeft
                  ]
                , [ infixOp Add AssocLeft, infixOp Sub AssocLeft ]
                , [ infixOp Equal AssocLeft
                  , infixOp Lt AssocLeft
                  , infixOp Gt AssocLeft
                  , infixOp Le AssocLeft
                  , infixOp Ge AssocLeft
                  , infixOp Notequal AssocLeft
                  ]
                , [ prefixOp Not ]
                , [ infixOp And AssocLeft ]
                , [ infixOp Or AssocLeft ]
                , [ infixOp Append AssocLeft ]
                , [ infixOp Cons AssocLeft ]
                ]
  where
    prefixOp unOp = let (Just op) = unOpName unOp
                    in
                        Prefix (reservedOp op >> return (UnaryOperator unOp))
    infixOp binOp = let (Just op) = (binOpName binOp)
                    in
                        Infix (reservedOp op >> return (BinaryOperator binOp))

--------------------------------------------------------------------------------
program :: MyParsec Expression
program = whiteSpace *> expression <* eof

expression :: MyParsec Expression
expression = do
    t <- expression'
    c <- optionMaybe conditional
    case c of
        Nothing -> return t
        Just (x, y) -> return $ Conditional t x y
  where
    conditional :: MyParsec (Expression, Expression)
    conditional = (,) <$> (reservedOp "->" *> expression)
                      <*> (comma *> expression)

    expression' :: MyParsec Expression
    expression' = buildExpressionParser operatorTable juxta

juxta :: MyParsec Expression
juxta = many1 term >>= return . foldl1 Application

term :: MyParsec Expression
term = parens expression
    <|> equationGroup
    <|> list
    <|> variable
    <|> constant

equationGroup :: MyParsec Expression
equationGroup = braces $
    resultOrEquation `sepEndBy1` semi >>=
        buildGroup . (lefts *** rights) . partition isLeft
  where
    buildGroup :: ([Equation], [Result]) -> MyParsec Expression
    buildGroup (equ, [ res ]) =
        return $ EquationGroup equ res
    buildGroup (_, []) = unexpected "no result clause in equation group"
    buildGroup (_, _) = unexpected "more than one result clause in equation group"

equation :: MyParsec Equation
equation = do
    f <- identifier
    args <- (many identifier <* reservedOp "==")
    body <- localNames args expression
    return $ Equation f args body

result :: MyParsec Result
result = reserved "result" *> (Result <$> expression)

resultOrEquation :: MyParsec (Either Equation Result)
resultOrEquation = Right <$> result <|> Left <$> equation

list :: MyParsec Expression
list = brackets $ do
    expression `sepBy` comma >>=
        return . (foldr (BinaryOperator Cons) (Constant Nil))

variable :: MyParsec Expression
variable = do
    i <- identifier
    kind <- lookupName i
    return $
        case kind of
            Name -> Variable i
            Val val -> Constant val
            Un unOp -> Abstraction "x" (UnaryOperator unOp (Variable "x"))
            Bin binOp -> Abstraction "x"
                                     (Abstraction "y"
                                                  (BinaryOperator binOp
                                                                  (Variable "x")
                                                                  (Variable "y")))

constant :: MyParsec Expression
constant = Constant <$> value

value :: MyParsec Value
value = Integer <$> natural

--------------------------------------------------------------------------------
filePathToExpression :: String -> IO Expression
filePathToExpression filePath = do
    source <- readFile filePath
    return (parseProgram source filePath)

stringToExpression :: String -> Expression
stringToExpression str =
    parseProgram str ""

parseProgram :: String -> String -> Expression
parseProgram source location =
    case runParser program userState location source of
        Left e -> error $ show e
        Right r -> r
