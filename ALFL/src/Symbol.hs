module Symbol
    ( Symbol
    , arbitrarySym
    ) where

import           Data.String         ( String )

import           Test.QuickCheck.Gen ( Gen, elements )

type Symbol = String

--------------------------------------------------------------------------------
arbitrarySym :: Gen Symbol
arbitrarySym = elements [ [ sym ]
                        | sym <- ['x' .. 'z'] ]
