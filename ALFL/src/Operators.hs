{-# LANGUAGE DuplicateRecordFields #-}

module Operators
    ( UnOp(..)
    , BinOp(..)
    , unOpApply
    , unOpName
    , unOpNames
    , unOpsCurried
    , binOpApply
    , binOpName
    , binOpNames
    , binOpsCurried
    ) where

import           Prelude     ( (*), (+), (-), Bounded, Enum, Integer, div
                             , error, maxBound, minBound, rem )

import           Data.Bool   ( (&&), (||), Bool, not )
import           Data.Eq     ( Eq((==), (/=)) )
import           Data.Maybe  ( Maybe(Nothing, Just), catMaybes )
import           Data.Ord    ( (<), (<=), (>), (>=) )
import           Data.String ( String )

import           Text.Show   ( Show(show) )

import           Value

data UnOp = Plus | Negate | Not | Hd | Tl
    deriving (Eq, Enum, Bounded)

unOpInfo :: UnOp -> UnOpInfo
unOpInfo Plus = UnOpInfo { opName = Just "+"
                         , curriedName = "plus"
                         , op = \(Integer i) -> Integer i
                         }
unOpInfo Negate = UnOpInfo { opName = Just "-"
                           , curriedName = "negate"
                           , op = \(Integer i) -> Integer (-i)
                           }
unOpInfo Not = UnOpInfo { opName = Just "~"
                        , curriedName = "not"
                        , op = \(Boolean b) -> Boolean (not b)
                        }
unOpInfo Hd = UnOpInfo { opName = Nothing
                       , curriedName = "hd"
                       , op = \(Pair x _) -> x
                       }
unOpInfo Tl = UnOpInfo { opName = Nothing
                       , curriedName = "tl"
                       , op = \(Pair _ y) -> y
                       }

unOpName :: UnOp -> Maybe String
unOpName unOp = let UnOpInfo{opName} = unOpInfo unOp
                in
                    opName

unOpCurriedName :: UnOp -> String
unOpCurriedName unOp = let UnOpInfo{curriedName} = unOpInfo unOp
                       in
                           curriedName

unOpApply :: UnOp -> Value -> Value
unOpApply unOp v = let UnOpInfo{op} = unOpInfo unOp
                   in
                       op v

unOpNames :: [String]
unOpNames = catMaybes [ unOpName op
                      | op <- [minBound .. maxBound] ]

unOpsCurried :: [(String, UnOp)]
unOpsCurried = [ (unOpCurriedName op, op)
               | op <- [minBound .. maxBound] ]

data BinOp = Add
           | Sub
           | Mult
           | Div
           | Rem
           | Equal
           | Lt
           | Gt
           | Le
           | Ge
           | Notequal
           | And
           | Or
           | Append
           | Cons
    deriving (Eq, Enum, Bounded)

binOpInfo :: BinOp -> BinOpInfo
binOpInfo Add = BinOpInfo { opName = Just "+"
                          , curriedName = "add"
                          , op = arithOp (+)
                          }
binOpInfo Sub = BinOpInfo { opName = Just "-"
                          , curriedName = "sub"
                          , op = arithOp (-)
                          }
binOpInfo Mult = BinOpInfo { opName = Just "*"
                           , curriedName = "mult"
                           , op = arithOp (*)
                           }
binOpInfo Div = BinOpInfo { opName = Just "/"
                          , curriedName = "div"
                          , op = arithOp div
                          }
binOpInfo Rem = BinOpInfo { opName = Just "\\"
                          , curriedName = "rem"
                          , op = arithOp rem
                          }
binOpInfo Equal = BinOpInfo { opName = Just "="
                            , curriedName = "equal"
                            , op = relOp (==)
                            }
binOpInfo Lt = BinOpInfo { opName = Just "<"
                         , curriedName = "lt"
                         , op = intRelOp (<)
                         }
binOpInfo Gt = BinOpInfo { opName = Just ">"
                         , curriedName = "gt"
                         , op = intRelOp (>)
                         }
binOpInfo Le = BinOpInfo { opName = Just "<="
                         , curriedName = "le"
                         , op = intRelOp (<=)
                         }
binOpInfo Ge = BinOpInfo { opName = Just ">="
                         , curriedName = "ge"
                         , op = intRelOp (>=)
                         }
binOpInfo Notequal = BinOpInfo { opName = Just "<>"
                               , curriedName = "notequal"
                               , op = relOp (/=)
                               }
binOpInfo And = BinOpInfo { opName = Just "&"
                          , curriedName = "and"
                          , op = boolOp (&&)
                          }
binOpInfo Or = BinOpInfo { opName = Just "|"
                         , curriedName = "or"
                         , op = boolOp (||)
                         }
binOpInfo Append = BinOpInfo { opName = Just "^^"
                             , curriedName = "append"
                             , op = error "NYI: append"
                             }
binOpInfo Cons = BinOpInfo { opName = Just "^", curriedName = "fby", op = Pair }

binOpName :: BinOp -> Maybe String
binOpName binOp = let BinOpInfo{opName} = binOpInfo binOp
                  in
                      opName

binOpCurriedName :: BinOp -> String
binOpCurriedName binOp =
    let BinOpInfo{curriedName} =
            binOpInfo binOp
    in
        curriedName

binOpApply :: BinOp -> Value -> Value -> Value
binOpApply binOp v1 v2 =
    let BinOpInfo{op} = binOpInfo binOp
    in
        op v1 v2

binOpNames :: [String]
binOpNames = catMaybes [ binOpName op
                       | op <- [minBound .. maxBound] ]

binOpsCurried :: [(String, BinOp)]
binOpsCurried = [ (binOpCurriedName op, op)
                | op <- [minBound .. maxBound] ]

relOp :: (Value -> Value -> Bool) -> Value -> Value -> Value
relOp op x y = Boolean (x `op` y)

intRelOp :: (Integer -> Integer -> Bool) -> Value -> Value -> Value
intRelOp op (Integer i) (Integer j) =
    Boolean (i `op` j)

arithOp :: (Integer -> Integer -> Integer) -> Value -> Value -> Value
arithOp op (Integer i) (Integer j) =
    Integer (i `op` j)

boolOp :: (Bool -> Bool -> Bool) -> Value -> Value -> Value
boolOp op (Boolean a) (Boolean b) =
    Boolean (a `op` b)

data UnOpInfo = UnOpInfo { opName      :: Maybe String
                         , curriedName :: String
                         , op          :: Value -> Value
                         }

data BinOpInfo = BinOpInfo { opName      :: Maybe String
                           , curriedName :: String
                           , op          :: Value -> Value -> Value
                           }

instance Show UnOp where
    show unOp = let UnOpInfo{opName,curriedName} =
                        unOpInfo unOp
                in
                    case opName of
                        Nothing -> curriedName
                        Just name -> name

instance Show BinOp where
    show binOp = let BinOpInfo{opName,curriedName} =
                         binOpInfo binOp
                 in
                     case opName of
                         Nothing -> curriedName
                         Just name -> name
