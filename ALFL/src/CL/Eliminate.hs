module CL.Eliminate ( eliminate ) where

import           Data.Bool   ( otherwise )
import           Data.Eq     ( Eq((==)) )
import           Data.List   ( (++), notElem )

import           Symbol
import           CL
import           CL.Optimize

eliminate :: Symbol -> CL -> CL
eliminate _ c@(CON _) = K .@. c
eliminate x v@(VAR y)
    | x == y = I
    | otherwise = K .@. v
eliminate _ u@(UNOP _) =
    K .@. u
eliminate _ b@(BINOP _) =
    K .@. b
eliminate _ IF = K .@. IF
eliminate _ S = K .@. S
eliminate _ K = K .@. K
eliminate _ I = K .@. I
eliminate _ Y = K .@. Y
eliminate x t | x `notElem` freeVars t =
                    K .@. t
eliminate x (t :@: u) = S .@. (eliminate x t) .@. (eliminate x u)

freeVars :: CL -> [Symbol]
freeVars (VAR x) = [ x ]
freeVars (t :@: u) = freeVars t ++ freeVars u
freeVars _ = []
