module CL.Eval ( clToValue ) where

import           Prelude     ( error )

import           Data.Bool   ( Bool(True) )
import           Data.Eq     ( Eq((==)) )

import           Text.Printf ( printf )
import           Text.Show   ( Show(show) )

import           Value
import           Operators
import           CL

clToValue :: CL -> Value
clToValue = eval

eval :: CL -> Value
eval (CON c) = c
eval (VAR v) = error (printf "not in scope: %s" v)
eval (UNOP unOp) = Fun (\x -> unOpApply unOp x)
eval (BINOP binOp) = Fun (\x -> Fun (\y -> binOpApply binOp x y))
eval IF = Fun (\t -> Fun (\c -> Fun (\a -> if t == (Boolean True) then c else a)))
eval (x :@: y) = apply (eval x) (eval y)
eval S = Fun (\x -> Fun (\y -> Fun (\z -> apply (apply x z) (apply y z))))
eval K = Fun (\x -> Fun (\_ -> x))
eval I = Fun (\x -> x)
eval Y = Fun (\f -> apply (Fun (\x -> apply f (apply x x)))
                          (Fun (\x -> apply f (apply x x))))

apply :: Value -> Value -> Value
apply (Fun x) y = x y
apply x y = error (printf "apply: %s %s" (show x) (show y))
{-# NOINLINE apply #-}
