module CL.Reduce ( reduce ) where

import           Prelude     ( (+) )

import           Data.Bool   ( Bool(False, True), otherwise )
import           Data.Eq     ( Eq((==)) )
import           Data.Int    ( Int )
import           Data.Ord    ( (<) )
import           Text.Printf ( printf )

import           Debug.Trace ( trace )

import           Value
import           Operators
import           CL

reduce :: CL -> CL
reduce = loop 0
  where
    loop :: Int -> CL -> CL
    loop n x
        | n < 10000 = let n' = n + 1
                          x' = (reduce0 x)
                      in
                          if (x == x')
                          then trace (printf "Reductions: %d\n" n') x'
                          else (loop n' x')
        | otherwise = trace "Exhausted" x

reduce0 :: CL -> CL
reduce0 c@(CON _) = c
reduce0 ((UNOP Hd) :@: ((BINOP Cons) :@: x :@: _)) =
    x
reduce0 ((UNOP Tl) :@: ((BINOP Cons) :@: _ :@: y)) =
    y
reduce0 ((UNOP unOp) :@: (CON c)) =
    CON (unOpApply unOp c)
reduce0 (u@(UNOP _) :@: x) =
    u :@: reduce0 x
reduce0 ((BINOP binOp) :@: (CON c) :@: (CON d)) =
    CON (binOpApply binOp c d)
reduce0 (b@(BINOP _) :@: x :@: y) =
    b :@: reduce0 x :@: reduce0 y
reduce0 (IF :@: (CON (Boolean True)) :@: c :@: _) =
    c
reduce0 (IF :@: (CON (Boolean False)) :@: _ :@: a) =
    a
reduce0 (IF :@: t :@: c :@: a) =
    IF :@: reduce0 t :@: c :@: a
reduce0 (S :@: x :@: y :@: z) =
    x :@: z :@: (y :@: z)
reduce0 (K :@: x :@: _) =
    x
reduce0 (I :@: x) = x
reduce0 (Y :@: f) = f :@: (Y :@: f)
reduce0 (x :@: y) = reduce0 x :@: y
reduce0 x = x
