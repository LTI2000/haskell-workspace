module CL.Optimize
    ( (.@.)
    ) where

import           Data.Bool ( Bool(False, True) )
import           Data.Eq   ( Eq((==)) )
import           Data.List ( elem )

import           Value
import           Operators
import           CL

(.@.) :: CL -> CL -> CL
x .@. y = optimize (x :@: y)

optimize :: CL -> CL
-- | 1. S K => K I
optimize (S :@: K) = K .@. I

-- | 2. S (K I) => I
optimize (S :@: (K :@: I)) =
    I

-- | 3. S (K (K x)) => K (K x)
optimize (S :@: (K :@: (K :@: x))) =
    K .@. (K .@. x)

-- | 4. S (K x) I => x
optimize (S :@: (K :@: x) :@: I) =
    x

-- | 5. S (K x) (K y) => K (x y)
optimize (S :@: (K :@: x) :@: (K :@: y)) =
    K .@. (x .@. y)

-- | 6. S f g x => f x (g x)
optimize (S :@: f :@: g :@: x) =
    (f .@. x) .@. (g .@. x)

-- | 7. K x y => x
optimize (K :@: x :@: _) =
    x

-- | 8. I x => x
optimize (I :@: x) = x

-- | 9. Y (K x) => x
optimize (Y :@: (K :@: x)) =
    x

-- | 10. binop c1 c2 => z, where c1 and c2 are constants, z = c1 binop c2,
--       and binop is one of +, *, -, / etc.
optimize ((BINOP binOp) :@: (CON c1) :@: (CON c2)) | binOp `elem`
                                                         [ Add
                                                         , Sub
                                                         , Mult
                                                         , Div
                                                         , Rem
                                                         , Lt
                                                         , Gt
                                                         , Le
                                                         , Ge
                                                         ] = CON (binOpApply binOp
                                                                             c1
                                                                             c2)

-- | 12. EQ x y => true, if x is the same object as y,
--       or if they have the same value
-- | 13. EQ x y => false, if x and y are of different types,
--       or if they have the same type but different values.
optimize ((BINOP Equal) :@: (CON x) :@: (CON y)) =
    CON (Boolean (x == y))

-- | 14. NOT true => false, NOT false => true
optimize ((UNOP Not) :@: (CON (Boolean True))) =
    CON (Boolean False)
optimize ((UNOP Not) :@: (CON (Boolean False))) =
    CON (Boolean True)

-- | 15. AND true => I, AND false => K false
optimize ((BINOP And) :@: (CON (Boolean True))) =
    I
optimize ((BINOP And) :@: (CON (Boolean False))) =
    K .@. CON (Boolean False)

-- | 16. AND x true => x, AND x false = false
optimize ((BINOP And) :@: x :@: (CON (Boolean True))) =
    x
optimize ((BINOP And) :@: _ :@: (CON (Boolean False))) =
    CON (Boolean False)

-- | 17. OR true => K true, OR false => I
optimize ((BINOP Or) :@: (CON (Boolean True))) =
    K .@. CON (Boolean True)
optimize ((BINOP Or) :@: (CON (Boolean False))) =
    I

-- | 18. OR x true => true, OR x false = x
optimize ((BINOP Or) :@: _ :@: (CON (Boolean True))) =
    CON (Boolean True)
optimize ((BINOP Or) :@: x :@: (CON (Boolean False))) =
    x

-- | 19. IF true => K, IF false => K I
optimize (IF :@: (CON (Boolean True))) =
    K
optimize (IF :@: (CON (Boolean False))) =
    K .@. I

-- | 20. IF (NOT x) => S (K (S (IF x))) K
optimize (IF :@: ((UNOP Not) :@: x)) =
    S .@. (K .@. (S .@. (IF .@. x))) .@. K

-- | 21. (IF p c a) x => IF p (c x) (a x)
optimize ((IF :@: p :@: c :@: a) :@: x) =
    IF .@. p .@. (c .@. x) .@. (a .@. x)

-- | 23. HD (P x y) => x, TL (P x y) => y
optimize ((UNOP Hd) :@: ((BINOP Cons) :@: x :@: _)) =
    x
optimize ((UNOP Tl) :@: ((BINOP Cons) :@: _ :@: y)) =
    y

-- | otherwise
optimize x = x
