module CL.Parse
    ( parse
    ) where

import           Prelude                   (  error )

import           Data.Function             ( flip )
import           Data.List                 ( (++), elem, foldr1 )
import           Data.String               ( String )

import           Text.Show                 ( Show(show) )

import CL

parse :: (CL -> CL -> CL) -> String -> CL
parse app = parse' ([], [])
  where
    parse' :: ([CL], [[CL]]) -> String -> CL
    parse' (s, []) "" = combine s
    parse' (_, _) "" = error "missing ')'"
    parse' s (' ' : r) = parse' s r
    parse' s ('S' : r) = parse' (push S s) r
    parse' s ('K' : r) = parse' (push K s) r
    parse' s ('I' : r) = parse' (push I s) r
    parse' s ('Y' : r) = parse' (push Y s) r
    parse' s ('(' : r) = parse' (enter s) r
    parse' s (')' : r) = parse' (leave s) r
    parse' s (x : r) | x `elem` ['a' .. 'z'] = parse' (push (VAR [ x ]) s) r
    parse' _ (x : _) = error ("bad token " ++ show x)

    push :: CL -> ([CL], [[CL]]) -> ([CL], [[CL]])
    push x (s, ss) = (x : s, ss)

    enter :: ([CL], [[CL]]) -> ([CL], [[CL]])
    enter (s, ss) = ([], s : ss)

    leave :: ([CL], [[CL]]) -> ([CL], [[CL]])
    leave (_, []) = error "extra ')'"
    leave (s, (s0 : s1)) = push (combine s) (s0, s1)

    combine :: [CL] -> CL
    combine [] = error "empty term"
    combine s = foldr1 (flip app) s
