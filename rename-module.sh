#!/bin/bash

set -eu

OLD_NAME=${1?"OLD_NAME missing!"}
NEW_NAME=${2?"NEW_NAME missing!"}

function process_haskell_files {
    local OLD_MODULE_NAME=$1 ; shift
    local NEW_MODULE_NAME=$1 ; shift
    local SRC_DIR_NAME=$1    ; shift
    local SPEC_SUFFIX=$1     ; shift

    local OLD_FILE_NAME=${SRC_DIR_NAME}/$(echo ${OLD_MODULE_NAME}${SPEC_SUFFIX} | sed -e 's|\.|/|g').hs
    local OLD_MODULE_PATTERN=$(echo ${OLD_MODULE_NAME} | sed -e 's|\.|\\.|g')

    local NEW_FILE_NAME=${SRC_DIR_NAME}/$(echo ${NEW_MODULE_NAME}${SPEC_SUFFIX} | sed -e 's|\.|/|g').hs
    local NEW_FILE_PARENT_DIR=$(dirname ${NEW_FILE_NAME})

    if [[ -e ${OLD_FILE_NAME} ]] ; then
        sed -i 's|\(^module \+\)'${OLD_MODULE_PATTERN}'\('${SPEC_SUFFIX}'\)|\1'${NEW_MODULE_NAME}'\2|' ${OLD_FILE_NAME}
        mkdir -p ${NEW_FILE_PARENT_DIR}
        mv ${OLD_FILE_NAME} ${NEW_FILE_NAME}
    fi
    for FILE_NAME in $(grep -Erl '^import +(qualified)?'${OLD_MODULE_PATTERN} ${SRC_DIR_NAME}) ; do
        sed -i 's|\(^import \+\(qualified\)\? \+\)'${OLD_MODULE_PATTERN}'|\1'${NEW_MODULE_NAME}'|' ${FILE_NAME}
    done
}

function process_cabal_files {
    local OLD_MODULE_NAME=$1 ; shift
    local NEW_MODULE_NAME=$1 ; shift

    local OLD_MODULE_PATTERN=$(echo ${OLD_MODULE_NAME} | sed -e 's|\.|\\.|g')

    for FILE_NAME in *.cabal ; do
        sed -i 's|'${OLD_MODULE_PATTERN}'|'${NEW_MODULE_NAME}'|' ${FILE_NAME}
    done
}

process_haskell_files ${OLD_NAME} ${NEW_NAME} 'app'  ''
process_haskell_files ${OLD_NAME} ${NEW_NAME} 'src'  ''
process_haskell_files ${OLD_NAME} ${NEW_NAME} 'test' 'Spec'
process_cabal_files   ${OLD_NAME} ${NEW_NAME}
