module Main where

import           Control.Monad
import           Prelude

import           Queens

main :: IO ()
main = printQueens n
 where
  n :: Int
  n = 8

printQueens :: Int -> IO ()
printQueens n =
  forM_
      (queens n)
      (\sol -> putStrLn (replicate (2 * (n + 2)) '#') >> forM_
        sol
        (\pos ->
          putStr "##"
            >> printIndented (pos - 1)
            >> putStr "  "
            >> printIndented (n - pos)
            >> putStrLn "##"
        )
      )
    >> putStrLn (replicate (2 * (n + 2)) '#')

printIndented :: Int -> IO ()
printIndented n = putStr (take (2 * n) (cycle "[]"))
