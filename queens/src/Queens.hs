module Queens
    ( queens
    ) where

import           Data.List
import           Prelude

queens :: Int -> [[Int]]
queens n = queens0 (permutations [1 .. n])

queens0 :: [[Int]] -> [[Int]]
queens0 = filter isSolution

isSolution :: [Int] -> Bool
isSolution []       = True
isSolution (x : xs) = isSafe x xs && isSolution xs

isSafe :: Int -> [Int] -> Bool
isSafe x xs = not (any (sameDiagonal (1, x)) (zip [2 ..] xs))

sameDiagonal :: (Int, Int) -> (Int, Int) -> Bool
sameDiagonal (x0, y0) (x1, y1) = abs (x1 - x0) == abs (y1 - y0)
