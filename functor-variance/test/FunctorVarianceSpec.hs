module FunctorVarianceSpec ( spec ) where

import           Data.Function
import           Data.Bool
import           Data.Char
import           Data.Int
import           Data.String

import           Test.Hspec
import           Test.Hspec.QuickCheck

import           Test.QuickCheck.Function

import           FunctorVariance

spec :: Spec
spec = do
    describe "Identity" $ do
        prop "fmapI id === id" $ do
            \i -> let i' = Identity i :: Identity Int
                      f' = id
                      Identity a = fmapI f' i'
                      Identity b = i'
                  in
                      a `shouldBe` b
        prop "fmapI f . fmapI g === fmapI (f . g)" $ do
            \(i, f, g) -> let i' = Identity i :: Identity Int
                              f' = applyFun f :: String -> Bool
                              g' = applyFun g :: Int -> String
                              Identity a = (fmapI f' . fmapI g') i'
                              Identity b = (fmapI (f' . g')) i'
                          in
                              a `shouldBe` b
    describe "Predicate" $ do
        prop "comtramapP id === id" $ do
            \(i, p) -> let p' = Predicate (applyFun p) :: Predicate Int
                           a = testPredicate p' i
                           b = testPredicate (contramapP id p') i
                       in
                           a `shouldBe` b
        prop "contramapP f . contramapP g === contramapP (g . f)" $ do
            \(i, p, f, g) -> let p' = Predicate (applyFun p) :: Predicate Int
                                 f' = applyFun g :: String -> Char
                                 g' = applyFun f :: Char -> Int
                                 a = testPredicate ((contramapP f' . contramapP g') p') i
                                 b = testPredicate (contramapP (g' . f') p') i
                             in
                                 a `shouldBe` b


testPredicate :: Predicate a -> a -> Bool
testPredicate (Predicate p) x = p x
