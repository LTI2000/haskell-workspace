module FunctorVariance where

import           Data.Bool ( Bool )

newtype Identity a = Identity a

fmapI :: (a -> b) -> Identity a -> Identity b
fmapI a'to'b (Identity a) =
    Identity (let b = a'to'b a
              in
                  b)

newtype Predicate a = Predicate (a -> Bool)

contramapP :: (b -> a) -> Predicate a -> Predicate b
contramapP b'to'a (Predicate a'to'bool) =
    Predicate (\b -> let a = b'to'a b
                         bool = a'to'bool a
                     in
                         bool)

newtype PredicateCaller a = PredicateCaller ((a -> Bool) -> Bool)

fmapPC :: (a -> b) -> PredicateCaller a -> PredicateCaller b
fmapPC a'to'b (PredicateCaller _a'to'bool_'to'bool) =
    PredicateCaller (\b'to'bool -> let a'to'bool = \a -> let b = a'to'b a
                                                             bool = b'to'bool b
                                                         in
                                                             bool
                                       bool = _a'to'bool_'to'bool a'to'bool
                                   in
                                       bool)
