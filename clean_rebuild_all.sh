#!/bin/bash

set -eux

function delete_stack_work_dir {
    rm -rf .stack-work
}

function build_stack_project {
    stack --install-ghc build
    stack test
}

function walk_project_dirs {
    local FUNCTION=$1 ; shift
    for i in $(find . -name "*cabal" ! -name "{{*}}.cabal") ; do
        pushd $(dirname $i)
        ${FUNCTION}
        popd
    done
}

for i in delete_stack_work_dir build_stack_project ; do
    walk_project_dirs $i
done
