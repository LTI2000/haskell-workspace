module Main ( main ) where

import           Prelude       hiding ( lines )
import           Data.Foldable

main :: IO ()
main = do
    let firstLine = [ 1 ]
        lines = iterate nextLine firstLine
        numberedLines = zip [0 ..] lines
        depth = 13
    traverse_ (prettyPrint depth) (take depth numberedLines)

nextLine :: [Integer] -> [Integer]
nextLine l = zipWith (+) ([ 0 ] ++ l) (l ++ [ 0 ])

prettyPrint :: Int -> (Int, [Integer]) -> IO ()
prettyPrint d (lineNo, line) = do
    let indent = 2 * (d - lineNo - 1)
        prettyLine = (++ " ") . center . show <$> line
    printIndent indent >> traverse_ putStr prettyLine >> putStrLn ""
    putStrLn ""
  where
    center :: String -> String
    center str
        | length str == 0 = " " ++ " " ++ " "
        | length str == 1 = " " ++ str ++ " "
        | length str == 2 = " " ++ str
        | otherwise = str
    printIndent i = putStr (take i $ repeat ' ')
