{-# LANGUAGE FlexibleContexts #-}

module Main where

import Control.Monad.Except
import Control.Monad.RWS

type E = String
type R = ()
type W = String
type S = Int
type A = ()

type ERWS e r w s = RWST r w s (Except e)
runERWS :: ERWS e r w s a -> r -> s -> Either e (a, s, w)
runERWS f r s = runExcept (runRWST f r s)

type RWSE e r w s = ExceptT e (RWS r w s)
runRWSE :: RWSE e r w s a -> r -> s -> (Either e a, s, w)
runRWSE f r s = runRWS (runExceptT f) r s

f0 :: (MonadError E (m E R W S), MonadReader R (m E R W S), MonadWriter W (m E R W S), MonadState S (m E R W S)) => m E R W S A
f0 = do
  tell $ "..."
  n0 <- get <* modify (+1)
  tell $ show n0
  tell $ "..."
  n1 <- get <* modify (+1)
  tell $ show n1
  tell $ "..."
  catchError
    (do { tell "xxx" ; modify (+1) ; throwError "nope" })
    (\ e -> do
      n2 <- get <* modify (+1)
      tell $ show n2
      tell $ "...")

f1 :: (MonadError E (m E R W S), MonadReader R (m E R W S), MonadWriter W (m E R W S), MonadState S (m E R W S)) => m E R W S A
f1 = do
  tell $ "..."
  n0 <- get <* modify (+1)
  tell $ show n0
  tell $ "..."
  n1 <- get <* modify (+1)
  tell $ show n1
  tell $ "..."
  catchError
    (do { tell "xxx" ; modify (+1) ; throwError "nope" })
    (\ e -> do
      n2 <- get <* modify (+1)
      tell $ show n2
      tell $ "..."
      throwError "nope2")

test :: String
test =
  case runERWS f r s0 of
   Left  e      -> " error:  " ++ show e
   Right (a, s, w) -> " answer: " ++ show a ++ " state: " ++ show s ++ " output: " ++ show w
  where
    f :: ERWS E R W S A
    f = do
      tell $ "..."
      n0 <- get <* modify (+1)
      tell $ show n0
      tell $ "..."
      n1 <- get <* modify (+1)
      tell $ show n1
      tell $ "..."
      catchError
        (do { tell "xxx" ; modify (+1) ; throwError "nope" })
        (\ e -> do
          n2 <- get <* modify (+1)
          tell $ show n2
          tell $ "...")

    r = ()
    s0 = 0

test1 :: String
test1 =
  case runERWS f r s0 of
   Left  e      -> " error:  " ++ show e
   Right (a, s, w) -> " answer: " ++ show a ++ " state: " ++ show s ++ " output: " ++ show w
  where
    f :: ERWS E R W S A
    f = do
      tell $ "..."
      n0 <- get <* modify (+1)
      tell $ show n0
      tell $ "..."
      n1 <- get <* modify (+1)
      tell $ show n1
      tell $ "..."
      catchError
        (do { tell "xxx" ; modify (+1) ; throwError "nope" })
        (\ e -> do
          n2 <- get <* modify (+1)
          tell $ show n2
          tell $ "..."
          throwError "nope2")

    r = ()
    s0 = 0

test2 :: String
test2 =
  case runRWSE f r s0 of
   (Left  e, s, w) -> " error:  " ++ show e ++ " state: " ++ show s ++ " output: " ++ show w
   (Right a, s, w) -> " answer: " ++ show a ++ " state: " ++ show s ++ " output: " ++ show w
  where
    f :: RWSE E R W S A
    f = do
      tell $ "..."
      n0 <- get <* modify (+1)
      tell $ show n0
      tell $ "..."
      n1 <- get <* modify (+1)
      tell $ show n1
      tell $ "..."
      catchError
        (do { tell "xxx" ; modify (+1) ; throwError "nope" })
        (\ e -> do
          n2 <- get <* modify (+1)
          tell $ show n2
          tell $ "...")

    r = ()
    s0 = 0

test3 :: String
test3 =
  case runRWSE f r s0 of
   (Left  e, s, w) -> " error:  " ++ show e ++ " state: " ++ show s ++ " output: " ++ show w
   (Right a, s, w) -> " answer: " ++ show a ++ " state: " ++ show s ++ " output: " ++ show w
  where
    f :: RWSE E R W S A
    f = do
      tell $ "..."
      n0 <- get <* modify (+1)
      tell $ show n0
      tell $ "..."
      n1 <- get <* modify (+1)
      tell $ show n1
      tell $ "..."
      catchError
        (do { tell "xxx" ; modify (+1) ; throwError "nope" })
        (\ e -> do
          n2 <- get <* modify (+1)
          tell $ show n2
          tell $ "..."
          throwError "nope2")

    r = ()
    s0 = 0

main :: IO ()
main = do
  putStrLn "test"
  putStrLn test

  putStrLn "test1"
  putStrLn test1

  putStrLn "test2"
  putStrLn test2

  putStrLn "test3"
  putStrLn test3
