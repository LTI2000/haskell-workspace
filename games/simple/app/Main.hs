module Main where

import           System.IO      ( IO )

import           Common.Imports

import           Game.Types
import           Game.Colors
import           Game.Input
import           Game.Output
import           Game.Game

import           Widget.Widget

import           Sdl.Game

data State = State { focussed      :: Bool
                   , mouseLocation :: Location
                   , widget        :: Widget
                   }

size :: Dimension
size = dimension 320 200

initialState :: State
initialState = State { focussed = False
                     , mouseLocation = location 0 0
                     , widget = button (bounds 10 10 128 32)
                     }

advance :: Advance State
advance state@State{widget} input@(MousePos loc) =
    state { mouseLocation = loc, widget = advanceWidget widget input }
advance state WindowFocussed =
    state { focussed = True }
advance state WindowBlurred =
    state { focussed = False }
advance _ Quit = stop
advance state (Tick _timestamp) =
    state
advance state _ = state

render :: Render State
render State{focussed,mouseLocation,widget} viewPort =
    Composite [ Clear (if focussed then clouds else silver)
              , renderWidget widget viewPort
              , DrawRect amethyst viewPort
              , DrawRect pumpkin (mouseLocation |.| 3)
              , DrawPoint pomegranate mouseLocation
              ]

main :: IO ()
main = start size "simple" (Game initialState advance render)
