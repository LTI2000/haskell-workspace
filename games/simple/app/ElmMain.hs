module Main where

import           System.IO      ( IO )

import           Common.Imports

import           Game.Types
import           Game.Colors
import           Game.Elm
import           Sdl.Elm

size :: Dimension
size = dimension 320 200

type Msg = ()
type Model = ()

init :: Init Model
init = ()

update :: Update Msg Model
update () model = model

view :: View Msg Model
view model = Div [ Background silver, Button (bounds 10 10 128 32) ]

main :: IO ()
main = start size "elm" (Elm init update view)
