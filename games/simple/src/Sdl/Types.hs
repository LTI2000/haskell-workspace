module Sdl.Types where

import           Common.Imports

import           SDL.Vect           ( Point(..), V2(..), V3(..), V4(..) )
import           SDL.Video.Renderer ( Rectangle(..) )

import           Foreign.C.Types    ( CDouble, CInt )

import           Game.Types         ( Bounds(..), Color, Dimension, Location )

type SdlInt = CInt

type SdlFloat = CDouble

type SdlPoint = Point V2 SdlInt

locationToSdlPoint :: Location -> SdlPoint
locationToSdlPoint = (fromIntegral <$>)

--type SdlVector = V2 SdlInt
type SdlDimension = V2 SdlInt

dimensionToSdlDimension :: Dimension -> SdlDimension
dimensionToSdlDimension =
    (fromIntegral <$>)

sdlDimensionToDimension :: SdlDimension -> Dimension
sdlDimensionToDimension =
    (fromIntegral <$>)

type SdlRectangle = Rectangle SdlInt

boundsToSdlRectangle :: Bounds -> SdlRectangle
boundsToSdlRectangle (Bounds loc dim) =
    Rectangle (fromIntegral <$> loc) (fromIntegral <$> dim)

type SdlColorComponent = Word8

type SdlRgb = V3 SdlColorComponent

type SdlAlpha = SdlColorComponent

type SdlRgba = V4 SdlColorComponent

colorToSdlRgba :: Color -> SdlRgba
colorToSdlRgba = (fromIntegral <$>)
