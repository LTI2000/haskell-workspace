module Sdl.Input ( eventsToInputs ) where

import           Common.Imports

import           SDL.Event          ( Event )
import           SDL.Event
import           SDL.Input.Keyboard

import           Game.Input         ( Input(..), InputKey(..), Modifier(..) )

eventsToInputs :: [Event] -> [Input]
eventsToInputs events = catMaybes (eventToInput <$> events)

eventToInput :: Event -> Maybe Input
eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = WindowShownEvent WindowShownEventData{}} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = WindowHiddenEvent WindowHiddenEventData{}} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = WindowExposedEvent WindowExposedEventData{}} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = WindowMovedEvent WindowMovedEventData{}} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = WindowResizedEvent WindowResizedEventData{windowResizedEventSize}} =
    Just (WindowSize (fromIntegral <$> windowResizedEventSize))

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = WindowSizeChangedEvent WindowSizeChangedEventData{}} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = WindowMinimizedEvent WindowMinimizedEventData{}} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = WindowMaximizedEvent WindowMaximizedEventData{}} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = WindowRestoredEvent WindowRestoredEventData{}} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = WindowGainedMouseFocusEvent WindowGainedMouseFocusEventData{}} =
    Just WindowFocussed

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = WindowLostMouseFocusEvent WindowLostMouseFocusEventData{}} =
    Just WindowBlurred

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = WindowGainedKeyboardFocusEvent WindowGainedKeyboardFocusEventData{}} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = WindowLostKeyboardFocusEvent WindowLostKeyboardFocusEventData{}} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = WindowClosedEvent WindowClosedEventData{}} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = KeyboardEvent e} =
    let KeyboardEventData{keyboardEventKeyMotion = keyMotion,keyboardEventRepeat = repeat,keyboardEventKeysym = Keysym{keysymScancode = Scancode{unwrapScancode},keysymKeycode = kc@Keycode{unwrapKeycode = keycode},keysymModifier = KeyModifier{keyModifierLeftShift = leftShift,keyModifierRightShift = rightShift,keyModifierLeftCtrl = leftCtrl,keyModifierRightCtrl = rightCtrl,keyModifierLeftAlt = leftAlt,keyModifierRightAlt = rightAlt,keyModifierLeftGUI = leftGUI,keyModifierRightGUI = rightGUI,keyModifierNumLock = numLock,keyModifierCapsLock = capsLock,keyModifierAltGr = altGr}}} =
            e
        modifier = case kc of
            KeycodeLShift -> LeftShift
            KeycodeRShift -> RightShift
            KeycodeLCtrl -> LeftCtrl
            KeycodeRCtrl -> RightCtrl
            KeycodeLAlt -> LeftAlt
            KeycodeRAlt -> RightAlt
            KeycodeLGUI -> LeftGui
            KeycodeRGUI -> RightGui
            KeycodeNumLockClear -> NumLock
            KeycodeCapsLock -> CapsLock
            _ -> None
        string = printf "%s %s %10s scan 0x%03X key 0x%08X [%s] [%s] [%s] [%s] [%s] [%s] [%s] [%s] [%s] [%s] [%s]"
                        (case keyMotion of
                             Pressed -> "DN"
                             Released -> "UP" :: String)
                        (if repeat then "R" else " " :: String)
                        (show modifier)
                        unwrapScancode
                        keycode
                        (if leftShift then "LS" else "  " :: String)
                        (if rightShift then "RS" else "  " :: String)
                        (if leftCtrl then "LC" else "  " :: String)
                        (if rightCtrl then "RC" else "  " :: String)
                        (if leftAlt then "LA" else "  " :: String)
                        (if rightAlt then "RA" else "  " :: String)
                        (if leftGUI then "LG" else "  " :: String)
                        (if rightGUI then "RG" else "  " :: String)
                        (if numLock then "NL" else "  " :: String)
                        (if capsLock then "CL" else "  " :: String)
                        (if altGr then "GR" else "  " :: String)
        keyEvent = assembleKeyEvent keyMotion kc modifier
    in
        if repeat
        then Nothing
        else case keyEvent of
            (Just ke) -> trace (printf "%s\n%s" (string :: String) (show ke))
                               keyEvent
            _ -> trace (printf "%s" string) keyEvent
  where
    assembleKeyEvent :: InputMotion -> Keycode -> Modifier -> Maybe Input
    assembleKeyEvent Pressed keycode modifier =
        KeysPressed . (: []) <$> keycodeToInputKey keycode
    assembleKeyEvent Released keycode modifier =
        KeysReleased . (: []) <$> keycodeToInputKey keycode

    keycodeToInputKey :: Keycode -> Maybe InputKey
    keycodeToInputKey KeycodeLeft =
        Just Key_Left
    keycodeToInputKey KeycodeRight =
        Just Key_Right
    keycodeToInputKey KeycodeUp =
        Just Key_Up
    keycodeToInputKey KeycodeDown =
        Just Key_Down
    keycodeToInputKey KeycodeEscape =
        Just Key_Esc
    keycodeToInputKey KeycodeSpace =
        Just Key_Space
    keycodeToInputKey KeycodeTab =
        Just Key_Tab
    keycodeToInputKey KeycodeF1 =
        Just Key_F1
    keycodeToInputKey KeycodeF2 =
        Just Key_F2
    keycodeToInputKey KeycodeF3 =
        Just Key_F3
    keycodeToInputKey KeycodeF4 =
        Just Key_F4
    keycodeToInputKey KeycodeF5 =
        Just Key_F5
    keycodeToInputKey KeycodeF6 =
        Just Key_F6
    keycodeToInputKey KeycodeF7 =
        Just Key_F7
    keycodeToInputKey KeycodeF8 =
        Just Key_F8
    keycodeToInputKey _ = Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = TextEditingEvent TextEditingEventData{}} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = TextInputEvent TextInputEventData{}} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = KeymapChangedEvent} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = MouseMotionEvent MouseMotionEventData{mouseMotionEventPos}} =
    Just (MousePos (fromIntegral <$> mouseMotionEventPos))

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = MouseButtonEvent MouseButtonEventData{}} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = MouseWheelEvent MouseWheelEventData{}} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = JoyAxisEvent JoyAxisEventData{}} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = JoyBallEvent JoyBallEventData{}} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = JoyHatEvent JoyHatEventData{}} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = JoyButtonEvent JoyButtonEventData{}} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = JoyDeviceEvent JoyDeviceEventData{}} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = ControllerAxisEvent ControllerAxisEventData{}} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = ControllerButtonEvent ControllerButtonEventData{}} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = ControllerDeviceEvent ControllerDeviceEventData{}} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = AudioDeviceEvent AudioDeviceEventData{}} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = QuitEvent} =
    Just Quit

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = UserEvent UserEventData{}} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = SysWMEvent SysWMEventData{}} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = TouchFingerEvent TouchFingerEventData{}} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = MultiGestureEvent MultiGestureEventData{}} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = DollarGestureEvent DollarGestureEventData{}} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = DropEvent DropEventData{}} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = ClipboardUpdateEvent} =
    Nothing

eventToInput Event{eventTimestamp = _eventTimestamp,eventPayload = UnknownEvent UnknownEventData{}} =
    Nothing
