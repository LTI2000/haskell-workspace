module Sdl.Context
    ( SdlContext(..)
    , withSdlContext
    , getLogicalSize
    ) where

import           Common.Imports

import           Data.Text          ( Text )

import           SDL                ( ($=), get )
import           SDL.Init           ( initializeAll, quit )
import           SDL.Input.Mouse    ( cursorVisible )
import           SDL.Video          ( Window, WindowConfig(..), WindowMode(..)
                                    , WindowPosition(..), createRenderer
                                    , createWindow, destroyRenderer
                                    , destroyWindow )
import           SDL.Video.Renderer ( BlendMode(..), Renderer
                                    , RendererConfig(..), RendererType(..)
                                    , rendererDrawBlendMode
                                    , rendererLogicalSize )


import           Sdl.Types          ( SdlDimension )

data SdlContext = SdlContext { window   :: Window
                             , renderer :: Renderer
                             }

withSdlContext :: SdlDimension -> Bool -> Text -> (SdlContext -> IO ()) -> IO ()
withSdlContext size fullscreen caption =
    let windowConfig = WindowConfig { windowBorder = True
                                    , windowHighDPI = False
                                    , windowInputGrabbed = False
                                    , windowMode = if fullscreen
                                                   then FullscreenDesktop
                                                   else Windowed
                                    , windowOpenGL = Nothing
                                    , windowPosition = Wherever
                                    , windowResizable = True -- False
                                    , windowInitialSize = size
                                    }
        rendererConfig = RendererConfig { rendererType = AcceleratedVSyncRenderer
                                        , rendererTargetTexture = True
                                        }
        acquire = do
            gfxCtx <- newSdlContext windowConfig rendererConfig size caption
            cursorVisible $= False
            return gfxCtx

        release gfxCtx = do
            cursorVisible $= True
            destroySdlContext gfxCtx
    in
        bracket acquire release

newSdlContext :: WindowConfig
              -> RendererConfig
              -> SdlDimension
              -> Text
              -> IO SdlContext
newSdlContext w r s c = do
    initializeAll
    window <- createWindow c w
    renderer <- createRenderer window (-1) r
    rendererLogicalSize renderer $= Just s
    rendererDrawBlendMode renderer $= BlendAlphaBlend
    return SdlContext { window, renderer }

destroySdlContext :: SdlContext -> IO ()
destroySdlContext SdlContext{window,renderer} = do
    destroyRenderer renderer
    destroyWindow window
    quit

getLogicalSize :: Renderer -> IO SdlDimension
getLogicalSize renderer = do
    size <- get (rendererLogicalSize renderer)
    return (maybe 0 id size)
