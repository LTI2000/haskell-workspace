module Sdl.Elm
    ( start
    , stop
    ) where

import           Common.Imports

import           System.IO          ( IO )

import           Data.Text          ( pack )

import           SDL.Video.Renderer ( Renderer, present )
import           SDL.Event


import           Game.Types         ( Bounds, Dimension, dimensionToBounds )
import           Game.Colors
import           Game.Input         ( Input(..) )
import           Game.Output
import           Game.Elm           -- ( Advance, Game(..), Render )

import           Sdl.Types          ( dimensionToSdlDimension
                                    , sdlDimensionToDimension )
import           Sdl.Context        ( SdlContext(..), getLogicalSize
                                    , withSdlContext )
import           Sdl.Input          ( eventsToInputs )
import           Sdl.Output         ( renderOutput )

start :: Dimension -> String -> Elm msg model -> IO ()
start size caption (Elm init update view) =
    withSdlContext (dimensionToSdlDimension size)
                   False
                   (pack caption)
                   (\SdlContext{renderer} -> gameLoop renderer init update view)
        `catch` \QuitException -> return ()

data QuitException = QuitException
    deriving Show

instance Exception QuitException

stop :: state
stop = throw QuitException

gameLoop :: Renderer -> model -> Update msg model -> View msg model -> IO ()
gameLoop renderer model update view = do
    events <- pollEvents
    let inputs = eventsToInputs events
        html = view model
    bounds <- (dimensionToBounds . sdlDimensionToDimension) <$> getLogicalSize renderer
    msgs <- renderHtml renderer inputs bounds html
    present renderer
    let model' = updateLoop msgs update model
    gameLoop renderer model' update view

updateLoop :: [msg] -> Update msg model -> model -> model
updateLoop [] _ model = model
updateLoop (msg : msgs) update model =
    updateLoop msgs update (update msg model)

renderHtml :: Renderer -> [Input] -> Bounds -> Html msg -> IO [msg]
renderHtml renderer inputs bounds (Background color) = do
    renderOutput renderer (Clear color)
    return []
renderHtml renderer inputs bounds (Button b) = do
    renderOutput renderer (FillRect amethyst b)
    return []
renderHtml renderer inputs bounds (Div htmls) = do
    join <$> traverse (renderHtml renderer inputs bounds) htmls
