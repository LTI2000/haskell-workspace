module Sdl.Output ( renderOutput ) where

import           Common.Imports

import           SDL                ( ($=) )
import           SDL.Video.Renderer ( Renderer, clear, drawPoint, drawRect
                                    , fillRect, rendererDrawColor )
import           Sdl.Types          ( boundsToSdlRectangle, colorToSdlRgba
                                    , locationToSdlPoint )

import           Game.Types         ( Color )
import           Game.Output        ( Output(..) )

renderOutput :: Renderer -> Output -> IO ()
renderOutput renderer (Clear color) = do
    setColor renderer color
    clear renderer
renderOutput renderer (DrawPoint color location) = do
    setColor renderer color
    drawPoint renderer (locationToSdlPoint location)
renderOutput renderer (DrawRect color bounds) = do
    setColor renderer color
    drawRect renderer (Just (boundsToSdlRectangle bounds))
renderOutput renderer (FillRect color bounds) = do
    setColor renderer color
    fillRect renderer (Just (boundsToSdlRectangle bounds))
renderOutput renderer (Composite outputs) = do
    traverse_ (renderOutput renderer) outputs

setColor :: Renderer -> Color -> IO ()
setColor renderer color =
    rendererDrawColor renderer $= colorToSdlRgba color
