module Sdl.Game
    ( start
    , stop
    ) where

import Common.Imports

import           System.IO          ( IO )

import           Data.Text          ( pack )

import           SDL.Video.Renderer ( Renderer, present )
import           SDL.Event


import           Game.Types         ( Dimension, dimensionToBounds )
import           Game.Input         ( Input(..) )
import           Game.Game          ( Advance, Game(..), Render )

import           Sdl.Types          ( dimensionToSdlDimension
                                    , sdlDimensionToDimension )
import           Sdl.Context        ( SdlContext(..), getLogicalSize
                                    , withSdlContext )
import           Sdl.Input          ( eventsToInputs )
import           Sdl.Output         ( renderOutput )

start :: Dimension -> String -> Game state -> IO ()
start size caption (Game initialState advance render) =
    withSdlContext (dimensionToSdlDimension size)
                   False
                   (pack caption)
                   (\SdlContext{renderer} ->
                        gameLoop renderer initialState advance render)
        `catch` \QuitException -> return ()

gameLoop :: Renderer -> state -> Advance state -> Render state -> IO ()
gameLoop renderer state advance render = do
    events <- pollEvents
    let inputs = eventsToInputs events
        state' = foldl' advance state inputs
        state'' = advance state' (Tick 0)
    size <- getLogicalSize renderer
    let output = render state''
                        (dimensionToBounds (sdlDimensionToDimension size))
    renderOutput renderer output
    present renderer
    gameLoop renderer state'' advance render

data QuitException = QuitException
    deriving Show

instance Exception QuitException

stop :: state
stop = throw QuitException
