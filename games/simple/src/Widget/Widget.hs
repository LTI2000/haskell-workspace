module Widget.Widget
    ( Widget
    , button
    , advanceWidget
    , renderWidget
    ) where

import           Common.Imports

import           Game.Types
import           Game.Colors
import           Game.Input     ( Input(..) )
import           Game.Output    ( Output(..) )
import           Game.Game      ( Advance, Render )

data Widget = Widget Bounds Bool X

data X = Button

button :: Bounds -> Widget
button b = Widget b False Button

advanceWidget :: Advance Widget
advanceWidget (Widget b _ x) (MousePos mouseLocation) =
    Widget b (b <.> mouseLocation) x
advanceWidget widget _ =
    widget

renderWidget :: Render Widget
renderWidget (Widget b hit x) viewPort =
    Composite [ FillRect (if hit then emerald else nephritis) b, renderX b x viewPort ]

renderX :: Bounds -> Render X
renderX b x viewPort = DrawRect black b
