module Common.Imports ( module X ) where

import           Prelude             as X ( (*), (+), (-), (/), Bounded(..)
                                          , Enum(..), Floating(..)
                                          , Fractional(..), Integer
                                          , Integral(..), Num(..)
                                          , RealFloat(..), RealFrac(..), div
                                          , error, fromIntegral, rem, undefined )

import           Control.Applicative as X ( Applicative(..) )
import           Control.Category    as X ( (<<<), (>>>), Category(..) )
import           Control.Exception   as X ( Exception, bracket, catch, throw )
import           Control.Monad       as X ( Monad(..), join )

import           Data.Bool           as X ( (&&), (||), Bool(..), not
                                          , otherwise )
import           Data.Char           as X ( Char, chr, ord )
import           Data.Eq             as X ( Eq(..) )
import           Data.Foldable       as X ( Foldable(..), foldl', traverse_ )
import           Data.Function       as X ( ($), const, flip, on )
import           Data.Functor        as X ( ($>), (<$>), Functor(..), void )
import           Data.Int            as X ( Int )
import           Data.Maybe          as X ( Maybe(..), catMaybes, maybe
                                          , maybeToList )
import           Data.Monoid         as X ( (<>), Monoid(..) )
import           Data.Ord            as X ( Ord(..), comparing )
import           Data.String         as X ( String )
import           Data.Traversable    as X ( Traversable(..) )
import           Data.Tuple          as X ( fst, snd, swap )
import           Data.Word           as X ( Word16, Word32, Word64, Word8 )

import           Text.Printf         as X ( printf )
import           Text.Show           as X ( Show(..) )

import           System.IO           as X ( IO, print, putStr, putStrLn )

import           Debug.Trace         as X ( trace )
