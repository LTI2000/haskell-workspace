module Game.Input
    ( Input(..)
    , InputKey(..)
    , Modifier(..)
    ) where

import           Common.Imports

import           Game.Types     ( Dimension, Location )

data Input = WindowFocussed
           | WindowBlurred
           | WindowSize Dimension
           | MousePos Location
           | KeysPressed [InputKey]
           | KeysReleased [InputKey]
           | Tick Integer
           | Quit
    deriving (Show)

data InputKey = Key_Left
              | Key_Right
              | Key_Up
              | Key_Down
              | Key_Esc
              | Key_Space
              | Key_Tab
              | Key_F1
              | Key_F2
              | Key_F3
              | Key_F4
              | Key_F5
              | Key_F6
              | Key_F7
              | Key_F8
    deriving (Eq, Ord, Show, Enum, Bounded)

data Modifier = None
              | LeftShift
              | RightShift
              | LeftCtrl
              | RightCtrl
              | LeftAlt
              | RightAlt
              | LeftGui
              | RightGui
              | NumLock
              | CapsLock
    deriving (Eq, Ord, Show, Enum, Bounded)
