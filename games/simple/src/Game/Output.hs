module Game.Output ( Output(..) ) where

import           Game.Types ( Bounds, Color, Location )

data Output = Clear Color
            | DrawPoint Color Location
            | DrawRect Color Bounds
            | FillRect Color Bounds
            | Composite [Output]
