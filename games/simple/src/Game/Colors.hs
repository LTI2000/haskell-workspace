module Game.Colors
    ( black
    , white
    , red
    , green
    , blue
    , turquoise
    , green_sea
    , emerald
    , nephritis
    , peter_river
    , belize_hole
    , amethyst
    , wisteria
    , wet_asphalt
    , midnight_blue
    , sun_flower
    , orange
    , carrot
    , pumpkin
    , alizarin
    , pomegranate
    , clouds
    , silver
    , concrete
    , asbestos
    ) where

import           Game.Types ( Color, rgb )

black :: Color
black = rgb 0 0 0

white :: Color
white = rgb 255 255 255

red :: Color
red = rgb 255 0 0

green :: Color
green = rgb 0 255 0

blue :: Color
blue = rgb 0 0 255

turquoise :: Color
turquoise = rgb 0x1A 0xBC 0x9C

green_sea :: Color
green_sea = rgb 0x16 0xA0 0x85

emerald :: Color
emerald = rgb 0x2E 0xCC 0x71

nephritis :: Color
nephritis = rgb 0x27 0xAE 0x60

peter_river :: Color
peter_river = rgb 0x34 0x98 0xDB

belize_hole :: Color
belize_hole = rgb 0x29 0x80 0xB9

amethyst :: Color
amethyst = rgb 0x9B 0x59 0xB6

wisteria :: Color
wisteria = rgb 0x8E 0x44 0xAD

wet_asphalt :: Color
wet_asphalt = rgb 0x34 0x49 0x5E

midnight_blue :: Color
midnight_blue = rgb 0x2C 0x3E 0x50

sun_flower :: Color
sun_flower = rgb 0xF1 0xC4 0x0F

orange :: Color
orange = rgb 0xF3 0x9C 0x12

carrot :: Color
carrot = rgb 0xE6 0x7E 0x22

pumpkin :: Color
pumpkin = rgb 0xD3 0x54 0x00

alizarin :: Color
alizarin = rgb 0xE7 0x4C 0x3C

pomegranate :: Color
pomegranate = rgb 0xC0 0x39 0x2B

clouds :: Color
clouds = rgb 0xEC 0xF0 0xF1

silver :: Color
silver = rgb 0xBD 0xC3 0xC7

concrete :: Color
concrete = rgb 0x95 0xA5 0xA6

asbestos :: Color
asbestos = rgb 0x7F 0x8C 0x8D
