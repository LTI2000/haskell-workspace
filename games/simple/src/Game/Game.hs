module Game.Game
    ( Advance
    , Render
    , Game(..)
    , product
    ) where

import           Game.Types ( Bounds )
import           Game.Input ( Input )
import           Game.Output ( Output(..) )

type Advance state = state -> Input -> state

type Render state = state -> Bounds -> Output

data Game state = Game state (Advance state) (Render state)

product :: Game a -> Game b -> Game (a, b)
product (Game ix ax rx) (Game iy ay ry) =
    Game (ix, iy)
         (\(x, y) input -> (ax x input, ay y input))
         (\(x, y) bounds -> Composite [ rx x bounds, ry y bounds ])
