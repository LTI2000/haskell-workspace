{-# LANGUAGE PatternSynonyms #-}

module Game.Types
    ( Dimension
    , dimension
    , pattern Dimension
    , Location
    , location
    , pattern Location
    , Bounds(..)
    , bounds
    , dimensionToBounds
    , (|.|)
    , (<.>)
    , Color
    , rgb
    , rgba
    ) where

import           Common.Imports

import           Linear.Affine  ( Point(..) )
import           Linear.V2      ( V2(..) )
import           Linear.V4      ( V4(..) )

type Dimension = V2 Int

dimension :: Int -> Int -> Dimension
dimension w h = V2 w h

pattern Dimension :: Int -> Int -> Dimension

pattern Dimension w h = V2 w h

type Location = Point V2 Int

location :: Int -> Int -> Location
location x y = P (V2 x y)

pattern Location :: Int -> Int -> Point V2 Int

pattern Location x y = P (V2 x y)

data Bounds = Bounds Location Dimension

bounds :: Int -> Int -> Int -> Int -> Bounds
bounds x y w h = Bounds (location x y) (dimension w h)

dimensionToBounds :: Dimension -> Bounds
dimensionToBounds (V2 w h) =
    bounds 0 0 w h

(|.|) :: Location -> Int -> Bounds
(Location x y) |.| size =
    let r = (size - 1) `div` 2
    in
        bounds (x - r) (y - r) size size

(<.>) :: Bounds -> Location -> Bool
Bounds (Location x y) (Dimension w h) <.> Location px py =
    px >= x && py >= y && px <= (x + w) && py <= (y + h)

type Color = V4 Word8

rgb :: Word8 -> Word8 -> Word8 -> Color
rgb r g b = rgba r g b 0xFF

rgba :: Word8 -> Word8 -> Word8 -> Word8 -> Color
rgba r g b a = V4 r g b a
