module Game.Elm
    ( Init
    , Update
    , View
    , Elm(..)
    , Html(..)
    ) where

import           Game.Types ( Bounds, Color )

type Init model = model

type Update msg model = msg -> model -> model

type View msg model = model -> Html msg

data Elm msg model = Elm (Init model) (Update msg model) (View msg model)

data Html msg = Background Color
              | Button Bounds
              | Div [Html msg]
