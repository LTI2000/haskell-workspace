{-# LANGUAGE RankNTypes #-}

module Render.Engine
    ( runEngine'
    ) where

import           Common.Imports

import           Engine

import           Render.RenderContext

runEngine' :: (EngineReader s ~ RenderInput, Engine s, RenderContext c)
           => c
           -> Dimension
           -> (RenderWriter s -> IO ())
           -> s
           -> [UpdateObject s]
           -> IO ()
runEngine' c size renderM userState userObjects =
    runEngine initialInput { dimension = size }
              (preRenderM c)
              renderM
              (postRenderM c)
              userState
              userObjects
