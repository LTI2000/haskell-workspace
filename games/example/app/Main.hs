module Main where

import           Common.Imports

import           Render.RenderContext
import           Render.SdlContext
import           Render.Engine

import           Example

main :: IO ()
main = do
    withSdlContext 800 600 False "example" f

f :: RenderContext c => c -> IO ()
f c = runEngine' c (V2 800 600) (renderM c) game objects
  where
    game = Game 800
    objects = join [ [ Ball (10 * p) (20 * p) 1 1
                     | p <- [1 .. 10] ]
                   , [ Drop (10 * p) (p + 10)
                     | p <- [1 .. 10] ]
                   ]
