{-# LANGUAGE RankNTypes #-}

module Example
    ( Game(Game)
    , Object(..)
    , RenderObject(..)
    , renderM
    ) where

import           Common.Imports

import           Engine

import           Render.RenderContext

data Game = Game { counter :: Int }

data Object = Ball { sx :: Int
                   , sy :: Int
                   , vx :: Int
                   , vy :: Int
                   }
            | Drop { sx :: Int
                   , sy :: Int
                   }

data RenderObject = RenderMouse { pos :: Position }
                  | RenderBall { pos :: Position }
                  | RenderDrop { pos :: Position }
                  | RenderGeneric { r :: forall c.
                                      (RenderContext c)
                                      => c
                                      -> IO ()
                                  }

instance Engine Game where
    type EngineReader Game = RenderInput
    type UpdateObject Game = Object
    type RenderWriter Game = RenderObject
    update = updateGame
    render = renderGame

updateGame :: UpdateM Game Bool
updateGame = do
    game@Game{counter} <- getState
    if (counter > 0)
        then do
            putState game { counter = counter - 1 }
            updateObjects updateObject
            return False
        else return True

updateObject :: Object -> UpdateM Game [Object]
updateObject Ball{sx,sy,vx,vy} = do
    RenderInput{dimension = V2 width height} <- getInput
    let sx' = sx + vx
        sy' = sy + vy
        vx' = if sx' < 0 || sx' >= width then (-vx) else vx
        vy' = if sy' < 0 || sy' >= height then (-vy) else vy
        sx'' = sx + vx'
        sy'' = sy + vy'
    return [ Ball { sx = sx'', sy = sy'', vx = vx', vy = vy' } ]
updateObject Drop{sx,sy} = do
    RenderInput{dimension = V2 _width height} <- getInput
    let sy' = sy + 1
    if sy' < height
        then do
            return [ Drop { sx, sy = sy' } ]
        else do
            return []

renderGame :: (Monad m) => RenderM Game m
renderGame = do
    RenderInput{dimension = V2 _ _,mouse} <- getInput
    output RenderGeneric { r = \c -> clearContext c
                                                  (rgbMod white
                                                          (maybe 51
                                                                 (const 102)
                                                                 mouse))
                         }
    outputObjects outputObject
    case mouse of
        Just m -> output RenderMouse { pos = m }
        Nothing -> return ()
  where
    outputObject Ball{sx,sy} =
        RenderBall { pos = P (V2 sx sy) }
    outputObject Drop{sx,sy} =
        RenderDrop { pos = P (V2 sx sy) }

renderM :: (RenderContext c) => c -> RenderObject -> IO ()
renderM c RenderMouse{pos} = do
    fillRectangle c (grow pos 4) white
renderM c RenderBall{pos} = do
    fillRectangle c (grow pos 3) red
renderM c RenderDrop{pos} = do
    drawRectangle c (grow pos 2) green
renderM c RenderGeneric{r} = do
    r c
