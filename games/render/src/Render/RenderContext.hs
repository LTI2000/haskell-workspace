module Render.RenderContext
    ( Position
    , Dimension
    , Bounds(..)
    , grow
    , RenderContext(..)
    , RenderInput(..)
    , initialInput
    , preRenderM
    , postRenderM
    , module Render.Colors
    , module X
    ) where

import           Common.Imports

import           Data.Default   ( Default(..) )

import           Linear.V2      as X ( V2(..) )
import           Linear.Affine  as X ( Point(..) )

import           Render.Colors

-- TODO move to Geom
type Position = Point V2 Int

type Dimension = V2 Int

data Bounds = Bounds Position Dimension

grow :: Position -> Int -> Bounds
grow (P (V2 x y)) size =
    (Bounds (P (V2 (x - size) (y - size))) (V2 (2 * size + 1) (2 * size + 1)))

-- TODO move to Geom
data RenderInput = RenderInput { dimension :: Dimension
                               , mouse     :: Maybe Position
                               }

instance Default RenderInput where
    def = RenderInput { dimension = V2 0 0, mouse = def }

initialInput :: RenderInput
initialInput = def

class RenderContext a where
    handleInput :: a -> RenderInput -> IO RenderInput
    clearContext :: a -> ColorRgba -> IO ()
    drawRectangle :: a -> Bounds -> ColorRgba -> IO ()
    fillRectangle :: a -> Bounds -> ColorRgba -> IO ()
    presentContext :: a -> IO ()

preRenderM :: (RenderContext c) => c -> RenderInput -> IO RenderInput
preRenderM c input = do
    handleInput c input

postRenderM :: (RenderContext c) => c -> IO ()
postRenderM c = do
    presentContext c
