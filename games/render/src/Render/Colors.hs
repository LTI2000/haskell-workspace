module Render.Colors where

import           Common.Imports

import           Linear.V4            ( V4(..) )

type ColorComponent = Word8

type ColorRgba = V4 ColorComponent

colorRgb :: ColorComponent
         -> ColorComponent
         -> ColorComponent
         -> ColorRgba
colorRgb r g b = V4 r g b 255

colorRgba :: ColorComponent
          -> ColorComponent
          -> ColorComponent
          -> ColorComponent
          -> ColorRgba
colorRgba = V4

black :: ColorRgba
black = colorRgb 0 0 0

white :: ColorRgba
white = colorRgb 255 255 255

red :: ColorRgba
red = colorRgb 255 0 0

green :: ColorRgba
green = colorRgb 0 255 0

blue :: ColorRgba
blue = colorRgb 0 0 255

rgbMod :: ColorRgba -> ColorComponent -> ColorRgba
rgbMod (V4 r g b a) m = V4 (modulate r m) (modulate g m) (modulate b m) a

aMod :: ColorRgba -> ColorComponent -> ColorRgba
aMod (V4 r g b a) m = V4 r g b (modulate a m)

modulate :: ColorComponent -> ColorComponent -> ColorComponent
modulate c m = let c' = fromIntegral c :: Word32
                   m' = fromIntegral m :: Word32
               in
                   fromIntegral (c' * m' `div` 255)
