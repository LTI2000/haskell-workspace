import           Criterion.Main
import           Criterion.Types

import           Imports

import           Engine
import           Game

run :: Integer -> Integer -> IO ()
run n i = do
    h <- openFile "/dev/null" WriteMode
    runEngine (hPutStrLn h) () (Game n) [ i ]
    hClose h
  where


m :: Int
m = 17

main :: IO ()
main = defaultMainWith defaultConfig { reportFile = Just "engine-benchmarks.html"
                                     }
                       [ bgroup "runEngine"
                                [ let n = m
                                      i = shift 1 n
                                      n' = 2 * (i - 1)
                                      i' = 1
                                  in
                                      bench (show n' ++ " " ++ show i') $
                                          nfIO (run n' i')
                                , let n = m
                                      i = shift 1 n
                                  in
                                      bench (show n ++ " " ++ show i) $
                                          nfIO (run (fromIntegral n) i)
                                ]
                       ]
