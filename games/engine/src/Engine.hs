module Engine
    ( Engine(..)
    , UpdateM
    , RenderM
    , runEngine
    , getInput
    , getState
    , putState
    , updateObjects
    , outputObjects
    , output
    ) where

import           Common.Imports

import           Control.Monad.Trans.RWS

type UpdateM s a = RWS (EngineReader s) () (EngineState s) a

type RenderM s m = EngineT s m ()

type EngineT s m a = RWST (EngineReader s) [(RenderWriter s)] (EngineState s) m a

data EngineState s = EngineState { userState   :: s
                                 , userObjects :: [UpdateObject s]
                                 }

class Engine s where
    type EngineReader s
    type UpdateObject s
    type RenderWriter s
    update :: UpdateM s Bool
    render :: (Monad m) => RenderM s m

runEngine :: (Monad m, Engine s)
          => EngineReader s                         -- ^ initial input
          -> (EngineReader s -> m (EngineReader s)) -- ^ pre renderer
          -> (RenderWriter s -> m ())               -- ^ renderer
          -> m ()                                   -- ^ post renderer
          -> s                                      -- ^ user state
          -> [UpdateObject s]                       -- ^ user objects
          -> m ()
runEngine r0 preRenderM renderM postRenderM userState userObjects = do
    go r0 EngineState { userState, userObjects }
  where
    go r s = do
        r' <- preRenderM r
        (s', w') <- execRWST render r' s
        forM_ w' renderM
        postRenderM
        let (a'', s'', _w'') = runRWS update r' s'
        case a'' of
            True -> do
                return ()
            _ -> do
                go r' s''

getInput :: (Monoid w, Monad m) => RWST r w (EngineState s) m r
getInput = ask

getState :: (Monoid w, Monad m) => RWST r w (EngineState s) m s
getState = do
    EngineState{userState} <- get
    return userState

putState :: (Monoid w, Monad m) => s -> RWST r w (EngineState s) m ()
putState s = do
    engineState <- get
    put engineState { userState = s }

updateObjects :: (UpdateObject s -> UpdateM s [UpdateObject s]) -> UpdateM s ()
updateObjects updateObject = do
    EngineState{userObjects} <- get
    userObjects' <- join <$> traverse updateObject userObjects
    engineState <- get
    put engineState { userObjects = userObjects' }

outputObjects :: (Monad m) => (UpdateObject s -> RenderWriter s) -> RenderM s m
outputObjects outputObject = do
    EngineState{userObjects} <- get
    tell (outputObject <$> userObjects)

output :: (Monad m) => (RenderWriter s) -> RenderM s m
output x = tell [ x ]
