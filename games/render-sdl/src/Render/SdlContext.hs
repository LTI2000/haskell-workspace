{-# LANGUAGE RankNTypes #-}

module Render.SdlContext
    ( withSdlContext
    , SdlContext
    , newSdlContext
    , destroySdlContext
    , newStaticTexture
    , newTargetTexture
    ) where

import           Common.Imports

import           Data.Text            ( Text, pack )

import           Render.RenderContext

import           Foreign.C.Types      ( CInt )
import           SDL

type SdlInt = CInt

--type SdlFloat = CDouble
--type SdlPoint = Point V2 SdlInt
--type SdlVector = V2 SdlInt
type SdlDimension = V2 SdlInt

type SdlRectangle = Rectangle SdlInt

--type SdlColorComponent = Word8
--type SdlRgb = V3 SdlColorComponent
--type SdlAlpha = SdlColorComponent
--type SdlRgba = V4 SdlColorComponent
withSdlContext :: Int
               -> Int
               -> Bool
               -> String
               -> (forall c. (RenderContext c) => c -> IO ())
               -> IO ()
withSdlContext width height fullscreen caption f = do
    let size = V2 (fromIntegral width) (fromIntegral height)
        windowConfig = WindowConfig { windowBorder = True
                                    , windowHighDPI = False
                                    , windowInputGrabbed = False
                                    , windowMode = if fullscreen
                                                   then FullscreenDesktop
                                                   else Windowed
                                    , windowOpenGL = Nothing
                                    , windowPosition = Wherever
                                    , windowResizable = True -- False
                                    , windowInitialSize = size
                                    }
        rendererConfig = RendererConfig { rendererType = AcceleratedVSyncRenderer
                                        , rendererTargetTexture = True
                                        }

    withSdlContext' windowConfig rendererConfig size (pack caption) f
  where
    withSdlContext' :: WindowConfig
                    -> RendererConfig
                    -> SdlDimension
                    -> Text
                    -> (SdlContext -> IO ())
                    -> IO ()
    withSdlContext' w r s c =
        bracket acquire release
      where
        acquire :: IO SdlContext
        acquire = do
            gfxCtx <- newSdlContext w r s c
            cursorVisible $= False
            return gfxCtx
        release :: SdlContext -> IO ()
        release gfxCtx = do
            cursorVisible $= True
            destroySdlContext gfxCtx

data SdlContext = SdlContext { window   :: Window
                             , renderer :: Renderer
                             }

instance RenderContext SdlContext where
    -- |
    handleInput SdlContext{renderer} input = do
        Just d <- get (rendererLogicalSize renderer)
        events <- pollEvents
        return (foldl updateInput
                      input { dimension = fromIntegral <$> d }
                      events)
    -- |
    clearContext SdlContext{renderer} color = do
        rendererDrawColor renderer $= color
        clear renderer
    -- |
    drawRectangle SdlContext{renderer} bounds color = do
        rendererDrawColor renderer $= color
        drawRect renderer (Just (boundsToRectangle bounds))
    -- |
    fillRectangle SdlContext{renderer} bounds color = do
        rendererDrawColor renderer $= color
        fillRect renderer (Just (boundsToRectangle bounds))
    -- |
    presentContext SdlContext{renderer} = do
        present renderer

boundsToRectangle :: Bounds -> SdlRectangle
boundsToRectangle (Bounds pos size) =
    Rectangle (fromIntegral <$> pos) (fromIntegral <$> size)

newSdlContext :: WindowConfig
              -> RendererConfig
              -> SdlDimension
              -> Text
              -> IO SdlContext
newSdlContext w r s c = do
    initializeAll
    window <- createWindow c w
    renderer <- createRenderer window (-1) r
    rendererLogicalSize renderer $= Just s
    rendererDrawBlendMode renderer $= BlendAlphaBlend
    return SdlContext { window, renderer }

destroySdlContext :: SdlContext -> IO ()
destroySdlContext SdlContext{window,renderer} = do
    destroyRenderer renderer
    destroyWindow window
    quit

--  newSurface :: NativeContext -> NativeDimension -> IO Surface
--  newSurface (window, _) size = do
--      pixelFormat <- getWindowPixelFormat window
--      --  putStrLn $ "newSurface: windowPixelFormat = " ++ show pixelFormat
--      pixelFormat' <- alternatePixelFormat
--      --  putStrLn $ "newSurface: alternatePixelFormat = " ++ show pixelFormat'
--      surface <- createRGBSurface size pixelFormat'
--      return surface
--    where
--      alternateBytesPerPixel :: NativeInt
--      alternateBytesPerPixel =
--          4
--
--      alternatePixelFormat :: IO PixelFormat
--      alternatePixelFormat = do
--          masksToPixelFormat (8 * alternateBytesPerPixel) alternateMasks
--        where
--          alternateMasks = (V4 0x00FF0000 0x0000FF00 0x000000FF 0xFF000000)
newStaticTexture :: SdlContext -> Surface -> IO Texture
newStaticTexture SdlContext{renderer} surface = do
    texture <- createTextureFromSurface renderer surface
    freeSurface surface
    return texture

newTargetTexture :: SdlContext -> SdlDimension -> IO Texture
newTargetTexture SdlContext{window,renderer} size = do
    pixelFormat <- getWindowPixelFormat window
    texture <- createTexture renderer pixelFormat TextureAccessTarget size
    --  textureInfo <- queryTexture texture
    --  putStrLn $ "Target Texture Info: " ++ show textureInfo
    return texture

updateInput :: RenderInput -- ^ current input
            -> Event       -- ^ SDL event
            -> RenderInput -- ^ updated input
updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = WindowShownEvent WindowShownEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = WindowHiddenEvent WindowHiddenEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = WindowExposedEvent WindowExposedEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = WindowMovedEvent WindowMovedEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = WindowResizedEvent WindowResizedEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = WindowSizeChangedEvent WindowSizeChangedEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = WindowMinimizedEvent WindowMinimizedEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = WindowMaximizedEvent WindowMaximizedEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = WindowRestoredEvent WindowRestoredEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = WindowGainedMouseFocusEvent WindowGainedMouseFocusEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = WindowLostMouseFocusEvent WindowLostMouseFocusEventData{}} =
    input { mouse = Nothing }

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = WindowGainedKeyboardFocusEvent WindowGainedKeyboardFocusEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = WindowLostKeyboardFocusEvent WindowLostKeyboardFocusEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = WindowClosedEvent WindowClosedEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = KeyboardEvent KeyboardEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = TextEditingEvent TextEditingEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = TextInputEvent TextInputEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = KeymapChangedEvent} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = MouseMotionEvent MouseMotionEventData{mouseMotionEventPos = m}} =
    input { mouse = Just (fromIntegral <$> m) }

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = MouseButtonEvent MouseButtonEventData{mouseButtonEventPos = m}} =
    input { mouse = Just (fromIntegral <$> m) }

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = MouseWheelEvent MouseWheelEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = JoyAxisEvent JoyAxisEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = JoyBallEvent JoyBallEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = JoyHatEvent JoyHatEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = JoyButtonEvent JoyButtonEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = JoyDeviceEvent JoyDeviceEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = ControllerAxisEvent ControllerAxisEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = ControllerButtonEvent ControllerButtonEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = ControllerDeviceEvent ControllerDeviceEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = AudioDeviceEvent AudioDeviceEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = QuitEvent} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = UserEvent UserEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = SysWMEvent SysWMEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = TouchFingerEvent TouchFingerEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = MultiGestureEvent MultiGestureEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = DollarGestureEvent DollarGestureEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = DropEvent DropEventData{}} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = ClipboardUpdateEvent} =
    input

updateInput input Event{eventTimestamp = _eventTimestamp,eventPayload = UnknownEvent UnknownEventData{}} =
    input
