{-# LANGUAGE DeriveFunctor #-}

module Lib ( someFunc ) where

someFunc :: IO ()
someFunc = someFunc1

someFunc0 :: IO ()
someFunc0 = do
    putStrLn (show example)
    putStrLn (show (convert example))

data Arrow = Arrow String
           | Id
           | If String [Arrow] [Arrow]
           | TryCatch [Arrow] [Arrow]
           | Throw String
    deriving Show

data ARROW = ARROW String
           | ID
           | COMPOSE ARROW ARROW
           | IF String ARROW ARROW
           | TRY ARROW ARROW
           | THROW String

instance Show ARROW where
    show (ARROW name) = name
    show ID = "id"
    show (COMPOSE f g) = show f ++ ".NEXT(" ++ show g ++ ")"
    show (IF t c a) = "IF(" ++
        t ++ ").THEN(" ++ show c ++ ").ELSE(" ++ show a ++ ")"
    show (TRY f g) = "TRY(" ++ show f ++ ").CATCH(" ++ show g ++ ")"
    show (THROW e) = "THROW(" ++ e ++ ")"

convert :: [Arrow] -> ARROW
convert = foldl compose ID
  where
    compose :: ARROW -> Arrow -> ARROW
    compose ID a = conv a
    compose a Id = a
    compose a b = COMPOSE a (conv b)

    conv :: Arrow -> ARROW
    conv Id = ID
    conv (Arrow name) = ARROW name
    conv (If t c a) = IF t (convert c) (convert a)
    conv (TryCatch a b) = TRY (convert a) (convert b)
    conv (Throw err) = THROW err

example :: [Arrow]
example = [ Arrow "f"
          , Arrow "g"
          , If "t" [ Arrow "c", Arrow "c1" ] [ Arrow "a" ]
          , TryCatch [] [ Arrow "i", Throw "e" ]
          ]

data Step a = Play a String
            | IfThenElse a String (Step a) (Step a)
            | Sequence [Step a]
    deriving (Functor, Show)

data Tree a = Tree a [Tree a]
    deriving Show

instance Functor Tree where
    fmap f (Tree a ts) = Tree (f a) (map (\t -> fmap f t) ts)

instance Applicative Tree where
    pure x = Tree x []
    Tree f tfs <*> ta@(Tree a tas) =
        Tree (f a)
             (map (\ta2 -> fmap f ta2) tas ++ map (\tf2 -> tf2 <*> ta) tfs)

instance Monad Tree where
    return = pure
    Tree a tas >>= f = case f a of
        Tree b tbs -> Tree b (tbs ++ map (\ta -> ta >>= f) tas)

steps1 :: Step ()
steps1 = Sequence [ Play () "hello", Play () "bye" ]

exampleSteps = steps1

tree1 :: Tree Int
tree1 = Tree 1 [ Tree 2 [], Tree 3 [] ]

treef :: Tree (Int -> Int)
treef = Tree (+ 1) [ Tree (* 2) [ Tree (* 3) [] ] ]

tree2 = treef <*> tree1

someFunc1 :: IO ()
someFunc1 = do
    putStrLn (show tree2)
