{-# LANGUAGE MultiParamTypeClasses, FlexibleInstances #-}

module Main where

import Prelude (Integer, (+), (*), String, Eq, Show, IO, print, undefined)

import Lib

data Either e a = Left e
                | Right a
                deriving (Eq, Show)


class Monad m where
  unit :: a -> m a
  bind :: m a -> (a -> m b) -> m b

class Monad m => MonadError e m where
  fail :: e -> m a

instance Monad (Either e) where
  unit = _unit
  bind = _bind

instance MonadError e (Either e) where
  fail = _fail

_unit :: a -> Either e a
_unit a = Right a

_bind :: Either e a -> (a -> Either e b) -> Either e b
_bind (Left e)  _ = Left e
_bind (Right a) f = f a

_fail :: e -> Either e a
_fail e = Left e

_either :: (e -> c) -> (a -> c) -> Either e a -> c
_either left  _ (Left e)  = left e
_either _ right (Right a) = right a

either :: (Consumer e, Consumer a) -> Consumer (Either e a)
either (left, right) = \ x -> _either (\ e -> left e) (\ a -> right a) x

type Void = IO ()



type Consumer a = a -> Void

type Action e a b = (a, Consumer (Either e b)) -> Void

apply :: (a -> b, a) -> b
apply (f, a) = f a

combine :: (Action e a b, Action e b c) -> Action e a c
combine (m1, m2) = \ (a, k) -> m1 (a,
                                  either (\ e1 -> k (fail e1),
                                          \ a1 -> m2 (a1, k)))



action0 :: Action String Integer Integer
action0 (a, k) = k (fail "miserable failure")

action1 :: Action String Integer Integer
action1 (a, k) = k (unit (a + 1))

action2 :: Action String Integer Integer
action2 (a, k) = k (unit (2 * a))



main :: Void
main = do
  action0 (0,
           either (\ e -> print e,
                   \ a -> print a))

  action1 (1,
           either (\ e -> print e,
                   \ a -> print a))

  action2 (2,
           either (\ e -> print e,
                   \ a -> print a))

  apply (combine (action0, action1), (3,
                                      either (\ e -> print e,
                                              \ a -> print a)))

  apply (combine (action0, action2), (4,
                                      either (\ e -> print e,
                                              \ a -> print a)))

  apply (combine (action1, action2), (5,
                                      either (\ e -> print e,
                                              \ a -> print a)))
