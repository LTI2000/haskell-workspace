{-# LANGUAGE DeriveFunctor #-}

module Internal.Cmd
    ( Cmd
    , command__
    , run_cmd____
    , batch
    ) where

import           Data.Foldable ( traverse_ )

newtype Cmd msg = Cmd { unCmd :: [msg] }
    deriving Functor

command__ :: msg -> Cmd msg
command__ msg = Cmd [ msg ]

run_cmd____ :: (msg -> IO ()) -> Cmd msg -> IO ()
run_cmd____ f (Cmd msgs) =
    traverse_ f msgs

batch :: [Cmd msg] -> Cmd msg
batch cmds = Cmd $ unCmd =<< cmds
