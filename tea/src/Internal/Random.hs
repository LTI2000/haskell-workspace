-- {-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Internal.Random
    ( Generator
    , bool
    , int
    , pair
    , list
    , map
    , map2
    , map3
    , andThen
    , generate
    ) where

import           Prelude       hiding ( map )

import           System.Random ( getStdRandom, random, randomR )

import           Internal.Cmd  ( Cmd, command__ )

newtype Generator a = Generator a

    -- deriving (Functor, Applicative, Monad)
instance Functor Generator where
    fmap f (Generator a) = Generator $ f a

instance Applicative Generator where
    pure = Generator
    (Generator f) <*> (Generator a) =
        Generator $ f a

instance Monad Generator where
    (Generator a) >>= k = k a

bool :: Generator Bool
bool = Generator $ True -- getStdRandom random

int :: Int -> Int -> Generator Int
int lo hi = Generator $ lo -- getStdRandom $ randomR (lo, hi)

pair :: Generator a -> Generator b -> Generator (a, b)
pair = map2 (,)

list :: Int -> Generator a -> Generator [a]
list len gen = undefined

map :: (a -> b) -> Generator a -> Generator b
map = fmap

map2 :: (a -> b -> c) -> Generator a -> Generator b -> Generator c
map2 f genA genB = f <$> genA <*> genB

map3 :: (a -> b -> c -> d)
     -> Generator a
     -> Generator b
     -> Generator c
     -> Generator d
map3 f genA genB genC = f <$> genA <*> genB <*> genC

andThen :: (a -> Generator b) -> Generator a -> Generator b
andThen = (=<<)

generate :: (a -> msg) -> Generator a -> Cmd msg
generate f (Generator action) =
    command__ $ f action
