{-# LANGUAGE DeriveFunctor #-}

module Internal.Types
    ( Result(..)
    , Never
    ) where

import           Data.Void ( Void )

data Result error value =
      Ok value
    | Err error
    deriving Functor

type Never = Void
