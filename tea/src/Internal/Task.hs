{-# LANGUAGE DeriveFunctor #-}

module Internal.Task
    ( Task
    , task__
    , taskOk____
    , task_succeed
    , task_fail
    , task_perform
    , task_attempt
    ) where

import           Internal.Types ( Never, Result(..) )
import           Internal.Cmd   ( Cmd, command__ )

newtype Task err ok = Task (Result err ok)
    deriving Functor

task__ :: Result err ok -> Task err ok
task__ = Task

taskOk____ :: ok -> Task x ok
taskOk____ = task__ . Ok

task_succeed :: a -> Task x a
task_succeed = Task . Ok

task_fail :: x -> Task x a
task_fail = Task . Err

task_perform :: (a -> msg) -> Task Never a -> Cmd msg
task_perform f = task_attempt (f . fromOk)
  where
    fromOk (Ok a) = a
    fromOk _ = error ""

task_attempt :: (Result x a -> msg) -> Task x a -> Cmd msg
task_attempt f (Task action) =
    command__ $ f action
