module Internal.Program
    ( Init
    , Update
    , View
    , Subscriptions
    , Html(..)
    , Program
    , program
    , runProgram
    ) where

import           Prelude                hiding ( init )

import           Control.Concurrent.STM ( TQueue, TVar, atomically, newTQueueIO
                                        , newTVarIO, readTQueue, readTVarIO
                                        , writeTQueue, writeTVar )

import           Internal.Cmd           ( Cmd, run_cmd____ )
import           Internal.Sub           ( Sub, batch, sub_added____
                                        , sub_diff____, sub_removed____ )

data Html a = Text String
    deriving Show

type Init model msg = (model, Cmd msg)

type Update model msg = msg -> model -> (model, Cmd msg)

type View model msg = model -> Html msg

type Subscriptions model msg = model -> Sub msg

data Program model msg =
      Program { init          :: Init model msg
              , update        :: Update model msg
              , view          :: View model msg
              , subscriptions :: Subscriptions model msg
              }

program :: Init model msg
        -> Update model msg
        -> View model msg
        -> Subscriptions model msg
        -> Program model msg
program = Program

--------------------------------------------------------------------------------
runProgram :: Program model msg -> IO ()
runProgram Program{init,update,view,subscriptions} = do
    msgQ <- newTQueueIO
    subV <- newTVarIO $ batch [] -- FIXME TVar not necessary

    let (model, cmd) = init
    let newSub = subscriptions model
    runSub subV newSub
    runCmd msgQ cmd
    loop msgQ subV model
  where
    loop msgQ subV model = do
        renderView $ view model
        msg <- atomically $ readTQueue msgQ
        let (model', cmd) = update msg model
        let newSub = subscriptions model
        runSub subV newSub
        runCmd msgQ cmd
        loop msgQ subV model'

runCmd :: TQueue msg -> Cmd msg -> IO ()
runCmd msgQ cmd = run_cmd____ sendMsg cmd
  where
    sendMsg msg = do
        atomically $ writeTQueue msgQ msg

runSub :: TVar (Sub msg) -> Sub msg -> IO ()
runSub subV newSub = do
    oldSub <- readTVarIO subV
    let (added, removed) = sub_diff____ oldSub newSub
    atomically $
        writeTVar subV newSub
    sub_added____ added
    sub_removed____ removed

renderView :: Html msg -> IO ()
renderView = print
