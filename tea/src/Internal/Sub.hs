{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE DeriveFunctor             #-}

module Internal.Sub
    ( Sub
    , subscription__
    , batch
    , sub_diff____
    , sub_added____
    , sub_removed____
    ) where

import           Data.Foldable   ( traverse_ )
import           Data.Ord        ( comparing )
import           Data.Map.Strict ( Map, difference, fromList, toList )

import           Internal.Types (Never)
import           Internal.Task (Task)

data Key = forall a. Show a => Key a

instance Show Key where
    show (Key key) = show key

instance Eq Key where
    a == b = show a == show b

instance Ord Key where
    compare = comparing show

type Entry msg = (IO (), IO (), Task Never msg)

data Sub msg = Sub { unSub :: [(Key, Entry msg)] }
    deriving Functor

instance Show (Sub msg) where
    show (Sub subs) = show (fmap (\(key, _) -> key) subs)

subscription__ :: Show key => key -> IO () -> IO () -> Task Never msg -> Sub msg
subscription__ key onAdded onRemoved task =
    Sub [ (Key key, (onAdded, onRemoved, task)) ]

batch :: [Sub msg] -> Sub msg
batch subs = Sub $ unSub =<< subs

sub_diff____ :: Sub msg -> Sub msg -> (Sub msg, Sub msg)
sub_diff____ oldSub newSub =
    let (added, removed) = sub_diff (toMap oldSub) (toMap newSub)
    in
        (toSub added, toSub removed)

type SubMap msg = Map Key (Entry msg)

toMap :: Sub msg -> SubMap msg
toMap = fromList . unSub

toSub :: SubMap msg -> Sub msg
toSub = Sub . toList

sub_diff :: SubMap msg -> SubMap msg -> (SubMap msg, SubMap msg)
sub_diff oldSubs newSubs =
    let added = difference newSubs oldSubs
        removed = difference oldSubs newSubs
    in
        (added, removed)

sub_added____ :: Sub msg -> IO ()
sub_added____ (Sub entries) =
    traverse_ xxx entries

sub_removed____ :: Sub msg -> IO ()
sub_removed____ (Sub entries) =
    traverse_ yyy entries

xxx :: (Key, Entry msg) -> IO ()
xxx (_, (onAdded, _, _)) =
    onAdded

yyy :: (Key, Entry msg) -> IO ()
yyy (_, (_, onRemoved, _)) =
    onRemoved
