{-# LANGUAGE TemplateHaskellQuotes #-}

module Internal.Time
    ( Time
    , now
    , every
    ) where

import           Control.Monad          ( forever )
import           Control.Concurrent     ( ThreadId, forkIO, killThread
                                        , threadDelay )
import           Control.Concurrent.STM ( TVar, atomically, modifyTVar'
                                        , newTVarIO, readTVar, writeTVar )

import           Data.Map.Strict        ( Map, empty, insert
                                        , updateLookupWithKey )
import           Data.Word              ( Word64 )
import           Data.Time.Clock.POSIX  ( getPOSIXTime )

import           Internal.Task          ( Task, taskOk____ )
import           Internal.Sub           ( Sub, subscription__ )

import           System.IO.Unsafe       ( unsafePerformIO )

import           Internal.Types         ( Never )

type Key = (String, String)

state :: TVar (Map Key ThreadId)
state = unsafePerformIO $ newTVarIO empty

{-# NOINLINE state #-} -- FIXME hack, use explicit init function instead

type Time = Word64

now :: Task x Time
now = taskOk____ 1337 -- currentTimeMillis

every :: Time -> (Time -> msg) -> Sub msg
every interval tagger = let key = (show 'every, show interval)
                            onAdded = onTimerAdded key interval action
                            onRemoved = onTimerRemoved key
                            action = tagger <$> now
                        in
                            subscription__ key onAdded onRemoved action

currentTimeMillis :: IO Time
currentTimeMillis = round <$> (* 1000.0) <$> getPOSIXTime

onTimerAdded :: Key -> Time -> Task Never a -> IO ()
onTimerAdded key interval action = do
    putStrLn ("onAdded " ++ show key)
    pid <- forkIO $ do
               putStrLn ("thread started for key " ++ show key)
               forever $ do
                   -- action
                   putStrLn ("action performed for key " ++ show key)
    -- threadDelay (1 * (fromIntegral interval))
    putStrLn ("forked thread " ++ show pid)
    atomically $ do
        modifyTVar' state (\dict -> insert key pid dict)

onTimerRemoved :: Key -> IO ()
onTimerRemoved key = do
    putStrLn ("onRemoved " ++ show key)
    maybePid <- atomically $ do
                    dict <- readTVar state
                    let (maybeVal, dict') = updateLookupWithKey (\_ _ -> Nothing)
                                                                key
                                                                dict
                    writeTVar state dict'
                    return maybeVal
    case maybePid of
        Just pid -> do
            putStrLn ("killing thread " ++ show pid)
            killThread pid
        Nothing -> do
            putStrLn ("no thread found for key: " ++ show key)
            return ()
