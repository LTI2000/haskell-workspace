module Platform.Sub
    ( Sub
    , none
    , batch
    ) where

import           Internal.Sub ( Sub, batch )

none :: Sub msg
none = batch []
