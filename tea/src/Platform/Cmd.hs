module Platform.Cmd
    ( Cmd
    , none
    , batch
    ) where

import           Internal.Cmd ( Cmd, batch )

none :: Cmd msg
none = batch []
