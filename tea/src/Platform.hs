module Platform
    ( Init
    , Update
    , View
    , Subscriptions
    , task_succeed
    , task_fail
    , task_perform
    , task_attempt
    , Html(..)
    , Program
    , program
    , runProgram
    , Time
    , now
    , every
    , Generator
    , bool
    , int
    , pair
    , map
    , map2
    , map3
    , andThen
    , generate
    , (!)
    ) where

import           Prelude          ()

import           Platform.Cmd     ( Cmd, batch )

import           Internal.Program
import           Internal.Task
import           Internal.Time
import           Internal.Random

(!) :: model -> [Cmd msg] -> (model, Cmd msg)
model !cmds = (model, batch cmds)
