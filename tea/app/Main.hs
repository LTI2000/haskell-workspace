module Main where

import           Prelude      hiding ( init )

import           Platform
import           Platform.Cmd as Cmd
import           Platform.Sub as Sub

data Model = Model { time    :: Maybe Time
                   , rolls   :: [Int]
                   , counter :: Int
                   }
    deriving (Eq, Show)

data Msg = Now Time
         | Inc
         | Roll Int
         | Tick Time

main :: IO ()
main = runProgram (program init update view subscriptions)

init :: Init Model Msg
init = Model { time = Nothing, rolls = [], counter = 0 } !
    [ time_now Now, send Inc ]

update :: Update Model Msg
update msg model@Model{rolls,counter} =
    case msg of
        Now newTime -> model { time = Just newTime } ! []
        Inc -> ( model { counter = counter + 1 }
               , case (counter + 1) of
                   10 -> Cmd.batch [ generate Roll $ int 1 6
                                   , generate Roll $ int 1 8
                                   , generate Roll $ int 0 65535
                                   ]
                   _ -> send Inc
               )
        Roll roll -> model { rolls = roll : rolls } ! []
        Tick _ -> model ! []

view :: View Model Msg
view Model{time,rolls,counter} =
    Text $ show time ++ " " ++ show rolls ++ " " ++ show counter

subscriptions :: Subscriptions Model Msg
subscriptions Model{counter}
    | counter < 8 = every 1000 Tick
    | otherwise = Sub.none

send :: msg -> Cmd msg
send = task_perform id . task_succeed

time_now :: (Time -> msg) -> Cmd msg
time_now tagger = task_perform tagger now
