for i in $(find . -name "*cabal" ! -name "{{*}}.cabal") ; do
    pushd $(dirname $i)
#    rm -rf .stack-work
    stack --install-ghc build
    stack test
    popd
done
